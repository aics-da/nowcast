require "narray"
require "fileutils"
require "time"

begin
  model = "../../src/nowcast.exe"

  disable_lanczos_in_da = true

  first_cycle = true
  outd_old = nil

  inputfiles = Dir.glob("../data/cappi*.nc").sort
  pwd = Dir.pwd
  (inputfiles.length - 1).times do |i|
    ### SETUP INPUT FILES FOR FORECAST
    p infile1 = inputfiles[i]
    p infile2 = inputfiles[i + 1]
    ### GET TIME
    p t1 = Time.parse(infile1.dup.gsub("cappi.", "").gsub("_", "").gsub(".nc", "") << "Z")
    p t2 = Time.parse(infile2.dup.gsub("cappi.", "").gsub("_", "").gsub(".nc", "") << "Z")
    p t2 - t1
    p outd = File.join("out", t2.strftime("%Y%m%d%H%M%S"))

    ### RUN
    infile1cp = "input_0001.nc"
    infile2cp = "input_0002.nc"
    unless File.exist?(File.join(outd, "raindata_001.nc"))
      puts "setup #{t2} ..."
      FileUtils.mkdir_p(outd)
      FileUtils.ln_sf(infile1, infile1cp)
      FileUtils.ln_sf(infile2, infile2cp)

      ### MODIFY setting.namelist
      File.open("setting.namelist", "w"){|fout|
        File.open("setting.namelist.org", "r"){|fin|
          fin.readlines.each{|l|
            case l
            when /input_interval/
              fout.puts "input_interval = #{t2 - t1}" ### OVERRIDE
            when /use_data_assimilation/
              if(first_cycle)
                fout.puts "use_data_assimilation = 0"
              else
                fout.print l
              end
            when /da_gues_path/
              unless(first_cycle)
                fout.puts "da_gues_path = \"#{File.expand_path(outd_old)}/\""
              end
            when /switching_lanczos/
              if(disable_lanczos_in_da && !first_cycle)
                fout.puts "switching_lanczos = 0"
              else
                fout.print l
              end
            else
              fout.print l
            end
          }
        }
      }

      puts "run model ..."
      system("#{model} > log.model 2>&1") || raise("failed to run model")
      system("mv *.nc log.model #{outd}")
    else
      puts "skip model"
    end

    outd_old = outd
    first_cycle = false
  end
end
