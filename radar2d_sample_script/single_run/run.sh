#!/usr/bin/bash
set -ex

ln -sf ../data/cappi.20181214_0601.nc ./input_0001.nc
ln -sf ../data/cappi.20181214_0609.nc ./input_0002.nc

if [ -e ../../src/nowcast-mpi-lwatch.exe ] ; then
    ../../src/nowcast-mpi-lwatch.exe
elif [ -e ../../src/nowcast-mpi.exe ] ; then
    ../../src/nowcast-mpi.exe
else
    ../../src/nowcast.exe
fi
