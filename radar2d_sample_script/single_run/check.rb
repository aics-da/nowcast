require "numru/ggraph"
include NumRu

p u = GPhys::IO.open("vector_u_001.nc", "u")
p v = GPhys::IO.open("vector_v_001.nc", "v")
p g = GPhys::IO.open("raindata_001.nc", "rain")
GGraph.tone(u)
DCLExt.color_bar
GGraph.tone(v)
DCLExt.color_bar
for i in 0...g.shape[-1]
  p g[false, i].val
  GGraph.tone(g[false, i], true, "tonc" => true)
  DCLExt.color_bar
end
DCL::grcls
