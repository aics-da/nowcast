#!/bin/sh
set -ax

make bicgstab

export OMP_NUM_THREADS=24
ulimit -s unlimited
time ./riken

