module module_output_lz4_each_proc
  use iso_c_binding
  use variables
  use module_io_common
  implicit none

  interface
    integer(c_int) function c_write_lz4_file(fname, dat, size) bind(C, name="write_lz4_file")
       use iso_c_binding
       character(c_char) :: fname(*)
       real(c_float) :: dat(*)
       integer(c_size_t), value :: size
     end function c_write_lz4_file
  end interface

  public

contains

  subroutine output_lz4_each_proc(l_out_rain, l_out_uvw, l_out_growth, j_start, j_end, counter)
    logical, intent(in) :: l_out_rain, l_out_uvw, l_out_growth
    integer, intent(in) :: j_start, j_end, counter
    integer tmptime1, tmptime2, timerate, timemax
    character(18) fname_ext
    character(19) fname_info
    logical file_exist
    integer(c_size_t) :: size
    integer(c_int) :: stat
    real(c_float), allocatable :: inbuf(:, :, :)
    integer :: oi_start, oi_end, oj_start, oj_end, ok_start, ok_end
    integer :: kz

    write(*, *) "output t = ", (it - 1) * dt + 1
    write(*, '("output start = ", I4, ", iter = ", I4, ", it = ", I6, ", t = ", F10.1)') start, iter, it, (it - 1) * dt + 1

    write(fname_ext, '(I3.3, "_", I6.6, "_", I3.3, ".lz4")') start + iter - 1, myrank, counter
    fname1 = 'raindata_' // fname_ext // c_null_char
    fname2 = 'burgersU_' // fname_ext // c_null_char
    fname3 = 'burgersV_' // fname_ext // c_null_char
    fname4 = 'burgersW_' // fname_ext // c_null_char
    fname5 = 'growth_'   // fname_ext // c_null_char

    !CROP OUTPUT DOMAIN FOR RAIN
    oi_start = o_offset_x + 1
    oi_end   = o_offset_x + ox
    oj_start = max(j_start, o_offset_y + 1)
    oj_end   = min(j_end,   o_offset_y + oy)
    ok_start = o_offset_z + 1
    ok_end   = o_offset_z + oz
    
    write(fname_info, '("lz4_", I6.6, "_info.dat")') myrank
    inquire(file = fname_info, exist = file_exist)
    if(.not. file_exist) then
       open(111, file = fname_info, status = "replace")
       write(111, *) 1, dx
       write(111, *) j_start, j_end
       write(111, *) 1, dz
       write(111, *) "raindata"
       write(111, *) oi_start, oi_end
       write(111, *) oj_start, oj_end
       write(111, *) ok_start, ok_end
       close(111)
    end if

    if(l_out_rain) then
       allocate(inbuf(oi_start:oi_end, oj_start:oj_end, ok_start:ok_end))
       size = ox * (oj_end - oj_start + 1) * oz
!$omp parallel do private(kz)
       do kz = ok_start, ok_end
          inbuf(:, :, kz) = rain_t_single(oi_start:oi_end, oj_start:oj_end, kz)
       end do !kz
!$omp end parallel do
       call system_clock(tmptime1, timerate, timemax)
       stat = c_write_lz4_file(fname1, inbuf, size)
       call system_clock(tmptime2, timerate, timemax)
       time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)
       deallocate(inbuf)
    end if

    allocate(inbuf(dx, j_end - j_start + 1, dz))
    size = dx * (j_end - j_start + 1) * dz
    if(l_out_uvw) then
!$omp parallel do private(kz)
       do kz = 1, dz
          inbuf(:, :, kz) = motion_t(1:dx, j_start:j_end, kz, it2jt(it, 1), 1)
       end do !kz
!$omp end parallel do
       call system_clock(tmptime1, timerate, timemax)
       stat = c_write_lz4_file(fname2, inbuf, size)
       call system_clock(tmptime2, timerate, timemax)
       time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)

!$omp parallel do private(kz)
       do kz = 1, dz
          inbuf(:, :, kz) = motion_t(1:dx, j_start:j_end, kz, it2jt(it, 1), 2)
       end do !kz
!$omp end parallel do
       call system_clock(tmptime1, timerate, timemax)
       stat = c_write_lz4_file(fname3, inbuf, size)
       call system_clock(tmptime2, timerate, timemax)
       time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)

!$omp parallel do private(kz)
       do kz = 1, dz
          inbuf(:, :, kz) = motion_t(1:dx, j_start:j_end, kz, it2jt(it, 1), 3)
       end do !kz
!$omp end parallel do
       call system_clock(tmptime1, timerate, timemax)
       stat = c_write_lz4_file(fname4, inbuf, size)
       call system_clock(tmptime2, timerate, timemax)
       time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)
    end if
    if(switching_growth == 1 .and. l_out_growth) then
!$omp parallel do private(kz)
       do kz = 1, dz
          inbuf(:, :, kz) = growth_t(1:dx, j_start:j_end, kz, it2jt(it, 1), 1)
       end do !kz
!$omp end parallel do
       call system_clock(tmptime1, timerate, timemax)
       stat = c_write_lz4_file(fname5, inbuf, size)
       call system_clock(tmptime2, timerate, timemax)
       time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)
    end if
    deallocate(inbuf)
  end subroutine output_lz4_each_proc
end module module_output_lz4_each_proc
