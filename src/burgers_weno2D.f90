subroutine burgers_weno2d(j0, j1, motion)
  use variables
  use weno_core
  implicit none

  integer, intent(in) :: j0, j1
  real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
  integer i, j, a, ii
  integer jt1, jt2
  integer i_start, i_end
  real(real_size), allocatable :: div(:, :)
  real(real_size) :: dt_div_damper_param

  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  if(switching_div_damper == 1) then ! for divergence damper
     allocate(div(dx, dy))
     dt_div_damper_param = dt * div_damper_param
!$omp parallel do private(i, j)
     do j = 2, dy - 1
        do i = 2 - margin_x_adv, dx - 1 + margin_x_adv
           div(i, j) = (motion(i + 1, j, 1, jt2, 1) - motion(i - 1, j, 1, jt2, 1)) * inv_gridx * 0.5d0 &
                &    + (motion(i, j + 1, 1, jt2, 2) - motion(i, j - 1, 1, jt2, 2)) * inv_gridy * 0.5d0
        end do
     end do
!$omp end parallel do
  end if

  do ii = 1, 3
     call burgers_weno2D_core(ii)
  end do

contains
  subroutine burgers_weno2D_core(iuvw)
    integer, intent(in) :: iuvw

    !5th order weno
 
    !Dweno	
    i_start = 2 - margin_x_adv
    i_end = dx - 1 + margin_x_adv
!$omp parallel do private(i, j)
    do j = 2, dy - 1
       do i = i_start, i_end
          
          if(motion(i, j, 1, jt2, 1) > 0) then
             Dweno(i, j, 1, 1) = (motion(i,     j, 1, jt2, iuvw) - motion(i - 1, j, 1, jt2, iuvw))
          else
             Dweno(i, j, 1, 1) = (motion(i + 1, j, 1, jt2, iuvw) - motion(i,     j, 1, jt2, iuvw))
          end if
          
          if(motion(i, j, 1, jt2, 2) > 0) then
             Dweno(i, j, 1, 2) = (motion(i, j,     1, jt2, iuvw) - motion(i, j - 1, 1, jt2, iuvw))
          else
             Dweno(i, j, 1, 2) = (motion(i, j + 1, 1, jt2, iuvw) - motion(i, j,     1, jt2, iuvw))
          end if
          
       end do !i

       Dweno(i_start:i_end, j, 1, 1) = Dweno(i_start:i_end, j, 1, 1) * inv_gridx
       Dweno(i_start:i_end, j, 1, 2) = Dweno(i_start:i_end, j, 1, 2) * inv_gridy
    end do !j
!$omp end parallel do

!$omp parallel do private(i)
    do i = 1, dx
       Dweno(i,  1, 1, 1:2) = Dweno(i, 2,      1, 1:2)
       Dweno(i, dy, 1, 1:2) = Dweno(i, dy - 1, 1, 1:2)
    end do
!$omp end parallel do
 
    if(boundary_type == 1) then !CYCLIC IN X
!$omp parallel do private(j)
       do j = 1, dy
          Dweno((1 - margin_x_adv):0,  j, 1, 1:2) = Dweno((dx - margin_x_adv + 1):dx,      j, 1, 1:2)
          Dweno((dx + 1):(dx + margin_x_adv), j, 1, 1:2) = Dweno(1:margin_x_adv, j, 1, 1:2)
       end do
!$omp end parallel do
    else
!$omp parallel do private(j)
       do j = 1, dy
          Dweno(1,  j, 1, 1:2) = Dweno(2,      j, 1, 1:2)
          Dweno(dx, j, 1, 1:2) = Dweno(dx - 1, j, 1, 1:2)
       end do
!$omp end parallel do
    end if

    i_start = 3 - margin_x_adv
    i_end = dx - 2 + margin_x_adv
!$omp parallel do private(i, j)
    do j = 3, dy - 2
       call weno_calc_adams_2D(j0, j1, motion, i_start, i_end, j, jt2, iuvw)

       ! divergence damper
       if(switching_div_damper == 1) then
          if(iuvw == 1) then
             do i = i_start, i_end
                adams_t(i, j, 1, i_adams(1), 1) = adams_t(i, j, 1, i_adams(1), 1) + dt_div_damper_param * (div(i + 1, j) - div(i - 1, j)) * inv_gridx * 0.5d0
             end do !i
          else if(iuvw == 2) then
             do i = i_start, i_end
                adams_t(i, j, 1, i_adams(1), 2) = adams_t(i, j, 1, i_adams(1), 2) + dt_div_damper_param * (div(i, j + 1) - div(i, j - 1)) * inv_gridy * 0.5d0
             end do !i
          end if
       end if

       if(it < 3 .or. mod(it, 10) .eq. 0) then
          motion(i_start:i_end, j, 1, jt1, iuvw) = motion(i_start:i_end, j, 1, jt2, iuvw) + adams_t(i_start:i_end, j, 1, i_adams(1), iuvw)
       else
          if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
             motion(i_start:i_end, j, 1, jt1, iuvw) = motion(i_start:i_end, j, 1, jt2, iuvw) &
                  & + 23.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), iuvw) &
                  & - 16.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(2), iuvw) &
                  & +  5.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(3), iuvw)
          elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
             motion(i_start:i_end, j, 1, jt1, iuvw) = motion(i_start:i_end, j, 1, jt2, iuvw) &
                  & + 0.5d0 * (3.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), iuvw) - adams_t(i_start:i_end, j, 1, i_adams(2), iuvw))
          end if
       end if
    end do
!$omp end parallel do
  end subroutine burgers_weno2D_core
end subroutine burgers_weno2d
