subroutine lanczos_filtering_growth()
  use variables
  implicit none
  integer i, j, k
  double precision new_x
  double precision, allocatable, dimension(:, :, :, :) :: growth_t_filter, tmp_growth_t_filter
  double precision, allocatable, dimension(:, :) :: growth_t_filter_mean
  integer aa1, MM1
  double precision fc   ! critical angular frequency in [0,pi]
  double precision, allocatable, dimension(:) :: weight_lan
  character(3) citer

  allocate(growth_t_filter(dx, dy, dz, 1), tmp_growth_t_filter(dx, dy, dz, 1), growth_t_filter_mean(dz, 1))
  do k = 1, dz
     growth_t_filter_mean(k, 1) = sum(growth_t(:, :, k, 1, 1)) / (dx * dy)
     !compute anomaly
     growth_t_filter(1:dx, 1:dy, k, 1) = growth_t(1:dx, 1:dy, k, 1, 1) - growth_t_filter_mean(k, 1)
  end do

  !---------------------------------lanczos filtering--------------------------------------------------------
  ! lanczos filtering on growth rate
  MM1 = lanczos_window
  fc = (4.0d0 * atan(1.0d0)) / lanczos_critical_wavelength ! PI/(wavelength)

  allocate(weight_lan(2 * MM1 + 1))
  call lanczos_filter_weight(weight_lan, fc, MM1)
  write(*, *) "lanczos sum of weight: ", sum(weight_lan)

!$omp parallel
!!! X
!$omp do private(i, j, k, aa1, new_x)
  do k = 1 + MM1, dz - MM1
     do j = 1 + MM1, dy - MM1
        do i = 1 + MM1, dx - MM1
           new_x = 0.0d0
           do aa1 = -MM1, MM1
              new_x = new_x + growth_t_filter(i + aa1, j, k, 1) * weight_lan(MM1 + aa1 + 1)
           enddo
           tmp_growth_t_filter(i, j, k, 1) = new_x
        enddo
     enddo
  enddo
!$omp end do

!$omp do private(k)
  do k = 1 + MM1, dz - MM1
     growth_t_filter((1 + MM1):(dx - MM1), (1 + MM1):(dy - MM1), k, 1:3) = tmp_growth_t_filter((1 + MM1):(dx - MM1), (1 + MM1):(dy - MM1), k, 1:3)
  end do
!$omp end do

!!! Y
!$omp do private(i, j, k, aa1, new_x)
  do k = 1 + MM1, dz - MM1
     do j = 1 + MM1, dy - MM1
        do i = 1 + MM1, dx - MM1
           new_x = 0.0d0
           do aa1 = -MM1, MM1
              new_x = new_x + growth_t_filter(i, j + aa1, k, 1) * weight_lan(MM1 + aa1 + 1)
           enddo
           tmp_growth_t_filter(i, j, k, 1) = new_x
        enddo
     enddo
  enddo
!$omp end do

!$omp do private(k)
  do k = 1 + MM1, dz - MM1
     growth_t_filter((1 + MM1):(dx - MM1), (1 + MM1):(dy - MM1), k, 1:3) = tmp_growth_t_filter((1 + MM1):(dx - MM1), (1 + MM1):(dy - MM1), k, 1:3)
  end do
!$omp end do

!!! Z
!$omp do private(i, j, k, aa1, new_x)
  do k = 1 + MM1, dz - MM1
     do j = 1 + MM1, dy - MM1
        do i = 1 + MM1, dx - MM1
           new_x = 0.0d0
           do aa1 = -MM1, MM1
              new_x = new_x + growth_t_filter(i, j, k + aa1, 1) * weight_lan(MM1 + aa1 + 1)
           enddo
           tmp_growth_t_filter(i, j, k, 1) = new_x
        enddo
     enddo
  enddo
!$omp end do

!$omp do private(k)
  do k = 1 + MM1, dz - MM1
     growth_t_filter((1 + MM1):(dx - MM1), (1 + MM1):(dy - MM1), k, 1:3) = tmp_growth_t_filter((1 + MM1):(dx - MM1), (1 + MM1):(dy - MM1), k, 1:3)
  end do
!$omp end do

!!! ANOMALY TO FULL FIELD
!$omp do private(k)
  do k = 1, dz
     growth_t_filter(:, :, k, 1) = growth_t_filter(:, :, k, 1) + growth_t_filter_mean(k, 1)
  end do
!$omp end do
  
  !------------------------use average nearest neighbours on margins----------------------------------
  !BOTTOM
!$omp do private(i, j, k)
  do k = 2, MM1
     do j = 2, dy - 1
        do i = 2, dx - 1
           growth_t_filter(i, j, k, 1) = (growth_t(i, j, k, 1, 1) &
                &                         + growth_t(i - 1, j, k, 1, 1) + growth_t(i + 1, j, k, 1, 1) &
                &                         + growth_t(i, j - 1, k, 1, 1) + growth_t(i, j + 1, k, 1, 1) &
                &                         + growth_t(i, j, k - 1, 1, 1) + growth_t(i, j, k + 1, 1, 1)) / 7.0
        enddo
     enddo
  enddo
!$omp end do

  !TOP
!$omp do private(i, j, k)
  do k = dz - MM1 + 1, dz - 1
     do j = 2, dy - 1
        do i = 2, dx - 1
           growth_t_filter(i, j, k, 1) = (growth_t(i, j, k, 1, 1) &
                &                            + growth_t(i - 1, j, k, 1, 1) + growth_t(i + 1, j, k, 1, 1) &
                &                            + growth_t(i, j - 1, k, 1, 1) + growth_t(i, j + 1, k, 1, 1) &
                &                            + growth_t(i, j, k - 1, 1, 1) + growth_t(i, j, k + 1, 1, 1)) / 7.0
        enddo
     enddo
  enddo
!$omp end do

  !SOUTH
!$omp do private(i, j, k)
  do k = MM1 + 1, dz - MM1
     do j = 2, MM1
        do i = 2, dx - 1
           growth_t_filter(i, j, k, 1) = (growth_t(i, j, k, 1, 1) &
                &                            + growth_t(i - 1, j, k, 1, 1) + growth_t(i + 1, j, k, 1, 1) &
                &                            + growth_t(i, j - 1, k, 1, 1) + growth_t(i, j + 1, k, 1, 1) &
                &                            + growth_t(i, j, k - 1, 1, 1) + growth_t(i, j, k + 1, 1, 1)) / 7.0
        enddo
     enddo
  enddo
!$omp end do

  !NORTH
!$omp do private(i, j, k)
  do k = MM1 + 1, dz - MM1
     do j = dy - MM1 + 1, dy - 1
        do i = 2, dx - 1
           growth_t_filter(i, j, k, 1) = (growth_t(i, j, k, 1, 1) &
                &                            + growth_t(i - 1, j, k, 1, 1) + growth_t(i + 1, j, k, 1, 1) &
                &                            + growth_t(i, j - 1, k, 1, 1) + growth_t(i, j + 1, k, 1, 1) &
                &                            + growth_t(i, j, k - 1, 1, 1) + growth_t(i, j, k + 1, 1, 1)) / 7.0
        enddo
     enddo
  enddo
!$omp end do

  !WEST
!$omp do private(i, j, k)
  do k = MM1 + 1, dz - MM1
     do j = MM1 + 1, dy - MM1
        do i = 2, MM1
           growth_t_filter(i, j, k, 1) = (growth_t(i, j, k, 1, 1) &
                &                            + growth_t(i - 1, j, k, 1, 1) + growth_t(i + 1, j, k, 1, 1) &
                &                            + growth_t(i, j - 1, k, 1, 1) + growth_t(i, j + 1, k, 1, 1) &
                &                            + growth_t(i, j, k - 1, 1, 1) + growth_t(i, j, k + 1, 1, 1)) / 7.0
        enddo
     enddo
  enddo
!$omp end do

  !EAST
!$omp do private(i, j, k)
  do k = MM1 + 1, dz - MM1
     do j = MM1 + 1, dy - MM1
        do i = dx - MM1 + 1, dx - 1
           growth_t_filter(i, j, k, 1) = (growth_t(i, j, k, 1, 1) &
                &                            + growth_t(i - 1, j, k, 1, 1) + growth_t(i + 1, j, k, 1, 1) &
                &                            + growth_t(i, j - 1, k, 1, 1) + growth_t(i, j + 1, k, 1, 1) &
                &                            + growth_t(i, j, k - 1, 1, 1) + growth_t(i, j, k + 1, 1, 1)) / 7.0
        enddo
     enddo
  enddo
!$omp end do

!$omp end parallel

  growth_t(1:dx, 1:dy, 1:dz, 1, 1:3) = growth_t_filter(1:dx, 1:dy, 1:dz, 1:3)

  deallocate(growth_t_filter, tmp_growth_t_filter, weight_lan)
end subroutine lanczos_filtering_growth
