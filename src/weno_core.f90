module weno_core
  use variables

  implicit none

  double precision, allocatable :: Dweno(:, :, :, :)
  !double precision, private, allocatable :: tmp_sweno_dy(:, :), tmp_sweno_d2y(:, :), tmp_sweno_2dy_sq(:, :)

  public

contains

  subroutine weno_allocate_workspace
    integer j_dweno1, j_dweno2 !, i_start, i_end, j_start, j_end

    ! from main
    j_dweno1 = j_mpi(1, myrank) - 2
    j_dweno2 = j_mpi(2, myrank) + 2
    if(myrank == 0)            j_dweno1 = 1
    if(myrank == mpiprocs - 1) j_dweno2 = dy

    allocate(Dweno((1 - margin_x_adv):(dx + margin_x_adv), j_dweno1:j_dweno2, dz, 3))

    ! from advection_weno
    !i_start = 3 - margin_x_adv
    !i_end = dx - 2 + margin_x_adv
    !j_start = j_mpi(1, myrank)
    !j_end   = j_mpi(2, myrank)
    !if(myrank == 0) j_start = 3
    !if(myrank == mpiprocs - 1) j_end = dy - 2
    !k_start = 3
    !k_end = dz - 2

    !allocate(tmp_sweno_dy(i_start:i_end, (j_start - 1):(j_end + 2)))
    !allocate(tmp_sweno_d2y(i_start:i_end, (j_start - 1):(j_end + 1)))
    !allocate(tmp_sweno_2dy_sq(i_start:i_end, j_start:j_end))

  end subroutine weno_allocate_workspace

  subroutine weno_deallocate_workspace
    !deallocate(tmp_sweno_dy, tmp_sweno_d2y, tmp_sweno_2dy_sq, Dweno)
    deallocate(Dweno)
  end subroutine weno_deallocate_workspace

  subroutine weno_dweno_boundary_condition
    integer i_start, i_end, j_start, j_end, j, k

    i_start = 2 - margin_x_adv
    i_end = dx - 1 + margin_x_adv
    j_start = j_mpi(1, myrank) - 2
    j_end   = j_mpi(2, myrank) + 2
    if(j_start < 2) j_start = 2
    if(j_end > dy - 1) j_end = dy - 1

!$omp parallel do private(k)
    do k = 1, dz
       if(boundary_type == 1) then !CYCLIC IN X
          Dweno((1 - margin_x_adv):0,         j_start:j_end, k, 1) = Dweno((dx - margin_x_adv + 1):dx, j_start:j_end, k, 1)
          Dweno((1 - margin_x_adv):0,         j_start:j_end, k, 2) = Dweno((dx - margin_x_adv + 1):dx, j_start:j_end, k, 2)
          Dweno((1 - margin_x_adv):0,         j_start:j_end, k, 3) = Dweno((dx - margin_x_adv + 1):dx, j_start:j_end, k, 3)

          Dweno((dx + 1):(dx + margin_x_adv), j_start:j_end, k, 1) = Dweno(1:margin_x_adv,             j_start:j_end, k, 1)
          Dweno((dx + 1):(dx + margin_x_adv), j_start:j_end, k, 2) = Dweno(1:margin_x_adv,             j_start:j_end, k, 2)
          Dweno((dx + 1):(dx + margin_x_adv), j_start:j_end, k, 3) = Dweno(1:margin_x_adv,             j_start:j_end, k, 3)
       else
          Dweno(1,  j_start:j_end, k, 1) = Dweno(2,      j_start:j_end, k, 1)
          Dweno(1,  j_start:j_end, k, 3) = Dweno(2,      j_start:j_end, k, 2)
          Dweno(1,  j_start:j_end, k, 3) = Dweno(2,      j_start:j_end, k, 3)

          Dweno(dx, j_start:j_end, k, 1) = Dweno(dx - 1, j_start:j_end, k, 1)
          Dweno(dx, j_start:j_end, k, 2) = Dweno(dx - 1, j_start:j_end, k, 2)
          Dweno(dx, j_start:j_end, k, 3) = Dweno(dx - 1, j_start:j_end, k, 3)
       end if
       if(myrank == 0) then
          Dweno(1:dx, 1,  k, 1) = Dweno(1:dx, 2,      k, 1)
          Dweno(1:dx, 1,  k, 2) = Dweno(1:dx, 2,      k, 2)
          Dweno(1:dx, 1,  k, 3) = Dweno(1:dx, 2,      k, 3)
       end if
       if(myrank == mpiprocs - 1) then
          Dweno(1:dx, dy, k, 1) = Dweno(1:dx, dy - 1, k, 1)
          Dweno(1:dx, dy, k, 2) = Dweno(1:dx, dy - 1, k, 2)
          Dweno(1:dx, dy, k, 3) = Dweno(1:dx, dy - 1, k, 3)
       end if
    end do
!$omp end parallel do

!$omp parallel do private(j)
    do j = j_start, j_end
       Dweno(1:dx, j,  1, 1) = Dweno(1:dx, j, 2,      1)
       Dweno(1:dx, j,  1, 2) = Dweno(1:dx, j, 2,      2)
       Dweno(1:dx, j,  1, 3) = Dweno(1:dx, j, 2,      3)

       Dweno(1:dx, j, dz, 1) = Dweno(1:dx, j, dz - 1, 1)
       Dweno(1:dx, j, dz, 2) = Dweno(1:dx, j, dz - 1, 2)
       Dweno(1:dx, j, dz, 3) = Dweno(1:dx, j, dz - 1, 3)
    end do
!$omp end parallel do
  end subroutine weno_dweno_boundary_condition

  subroutine weno_calc_adams(j0, j1, motion, i_start, i_end, j_start, j_end, k_start, k_end, jt2, val_id)
    integer, intent(in) :: j0, j1
    real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    integer, intent(in) :: i_start, i_end, j_start, j_end, k_start, k_end, jt2, val_id
    double precision Fweno(i_start:i_end, 3, 3), Sweno(i_start:i_end, 3, 3), Aweno(3)
    double precision tmp_sweno_dx((i_start - 1):(i_end + 2))
    double precision tmp_sweno_d2x((i_start - 1):(i_end + 1))
    double precision tmp_sweno_2dx_sq(i_start:i_end)
    double precision tmp_sweno_x1_m4_3_sq(i_start:i_end)
    double precision tmp_sweno_x3_m4_1_sq(i_start:i_end)
    double precision, allocatable :: tmp_sweno_dy(:, :), tmp_sweno_d2y(:, :), tmp_sweno_2dy_sq(:, :)

    integer aa, ii, jj, kk

!$omp parallel do private(ii, jj, kk, aa, Fweno, Sweno, Aweno, tmp_sweno_dx, tmp_sweno_d2x, tmp_sweno_2dx_sq, tmp_sweno_x1_m4_3_sq, tmp_sweno_x3_m4_1_sq, tmp_sweno_dy, tmp_sweno_d2y, tmp_sweno_2dy_sq)
    do kk = k_start, k_end

       allocate(tmp_sweno_dy(i_start:i_end, (j_start - 1):(j_end + 2)))
       allocate(tmp_sweno_d2y(i_start:i_end, (j_start - 1):(j_end + 1)))
       allocate(tmp_sweno_2dy_sq(i_start:i_end, j_start:j_end))

       tmp_sweno_dy = Dweno(i_start:i_end, (j_start - 1):(j_end + 2), kk, 1) - Dweno(i_start:i_end, (j_start - 2):(j_end + 1), kk, 1)
       tmp_sweno_d2y = tmp_sweno_dy(i_start:i_end, j_start:(j_end + 2)) - tmp_sweno_dy(i_start:i_end, (j_start - 1):(j_end + 1))
       tmp_sweno_d2y = (13.0d0 / 12.0d0) * tmp_sweno_d2y * tmp_sweno_d2y
       tmp_sweno_2dy_sq = Dweno(i_start:i_end, (j_start + 1):(j_end + 1), kk, 1) - Dweno(i_start:i_end, (j_start - 1):(j_end - 1), kk, 1)
       tmp_sweno_2dy_sq = 1.0d0 / 4.0d0 * tmp_sweno_2dy_sq * tmp_sweno_2dy_sq

       do jj = j_start, j_end
          tmp_sweno_dx = Dweno((i_start - 1):(i_end + 2), jj, kk, 1) - Dweno((i_start - 2):(i_end + 1), jj, kk, 1)
          tmp_sweno_d2x = tmp_sweno_dx(i_start:(i_end + 2)) - tmp_sweno_dx((i_start - 1):(i_end + 1))
          tmp_sweno_d2x = (13.0d0 / 12.0d0) * tmp_sweno_d2x * tmp_sweno_d2x
          tmp_sweno_2dx_sq = Dweno((i_start + 1):(i_end + 1), jj, kk, 1) - Dweno((i_start - 1):(i_end - 1), jj, kk, 1)
          tmp_sweno_2dx_sq = 1.0d0 / 4.0d0 * tmp_sweno_2dx_sq * tmp_sweno_2dx_sq
          tmp_sweno_x1_m4_3_sq = 1.0d0 / 4.0d0 * (Dweno((i_start - 2):(i_end - 2), jj, kk, 1) - 4.0d0 * Dweno((i_start - 1):(i_end - 1), jj, kk, 1) + 3.0d0 * Dweno(i_start:i_end, jj, kk, 1)) ** 2.0d0
          tmp_sweno_x3_m4_1_sq = 1.0d0 / 4.0d0 * (3.0d0 * Dweno(i_start:i_end, jj, kk, 1) - 4.0d0 * Dweno((i_start + 1):(i_end + 1), jj, kk, 1) + Dweno((i_start + 2):(i_end + 2), jj, kk, 1)) ** 2.0d0

          do ii = i_start, i_end
             !Fweno, Sweno
             if(motion(ii, jj, kk, jt2, 1) > 0) then
                Fweno(ii, 1, 1) = 1.0d0 / 3.0d0 * Dweno(ii - 2, jj, kk, 1) - 7.0d0 / 6.0d0 * Dweno(ii - 1, jj, kk, 1) + 11.0d0 / 6.0d0 * Dweno(ii, jj, kk, 1)
                Fweno(ii, 2, 1) = -1.0d0 / 6.0d0 * Dweno(ii - 1, jj, kk, 1) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk, 1) + 1.0d0 / 3.0d0 * Dweno(ii + 1, jj, kk, 1)
                Fweno(ii, 3, 1) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk, 1) + 5.0d0 / 6.0d0 * Dweno(ii + 1, jj, kk, 1) - 1.0d0 / 6.0d0 * Dweno(ii + 2, jj, kk, 1)

                Sweno(ii, 1, 1) = tmp_sweno_d2x(ii - 1) + tmp_sweno_x1_m4_3_sq(ii)
                Sweno(ii, 2, 1) = tmp_sweno_d2x(ii)     + tmp_sweno_2dx_sq(ii)
                Sweno(ii, 3, 1) = tmp_sweno_d2x(ii + 1) + tmp_sweno_x3_m4_1_sq(ii)
             else
                Fweno(ii, 1, 1) = 1.0d0 / 3.0d0 * Dweno(ii + 2, jj, kk, 1) - 7.0d0 / 6.0d0 * Dweno(ii + 1, jj, kk, 1) + 11.0d0 / 6.0d0 * Dweno(ii, jj, kk, 1)
                Fweno(ii, 2, 1) = -1.0d0 / 6.0d0 * Dweno(ii + 1, jj, kk, 1) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk, 1) + 1.0d0 / 3.0d0 * Dweno(ii - 1, jj, kk, 1)
                Fweno(ii, 3, 1) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk, 1) + 5.0d0 / 6.0d0 * Dweno(ii - 1, jj, kk, 1) - 1.0d0 / 6.0d0 * Dweno(ii - 2, jj, kk, 1)

                Sweno(ii, 1, 1) = tmp_sweno_d2x(ii + 1) + tmp_sweno_x3_m4_1_sq(ii)
                Sweno(ii, 2, 1) = tmp_sweno_d2x(ii)     + tmp_sweno_2dx_sq(ii)
                Sweno(ii, 3, 1) = tmp_sweno_d2x(ii - 1) + tmp_sweno_x1_m4_3_sq(ii)
             end if
             if(motion(ii, jj, kk, jt2, 2) > 0) then
                Fweno(ii, 1, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj - 2, kk, 2) - 7.0d0 / 6.0d0 * Dweno(ii, jj - 1, kk, 2) + 11.0d0 / 6.0d0 * Dweno(ii, jj, kk, 2)
                Fweno(ii, 2, 2) = -1.0d0 / 6.0d0 * Dweno(ii, jj - 1, kk, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk, 2) + 1.0d0 / 3.0d0 * Dweno(ii, jj + 1, kk, 2)
                Fweno(ii, 3, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj + 1, kk, 2) - 1.0d0 / 6.0d0 * Dweno(ii, jj + 2, kk, 2)

                Sweno(ii, 1, 2) = tmp_sweno_d2y(ii, jj - 1) + &
                     & 1.0d0 / 4.0d0 * (Dweno(ii, jj - 2, kk, 2) - 4.0d0 * Dweno(ii, jj - 1, kk, 2) + 3.0d0 * Dweno(ii, jj, kk, 2)) ** 2.0d0
                Sweno(ii, 2, 2) = tmp_sweno_d2y(ii, jj) + tmp_sweno_2dy_sq(ii, jj)
                Sweno(ii, 3, 2) = tmp_sweno_d2y(ii, jj + 1) + &
                     & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, kk, 2) - 4.0d0 * Dweno(ii, jj + 1, kk, 2) + Dweno(ii, jj + 2, kk, 2)) ** 2.0d0
             else
                Fweno(ii, 1, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj + 2, kk, 2) - 7.0d0 / 6.0d0 * Dweno(ii, jj + 1, kk, 2) + 11.0d0 / 6.0d0 * Dweno(ii, jj, kk, 2)
                Fweno(ii, 2, 2) = -1.0d0 / 6.0d0 * Dweno(ii, jj + 1, kk, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk, 2) + 1.0d0 / 3.0d0 * Dweno(ii, jj - 1, kk, 2)
                Fweno(ii, 3, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj - 1, kk, 2) - 1.0d0 / 6.0d0 * Dweno(ii, jj - 2, kk, 2)

                Sweno(ii, 1, 2) = tmp_sweno_d2y(ii, jj + 1) + &
                     & 1.0d0 / 4.0d0 * (Dweno(ii, jj + 2, kk, 2) - 4.0d0 * Dweno(ii, jj + 1, kk, 2) + 3.0d0 * Dweno(ii, jj, kk, 2)) ** 2.0d0
                Sweno(ii, 2, 2) = tmp_sweno_d2y(ii, jj) + tmp_sweno_2dy_sq(ii, jj)
                Sweno(ii, 3, 2) = tmp_sweno_d2y(ii, jj - 1) + &
                     & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, kk, 2) - 4.0d0 * Dweno(ii, jj - 1, kk, 2) + Dweno(ii, jj - 2, kk, 2)) ** 2.0d0
             end if
             if(motion(ii, jj, kk, jt2, 3) > 0) then
                Fweno(ii, 1, 3) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk - 2, 3) - 7.0d0 / 6.0d0 * Dweno(ii, jj, kk - 1, 3) + 11.0d0 / 6.0d0 * Dweno(ii, jj, kk, 3)
                Fweno(ii, 2, 3) = -1.0d0 / 6.0d0 * Dweno(ii, jj, kk - 1, 3) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk, 3) + 1.0d0 / 3.0d0 * Dweno(ii, jj, kk + 1, 3)
                Fweno(ii, 3, 3) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk, 3) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk + 1, 3) - 1.0d0 / 6.0d0 * Dweno(ii, jj, kk + 2, 3)

                Sweno(ii, 1, 3) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, kk - 2, 3) - 2.0d0 * Dweno(ii, jj, kk - 1, 3) + Dweno(ii, jj, kk, 3)) ** 2.0d0 + &
                     & 1.0d0 / 4.0d0 * (Dweno(ii, jj, kk - 2, 3) - 4.0d0 * Dweno(ii, jj, kk - 1, 3) + 3.0d0 * Dweno(ii, jj, kk, 3)) ** 2.0d0
                Sweno(ii, 2, 3) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, kk - 1, 3) - 2.0d0 * Dweno(ii, jj, kk, 3) + Dweno(ii, jj, kk + 1, 3)) ** 2.0d0 + &
                     & 1.0d0 / 4.0d0 * (Dweno(ii, jj, kk - 1, 3) - Dweno(ii, jj, kk + 1, 3)) ** 2.0d0
                Sweno(ii, 3, 3) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, kk, 3) - 2.0d0 * Dweno(ii, jj, kk + 1, 3) + Dweno(ii, jj, kk + 2, 3)) ** 2.0d0 + &
                     & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, kk, 3) - 4.0d0 * Dweno(ii, jj, kk + 1, 3) + Dweno(ii, jj, kk + 2, 3)) ** 2.0d0
             else
                Fweno(ii, 1, 3) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk + 2, 3) - 7.0d0 / 6.0d0 * Dweno(ii, jj, kk + 1, 3) + 11.0d0 / 6.0d0 * Dweno(ii, jj, kk, 3)
                Fweno(ii, 2, 3) = -1.0d0 / 6.0d0 * Dweno(ii, jj, kk + 1, 3) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk, 3) + 1.0d0 / 3.0d0 * Dweno(ii, jj, kk - 1, 3)
                Fweno(ii, 3, 3) = 1.0d0 / 3.0d0 * Dweno(ii, jj, kk, 3) + 5.0d0 / 6.0d0 * Dweno(ii, jj, kk - 1, 3) - 1.0d0 / 6.0d0 * Dweno(ii, jj, kk - 2, 3)

                Sweno(ii, 1, 3) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, kk + 2, 3) - 2.0d0 * Dweno(ii, jj, kk + 1, 3) + Dweno(ii, jj, kk, 3)) ** 2.0d0 + &
                     & 1.0d0 / 4.0d0 * (Dweno(ii, jj, kk + 2, 3) - 4.0d0 * Dweno(ii, jj, kk + 1, 3) + 3.0d0 * Dweno(ii, jj, kk, 3)) ** 2.0d0
                Sweno(ii, 2, 3) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, kk + 1, 3) - 2.0d0 * Dweno(ii, jj, kk, 3) + Dweno(ii, jj, kk - 1, 3)) ** 2.0d0 + &
                     & 1.0d0 / 4.0d0 * (Dweno(ii, jj, kk + 1, 3) - Dweno(ii, jj, kk - 1, 3)) ** 2.0d0
                Sweno(ii, 3, 3) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, kk, 3) - 2.0d0 * Dweno(ii, jj, kk - 1, 3) + Dweno(ii, jj, kk - 2, 3)) ** 2.0d0 + &
                     & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, kk, 3) - 4.0d0 * Dweno(ii, jj, kk - 1, 3) + Dweno(ii, jj, kk - 2, 3)) ** 2.0d0
             end if
          end do !ii

          Sweno = Sweno + ipu
          do aa = 1, 3
             do ii = i_start, i_end
                !Aweno
                Aweno(1) = 0.1d0 / (Sweno(ii, 1, aa) * Sweno(ii, 1, aa))
                Aweno(2) = 0.6d0 / (Sweno(ii, 2, aa) * Sweno(ii, 2, aa))
                Aweno(3) = 0.3d0 / (Sweno(ii, 3, aa) * Sweno(ii, 3, aa))
                !Wweno is directly multiplied with Fweno
                Fweno(ii, 1, aa) = Fweno(ii, 1, aa) * Aweno(1) / (Aweno(1) + Aweno(2) + Aweno(3))
                Fweno(ii, 2, aa) = Fweno(ii, 2, aa) * Aweno(2) / (Aweno(1) + Aweno(2) + Aweno(3))
                Fweno(ii, 3, aa) = Fweno(ii, 3, aa) * Aweno(3) / (Aweno(1) + Aweno(2) + Aweno(3))
             end do !ii
          end do !aa

          do ii = i_start, i_end
             !adams
             adams_t(ii, jj, kk, i_adams(1), val_id) &
                  & = -dt * (motion(ii, jj, kk, jt2, 1) * (Fweno(ii, 1, 1) + Fweno(ii, 2, 1) + Fweno(ii, 3, 1)) + &
                  &          motion(ii, jj, kk, jt2, 2) * (Fweno(ii, 1, 2) + Fweno(ii, 2, 2) + Fweno(ii, 3, 2)) + &
                  &          motion(ii, jj, kk, jt2, 3) * (Fweno(ii, 1, 3) + Fweno(ii, 2, 3) + Fweno(ii, 3, 3)))
          end do !ii
       end do !jj

       deallocate(tmp_sweno_dy, tmp_sweno_d2y, tmp_sweno_2dy_sq)

    end do !kk
!$omp end parallel do

    return
  end subroutine weno_calc_adams


  subroutine weno_calc_adams_2D(j0, j1, motion, i_start, i_end, jj, jt2, val_id)
    integer, intent(in) :: j0, j1
    real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    integer, intent(in) :: i_start, i_end, jj, jt2, val_id
    integer ii, a
    double precision :: Fweno(3, 2), Sweno(3, 2), Aweno(3)

    do ii = i_start, i_end
       !Fweno
       if(motion(ii, jj, 1, jt2, 1) > 0) then
          Fweno(1, 1) = 1.0d0 / 3.0d0 * Dweno(ii - 2, jj, 1, 1) - 7.0d0 / 6.0d0 * Dweno(ii - 1, jj, 1, 1) + 11.0d0 / 6.0d0 * Dweno(ii, jj, 1, 1)
          Fweno(2, 1) = -1.0d0 / 6.0d0 * Dweno(ii - 1, jj, 1, 1) + 5.0d0 / 6.0d0 * Dweno(ii, jj, 1, 1) + 1.0d0 / 3.0d0 * Dweno(ii + 1, jj, 1, 1)
          Fweno(3, 1) = 1.0d0 / 3.0d0 * Dweno(ii, jj, 1, 1) + 5.0d0 / 6.0d0 * Dweno(ii + 1, jj, 1, 1) - 1.0d0 / 6.0d0 * Dweno(ii + 2, jj, 1, 1)
       else
          Fweno(1, 1) = 1.0d0 / 3.0d0 * Dweno(ii + 2, jj, 1, 1) - 7.0d0 / 6.0d0 * Dweno(ii + 1, jj, 1, 1) + 11.0d0 / 6.0d0 * Dweno(ii, jj, 1, 1)
          Fweno(2, 1) = -1.0d0 / 6.0d0 * Dweno(ii + 1, jj, 1, 1) + 5.0d0 / 6.0d0 * Dweno(ii, jj, 1, 1) + 1.0d0 / 3.0d0 * Dweno(ii - 1, jj, 1, 1)
          Fweno(3, 1) = 1.0d0 / 3.0d0 * Dweno(ii, jj, 1, 1) + 5.0d0 / 6.0d0 * Dweno(ii - 1, jj, 1, 1) - 1.0d0 / 6.0d0 * Dweno(ii - 2, jj, 1, 1)
       end if
       if(motion(ii, jj, 1, jt2, 2) > 0) then
          Fweno(1, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj - 2, 1, 2) - 7.0d0 / 6.0d0 * Dweno(ii, jj - 1, 1, 2) + 11.0d0 / 6.0d0 * Dweno(ii, jj, 1, 2)
          Fweno(2, 2) = -1.0d0 / 6.0d0 * Dweno(ii, jj - 1, 1, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj, 1, 2) + 1.0d0 / 3.0d0 * Dweno(ii, jj + 1, 1, 2)
          Fweno(3, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj, 1, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj + 1, 1, 2) - 1.0d0 / 6.0d0 * Dweno(ii, jj + 2, 1, 2)
       else
          Fweno(1, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj + 2, 1, 2) - 7.0d0 / 6.0d0 * Dweno(ii, jj + 1, 1, 2) + 11.0d0 / 6.0d0 * Dweno(ii, jj, 1, 2)
          Fweno(2, 2) = -1.0d0 / 6.0d0 * Dweno(ii, jj + 1, 1, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj, 1, 2) + 1.0d0 / 3.0d0 * Dweno(ii, jj - 1, 1, 2)
          Fweno(3, 2) = 1.0d0 / 3.0d0 * Dweno(ii, jj, 1, 2) + 5.0d0 / 6.0d0 * Dweno(ii, jj - 1, 1, 2) - 1.0d0 / 6.0d0 * Dweno(ii, jj - 2, 1, 2)
       end if
       !Sweno
       if(motion(ii, jj, 1, jt2, 1) > 0) then
          Sweno(1, 1) = 13.0d0 / 12.0d0 * (Dweno(ii - 2, jj, 1, 1) - 2.0d0 * Dweno(ii - 1, jj, 1, 1) + Dweno(ii, jj, 1, 1)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii - 2, jj, 1, 1) - 4.0d0 * Dweno(ii - 1, jj, 1, 1) + 3.0d0 * Dweno(ii, jj, 1, 1)) ** 2.0d0
          Sweno(2, 1) = 13.0d0 / 12.0d0 * (Dweno(ii - 1, jj, 1, 1) - 2.0d0 * Dweno(ii, jj, 1, 1) + Dweno(ii + 1, jj, 1, 1)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii - 1, jj, 1, 1) - Dweno(ii + 1, jj, 1, 1)) ** 2.0d0
          Sweno(3, 1) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, 1, 1) - 2.0d0 * Dweno(ii + 1, jj, 1, 1) + Dweno(ii + 2, jj, 1, 1)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, 1, 1) - 4.0d0 * Dweno(ii + 1, jj, 1, 1) + Dweno(ii + 2, jj, 1, 1)) ** 2.0d0
       else
          Sweno(1, 1) = 13.0d0 / 12.0d0 * (Dweno(ii + 2, jj, 1, 1) - 2.0d0 * Dweno(ii + 1, jj, 1, 1) + Dweno(ii, jj, 1, 1)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii + 2, jj, 1, 1) - 4.0d0 * Dweno(ii + 1, jj, 1, 1) + 3.0d0 * Dweno(ii, jj, 1, 1)) ** 2.0d0
          Sweno(2, 1) = 13.0d0 / 12.0d0 * (Dweno(ii + 1, jj, 1, 1) - 2.0d0 * Dweno(ii, jj, 1, 1) + Dweno(ii - 1, jj, 1, 1)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii + 1, jj, 1, 1) - Dweno(ii - 1, jj, 1, 1)) ** 2.0d0
          Sweno(3, 1) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, 1, 1) - 2.0d0 * Dweno(ii - 1, jj, 1, 1) + Dweno(ii - 2, jj, 1, 1)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, 1, 1) - 4.0d0 * Dweno(ii - 1, jj, 1, 1) + Dweno(ii - 2, jj, 1, 1)) ** 2.0d0
       end if
       if(motion(ii, jj, 1, jt2, 2) > 0) then
          Sweno(1, 2) = 13.0d0 / 12.0d0 * (Dweno(ii, jj - 2, 1, 2) - 2.0d0 * Dweno(ii, jj - 1, 1, 2) + Dweno(ii, jj, 1, 2)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii, jj - 2, 1, 2) - 4.0d0 * Dweno(ii, jj - 1, 1, 2) + 3.0d0 * Dweno(ii, jj, 1, 2)) ** 2.0d0
          Sweno(2, 2) = 13.0d0 / 12.0d0 * (Dweno(ii, jj - 1, 1, 2) - 2.0d0 * Dweno(ii, jj, 1, 2) + Dweno(ii, jj + 1, 1, 2)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii, jj - 1, 1, 2) - Dweno(ii, jj + 1, 1, 2)) ** 2.0d0
          Sweno(3, 2) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, 1, 2) - 2.0d0 * Dweno(ii, jj + 1, 1, 2) + Dweno(ii, jj + 2, 1, 2)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, 1, 2) - 4.0d0 * Dweno(ii, jj + 1, 1, 2) + Dweno(ii, jj + 2, 1, 2)) ** 2.0d0
       else
          Sweno(1, 2) = 13.0d0 / 12.0d0 * (Dweno(ii, jj + 2, 1, 2) - 2.0d0 * Dweno(ii, jj + 1, 1, 2) + Dweno(ii, jj, 1, 2)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii, jj + 2, 1, 2) - 4.0d0 * Dweno(ii, jj + 1, 1, 2) + 3.0d0 * Dweno(ii, jj, 1, 2)) ** 2.0d0
          Sweno(2, 2) = 13.0d0 / 12.0d0 * (Dweno(ii, jj + 1, 1, 2) - 2.0d0 * Dweno(ii, jj, 1, 2) + Dweno(ii, jj - 1, 1, 2)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (Dweno(ii, jj + 1, 1, 2) - Dweno(ii, jj - 1, 1, 2)) ** 2.0d0
          Sweno(3, 2) = 13.0d0 / 12.0d0 * (Dweno(ii, jj, 1, 2) - 2.0d0 * Dweno(ii, jj - 1, 1, 2) + Dweno(ii, jj - 2, 1, 2)) ** 2.0d0 + &
               & 1.0d0 / 4.0d0 * (3.0d0 * Dweno(ii, jj, 1, 2) - 4.0d0 * Dweno(ii, jj - 1, 1, 2) + Dweno(ii, jj - 2, 1, 2)) ** 2.0d0
       end if
    
       Sweno = Sweno + ipu
       do a = 1, 2
          !Aweno
          Aweno(1:3) = (/ 0.1d0, 0.6d0, 0.3d0 /) / (Sweno(1:3, a) * Sweno(1:3, a))
          !Wweno is directly multiplied with Fweno
          Fweno(1:3, a) = Fweno(1:3, a) * Aweno(1:3) / sum(Aweno(1:3))
       end do !a

       !adams
       adams_t(ii, jj, 1, i_adams(1), val_id) = -dt * sum(motion(ii, jj, 1, jt2, 1:2) * sum(Fweno(1:3, 1:2), 1))
    end do !ii
  end subroutine weno_calc_adams_2D
end module weno_core
