subroutine advection_2d(j0, j1, rain, jj0, jj1, motion, last)
  use variables
  use weno_core
  use nowcast_mpi_utils
  implicit none

  integer, intent(in) :: j0, j1, jj0, jj1
  double precision, intent(inout) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)
  real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), jj0:jj1, dz, maxnt, 3)
  logical, intent(in) :: last
  integer i, j, a
  integer jt1, jt2
  integer i_start, i_end, j_start, j_end
  double precision d_rain_dx((1 - margin_x_adv):(dx - 1 + margin_x_adv))
  integer :: time1, time2, time3, time4, timerate, timemax

  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  !5th order weno
  !CFL = maxval(abs(motion(1:dx, 1:dy, 1:dz, jt2, 1:3))) * dt * inv_gridx
  !!write(*, *) it, CFL, maxval(rain(1:dx, 1:dy, 1:dz, jt2, 1)), maxval(motion(1:dx, 1:dy, 1:dz, jt2, 3))
  !write(*, '("it = ", I6, " CFL ", F10.5, " rain max ", F10.5, " min ", F10.5, " w max ", F10.5)') &
  !     & it, CFL, maxval(rain(1:dx, 1:dy, 1:dz, jt2, 1)), minval(rain(1:dx, 1:dy, 1:dz, jt2, 1)), maxval(motion(1:dx, 1:dy, 1:dz, jt2, 3))

  if(mpiprocs > 1) then
     call system_clock(time1, timerate, timemax)
     i_start = 3 - margin_x_adv
     i_end = dx - 2 + margin_x_adv
     j_start = j_mpi(1, myrank)
     j_end   = j_mpi(2, myrank)
     if(myrank == 0) j_start = 3
     if(myrank == mpiprocs - 1) j_end = dy - 2
     call exchange_rain_halo_by_mpi_sendrecv_wait(j0, j1, rain, i_start, i_end, j_start, j_end, 1, 1, jt2, 3)
     call system_clock(time2, timerate, timemax)
     time_adv_weno_mpi = time_adv_weno_mpi + (time2 - time1) / dble(timerate)
  end if

  !Dweno
  call system_clock(time1, timerate, timemax)
  i_start = 2 - margin_x_adv
  i_end = dx - 1 + margin_x_adv
  j_start = j_mpi(1, myrank) - 2
  j_end   = j_mpi(2, myrank) + 2
  if(j_start < 2) j_start = 2
  if(j_end > dy - 1) j_end = dy - 1
!$omp parallel do private(i, j, d_rain_dx)
  do j = j_start, j_end
     d_rain_dx((i_start - 1):i_end) = rain(i_start:(i_end + 1), j, 1, jt2, 1) - rain((i_start - 1):i_end, j, 1, jt2, 1)

     do i = i_start, i_end
        if(motion(i, j, 1, jt2, 1) > 0) then
           Dweno(i, j, 1, 1) = d_rain_dx(i - 1)
        else
           Dweno(i, j, 1, 1) = d_rain_dx(i)
        end if
        if(motion(i, j, 1, jt2, 2) > 0) then
           Dweno(i, j, 1, 2) = rain(i, j, 1, jt2, 1) - rain(i, j - 1, 1, jt2, 1)
        else
           Dweno(i, j, 1, 2) = rain(i, j + 1, 1, jt2, 1) - rain(i, j, 1, jt2, 1)
        end if
     end do !i

     Dweno(i_start:i_end, j, 1, 1) = Dweno(i_start:i_end, j, 1, 1) * inv_gridx
     Dweno(i_start:i_end, j, 1, 2) = Dweno(i_start:i_end, j, 1, 2) * inv_gridy
  end do !j
!$omp end parallel do

  if(myrank == 0) &
       & Dweno(i_start:i_end, 1,  1, 1:2) = Dweno(i_start:i_end, 2,      1, 1:2)
  if(myrank == mpiprocs - 1) &
       & Dweno(i_start:i_end, dy, 1, 1:2) = Dweno(i_start:i_end, dy - 1, 1, 1:2)

  if(boundary_type == 1) then !CYCLIC IN X
!$omp parallel do private(j)
     do j = j_start, j_end
        Dweno((1 - margin_x_adv):0,         j, 1, 1:2) = Dweno((dx - margin_x_adv + 1):dx, j, 1, 1:2)
        Dweno((dx + 1):(dx + margin_x_adv), j, 1, 1:2) = Dweno(1:margin_x_adv,             j, 1, 1:2)
     end do
!$omp end parallel do
  else
!$omp parallel do private(j)
     do j = j_start, j_end
        Dweno(1,  j, 1, 1:2) = Dweno(2,      j, 1, 1:2)
        Dweno(dx, j, 1, 1:2) = Dweno(dx - 1, j, 1, 1:2)
     end do
!$omp end parallel do
  end if
  call system_clock(time2, timerate, timemax)
  time_adv_weno_dweno = time_adv_weno_dweno + (time2 - time1) / dble(timerate)

  i_start = 3 - margin_x_adv
  i_end = dx - 2 + margin_x_adv
  j_start = j_mpi(1, myrank)
  j_end   = j_mpi(2, myrank)
  if(myrank == 0) j_start = 3
  if(myrank == mpiprocs - 1) j_end = dy - 2
!$omp parallel do private(i, j)
  do j = j_start, j_end
     call weno_calc_adams_2D(jj0, jj1, motion, i_start, i_end, j, jt2, 4)
  
     if(it < 3 .or. mod(it, 10) .eq. 0) then
        rain(i_start:i_end, j, 1, jt1, 1) = rain(i_start:i_end, j, 1, jt2, 1) + adams_t(i_start:i_end, j, 1, i_adams(1), 4)
     else
        if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
           rain(i_start:i_end, j, 1, jt1, 1) = rain(i_start:i_end, j, 1, jt2, 1) &
                & + 23.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), 4) &
                & - 16.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(2), 4) &
                & +  5.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(3), 4)
        elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
           rain(i_start:i_end, j, 1, jt1, 1) = rain(i_start:i_end, j, 1, jt2, 1) &
                & + 0.5d0 * (3.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), 4) - adams_t(i_start:i_end, j, 1, i_adams(2), 4))
        end if
     end if
  
     do i = i_start, i_end
        if(tmpgrowth == 1) then
           rain(i, j, 1, jt1, 1) = rain(i, j, 1, jt1, 1) + growth_t(i, j, 1, jt2, 1)
           if((rain(i, j, 1, jt1, 1) .le. rain_missing) .and. (growth_t(i, j, 1, jt2, 1) .lt. 0.0d0)) then
              growth_t(i, j, 1, jt2, 1) = 0.0d0
           end if
        end if
        
        if(rain(i, j, 1, jt1, 1) < rain_missing) then
           rain(i, j, 1, jt1, 1) = rain_missing
        end if
     end do !i
  end do !j
!$omp end parallel do

  if(mpiprocs > 1 .and. (.not. last)) then
     call system_clock(time1, timerate, timemax)
     call exchange_rain_halo_by_mpi_sendrecv_start(j0, j1, rain, i_start, i_end, j_start, j_end, 1, 1, jt1, 3)
     call system_clock(time2, timerate, timemax)
     time_adv_weno_mpi = time_adv_weno_mpi + (time2 - time1) / dble(timerate)
  end if

end subroutine advection_2d
