subroutine lanczos_filtering(data, nval)
  use variables
  implicit none

  !data: motion_t or growth_t
  real(4), intent(inout) :: data((1 - margin_x):(dx + margin_x), dy, dz, maxnt, nval)
  integer, intent(in) :: nval

  integer i, j, k, a
  double precision, allocatable :: data_filter(:, :, :, :, :)
  double precision, allocatable :: data_filter_mean(:, :)
  integer MM1
  double precision fc   ! critical angular frequency in [0,pi]
  double precision, allocatable :: weight_lan(:)
  integer i_start, i_end, j_start, j_end

  allocate(data_filter((1 - margin_x):(dx + margin_x), dy, dz, nval, 2), &
       &   data_filter_mean(dz, nval))
!$omp parallel do private(k, a) collapse(2)
  do a = 1, nval
     do k = 1, dz
        data_filter_mean(k, a) = sum(data(1:dx, 1:dy, k, 1, a)) / (dx * dy)
        !compute anomaly
        data_filter(:, :, k, a, 1) = data(:, :, k, 1, a) - data_filter_mean(k, a)
     end do !k
  end do !a
!$omp end parallel do

  !---------------------------------lanczos filtering--------------------------------------------------------
  ! lanczos filtering on motion vectors
  write(*, *) "Lanczos filter weight for ", lanczos_window, lanczos_critical_wavelength
  MM1 = lanczos_window
  fc = (4.0d0 * atan(1.0d0)) / lanczos_critical_wavelength ! PI/(wavelength)

  allocate(weight_lan(2 * MM1 + 1))
  call lanczos_filter_weight(weight_lan, fc, MM1)
  write(*, *) "lanczos sum of weight: ", sum(weight_lan)
  if(dx <= MM1 .or. dy <= MM1 .or. dz <= MM1) then
     write(*, *) "Lanczos window size is too large"
     stop 999
  end if

  !data_filter(1) -> data_filter(2)

  i_start = 1 - margin_x + MM1
  i_end = dx + margin_x - MM1
  j_start = j_mpi(1, myrank) - MM1
  j_end = j_mpi(2, myrank) + MM1
  if(j_start < 1 + MM1) j_start = 1 + MM1
  if(j_end > dy - MM1) j_end = dy - MM1
  !!! X
!$omp parallel do private(i, j, k, a)
  do k = 1 + MM1, dz - MM1
     do a = 1, nval !!! U, V, W
        do j = j_start, j_end
           do i = i_start, i_end
              data_filter(i, j, k, a, 2) = sum(data_filter((i - MM1):(i + MM1), j, k, a, 1) * weight_lan(:))
           end do !i
        end do !j
     end do !a
  end do !k
!$omp end parallel do

  !data_filter(2) -> data_filter(1)

  j_start = j_mpi(1, myrank)
  j_end = j_mpi(2, myrank)
  if(j_start < 1 + MM1) j_start = 1 + MM1
  if(j_end > dy - MM1) j_end = dy - MM1
  !!! Y
!$omp parallel do private(i, j, k, a)
  do k = 1 + MM1, dz - MM1
     do a = 1, nval !!! U, V, W
        do j = j_start, j_end
           do i = i_start, i_end
              data_filter(i, j, k, a, 1) = sum(data_filter(i, (j - MM1):(j + MM1), k, a, 2) * weight_lan(:))
           end do !i
        end do !j
     end do !a
  end do !k
!$omp end parallel do

  !data_filter(1) -> data_filter(2)

  !!! Z
!$omp parallel do private(i, j, k, a)
  do k = 1 + MM1, dz - MM1
     do a = 1, nval !!! U, V, W
        do j = j_start, j_end
           do i = i_start, i_end
              data_filter(i, j, k, a, 2) = sum(data_filter(i, j, (k - MM1):(k + MM1), a, 1) * weight_lan(:))
           end do !i
        end do !j
     end do !a
  end do !k
!$omp end parallel do

!!! ANOMALY TO FULL FIELD
!$omp parallel do private(k, a) collapse(2)
  do a = 1, nval
     do k = 1, dz
        data_filter(:, j_start:j_end, k, a, 2) = data_filter(:, j_start:j_end, k, a, 2) + data_filter_mean(k, a)
     end do !k
  end do !a
!$omp end parallel do

  !------------------------use average nearest neighbours on margins----------------------------------
  i_start = 2 - margin_x
  i_end = dx - 1 + margin_x
  j_start = j_mpi(1, myrank)
  j_end = j_mpi(2, myrank)
  if(j_start < 2) j_start = 2
  if(j_end > dy - 1) j_end = dy - 1
  
  !BOTTOM
  call lanczos_filtering_7point_mean(i_start, i_end, j_start, j_end, 2, MM1)
  !TOP
  call lanczos_filtering_7point_mean(i_start, i_end, j_start, j_end, dz - MM1 + 1, dz - 1)
  !SOUTH
  if(j_mpi(1, myrank) .le. MM1) &
       & call lanczos_filtering_7point_mean(i_start, i_end, j_start, min(MM1, j_end), MM1 + 1, dz - MM1)
  !NORTH
  if(j_mpi(2, myrank) .ge. dy - MM1 + 1) &
       & call lanczos_filtering_7point_mean(i_start, i_end, max(dy - MM1 + 1, j_start), j_end, MM1 + 1, dz - MM1)
  if(boundary_type .ne. 1) then !NOT CYCLIC IN X
     !WEST
     call lanczos_filtering_7point_mean(2, MM1, MM1 + 1, dy - MM1, MM1 + 1, dz - MM1)
     !EAST
     call lanczos_filtering_7point_mean(dx - MM1 + 1, dx - 1, MM1 + 1, dy - MM1, MM1 + 1, dz - MM1)
  end if

!$omp parallel do private(k)
  do k = 1, dz
     data(i_start:i_end, j_start:j_end, k, 1, 1:nval) = data_filter(i_start:i_end, j_start:j_end, k, 1:nval, 2)
  end do
!$omp end parallel do

  deallocate(data_filter, data_filter_mean, weight_lan)

contains

  subroutine lanczos_filtering_7point_mean(i0, i1, j0, j1, k0, k1)
    integer, intent(in) :: i0, i1, j0, j1, k0, k1

!$omp parallel do private(i, j, k, a) collapse(2)
    do a = 1, nval !!! U, V, W
       do k = k0, k1
          do j = j0, j1
             do i = i0, i1
                data_filter(i, j, k, a, 2) = (data(i, j, k, 1, a) &
                     &                        + data(i - 1, j, k, 1, a) + data(i + 1, j, k, 1, a) &
                     &                        + data(i, j - 1, k, 1, a) + data(i, j + 1, k, 1, a) &
                     &                        + data(i, j, k - 1, 1, a) + data(i, j, k + 1, 1, a)) / 7.0d0
             end do !i
          end do !j
       end do !k
    end do !a
!$omp end parallel do
  end subroutine lanczos_filtering_7point_mean

end subroutine lanczos_filtering

!-----------------------------------------------------------------------!
! Lanczos Filter (Low-pass) 
!-----------------------------------------------------------------------!
SUBROUTINE lanczos_filter_weight(weight_lan, fc, MM1)
  IMPLICIT NONE
  integer, intent(in) :: MM1
  double precision, intent(inout), dimension(2 * MM1 + 1) :: weight_lan
  double precision, intent(in) :: fc
  double precision pi
  integer aa1

  pi = 4.0d0 * atan(1.0d0)
  ! calculate weight
  
  do aa1 = -MM1, MM1
     if (aa1 == 0) then
        weight_lan(MM1 + aa1 + 1) = fc / pi
     else
        weight_lan(MM1 + aa1 + 1) = (SIN(aa1 * fc) / (pi * aa1)) * (SIN(aa1 * pi / MM1) / (aa1 * pi / MM1))
     endif
  enddo
   
END SUBROUTINE lanczos_filter_weight
