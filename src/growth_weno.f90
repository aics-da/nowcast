subroutine growth_rain_weno(j0, j1, motion)
  use variables
  use weno_core
  use nowcast_mpi_utils
  implicit none

  integer, intent(in) :: j0, j1
  real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
  real(4), save, allocatable :: mpi_buf(:, :, :, :, :)
  integer i, j, k, a, i_start, i_end, j_start, j_end, k_start, k_end
  integer jt1, jt2
  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  i_start = 3 - margin_x_adv
  i_end = dx - 2 + margin_x_adv
  j_start = j_mpi(1, myrank)
  j_end   = j_mpi(2, myrank)
  if(myrank == 0) j_start = 3
  if(myrank == mpiprocs - 1) j_end = dy - 2
  k_start = 3
  k_end = dz - 2
  if(mpiprocs > 1) &
     & call exchange_real4_halo_by_mpi_sendrecv_wait( &
     & 1, dy, growth_t, i_start, i_end, j_start, j_end, k_start, k_end, jt2, 1, 1, mpi_buf, mpi_req_growth_halo)

  i_start = 2 - margin_x_adv
  i_end = dx - 1 + margin_x_adv
  j_start = j_mpi(1, myrank) - 2
  j_end   = j_mpi(2, myrank) + 2
  if(j_start < 2)    j_start = 2
  if(j_end > dy - 1) j_end   = dy - 1

  !5th order weno

  !Dweno
!$omp parallel do private(i, j, k)
  do k = 2, dz - 1
     do j = j_start, j_end
        do i = i_start, i_end
           if(motion(i, j, k, jt2, 1) > 0) then
              Dweno(i, j, k, 1) = (growth_t(i, j, k, jt2, 1) - growth_t(i - 1, j, k, jt2, 1)) * inv_gridx
           else
              Dweno(i, j, k, 1) = (growth_t(i + 1, j, k, jt2, 1) - growth_t(i, j, k, jt2, 1)) * inv_gridx
           end if

           if(motion(i, j, k, jt2, 2) > 0) then
              Dweno(i, j, k, 2) = (growth_t(i, j, k, jt2, 1) - growth_t(i, j - 1, k, jt2, 1)) * inv_gridy
           else
              Dweno(i, j, k, 2) = (growth_t(i, j + 1, k, jt2, 1) - growth_t(i, j, k, jt2, 1)) * inv_gridy
           end if

           if(motion(i, j, k, jt2, 3) > 0) then
              Dweno(i, j, k, 3) = (growth_t(i, j, k, jt2, 1) - growth_t(i, j, k - 1, jt2, 1)) * inv_gridz
           else
              Dweno(i, j, k, 3) = (growth_t(i, j, k + 1, jt2, 1) - growth_t(i, j, k, jt2, 1)) * inv_gridz
           end if
        end do
     end do
  end do
!$omp end parallel do

  call weno_dweno_boundary_condition

  i_start = 3 - margin_x_adv
  i_end = dx - 2 + margin_x_adv
  j_start = j_mpi(1, myrank)
  j_end   = j_mpi(2, myrank)
  if(myrank == 0) j_start = 3
  if(myrank == mpiprocs - 1) j_end = dy - 2
  k_start = 3
  k_end = dz - 2

  call weno_calc_adams(j0, j1, motion, i_start, i_end, j_start, j_end, k_start, k_end, jt2, 5)

!$omp parallel do private(i, j, k, a)
  do k = 3, dz - 2
     if(it < 3 .or. mod(it, 10) .eq. 0) then
        growth_t(i_start:i_end, j_start:j_end, k, jt1, 1) = growth_t(i_start:i_end, j_start:j_end, k, jt2, 1) + adams_t(i_start:i_end, j_start:j_end, k, i_adams(1), 5)
     else
        if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
           growth_t(i_start:i_end, j_start:j_end, k, jt1, 1) = growth_t(i_start:i_end, j_start:j_end, k, jt2, 1) &
                & + 23.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(1), 5) &
                & - 16.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(2), 5) &
                & +  5.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(3), 5)
        elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
           growth_t(i_start:i_end, j_start:j_end, k, jt1, 1) = growth_t(i_start:i_end, j_start:j_end, k, jt2, 1) &
                & + 0.5d0 * (3.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(1), 5) - adams_t(i_start:i_end, j_start:j_end, k, i_adams(2), 5))
        end if
     end if
  end do
!$omp end parallel do

  if(mpiprocs > 1) &
       & call exchange_real4_halo_by_mpi_sendrecv_start( &
       & 1, dy, growth_t, i_start, i_end, j_start, j_end, k_start, k_end, jt1, 1, 2, mpi_buf, mpi_req_growth_halo)
end subroutine growth_rain_weno
