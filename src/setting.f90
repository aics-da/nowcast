module setting
  use variables
  implicit none

  public
  
contains
  subroutine set_params
  
    logical file_exist

    namelist /input_config/ input_basename, wind_basename, qcf_basename, phidp_basename, input_suffix, &
         & expconv, read_topo_mask, topo_max, topo_norain_threshold, &
         & input_interval, max_input_interval, output_interval, &
         & use_precomputed_vector, switching_input_missing, input_missing_newval, input_valid_min, input_valid_max, read_from_jit_dt, jit_monitor_path, &
         & output_rain_cartesian, output_rain_cartesian2, output_latlon, output_vector, output_init_growth, output_vector_quality, &
         & output_history, split_history, history_rain_max_it, history_uvw_max_it, history_growth_max_it, &
         & io_proc_permutation, output_da_anal, input_type, input_nc_var, &
         & use_radar_qc, radar_qcf_mask, radar_smooth_radial, radar_smooth_radial_wid, radar_smooth_azimuth, radar_smooth_elev, use_phidp, output_corrected_zh,&
         & ensemble_size, ensemble_share_input, monitor_mean_vector, smooth_rain_field, smooth_rain_field_ratio_to_frame_num, output_polar_qced
    namelist /time_config/ dt, calnt, start, iteration_all, nt
    namelist /domain_config/ ddx, ddy, ddz, &
         & dx, dy, dz, posix, posiy, posiz, &
         & ox, oy, oz, o_offset_x, o_offset_y, o_offset_z, &
         & gridx, gridy, gridz, &
         & boundary_type, proj_type, base_x, base_y, base_lon, base_lat, std_lon, std_lat1, std_lat2
    namelist /search_config/ frame_num, frame_numz, use_variable_frame_num, &
         & search_num, search_numz, adjust_search_num, use_variable_search_num, &
         & switching_vector, &
         & switching_vector_centroid, vector_centroid_variable_range, vector_centroid_global, &
         & corr_qc_threshold, corr_diff_qc_threshold, corr_qc_normalize_diff_by_autocorr, &
         & switching_vector_skip_missing, vector_missing_threshold, min_valid_grid_ratio, min_valid_direction_ratio, &
         & fill_missing, fill_missing_threshold, fill_missing_w, match_by_sphere, search_within_sphere, x_skip, y_skip, vector_align_skip, &
         & horizontally_uniform_vector, vector_time_assignment, vect_interp_z_if_all_missing, vector_output_corr_diff, &
         & vector_set_core_minval, vector_core_minval, vector_core_radius
    namelist /cotrec_config/ threshold, countermax, sor_accel, swiching_cotrec
    namelist /driver_config/ swiching_burgers, switching_growth, switching_growth_weno, growth_rate_max_it, switching_boundary_motion, switching_time_integration, &
         & switching_div_damper, div_damper_param, it_switch_3d_to_2d
    namelist /noise_config/ rain_noise, fuzzy_sad_min, fuzzy_sad_max, switching_lanczos, lanczos_window, lanczos_critical_wavelength
    namelist /da_config/ use_data_assimilation, variance_f_init, variance_f_inflation, variance_f_max, variance_o, da_start_from, da_qc_threshold, &
         & da_gues_iter, da_gues_rec_num, da_gues_path, &
         & da_qc_temp_consistency, da_qc_temp_consistency_sigma, da_qc_temp_consistency_threshold, da_qc_temp_consistency_hist_save_rejected, &
         & da_assimilate_boundary, da_assimilate_filled_wind
    
    ox = -1
    oy = -1
    oz = -1
    o_offset_x = 0
    o_offset_y = 0
    o_offset_z = 0

    inquire(file = "setting.namelist", exist = file_exist)
    if(file_exist) then !NEW VERSION
       !DEFALUT VALUES
       fill_missing_threshold = -1 !UNDEFINED
       
       !READ NAMELIST
       open(12, file = 'setting.namelist', form = 'formatted')
       read(12, nml = input_config)
       read(12, nml = time_config)
       read(12, nml = domain_config)
       read(12, nml = search_config)
       read(12, nml = cotrec_config)
       read(12, nml = driver_config)
       read(12, nml = noise_config)
       read(12, nml = da_config)
       close(12)
    else !OLD VERSION
       open(12, file = 'setting.dat', form = 'formatted')
       read(12,*) dt
       read(12,*) calnt
       read(12,*) start
       read(12,*) iteration_all
       read(12,*) nt
       read(12,*) 
       read(12,*) ddx
       read(12,*) ddy
       read(12,*) ddz
       read(12,*) dx
       read(12,*) dy
       read(12,*) dz
       read(12,*) gridx
       read(12,*) gridy
       read(12,*) gridz
       read(12,*) posix
       read(12,*) posiy
       read(12,*) posiz
       read(12,*) 
       read(12,*) frame_num
       read(12,*) frame_numz
       read(12,*) search_num
       read(12,*) search_numz
       read(12,*) switching_vector
       read(12,*) switching_vector_centroid
       read(12,*) corr_qc_threshold
       read(12,*) 
       read(12,*) threshold
       read(12,*) countermax
       read(12,*) sor_accel !for COTREC SOR accelerator
       read(12,*) swiching_cotrec
       read(12,*) swiching_burgers
       read(12,*) switching_growth
       read(12,*) switching_growth_weno
       read(12,*) growth_rate_max_it
       read(12,*) switching_boundary_motion
       read(12,*) 
       read(12,*) rain_noise
       read(12,*) fuzzy_sad_min
       read(12,*) fuzzy_sad_max
       read(12,*) 
       read(12,*) switching_lanczos
       read(12,*) lanczos_window
       read(12,*) lanczos_critical_wavelength
       read(12,*) 
       read(12,*) expconv
       read(12,*) 
       read(12,*) switching_time_integration
       read(12,*) 
       read(12,*) input_basename
       read(12,*) switching_input_missing
       read(12,*) input_missing_newval
       read(12,*)
       read(12,*) switching_vector_skip_missing
       read(12,*) vector_missing_threshold
       read(12,*) min_valid_grid_ratio
       read(12,*) min_valid_direction_ratio
       read(12,*)
       read(12,*) read_topo_mask
       read(12,*) topo_max
       close(12)
    end if

    if(ddz == 1) then !FORCE PURE 2D RUN IF INPUT IS 2D
       dz = 1
       posiz = 0
    end if

    if(dz == 1) then !FORCE PURE 2D RUN IF WORKSPACE IS 2D
       frame_numz = 1
       search_numz = 0
    end if

    if(ox < 0) ox = dx
    if(oy < 0) oy = dy
    if(oz < 0) oz = dz

    frame_num_2 = frame_num / 2
    frame_numz_2 = frame_numz / 2
    frame_num_2a = frame_num - frame_num_2 - 1
    frame_numz_2a = frame_numz - frame_numz_2 - 1

    if(use_variable_frame_num == 1) then
       if(allocated(variable_frame_num_y)) deallocate(variable_frame_num_y)
       allocate(variable_frame_num_y(dy))
       open(44, file = "variable_frame_num_y.txt")
       read(44, *) variable_frame_num_y
       close(44)
    end if

    if(allocated(search_numx)) deallocate(search_numx)
    if(allocated(search_numy)) deallocate(search_numy)
    allocate(search_numx(dy), search_numy(dy))
    if(use_variable_search_num == 1 .or. use_variable_search_num == 3) then
       open(44, file = "variable_search_num_y.txt")
       read(44, *) search_numy
       close(44)
       search_numy_max = maxval(search_numy)
    else
       search_numy = search_num
       search_numy_max = search_num
    end if
    if(use_variable_search_num == 2 .or. use_variable_search_num == 3) then
       open(44, file = "variable_search_num_x.txt")
       read(44, *) search_numx
       close(44)
       search_numx_max = maxval(search_numx)
    else
       search_numx = search_num
       search_numx_max = search_num
    end if

    inv_gridx = 1.0d0 / gridx
    inv_gridy = 1.0d0 / gridy
    inv_gridz = 1.0d0 / gridz

    if(boundary_type == 0) then
       margin_x_adv = 0
       margin_x = 0
    else if(boundary_type == 1) then !CYCLIC IN X
       margin_x_adv = 2
       margin_x = margin_x_adv
       if(switching_lanczos == 1 .and. lanczos_window > margin_x) margin_x = lanczos_window
    end if
    dx_w_margin = dx + margin_x * 2

    ! "maxnt" includes initial condition and 
    ! one (or more) time steps for time integration
    if(allocated(it2jt)) deallocate(it2jt)
    allocate(it2jt(max(calnt, maxnt), maxnt))
    if(allocated(mean_vect)) deallocate(mean_vect)
    if(allocated(num_valid_vect)) deallocate(num_valid_vect)
    allocate(mean_vect(3, dz), num_valid_vect(dz))

    if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
       max_i_adams = 3
    elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
       max_i_adams = 2
    else
       write(*, *) "error: switching_time_integration > 1"
       stop 999
    end if

    if(allocated(date)) deallocate(date)
    allocate(date(6, nt))
    date = -1 !UNDEF

    !mountain
    if(allocated(topo)) deallocate(topo)
    allocate(topo(dx, dy))

    !DATA ASSIMILATION
    if(allocated(variance_f)) deallocate(variance_f)
    if(use_data_assimilation > 0) allocate(variance_f(1:dx, 1:dy, 1:dz, 1:3))

  end subroutine  set_params
end module setting
