module vector_core
  use variables, only: real_size, dx, dy, dz, dt, input_interval, x_skip, y_skip, &
       & min_valid_grid_ratio, min_valid_direction_ratio, match_by_sphere, search_within_sphere, vector_core_radius, &
       & switching_vector, vector_time_assignment, switching_vector_centroid, vector_centroid_global, vector_centroid_variable_range, &
       & fuzzy_sad_min, fuzzy_sad_max, &
       & expconv, growth_t, frame_t, motion_t, motion_undef, num_valid_vect
  implicit none
  public
contains

  function corr_vector(fxy, fz, sx, sy, sz, tmpframe1_anom, corr2, tmpframe2, ix00, jy00, kz00, ix0, jy0, kz0, margin_x_vec1, margin_x_vec2, tmpaverage, tmpvariance)
    real(real_size) :: corr_vector
    integer, intent(in) :: fxy, fz, sx, sy, sz, margin_x_vec1, margin_x_vec2
    real(real_size), intent(in) :: tmpframe1_anom(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))
    integer, intent(in) :: ix0, jy0, kz0, ix00, jy00, kz00
    integer tmpix, tmpjy, tmpkz, dd, ee, ff
    real(real_size), intent(in) :: corr2
    real(real_size), intent(inout) :: tmpvariance((1 - margin_x_vec1):(dx + margin_x_vec2), (1 - margin_x_vec1):(dx + margin_x_vec2), dz)
    real(real_size), intent(inout) :: tmpaverage((1 - margin_x_vec1):(dx + margin_x_vec2), (1 - margin_x_vec1):(dx + margin_x_vec2), dz)

    real(real_size) sub_ave, corr1, corr3, tmp1, tmp2

    !correlation coefficient
    corr1 = 0.0d0

    if(tmpvariance(ix00 + ix0, jy00 + jy0, kz00 + kz0) .gt. 0.0d0) then
       corr3 = tmpvariance(ix00 + ix0, jy00 + jy0, kz00 + kz0)
       sub_ave = tmpaverage(ix00 + ix0, jy00 + jy0, kz00 + kz0)
       tmpkz = kz0
       do ff = 1, fz
          tmpjy = jy0
          do ee = 1, fxy
             tmpix = ix0
             do dd = 1, fxy
                tmp1 = tmpframe1_anom(dd, ee, ff)
                tmp2 = tmpframe2(tmpix, tmpjy, tmpkz) - sub_ave
                corr1 = corr1 + tmp1 * tmp2
                tmpix = tmpix + 1
             end do
             tmpjy = tmpjy + 1
          end do
          tmpkz = tmpkz + 1
       end do
    else
       sub_ave = sum(tmpframe2(ix0:(ix0 + fxy - 1), jy0:(jy0 + fxy - 1), kz0:(kz0 + fz - 1))) / (fxy * fxy * fz)
       corr3 = 0.0d0
       tmpkz = kz0
       do ff = 1, fz
          tmpjy = jy0
          do ee = 1, fxy
             tmpix = ix0
             do dd = 1, fxy
                tmp1 = tmpframe1_anom(dd, ee, ff)
                tmp2 = tmpframe2(tmpix, tmpjy, tmpkz) - sub_ave
                corr1 = corr1 + tmp1 * tmp2
                corr3 = corr3 + tmp2 * tmp2
                tmpix = tmpix + 1
             end do
             tmpjy = tmpjy + 1
          end do
          tmpkz = tmpkz + 1
       end do

       if(kz0 == 0) then !for thread safe
          tmpaverage(ix00 + ix0, jy00 + jy0, kz00 + kz0) = sub_ave
          tmpvariance(ix00 + ix0, jy00 + jy0, kz00 + kz0) = corr3
       end if
    end if

    if(corr2 == 0 .or. corr3 == 0) then
       corr_vector = 0
    else
       corr_vector = corr1 / sqrt(corr2 * corr3) !correlation
    endif
  end function corr_vector

  subroutine corr_vector_2(fxy, fz, sx, sy, sz, tmpframe1, tmpframe2, ix0, jy0, kz0, &
                  & margin_x_vec1, margin_x_vec2, tmpaverage, tmpvariance, &
                  & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
    integer, intent(in) :: fxy, fz, sx, sy, sz, ix0, jy0, kz0, margin_x_vec1, margin_x_vec2
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))
    real(real_size), intent(inout) :: tmpvariance((1 - margin_x_vec1):(dx + margin_x_vec2), (1 - margin_x_vec1):(dx + margin_x_vec2), dz)
    real(real_size), intent(inout) :: tmpaverage((1 - margin_x_vec1):(dx + margin_x_vec2), (1 - margin_x_vec1):(dx + margin_x_vec2), dz)
    real(real_size), intent(inout) :: corrmap((-sx):sx, (-sy):sy, (-sz):sz)
    real(real_size), intent(inout) :: max_corr, min_corr
    integer, intent(inout) :: max_corrxyz(3)
    logical, intent(in) :: search_within_sphere_flag
    logical, intent(in) :: search_mask(-sx:sx, -sy:sy, -sz:sz)

    real(real_size), allocatable :: tmpframe1_anom(:, :, :)
    real(real_size) tmpframe1_var, corr
    integer dd, ee, ff

    allocate(tmpframe1_anom(fxy, fxy, fz))
    tmpframe1_anom = tmpframe1 - sum(tmpframe1) / (fxy * fxy * fz)
    tmpframe1_var = sum(tmpframe1_anom * tmpframe1_anom)
    do ff = -sz, sz
       do ee = -sy, sy
          do dd = -sx, sx
             if(search_within_sphere_flag .and. search_mask(dd, ee, ff)) cycle
             corr = corr_vector(fxy, fz, sx, sy, sz, tmpframe1_anom, tmpframe1_var, tmpframe2, ix0, jy0, kz0, dd, ee, ff, &
                  & margin_x_vec1, margin_x_vec2, tmpaverage, tmpvariance)
             corrmap(dd, ee, ff) = corr
             if(max_corr < corr) then
                max_corr = corr
                max_corrxyz = (/ dd, ee, ff /)
             else if(min_corr > corr) then
                min_corr = corr
             end if
          end do !!! dd
       end do !!! ee
    end do !!! ff
    deallocate(tmpframe1_anom)
  end subroutine corr_vector_2

  function corr_vector_skip_noise(fxy, fz, sx, sy, sz, tmpframe1, tmpframe2, work1, work2, noise_threshold, ix0, jy0, kz0, match_grid_num)
    real(real_size) :: corr_vector_skip_noise
    integer, intent(in) :: fxy, fz, sx, sy, sz
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))
    real(real_size), intent(out), dimension(fxy * fxy * fz) :: work1, work2
    real(real_size), intent(in) :: noise_threshold
    integer, intent(in) :: ix0, jy0, kz0
    integer, intent(in) :: match_grid_num

    real(real_size) mean1, mean2, corr1, corr2, corr3
    integer dd, ee, ff, valid_count, min_count, ix1, jy1, kz1

    if(match_grid_num == 0) then
       min_count = fxy * fxy * fz * min_valid_grid_ratio
    else
       min_count = match_grid_num * min_valid_grid_ratio
    end if

    corr_vector_skip_noise = -1.0d2 !INVALID
    valid_count = 0
    kz1 = kz0
    do ff = 1, fz
       jy1 = jy0
       do ee = 1, fxy
          ix1 = ix0
          do dd = 1, fxy
             if(tmpframe1(dd, ee, ff) .gt. noise_threshold .and. (tmpframe2(ix1, jy1, kz1) .gt. noise_threshold)) then
                valid_count = valid_count + 1
                work1(valid_count) = tmpframe1(dd, ee, ff)
                work2(valid_count) = tmpframe2(ix1, jy1, kz1)
             end if
             ix1 = ix1 + 1
          end do !dd
          jy1 = jy1 + 1
       end do !ee
       kz1 = kz1 + 1
    end do !ff

    if(valid_count > min_count) then
       mean1 = sum(work1(1:valid_count)) / valid_count
       mean2 = sum(work2(1:valid_count)) / valid_count
       corr1 = 0
       corr2 = 0
       corr3 = 0
       do dd = 1, valid_count
          corr1 = corr1 + work1(dd) * work2(dd)
          corr2 = corr2 + work1(dd) * work1(dd)
          corr3 = corr3 + work2(dd) * work2(dd)
       end do !dd
       corr1 = corr1 - mean1 * mean2 * valid_count
       corr2 = corr2 - mean1 * mean1 * valid_count
       corr3 = corr3 - mean2 * mean2 * valid_count
       if(corr2 == 0 .or. corr3 == 0) then
          corr_vector_skip_noise = 0
       else
          corr_vector_skip_noise = corr1 / sqrt(corr2 * corr3)
       end if
    end if
  end function corr_vector_skip_noise

  subroutine corr_vector_skip_noise_2(fxy, fz, sx0, sy0, sz0, sx, sy, sz, tmpframe1, tmpframe2, noise_threshold, match_grid_num, &
       & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
    integer, intent(in) :: fxy, fz, sx0, sy0, sz0, sx, sy, sz
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx0):(sx0 + fxy - 1), &
         &                                   (-sy0):(sy0 + fxy - 1), &
         &                                   (-sz0):(sz0 + fz - 1))
    integer, intent(in) :: match_grid_num
    real(real_size), intent(in) :: noise_threshold
    real(real_size), intent(inout) :: corrmap((-sx0):sx0, (-sy0):sy0, (-sz0):sz0)
    real(real_size), intent(inout) :: max_corr, min_corr
    integer, intent(inout) :: max_corrxyz(3)
    logical, intent(in) :: search_within_sphere_flag
    logical, intent(in) :: search_mask(-sx0:sx0, -sy0:sy0, -sz0:sz0)

    real(real_size), allocatable :: work1(:), work2(:)
    integer all_count
    real(real_size) corr
    integer dd, ee, ff, valid_count
    integer sx1, sx2, sy1, sy2, sz1, sz2

    allocate(work1(fxy * fxy * fz), work2(fxy * fxy * fz))

    sx1 = max_corrxyz(1) - sx
    sx2 = max_corrxyz(1) + sx
    sy1 = max_corrxyz(2) - sy
    sy2 = max_corrxyz(2) + sy
    sz1 = max_corrxyz(3) - sz
    sz2 = max_corrxyz(3) + sz
    if(sx1 < -sx0) sx1 = -sx0
    if(sx2 >  sx0) sx2 =  sx0
    if(sy1 < -sy0) sy1 = -sy0
    if(sy2 >  sy0) sy2 =  sy0
    if(sz1 < -sz0) sz1 = -sz0
    if(sz2 >  sz0) sz2 =  sz0
    all_count = (sx2 - sx1 + 1) * (sy2 - sy1 + 1) * (sz2 - sz1 + 1) - count(search_mask(sx1:sx2, sy1:sy2, sz1:sz2))

    valid_count = 0
    do ff = sz1, sz2
       do ee = sy1, sy2
          do dd = sx1, sx2
             if(search_within_sphere_flag .and. search_mask(dd, ee, ff)) cycle
             if(corrmap(dd, ee, ff) < -2.0d2) then !IF MISSING
                corr = corr_vector_skip_noise(fxy, fz, sx0, sy0, sz0, tmpframe1, tmpframe2, work1, work2, noise_threshold, dd, ee, ff, match_grid_num)
                corrmap(dd, ee, ff) = corr
             else !READ CACHED DATA
                corr = corrmap(dd, ee, ff)
             end if
             if(corr > -10.0d0) then !IF NOT NOISE
                if(max_corr < corr) then
                   max_corr = corr
                   max_corrxyz = (/ dd, ee, ff /)
                else if(min_corr > corr) then
                   min_corr = corr
                end if
                valid_count = valid_count + 1
             end if
          end do !!! dd
       end do !!! ee
    end do !!! ff

    if(valid_count < all_count * min_valid_direction_ratio) max_corr = -100.0d0 !IF MANY NOISE
    deallocate(work1, work2)
  end subroutine corr_vector_skip_noise_2

  subroutine corr_vector_skip_noise_2_hv(fxy, fz, sx, sy, sz, tmpframe1, tmpframe2, noise_threshold, match_grid_num, &
       & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
    integer, intent(in) :: fxy, fz, sx, sy, sz
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))
    integer, intent(in) :: match_grid_num
    real(real_size), intent(in) :: noise_threshold
    real(real_size), intent(inout) :: corrmap((-sx):sx, (-sy):sy, (-sz):sz)
    real(real_size), intent(inout) :: max_corr, min_corr
    integer, intent(inout) :: max_corrxyz(3)
    logical, intent(in) :: search_within_sphere_flag
    logical, intent(in) :: search_mask(-sx:sx, -sy:sy, -sz:sz)
    integer tmpxyz(3), dd

    tmpxyz = max_corrxyz
    do dd = 1, 3
       !SEMI-HORIZONTAL SEARCH
       call corr_vector_skip_noise_2(fxy, fz, sx, sy, sz, sx, sy, 1, tmpframe1, tmpframe2, &
            & noise_threshold, match_grid_num, &
            & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
       !SEMI-VERTICAL SEARCH
       call corr_vector_skip_noise_2(fxy, fz, sx, sy, sz, 1, 1, sz, tmpframe1, tmpframe2, &
            & noise_threshold, match_grid_num, &
            & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
       if(tmpxyz(1) == max_corrxyz(1) .and. tmpxyz(2) == max_corrxyz(2) .and. tmpxyz(3) == max_corrxyz(3)) exit
       tmpxyz = max_corrxyz
    end do
    !FINAL ADJUSTMENT IN THE HORIZONTAL
    call corr_vector_skip_noise_2(fxy, fz, sx, sy, sz, sx, sy, 0, tmpframe1, tmpframe2, &
         & noise_threshold, match_grid_num, &
         & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
  end subroutine corr_vector_skip_noise_2_hv


  subroutine wgtcorr_set_weight(fxy, fz, weight)
    integer, intent(in) :: fxy, fz
    real(real_size), allocatable, intent(out) :: weight(:, :, :)
    real rij0, rk0, dist
    integer i, j, k
    allocate(weight(fxy, fxy, fz))
    rij0 = real(1 + fxy) * 0.5
    rk0 = real(1 + fz) * 0.5

!$omp parallel do private(i, j, k, dist)
    do k = 1, fz
       do j = 1, fxy
          do i = 1, fxy
             dist = ((real(i) - rij0) ** 2 + (real(j) - rij0) ** 2) / (rij0 * rij0) &
                  & + ((real(k) - rk0) ** 2) / (rk0 * rk0)
             if(dist > 1) then
                !weight(i, j, k) = 0.1
                weight(i, j, k) = 0.01
             !else if(dist < 0.5) then
             !   weight(i, j, k) = 1.0
             else
                !weight(i, j, k) = (1.0 - dist) * 1.8 + 0.1
                weight(i, j, k) = (1.0 - dist) * (0.99 / 1.0) + 0.01
             end if
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine wgtcorr_set_weight

  function wgtcorr_vector_skip_noise_core( &
       & fxy, fz, sx, sy, sz, tmpframe1, tmpframe2, weight, work1, work2, work3, noise_threshold, ix0, jy0, kz0, match_grid_num)
    integer, intent(in) :: fxy, fz, sx, sy, sz
    real(real_size) :: wgtcorr_vector_skip_noise_core
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))
    real(real_size), intent(in) :: weight(fxy, fxy, fz)
    real(real_size), intent(out), dimension(fxy * fxy * fz) :: work1, work2, work3
    real(real_size), intent(in) :: noise_threshold
    integer, intent(in) :: ix0, jy0, kz0
    integer, intent(in) :: match_grid_num
    real(real_size) mean1, mean2, corr1, corr2, corr3

    integer dd, ee, ff, valid_count, min_count, ix1, jy1, kz1
    double precision wgtsum

    if(match_grid_num == 0) then
       min_count = fxy * fxy * fz * min_valid_grid_ratio
    else
       min_count = match_grid_num * min_valid_grid_ratio
    end if

    wgtcorr_vector_skip_noise_core = -1.0d2 !INVALID
    valid_count = 0
    wgtsum = 0
    kz1 = kz0
    do ff = 1, fz
       jy1 = jy0
       do ee = 1, fxy
          ix1 = ix0
          do dd = 1, fxy
             if(tmpframe1(dd, ee, ff) .gt. noise_threshold .and. (tmpframe2(ix1, jy1, kz1) .gt. noise_threshold)) then
                valid_count = valid_count + 1
                work1(valid_count) = tmpframe1(dd, ee, ff)
                work2(valid_count) = tmpframe2(ix1, jy1, kz1)
                work3(valid_count) = weight(dd, ee, ff)
                wgtsum = wgtsum + weight(dd, ee, ff)
             end if
             ix1 = ix1 + 1
          end do !dd
          jy1 = jy1 + 1
       end do !ee
       kz1 = kz1 + 1
    end do !ff

    if(wgtsum > min_count) then
       mean1 = sum(work1(1:valid_count) * work3(1:valid_count)) / wgtsum
       mean2 = sum(work2(1:valid_count) * work3(1:valid_count)) / wgtsum
       corr1 = 0
       corr2 = 0
       corr3 = 0
       do dd = 1, valid_count
          corr1 = corr1 + work1(dd) * work2(dd) * work3(dd)
          corr2 = corr2 + work1(dd) * work1(dd) * work3(dd)
          corr3 = corr3 + work2(dd) * work2(dd) * work3(dd)
       end do !dd
       corr1 = corr1 - mean1 * mean2 * wgtsum
       corr2 = corr2 - mean1 * mean1 * wgtsum
       corr3 = corr3 - mean2 * mean2 * wgtsum
       if(corr2 == 0 .or. corr3 == 0) then
          wgtcorr_vector_skip_noise_core = 0
       else
          wgtcorr_vector_skip_noise_core = corr1 / sqrt(corr2 * corr3)
       end if
    end if
  end function wgtcorr_vector_skip_noise_core

  subroutine wgtcorr_vector_skip_noise( &
       & fxy, fz, sx0, sy0, sz0, sx, sy, sz, tmpframe1, tmpframe2, weight, noise_threshold, match_grid_num, &
       & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
    integer, intent(in) :: fxy, fz, sx0, sy0, sz0, sx, sy, sz
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx0):(sx0 + fxy - 1), &
         &                                   (-sy0):(sy0 + fxy - 1), &
         &                                   (-sz0):(sz0 + fz - 1))
    real(real_size), intent(in) :: weight(fxy, fxy, fz)
    integer, intent(in) :: match_grid_num
    real(real_size), intent(in) :: noise_threshold
    real(real_size), intent(inout) :: corrmap((-sx0):sx0, (-sy0):sy0, (-sz0):sz0)
    real(real_size), intent(inout) :: max_corr, min_corr
    integer, intent(inout) :: max_corrxyz(3)
    logical, intent(in) :: search_within_sphere_flag
    logical, intent(in) :: search_mask(-sx0:sx0, -sy0:sy0, -sz0:sz0)

    real(real_size), dimension(fxy * fxy * fz) :: work1, work2, work3
    real(real_size) corr
    integer all_count
    integer dd, ee, ff, valid_count
    integer sx1, sx2, sy1, sy2, sz1, sz2

    sx1 = max_corrxyz(1) - sx
    sx2 = max_corrxyz(1) + sx
    sy1 = max_corrxyz(2) - sy
    sy2 = max_corrxyz(2) + sy
    sz1 = max_corrxyz(3) - sz
    sz2 = max_corrxyz(3) + sz
    if(sx1 < -sx0) sx1 = -sx0
    if(sx2 >  sx0) sx2 =  sx0
    if(sy1 < -sy0) sy1 = -sy0
    if(sy2 >  sy0) sy2 =  sy0
    if(sz1 < -sz0) sz1 = -sz0
    if(sz2 >  sz0) sz2 =  sz0
    all_count = (sx2 - sx1 + 1) * (sy2 - sy1 + 1) * (sz2 - sz1 + 1) - count(search_mask(sx1:sx2, sy1:sy2, sz1:sz2))

    valid_count = 0
    do ff = sz1, sz2
       do ee = sy1, sy2
          do dd = sx1, sx2
             if(search_within_sphere_flag .and. search_mask(dd, ee, ff)) cycle
             if(corrmap(dd, ee, ff) < -2.0d2) then !IF MISSING
                corr = wgtcorr_vector_skip_noise_core( &
                     & fxy, fz, sx0, sy0, sz0, tmpframe1, tmpframe2, weight, work1, work2, work3, noise_threshold, dd, ee, ff, match_grid_num)
                corrmap(dd, ee, ff) = corr
             else !READ CACHED DATA
                corr = corrmap(dd, ee, ff)
             end if
             if(corr > -10.0d0) then !IF NOT NOISE
                if(max_corr < corr) then
                   max_corr = corr
                   max_corrxyz = (/ dd, ee, ff /)
                else if(min_corr > corr) then
                   min_corr = corr
                end if
                valid_count = valid_count + 1
             end if
          end do !!! dd
       end do !!! ee
    end do !!! ff

    if(valid_count < all_count * min_valid_direction_ratio) max_corr = -100.0d0 !IF MANY NOISE
  end subroutine wgtcorr_vector_skip_noise

  function SSD_vector(fxy, fz, sx, sy, sz, tmpframe1, tmpframe2, ix0, jy0, kz0)
    real(real_size) :: SSD_vector
    integer, intent(in) :: fxy, fz, sx, sy, sz, ix0, jy0, kz0
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))

    integer dd, ee, ff, ix1, jy1, kz1

    !SSD_vector = sum(abs(tmpframe1(1:fxy, 1:fxy, 1:fz) &
    !     & - tmpframe2(ix0:(ix0 + fxy - 1), jy0:(jy0 + fxy - 1), kz0:(kz0 + fz - 1)))) !SAD

    SSD_vector = 0.0d0
    kz1 = kz0
    do ff = 1, fz
       jy1 = jy0
       do ee = 1, fxy
          ix1 = ix0
          do dd = 1, fxy
             SSD_vector = SSD_vector + abs(tmpframe1(dd, ee, ff) - tmpframe2(ix1, jy1, kz1))
             ix1 = ix1 + 1
          end do
          jy1 = jy1 + 1
       end do
       kz1 = kz1 + 1
    end do
  end function SSD_vector

  subroutine SSD_vector_skip_noise_fast(fxy, fz, sx0, sy0, sz0, sx, sy, sz, tmpframe1, tmpframe2, noise_threshold, match_grid_num, &
       & corrmap, max_corr, min_corr, max_corrxyz, search_within_sphere_flag, search_mask)
    integer, intent(in) :: fxy, fz, sx0, sy0, sz0, sx, sy, sz
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx0):(sx0 + fxy - 1), &
         &                                   (-sy0):(sy0 + fxy - 1), &
         &                                   (-sz0):(sz0 + fz - 1))
    real(real_size), intent(in) :: noise_threshold
    integer, intent(in) :: match_grid_num

    integer d, e, f, valid_count, min_count, jy1, kz1
    integer, target :: ddeeff(3)
    integer, pointer :: dd, ee, ff

    real(real_size), intent(inout) :: corrmap((-sx0):sx0, (-sy0):sy0, (-sz0):sz0)
    real(real_size), intent(inout) :: max_corr, min_corr
    integer, intent(inout) :: max_corrxyz(3)
    logical, intent(in) :: search_within_sphere_flag
    logical, intent(in) :: search_mask(-sx0:sx0, -sy0:sy0, -sz0:sz0)

    integer all_count

    real(real_size) :: vcount((-sx0):sx0, (-sy0):sy0, (-sz0):sz0)

    integer sx1, sx2, sy1, sy2, sz1, sz2

    dd => ddeeff(1)
    ee => ddeeff(2)
    ff => ddeeff(3)

    sx1 = max_corrxyz(1) - sx
    sx2 = max_corrxyz(1) + sx
    sy1 = max_corrxyz(2) - sy
    sy2 = max_corrxyz(2) + sy
    sz1 = max_corrxyz(3) - sz
    sz2 = max_corrxyz(3) + sz
    if(sx1 < -sx0) sx1 = -sx0
    if(sx2 >  sx0) sx2 =  sx0
    if(sy1 < -sy0) sy1 = -sy0
    if(sy2 >  sy0) sy2 =  sy0
    if(sz1 < -sz0) sz1 = -sz0
    if(sz2 >  sz0) sz2 =  sz0
    all_count = (sx2 - sx1 + 1) * (sy2 - sy1 + 1) * (sz2 - sz1 + 1) - count(search_mask(sx1:sx2, sy1:sy2, sz1:sz2))

    if(match_grid_num == 0) then
       min_count = fxy * fxy * fz * min_valid_grid_ratio
    else
       min_count = match_grid_num * min_valid_grid_ratio
    end if

    vcount = 0
    !corrmap = 0.0d0
    do ff = sz1, sz2
       kz1 = ff
       do f = 1, fz
          do ee = sy1, sy2
             jy1 = ee
             do e = 1, fxy
                do dd = sx1, sx2
                   if(search_within_sphere_flag .and. search_mask(dd, ee, ff)) cycle
!dir$ ivdep
                   do d = 1, fxy
                      if(tmpframe1(d, e, f) .gt. noise_threshold .and. (tmpframe2(dd + d - 1, jy1, kz1) .gt. noise_threshold)) then
                         corrmap(dd, ee, ff) = corrmap(dd, ee, ff) - abs(tmpframe1(d, e, f) - tmpframe2(dd + d - 1, jy1, kz1))
                         vcount(dd, ee, ff) = vcount(dd, ee, ff) + 1
                      end if
                   end do !!! d
                end do !!! dd
                jy1 = jy1 + 1
             end do !!! e
          end do !!! ee
          kz1 = kz1 + 1
       end do !!! f
    end do !!! ff

    valid_count = 0
    do ff = sz1, sz2
       do ee = sy1, sy2
          do dd = sx1, sx2
             if(vcount(dd, ee, ff) > min_count) then
                corrmap(dd, ee, ff) = corrmap(dd, ee, ff) / vcount(dd, ee, ff)
                if(max_corr < corrmap(dd, ee, ff)) then
                   max_corr = corrmap(dd, ee, ff)
                   max_corrxyz = ddeeff
                else if(min_corr > corrmap(dd, ee, ff)) then
                   min_corr = corrmap(dd, ee, ff)
                end if
                valid_count = valid_count + 1
             end if
          end do !!! dd
       end do !!! ee
    end do !!! ff

    if(valid_count < all_count * min_valid_direction_ratio) max_corr = 1 !IF MANY NOISE  
  end subroutine SSD_vector_skip_noise_fast


  function fuzzy_vector(fxy, fz, sx, sy, sz, tmpframe1, tmpframe1_anom, tmpframe1_var, tmpframe2, ix00, jy00, kz00, ix0, jy0, kz0, margin_x_vec1, margin_x_vec2, tmpaverage, tmpvariance)
    real(real_size) :: fuzzy_vector
    integer, intent(in) :: fxy, fz, sx, sy, sz, ix0, jy0, kz0, ix00, jy00, kz00, margin_x_vec1, margin_x_vec2
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe1_anom(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe1_var
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))
    real(real_size), intent(inout) :: tmpvariance((1 - margin_x_vec1):(dx + margin_x_vec2), (1 - margin_x_vec1):(dx + margin_x_vec2), dz)
    real(real_size), intent(inout) :: tmpaverage((1 - margin_x_vec1):(dx + margin_x_vec2), (1 - margin_x_vec1):(dx + margin_x_vec2), dz)
    real(real_size) :: v_ssd, v_corr
    real(real_size), parameter :: w_ssd = 1.0d0
    real(real_size), parameter :: w_corr = 1.0d0
    integer totalgrids

    totalgrids = fxy * fxy * fz

    v_ssd = SSD_vector(fxy, fz, sx, sy, sz, tmpframe1, tmpframe2, ix0, jy0, kz0)
    v_corr = corr_vector(fxy, fz, sx, sy, sz, tmpframe1_anom, tmpframe1_var, tmpframe2, ix00, jy00, kz00, ix0, jy0, kz0, margin_x_vec1, margin_x_vec2, tmpaverage, tmpvariance)
    !write(*,*) v_ssd, v_corr

    if(v_corr < 0) v_corr = 0
    if(v_ssd < totalgrids * fuzzy_sad_min) then
       v_ssd = 1
    else if(v_ssd < totalgrids * fuzzy_sad_max) then
       v_ssd = (fuzzy_sad_max - v_ssd / totalgrids) / (fuzzy_sad_max - fuzzy_sad_min)
    else
       v_ssd = 0
    endif

    fuzzy_vector = (v_ssd * w_ssd + v_corr * w_corr) / (w_ssd + w_corr)
    !write(*,*) v_ssd, v_corr, fuzzy_vector
  end function fuzzy_vector

  subroutine fuzzy_vector2(fxy, fz, sx, sy, sz, ix00, jy00, kz00, tmpframe1, tmpframe1_anom, corr2, tmpframe2, corrmap, max_corr, min_corr, max_corrxyz)
    integer, intent(in) :: fxy, fz, sx, sy, sz, ix00, jy00, kz00
    real(real_size), intent(in) :: tmpframe1(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe1_anom(fxy, fxy, fz)
    real(real_size), intent(in) :: tmpframe2((-sx):(sx + fxy - 1), &
         &                                   (-sy):(sy + fxy - 1), &
         &                                   (-sz):(sz + fz - 1))
    real(real_size), intent(inout) :: corrmap((-sx):sx, (-sy):sy, (-sz):sz)
    real(real_size), intent(inout) :: max_corr, min_corr
    integer, intent(inout) :: max_corrxyz(3)
    integer ix0, jy0, kz0, dd, ee, ff, ddd, eee, fff, d0, d1, e0, e1, f0, f1, totalgrids, tmpix, tmpjy, tmpkz
    real(real_size), parameter :: w_ssd = 1.0d0
    real(real_size), parameter :: w_corr = 1.0d0
    integer, parameter :: skip = 1
    real(real_size) corr, v_ssd, v_corr
    real(real_size) totalgrids_inv
    real(real_size) sub_ave, corr1, corr2, corr3, tmp1, tmp2

    totalgrids = fxy * fxy * fz
    totalgrids_inv = 1.0d0 / real(totalgrids)

    !correlation coefficient
    !ori_ave = sum(tmpframe1(:, :, :)) * totalgrids_inv
    sub_ave = sum(tmpframe2(0:(fxy - 1), 0:(fxy - 1), 0:(fz - 1))) * totalgrids_inv
    corr1 = 0
    !corr2 = 0
    corr3 = 0
    do ff = 1, fz
       do ee = 1, fxy
          do dd = 1, fxy
             !tmp1 = tmpframe1(dd, ee, ff) - ori_ave
             tmp1 = tmpframe1_anom(dd, ee, ff)
             tmp2 = tmpframe2(dd - 1, ee - 1, ff - 1) - sub_ave
             corr1 = corr1 + tmp1 * tmp2
             !corr2 = corr2 + tmp1 * tmp1
             corr3 = corr3 + tmp2 * tmp2
          end do !dd
       end do !ee
    end do !ff
    if(corr2 == 0 .or. corr3 == 0) then
       v_corr = 0
    else
       v_corr = corr1 / sqrt(corr2 * corr3) !correlation
    endif
    v_ssd = sum(abs(tmpframe1(1:fxy, 1:fxy, 1:fz) &
         & - tmpframe2(0:(fxy - 1), 0:(fxy - 1), 0:(fz - 1)))) !SAD

    if(v_corr < 0) v_corr = 0
    if(v_ssd < totalgrids * fuzzy_sad_min) then
       v_ssd = 1
    else if(v_ssd < totalgrids * fuzzy_sad_max) then
       v_ssd = (fuzzy_sad_max - v_ssd / totalgrids) / (fuzzy_sad_max - fuzzy_sad_min)
    else
       v_ssd = 0
    endif
    corr = (v_ssd * w_ssd + v_corr * w_corr) / (w_ssd + w_corr)

    !Acorr(ii, jj, kk, 2) = corr
    max_corrxyz = 0
    min_corr = corr
    max_corr = corr

    do ff = -sz, sz, skip
       do ee = -sy, sy, skip
          do dd = -sx, sx, skip
             sub_ave = sum(tmpframe2(dd:(dd + fxy - 1), ee:(ee + fxy - 1), ff:(ff + fz - 1))) * totalgrids_inv
             corr1 = 0
             !corr2 = 0
             corr3 = 0
             tmpkz = ff
             do fff = 1, fz
                tmpjy = ee
                do eee = 1, fxy
                   tmpix = dd
                   do ddd = 1, fxy
                      tmp1 = tmpframe1_anom(ddd, eee, fff)
                      tmp2 = tmpframe2(tmpix, tmpjy, tmpkz) - sub_ave
                      corr1 = corr1 + tmp1 * tmp2
                      !corr2 = corr2 + tmp1 * tmp1
                      corr3 = corr3 + tmp2 * tmp2
                      tmpix = tmpix + 1
                   end do !ddd
                   tmpjy = tmpjy + 1
                end do !eee
                tmpkz = tmpkz + 1
             end do !fff
             if(corr2 == 0 .or. corr3 == 0) then
                v_corr = 0
             else
                v_corr = corr1 / sqrt(corr2 * corr3) !correlation
             endif
             v_ssd = sum(abs(tmpframe1(1:fxy, 1:fxy, 1:fz) &
                  & - tmpframe2(dd:(dd + fxy - 1), ee:(ee + fxy - 1), ff:(ff + fz - 1)))) !SAD

             if(v_corr < 0) v_corr = 0
             if(v_ssd < totalgrids * fuzzy_sad_min) then
                v_ssd = 1
             else if(v_ssd < totalgrids * fuzzy_sad_max) then
                v_ssd = (fuzzy_sad_max - v_ssd / totalgrids) / (fuzzy_sad_max - fuzzy_sad_min)
             else
                v_ssd = 0
             endif
             corr = (v_ssd * w_ssd + v_corr * w_corr) / (w_ssd + w_corr)

             !quality_full(i, j, k, 1 + dd + sx, 1 + ee + sy, 1 + ff + sz) = corr
             corrmap(dd, ee, ff) = corr
             if(max_corr < corr) then
                max_corr = corr
                max_corrxyz = (/ dd, ee, ff /)
             else if(min_corr > corr) then
                min_corr = corr
             end if
          end do !!! dd
       end do !!! ee
    end do !!! ff
    d0 = max_corrxyz(1) - skip + 1
    d1 = max_corrxyz(1) + skip - 1
    e0 = max_corrxyz(2) - skip + 1
    e1 = max_corrxyz(2) + skip - 1
    f0 = max_corrxyz(3) - skip + 1
    f1 = max_corrxyz(3) + skip - 1
    do ff = f0, f1
       kz0 = kz00 + ff
       if(kz0 < 1 .or. kz0 + fz - 1 > dz) cycle
       !kz1 = kz0 + fz - 1
     
       do ee = e0, e1
          jy0 = jy00 + ee
          if(jy0 < 1 .or. jy0 + fxy - 1 > dy) cycle
          !jy1 = jy0 + fxy - 1
        
          do dd = d0, d1
             ix0 = ix00 + dd
             if(ix0 < 1 .or. ix0 + fxy - 1 > dx) cycle
             if(dd == max_corrxyz(1) .and. ee == max_corrxyz(2) .and. ff == max_corrxyz(3)) cycle
             !ix1 = ix0 + fxy - 1

             sub_ave = sum(tmpframe2(dd:(dd + fxy - 1), ee:(ee + fxy - 1), ff:(ff + fz - 1))) * totalgrids_inv
             corr1 = 0
             corr3 = 0
             tmpkz = ff
             do fff = 1, fz
                tmpjy = ee
                do eee = 1, fxy
                   tmpix = dd
                   do ddd = 1, fxy
                      tmp1 = tmpframe1_anom(ddd, eee, fff)
                      tmp2 = tmpframe2(tmpix, tmpjy, tmpkz) - sub_ave
                      corr1 = corr1 + tmp1 * tmp2
                      corr3 = corr3 + tmp2 * tmp2
                      tmpix = tmpix + 1
                   end do !ddd
                   tmpjy = tmpjy + 1
                end do !eee
                tmpkz = tmpkz + 1
             end do !fff
             if(corr2 == 0 .or. corr3 == 0) then
                v_corr = 0
             else
                v_corr = corr1 / sqrt(corr2 * corr3) !correlation
             endif
             v_ssd = sum(abs(tmpframe1(1:fxy, 1:fxy, 1:fz) &
                  & - tmpframe2(dd:(dd + fxy - 1), ee:(ee + fxy - 1), ff:(ff + fz - 1)))) !SAD                                                                                             
             if(v_corr < 0) v_corr = 0
             if(v_ssd < totalgrids * fuzzy_sad_min) then
                v_ssd = 1
             else if(v_ssd < totalgrids * fuzzy_sad_max) then
                v_ssd = (fuzzy_sad_max - v_ssd / totalgrids) / (fuzzy_sad_max - fuzzy_sad_min)
             else
                v_ssd = 0
             endif
             corr = (v_ssd * w_ssd + v_corr * w_corr) / (w_ssd + w_corr)

             corrmap(dd, ee, ff) = corr
             if(max_corr < corr) then
                max_corr = corr
                max_corrxyz = (/ dd, ee, ff /)
             else if(min_corr > corr) then
                min_corr = corr
             end if
          end do !!! dd
       end do !!! ee
    end do !!! ff
    !write(*,*) "fuzzy_vector2", max_corr, max_corrxyz(1), max_corrxyz(2), max_corrxyz(3)
  end subroutine fuzzy_vector2

  subroutine setup_match_by_sphere(fxy, fz, tmpframe1_mask, match_grid_num)
    integer, intent(in) :: fxy, fz
    logical, allocatable, intent(inout) :: tmpframe1_mask(:, :, :)
    integer, intent(out) :: match_grid_num
    real(real_size) :: rij0, rk0, tmpy, tmpz
    integer i, j, k

    if(match_by_sphere == 1) then
       allocate(tmpframe1_mask(fxy, fxy, fz))
       rij0 = real(1 + fxy) * 0.5
       rk0 = real(1 + fz) * 0.5
       match_grid_num = 0
!$omp parallel do private(i, j, k, tmpy, tmpz) collapse(2) reduction(+: match_grid_num)
       do k = 1, fz
          do j = 1, fxy
             tmpz = ((real(k) - rk0) ** 2) / (rk0 * rk0) !TO APPLY COLLAPSE(2)
             tmpy = (real(j) - rij0) ** 2
             do i = 1, fxy
                if(((real(i) - rij0) ** 2 + tmpy) / (rij0 * rij0) &
                     & + tmpz .le. 1) then
                   tmpframe1_mask(i, j, k) = .false.
                   match_grid_num = match_grid_num + 1
                else
                   tmpframe1_mask(i, j, k) = .true.
                end if
             end do !i
          end do !j
       end do !k
!$omp end parallel do
    else
       match_grid_num = fxy * fxy * fz
    end if
  end subroutine setup_match_by_sphere

  subroutine setup_search_within_sphere(sx, sy, sz, search_within_sphere_flag, all_count, search_mask)
    integer, intent(in) :: sx, sy, sz
    logical, intent(inout) :: search_within_sphere_flag
    integer, intent(out) :: all_count
    logical, intent(out) :: search_mask(-sx:sx, -sy:sy, -sz:sz)
    real search_dist_x, search_dist_y, search_dist_z
    integer i, j, k

    if(search_within_sphere == 1) then
       search_within_sphere_flag = .true.
       all_count = 0
       search_dist_x = 1.0d0 / real(sx)
       search_dist_y = 1.0d0 / real(sy)
       if(sz > 0) then
          search_dist_z = 1.0d0 / real(sz)
       else
          search_dist_z = 1.0d0
       end if
!$omp parallel do private(i, j, k) collapse(2) reduction(+: all_count)
       do k = -sz, sz
          do j = -sy, sy
             do i = -sx, sx
                if((real(i) * search_dist_x) ** 2 + (real(j) * search_dist_y) ** 2  + (real(k) * search_dist_z) ** 2 .le. 1.0) then
                   search_mask(i, j, k) = .false.
                   all_count = all_count + 1
                else
                   search_mask(i, j, k) = .true.
                end if
             end do !i
          end do !j
       end do !k
!$omp end parallel do
    else
       search_mask = .false.
       all_count = (sx * 2 + 1) * (sy * 2 + 1) * (sz * 2 + 1)
    end if
  end subroutine setup_search_within_sphere

  subroutine setup_vector_core_minval(fxy, fz, tmpframe1_coremask)
    integer, intent(in) :: fxy, fz
    logical, allocatable, intent(inout) :: tmpframe1_coremask(:, :, :)
    real(real_size) :: rij0, rk0, tmpy, tmpz
    double precision vector_core_radius_z
    integer i, j, k

    allocate(tmpframe1_coremask(fxy, fxy, fz))
    rij0 = real(1 + fxy) * 0.5
    rk0 = real(1 + fz) * 0.5
    vector_core_radius_z = rk0 / rij0 * vector_core_radius
!$omp parallel do private(i, j, k, tmpy, tmpz) collapse(2)
    do k = 1, fz
       do j = 1, fxy
          tmpz = ((real(k) - rk0) ** 2) / (vector_core_radius_z * vector_core_radius_z) !TO APPLY COLLAPSE(2)
          tmpy = (real(j) - rij0) ** 2
          do i = 1, fxy
             if(((real(i) - rij0) ** 2 + tmpy) / (vector_core_radius * vector_core_radius) + tmpz .le. 1) then
                tmpframe1_coremask(i, j, k) = .true.
             else
                tmpframe1_coremask(i, j, k) = .false.
             end if
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine setup_vector_core_minval

  subroutine vector_centroid(sx, sy, sz, max_corr, min_corr, max_corrxyz, corrmap, motion_sign, search_within_sphere_flag, search_mask, motion_out)
    integer, intent(in) :: sx, sy, sz
    real(real_size), intent(inout) ::  max_corr, min_corr
    integer, intent(inout) :: max_corrxyz(3)
    real(real_size), intent(in) :: corrmap((-sx):sx, (-sy):sy, (-sz):sz)
    integer, intent(in) :: motion_sign
    logical, intent(in) :: search_within_sphere_flag
    logical, intent(in) :: search_mask(-sx:sx, -sy:sy, -sz:sz)
    real(4), intent(inout) :: motion_out(3)

    integer dd, ee, ff
    integer xx0, xx1, yy0, yy1, zz0, zz1
    real(real_size) tmp_weight, weight, corr_threshold
    real tmpmotion(3), tmpidx(3)

    if(vector_centroid_global == 1) then
       xx0 = -sx
       xx1 = sx
       yy0 = -sy
       yy1 = sy
       zz0 = -sz
       zz1 = sz
    else
       if(max_corrxyz(1) > 0) then
          xx0 = 2 * max_corrxyz(1) - sx
          xx1 = sx
       else
          xx0 = -sx
          xx1 = 2 * max_corrxyz(1) + sx
       end if
       if(max_corrxyz(2) > 0) then
          yy0 = 2 * max_corrxyz(2) - sy
          yy1 = sy
       else
          yy0 = -sy
          yy1 = 2 * max_corrxyz(2) + sy
       end if
       if(max_corrxyz(3) > 0) then
          zz0 = 2 * max_corrxyz(3) - sz
          zz1 = sz
       else
          zz0 = -sz
          zz1 = 2 * max_corrxyz(3) + sz
       end if
    end if

    tmpmotion(1:3) = 0.0
    if(switching_vector_centroid == 1) then
       corr_threshold = min_corr + (max_corr - min_corr) * (1.0d0 - vector_centroid_variable_range)
    else
       corr_threshold = max_corr - vector_centroid_variable_range !FIXED RANGE
    end if
    if(switching_vector == 6 .or. switching_vector == 7) then
       !HORIZONTAL
       weight = 0
       ff = max_corrxyz(3)
       do ee = yy0, yy1
          tmpidx(2) = ee
          do dd = xx0, xx1
             if(search_within_sphere_flag .and. search_mask(dd, ee, ff)) cycle
             tmpidx(1) = dd
             if(corrmap(dd, ee, max_corrxyz(3)) .ge. corr_threshold) then
                tmp_weight = corrmap(dd, ee, max_corrxyz(3)) - corr_threshold
                tmpmotion(1:2) = tmpmotion(1:2) + tmp_weight * tmpidx(1:2)
                weight = weight + tmp_weight
             end if
          end do !dd
       end do !ee
       if(weight > 0) motion_out(1:2) = motion_sign * tmpmotion(1:2) / weight
       !VERTICAL
       weight = 0
       do ff = zz0, zz1
          if(search_within_sphere_flag .and. search_mask(max_corrxyz(1), max_corrxyz(2), ff)) cycle
          tmpidx(3) = ff
          if(corrmap(max_corrxyz(1), max_corrxyz(2), ff) .ge. corr_threshold) then
             tmp_weight = corrmap(max_corrxyz(1), max_corrxyz(2), ff) - corr_threshold
             tmpmotion(3) = tmpmotion(3) + tmp_weight * tmpidx(3)
             weight = weight + tmp_weight
          end if
       end do !ff
       if(weight > 0) motion_out(3) = motion_sign * tmpmotion(3) / weight
       max_corrxyz = motion_sign * nint(motion_out(:))
    else
       weight = 0
       do ff = zz0, zz1
          tmpidx(3) = ff
          do ee = yy0, yy1
             tmpidx(2) = ee
             do dd = xx0, xx1
                if(search_within_sphere_flag .and. search_mask(dd, ee, ff)) cycle
                tmpidx(1) = dd
                if(corrmap(dd, ee, ff) .ge. corr_threshold) then
                   tmp_weight = corrmap(dd, ee, ff) - corr_threshold
                   tmpmotion(1:3) = tmpmotion(1:3) + tmp_weight * tmpidx(1:3)
                   weight = weight + tmp_weight
                end if
             end do !dd
          end do !ee
       end do !ff
       if(weight > 0) motion_out(1:3) = motion_sign * tmpmotion(1:3) / weight
       max_corrxyz = motion_sign * nint(motion_out(:))
    end if
  end subroutine vector_centroid

  subroutine rain_growth_rate(i, j, k, max_corrxyz)
    integer, intent(in) :: i, j, k, max_corrxyz(3)
    double precision, parameter :: growth_min_rr = 20.0d0 !dbz
    integer i_g, j_g, k_g, i_g0, j_g0, k_g0

    if(vector_time_assignment == 0) then
       i_g = i + max_corrxyz(1)
       j_g = j + max_corrxyz(2)
       k_g = k + max_corrxyz(3)
       i_g0 = i
       j_g0 = j
       k_g0 = k
    else
       i_g = i
       j_g = j
       k_g = k
       i_g0 = i + max_corrxyz(1)
       j_g0 = j + max_corrxyz(2)
       k_g0 = k + max_corrxyz(3)
    end if
    if(expconv == 1) then !!! Z
       growth_t(i_g, j_g, k_g, 1, 1) = &
            & dble(10.0 ** (0.1 * frame_t(i_g, j_g, k_g, 2, 1)) &
            &    - 10.0 ** (0.1 * frame_t(i_g0, j_g0, k_g0, 1, 1))) / input_interval
    elseif(expconv == 2) then !!! mm/hr
       if(frame_t(i_g0, j_g0, k_g0, 1, 1) .gt. growth_min_rr .and. frame_t(i_g, j_g, k_g, 2, 1) .gt. growth_min_rr) then
          if(frame_t(i_g0, j_g0, k_g0, 1, 1) .gt. 100.0d0) then
             write(*, *) "growth: frame 1 exceeds limit at", i_g0, j_g0, k_g0, "val = ", frame_t(i_g0, j_g0, k_g0, 1, 1)
          elseif(frame_t(i_g, j_g, k_g, 2, 1) .gt. 100.0d0) then
             write(*, *) "growth: frame 2 exceeds limit at", i_g, j_g, k_g, "val = ", frame_t(i_g, j_g, k_g, 2, 1)
          else
             growth_t(i_g, j_g, k_g, 1, 1) = &
                  & dble(((10.0 ** (0.1 * frame_t(i_g, j_g, k_g, 2, 1))) / 200.0) ** (1.0 / 1.6) &
                  &    - ((10.0 ** (0.1 * frame_t(i_g0, j_g0, k_g0, 1, 1))) / 200.0) ** (1.0 / 1.6)) / input_interval
          end if
       end if
    else !!! dBZ
       !if(frame_t(i, j, k, 1, 1) .gt. growth_min_rr .and. frame_t(i + max_corrxyz(1), j + max_corrxyz(2), k + max_corrxyz(3), 2, 1) .gt. growth_min_rr) then
       growth_t(i_g, j_g, k_g, 1, 1) = &
            & dble(frame_t(i_g, j_g, k_g, 2, 1) &
            &    - frame_t(i_g0, j_g0, k_g0, 1, 1)) / input_interval * dt
       !if(expconv == 1) then
       !   growth_t(i + max_corrxyz(1), j + max_corrxyz(2), k + max_corrxyz(3), 1, 1) = &
       !        & (10.0d0 ** (growth_t(i + max_corrxyz(1), j + max_corrxyz(2), k + max_corrxyz(3), 1, 1) * 0.1d0))
       !elseif(expconv == 2) then
       !   growth_t(i + max_corrxyz(1), j + max_corrxyz(2), k + max_corrxyz(3), 1, 1) = &
       !        & (10.0d0 ** (growth_t(i + max_corrxyz(1), j + max_corrxyz(2), k + max_corrxyz(3), 1, 1) / 16.0d0))
       !end if
       !end if
    end if
  end subroutine rain_growth_rate

  subroutine vector_interpolate_skipped_grids(i_total, i_start, i_end, j_total, j_start, j_end, k)
    integer, intent(in) :: i_total, i_start, i_end, j_total, j_start, j_end, k
    integer i, j, ij, tmpmod

    !INTERPOLATION of SKIPPED GRIDS IN X
    if(x_skip > 1) then
!$omp parallel do private(ij, i, j, tmpmod)
       do ij = 0, i_total * j_total - 1
          i = mod(ij, i_total) + i_start
          j = ij / i_total + j_start
          if(mod(j - j_start, y_skip) .ne. 0) cycle
          tmpmod = mod(i - i_start, x_skip)
          if(tmpmod == 0) cycle
          if(i + x_skip > i_end) then
             if(motion_t(i - tmpmod, j, k, 1, 1) < motion_undef) &
                  & motion_t(i, j, k, 1, 1:3) = motion_t(i - tmpmod, j, k, 1, 1:3)
          else
             if(motion_t(i - tmpmod, j, k, 1, 1) < motion_undef) then
                if(motion_t(i + x_skip - tmpmod, j, k, 1, 1) < motion_undef) then
                   motion_t(i, j, k, 1, 1:3) = (motion_t(i - tmpmod, j, k, 1, 1:3) * (x_skip - tmpmod) &
                        &                       + motion_t(i + x_skip - tmpmod, j, k, 1, 1:3) * tmpmod) / dble(x_skip)
                else
                   motion_t(i, j, k, 1, 1:3) = motion_t(i - tmpmod, j, k, 1, 1:3)
                end if
             else if(motion_t(i + x_skip - tmpmod, j, k, 1, 1) < motion_undef) then
                motion_t(i, j, k, 1, 1:3) = motion_t(i + x_skip - tmpmod, j, k, 1, 1:3)
             end if
          end if
       end do
!$omp end parallel do
    end if

    !INTERPOLATION of SKIPPED GRIDS IN Y
    if(y_skip > 1) then
!$omp parallel do private(ij, i, j, tmpmod)
       do ij = 0, i_total * j_total - 1
          i = mod(ij, i_total) + i_start
          j = ij / i_total + j_start
          tmpmod = mod(j - j_start, y_skip)
          if(tmpmod == 0) cycle
          if(j + y_skip > j_end) then
             if(motion_t(i, j - tmpmod, k, 1, 1) < motion_undef) &
                  & motion_t(i, j, k, 1, 1:3) = motion_t(i, j - tmpmod, k, 1, 1:3)
          else
             if(motion_t(i, j - tmpmod, k, 1, 1) < motion_undef) then
                if(motion_t(i, j + y_skip - tmpmod, k, 1, 1) < motion_undef) then
                   motion_t(i, j, k, 1, 1:3) = (motion_t(i, j - tmpmod, k, 1, 1:3) * (y_skip - tmpmod) &
                        &                       + motion_t(i, j + y_skip - tmpmod, k, 1, 1:3) * tmpmod) / dble(y_skip)
                else
                   motion_t(i, j, k, 1, 1:3) = motion_t(i, j - tmpmod, k, 1, 1:3)
                end if
             else if(motion_t(i, j + y_skip - tmpmod, k, 1, 1) < motion_undef) then
                motion_t(i, j, k, 1, 1:3) = motion_t(i, j + y_skip - tmpmod, k, 1, 1:3)
             end if
          end if
       end do
!$omp end parallel do
    end if
  end subroutine vector_interpolate_skipped_grids

  subroutine vector_interpolate_all_missing_z(use_data_assimilation, iter, da_start_from, da_assimilate_filled_wind)
    integer, intent(in) :: use_data_assimilation, iter, da_start_from, da_assimilate_filled_wind
    integer k, k0, k1
    logical fill_zero
    if(use_data_assimilation > 0 .and. iter .ge. da_start_from .and. da_assimilate_filled_wind > 0) then
       fill_zero = .false.
    else
       fill_zero = .true.
    end if
!$omp parallel do private(k, k0, k1)
    do k = 1, dz
       if(num_valid_vect(k) == 0) then
          do k0 = k, 1, -1
             if(num_valid_vect(k0) > 0) exit
          end do !k0
          do k1 = k, dz
             if(num_valid_vect(k1) > 0) exit
          end do !k1
          if(k0 .ge. 1 .and. k1 .le. dz) then
             motion_t(1:dx, 1:dy, k, 1, 1:3) &
                  & = motion_t(1:dx, 1:dy, k0, 1, 1:3) * ((k1 - k) / (k1 - k0)) &
                  & + motion_t(1:dx, 1:dy, k1, 1, 1:3) * ((k - k0) / (k1 - k0))
          else if(k0 .ge. 1) then
             motion_t(1:dx, 1:dy, k, 1, 1:3) = motion_t(1:dx, 1:dy, k0, 1, 1:3)
          else if(k1 .le. dz) then
             motion_t(1:dx, 1:dy, k, 1, 1:3) = motion_t(1:dx, 1:dy, k1, 1, 1:3)
          else if(fill_zero) then
             motion_t(1:dx, 1:dy, k, 1, 1:3) = 0.0
          end if
       end if
    end do !k
!$omp end parallel do
  end subroutine vector_interpolate_all_missing_z
end module vector_core
