module growth
  use variables
  use nowcast_mpi_utils
  implicit none

  real(4), save, allocatable :: mpi_buf(:, :, :, :, :)

  public growth_rain, growth_rain_free_mpi_buf

contains

  subroutine growth_rain(j0, j1, motion)
    integer, intent(in) :: j0, j1
    real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)

    integer i, j, k, i_start, i_end, j_start2, j_end2, k_start, k_end
    integer jt1, jt2

    !INDEX CONVERTER
    jt1 = it2jt(it, 1) !it
    if(it == 2) then
       jt2 = 1 !INITIAL
    else
       jt2 = it2jt(it, 2) !it - 1
    end if

    i_start = 2 - margin_x_adv
    i_end = dx - 1 + margin_x_adv
    j_start2 = j_mpi(1, myrank)
    j_end2   = j_mpi(2, myrank)
    if(j_start2 < 2)    j_start2 = 2
    if(j_end2 > dy - 1) j_end2   = dy - 1
    k_start = 2
    k_end = dz - 1

    !1st order upwind scheme
    if(mpiprocs > 1) &
         & call exchange_real4_halo_by_mpi_sendrecv_wait( &
         & 1, dy, growth_t, i_start, i_end, j_start2, j_end2, k_start, k_end, jt2, 1, 1, mpi_buf, mpi_req_growth_halo)

    !boundary condition!

    !growthmax = maxval(growth_t(1:dx,j_start2:j_end2,1:dz,jt2,1))
    !write(114, *) it, myrank, growthmax

!$omp parallel
!$omp do private(k)
    do k = 1, dz
       !x
       if(myrank == 0) &
            & growth_t(1:dx, 1,  k, jt2, 1) = growth_t(1:dx, 2,      k, jt2, 1)
       if(myrank == mpiprocs - 1) &
            & growth_t(1:dx, dy, k, jt2, 1) = growth_t(1:dx, dy - 1, k, jt2, 1)
       !y
       growth_t(1,  j_start2:j_end2, k, jt2, 1) = growth_t(2,      j_start2:j_end2, k, jt2, 1)
       growth_t(dx, j_start2:j_end2, k, jt2, 1) = growth_t(dx - 1, j_start2:j_end2, k, jt2, 1)
    end do
!$omp end do

!$omp do private(j)
    do j = j_start2, j_end2
       !z
       growth_t(1:dx, j, 1,  jt2, 1) = growth_t(1:dx, j, 2,      jt2, 1)
       growth_t(1:dx, j, dz, jt2, 1) = growth_t(1:dx, j, dz - 1, jt2, 1)
    end do
!$omp end do

!$omp do private(i, j, k, adv_a, adv_b, adv_c, adv_d, adv_e, adv_f)
    do k = k_start, k_end
       do j = j_start2, j_end2
          do i = i_start, i_end
             if(motion(i, j, k, jt2, 1) > 0.0d0) then
                adv_a = 0  !1st upwind
                adv_b = -1 !1st upwind
             else
                adv_a = 1 !1st upwind
                adv_b = 0 !1st upwind
             end if
             if(motion(i, j, k, jt2, 2) > 0.0d0) then
                adv_c = 0  !1st upwind
                adv_d = -1 !1st upwind
             else
                adv_c = 1 !1st upwind
                adv_d = 0 !1st upwind
             end if
             if(motion(i, j, k, jt2, 3) > 0.0d0) then
                adv_e = 0  !1st upwind
                adv_f = -1 !1st upwind
             else
                adv_e = 1 !1st upwind
                adv_f = 0 !1st upwind
             end if

             !adams
             adams_t(i, j, k, i_adams(1), 5) = -dt * ( &
                  &   motion(i, j, k, jt2, 1) * inv_gridx * (growth_t(i + adv_a, j, k, jt2, 1) - growth_t(i + adv_b, j, k, jt2, 1)) &
                  & + motion(i, j, k, jt2, 2) * inv_gridy * (growth_t(i, j + adv_c, k, jt2, 1) - growth_t(i, j + adv_d, k, jt2, 1)) & 
                  & + motion(i, j, k, jt2, 3) * inv_gridz * (growth_t(i, j, k + adv_e, jt2, 1) - growth_t(i, j, k + adv_f, jt2, 1)))
          end do
       end do

       if(it < 3 .or. mod(it, 10) .eq. 0) then
          growth_t(i_start:i_end, j_start2:j_end2, k, jt1, 1) = growth_t(i_start:i_end, j_start2:j_end2, k, jt2, 1) + adams_t(i_start:i_end, j_start2:j_end2, k, i_adams(1), 5)
       else
          if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
             growth_t(i_start:i_end, j_start2:j_end2, k, jt1, 1) = growth_t(i_start:i_end, j_start2:j_end2, k, jt2, 1) &
                  & + 23.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start2:j_end2, k, i_adams(1), 5) &
                  & - 16.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start2:j_end2, k, i_adams(2), 5) &
                  & +  5.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start2:j_end2, k, i_adams(3), 5)
          elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
             growth_t(i_start:i_end, j_start2:j_end2, k, jt1, 1) = growth_t(i_start:i_end, j_start2:j_end2, k, jt2, 1) &
                  & + 0.5d0 * (3.0d0 * adams_t(i_start:i_end, j_start2:j_end2, k, i_adams(1), 5) - adams_t(i_start:i_end, j_start2:j_end2, k, i_adams(2), 5))
          end if
       end if
    end do
!$omp end do
!$omp end parallel

    if(mpiprocs > 1) &
         & call exchange_real4_halo_by_mpi_sendrecv_start( &
         & 1, dy, growth_t, i_start, i_end, j_start2, j_end2, k_start, k_end, jt1, 1, 1, mpi_buf, mpi_req_growth_halo)
  end subroutine  growth_rain

  subroutine growth_rain_free_mpi_buf
    if(allocated(mpi_buf)) deallocate(mpi_buf)
  end subroutine growth_rain_free_mpi_buf
end module growth
