module module_cotrec
  use variables
  use bcg2
  implicit none

  private

  real(4), allocatable :: motion_t_cotrec(:, :, :, :, :)
  integer totalgrids, idxoffset0, idxoffset1, idxoffset2

  public cotrec2D, cotrec2D_2d_cyclic_x

contains

  subroutine cotrec2D
    integer kk
    idxoffset0 = frame_num_2 + search_num
    idxoffset1 = frame_num_2a + search_num
    idxoffset2 = frame_num + search_num * 2 - 1
    totalgrids = (dx - idxoffset2 + 2) * (dy - idxoffset2 + 2)

    allocate(motion_t_cotrec((1 - margin_x):(dx + margin_x), dy, dz, 1, 2))
    if(dz > 1) then
!$omp parallel do schedule(dynamic) private(kk)
       do kk = 1, dz
          call cotrec2D_core(kk)
       end do !k
!$omp end parallel do
    else
       call cotrec2D_core(1)
    end if
    deallocate(motion_t_cotrec)
  end subroutine cotrec2D

  subroutine cotrec2D_core(k)
    !READ ME**********************************************************
    !This subroutine convert TREC data to COTREC one.
    !Input matrix ... [motion_t] calculated by vector.f90
    !Output matrix ... [motion_t_cotrec]
    !All methods follow Li et al.(1995)
    !Almost all variables also follow it.(ex: upsolon0, lambda ...)
    !*****************************************************************
  
    integer, intent(in) :: k
    double precision, allocatable :: tmplambda(:, :), tmpupsilon0_x_2(:, :)
    double precision, allocatable :: lambda(:, :, :, :), upsilon0_x_2(:, :, :, :)
    real(4), allocatable :: error(:, :, :)
    integer i, j, counter, errorsum, info, mxmv
    double precision tol, dtmplambda

    if(myrank == 0) write(*, '("cotrec2D_core, k = ", I3)') k
    allocate(tmplambda(0:(dx - idxoffset2 + 1), 0:(dy - idxoffset2 + 1)), &
         &   tmpupsilon0_x_2(0:(dx - idxoffset2 + 1), 0:(dy - idxoffset2 + 1)))
    motion_t_cotrec(:, 1:dy, k, 1, 1:2) = motion_t(:, 1:dy, k, 1, 1:2)
    tmplambda(:, :) = 0.0d0

    !Calcurate upsilon0 (ref: Li et al. 1995)*************************
    tmpupsilon0_x_2(:, :) = 0
    do j = 1 + idxoffset0 + 1, dy - idxoffset1 - 1
       do i = 1 + idxoffset0 + 1, dx - idxoffset1 - 1
          tmpupsilon0_x_2(i - idxoffset0, j - idxoffset0) &
               & = (motion_t(i + 1, j, k, 1, 1) - motion_t(i - 1, j, k, 1, 1)) & !Li et al. (1995) has a typo here
               & + (motion_t(i, j + 1, k, 1, 2) - motion_t(i, j - 1, k, 1, 2))   !Li et al. (1995) has a typo here
       end do !i
    end do !j
    i = 1 + idxoffset0
    do j = 1 + idxoffset0 + 1, dy - idxoffset1 - 1
       tmpupsilon0_x_2(i - idxoffset0, j - idxoffset0) &
            & = (motion_t(i, j + 1, k, 1, 2) - motion_t(i, j - 1, k, 1, 2))   !Li et al. (1995) has a typo here
    end do !j
    i = dx - idxoffset1
    do j = 1 + idxoffset0 + 1, dy - idxoffset1 - 1
       tmpupsilon0_x_2(i - idxoffset0, j - idxoffset0) &
            & = (motion_t(i, j + 1, k, 1, 2) - motion_t(i, j - 1, k, 1, 2))   !Li et al. (1995) has a typo here
    end do !j
    j = 1 + idxoffset0
    do i = 1 + idxoffset0 + 1, dx - idxoffset1 - 1
       tmpupsilon0_x_2(i - idxoffset0, j - idxoffset0) &
            & = (motion_t(i + 1, j, k, 1, 1) - motion_t(i - 1, j, k, 1, 1))   !Li et al. (1995) has a typo here
    end do !i
    j = dy - idxoffset1
    do i = 1 + idxoffset0, dx - idxoffset1
       tmpupsilon0_x_2(i - idxoffset0, j - idxoffset0) &
            & = (motion_t(i + 1, j, k, 1, 1) - motion_t(i - 1, j, k, 1, 1))   !Li et al. (1995) has a typo here
    end do !i
    !*****************************************************************
     
    mxmv = countermax !mxmv is cleared by openmp
    tol = threshold   !tol is cleared by openmp
    call bicgstab_new(totalgrids, tmplambda, tmpupsilon0_x_2, matvec, tol, mxmv, info)
    if(myrank == 0) write(*, '("myrank:", I5.5, ", k:", I3.3, ", info:", I3.3, ", mxmv:", I5.5, ", tol:", F12.5)') myrank, k, info, mxmv, tol

    if(info .ne. 0) then !fallback to SOR
       !initialize*******************************************************
       allocate(lambda(dx, dy, 1, 2))
       allocate(upsilon0_x_2(dx, dy, 1, 1))
       allocate(error(dx, dy, 1))
       error(:,:,:)=9999.9d0
       lambda(:,:,:,1)=0.0d0!Lagrangian multiplier (ref: Li et al. 1995)
       !*****************************************************************
       write(*, *) "There is something wrong in BiCGSTAB at z =", k, "; use SOR"
       counter = 1
       do while (errorsum < totalgrids)
          errorsum = 0
          do j = 1 + idxoffset0, dy - idxoffset1
             do i = 1 + idxoffset0, dx - idxoffset1
                dtmplambda = (tmpupsilon0_x_2(i - idxoffset0, j - idxoffset0) &
                     &        + lambda(i + 1, j, 1, 1) + lambda(i - 1, j, 1, 1) &
                     &        + lambda(i, j + 1, 1, 1) + lambda(i, j - 1, 1, 1)) * 0.25d0 &
                     &     - lambda(i, j, 1, 1)
                if(abs(dtmplambda) < threshold) errorsum = errorsum + 1
                lambda(i, j, 1, 1) = lambda(i, j, 1, 1) + sor_accel * dtmplambda !SOR
             end do !i
          end do !j
          counter = counter + 1
          if(counter > countermax) then
             write(*, '("k: ", I4, " max iteration reached")') k
             exit
          end if
       end do !while
       write(*, *) "counter:", counter, "errorsum:", errorsum
       deallocate(lambda, upsilon0_x_2, error)
    end if

    !Update the motion_t_cotrec(output)******************************
    do j = 1 + idxoffset0, dy - idxoffset1
       do i = 1 + idxoffset0, dx - idxoffset1
          motion_t_cotrec(i, j, k, 1, 1) &
               & = (motion_t(i + 1, j, k, 1, 1) + 2.0d0 * motion_t(i, j, k, 1, 1) + motion_t(i - 1, j, k, 1, 1) &
               &    + tmplambda(i + 1 - idxoffset0, j - idxoffset0) &
               &    - tmplambda(i - 1 - idxoffset0, j - idxoffset0)) * 0.25d0
          motion_t_cotrec(i, j, k, 1, 2) &
               & = (motion_t(i, j + 1, k, 1, 2) + 2.0d0 * motion_t(i, j, k, 1, 2) + motion_t(i, j - 1, k, 1, 2) &
               &    + tmplambda(i - idxoffset0, j + 1 - idxoffset0) &
               &    - tmplambda(i - idxoffset0, j - 1 - idxoffset0)) * 0.25d0
       end do !i
    end do !j
    motion_t(:, 1:dy, k, 1, 1:2) = motion_t_cotrec(:, 1:dy, k, 1, 1:2)
    deallocate(tmplambda, tmpupsilon0_x_2)
  end subroutine cotrec2D_core

  subroutine matvec(nn, x, y)
    integer, intent(in) :: nn
    double precision, intent(in) :: x(1:nn)
    double precision, intent(inout) :: y(1:nn)
    integer ii, jj, nnx, nny
    integer(8) index

    nnx = dx - (frame_num + search_num * 2 - 1) + 2
    nny = dy - (frame_num + search_num * 2 - 1) + 2

    do jj = 2, nny - 1
       do ii = 2, nnx - 1
          index = ii + (jj - 1) * nnx
          y(index) = x(index) * 4.0d0 &
               & - x(index + 1)   - x(index - 1) &
               & - x(index + nnx) - x(index - nnx)
       end do !ii
    end do !jj

    ii = 1
    do jj = 2, nny - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - x(index + 1)   - 0.0d0 &
            & - x(index + nnx) - x(index - nnx)
    end do !jj

    ii = nnx
    do jj = 2, nny - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - 0.0d0          - x(index - 1) &
            & - x(index + nnx) - x(index - nnx)
    end do !ii

    jj = 1
    do ii = 2, nnx - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - x(index + 1)   - x(index - 1) &
            & - x(index + nnx) - 0.0d0
    end do !ii

    jj = nny
    do ii = 2, nnx - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - x(index + 1)   - x(index - 1) &
            & - 0.0d0          - x(index - nnx)
    end do !ii
  end subroutine matvec

  subroutine cotrec2D_2d_cyclic_x
    double precision, allocatable :: tmplambda(:, :), tmpupsilon0_x_2(:, :)
    double precision, allocatable :: lambda(:, :, :, :)
    real(4), allocatable :: error(:, :, :)
    integer i, j, counter, errorsum, info, mxmv
    double precision tol, dtmplambda

    !initialize*******************************************************
    error(:,:,:)=9999.9d0
    !motion_t_cotrec(:,:,:,1,1)=0.0d0!Final output of this subroutine
    !motion_t_cotrec(:,:,:,1,2)=0.0d0!Final output of this subroutine
    !motion_t_cotrec(:,:,:,1,3)=0.0d0!Final output of this subroutine
    lambda(:,:,:,1)=0.0d0!Lagrangian multiplier (ref: Li et al. 1995)
    !*****************************************************************

    idxoffset0 = frame_num_2 + search_num
    idxoffset1 = frame_num_2a + search_num
    idxoffset2 = frame_num + search_num * 2 - 1
    totalgrids = dx * (dy - idxoffset2 + 2)

    allocate(motion_t_cotrec((1 - margin_x):(dx + margin_x), dy, dz, 1, 2))
    motion_t_cotrec(:, 1:dy, 1:dz, 1, 1:2) = motion_t(:, 1:dy, 1:dz, 1, 1:2)

! parallel do schedule(dynamic) private(i, j, k, tol, mxmv, info, tmplambda, tmpupsilon0_x_2, errorsum, counter, dtmplambda)
    allocate(tmplambda(1:dx, 0:(dy - idxoffset2 + 1)), &
         &   tmpupsilon0_x_2(1:dx, 0:(dy - idxoffset2 + 1)))
    tmplambda(:, :) = 0.0d0
    !Calcurate upsilon0 (ref: Li et al. 1995)*************************
    tmpupsilon0_x_2(:, :) = 0
    do j = 1 + idxoffset0 + 1, dy - idxoffset1 - 1
       do i = 1, dx
          tmpupsilon0_x_2(i, j - idxoffset0) &
               & = (motion_t(i + 1, j, 1, 1, 1) - motion_t(i - 1, j, 1, 1, 1)) & !Li et al. (1995) has a typo here
               & + (motion_t(i, j + 1, 1, 1, 2) - motion_t(i, j - 1, 1, 1, 2))   !Li et al. (1995) has a typo here
       end do !i
    end do !j
    j = 1 + idxoffset0
    do i = 1, dx
       tmpupsilon0_x_2(i, j - idxoffset0) &
            & = (motion_t(i + 1, j, 1, 1, 1) - motion_t(i - 1, j, 1, 1, 1))   !Li et al. (1995) has a typo here
    end do !i
    j = dy - idxoffset1
    do i = 1, dx
       tmpupsilon0_x_2(i, j - idxoffset0) &
            & = (motion_t(i + 1, j, 1, 1, 1) - motion_t(i - 1, j, 1, 1, 1))   !Li et al. (1995) has a typo here
    end do !i
    !*****************************************************************
     
    !Solve lambda by Gauss-Seidel method*****************************
    !Meaning of variables
    ![error]...Difference between updated lambda and previous lambda.
    ![threshold]...Thereshold of error.
    ![errorsum]...The number of NON-error grids.
    ![counter]...How many times loops did

    mxmv = countermax !mxmv is cleared by openmp
    tol = threshold   !tol is cleared by openmp
    !subroutine bicgstab_new(n, x, rhs, matvec, tol, mxmv, info)
    call bicgstab_new(totalgrids, tmplambda, tmpupsilon0_x_2, matvec_cyclic_x, tol, mxmv, info)
    write(*, '("k:", I3.3, ", info:", I3.3, ", mxmv:", I5.5, ", tol:", F12.5)') 1, info, mxmv, tol
    !write(*, *) maxval(tmplambda)

    if(info .ne. 0) then !fallback to SOR
       write(*, *) "There is something wrong in BiCGSTAB at z =", 1, "; use SOR"
       counter = 1
       do while (errorsum < totalgrids)
          errorsum = 0
          do j = 1 + idxoffset0, dy - idxoffset1
             do i = 1 + idxoffset0, dx - idxoffset1
                dtmplambda = (tmpupsilon0_x_2(i, j - idxoffset0) &
                     &        + lambda(i + 1, j, 1, 1) + lambda(i - 1, j, 1, 1) &
                     &        + lambda(i, j + 1, 1, 1) + lambda(i, j - 1, 1, 1)) * 0.25d0 &
                     &     - lambda(i, j, 1, 1)
                if(abs(dtmplambda) < threshold) errorsum = errorsum + 1
                lambda(i, j, 1, 1) = lambda(i, j, 1, 1) + sor_accel * dtmplambda !SOR
             end do !i
          end do !j
          counter = counter + 1
          if(counter > countermax) then
             write(*, '("k: ", I4, " max iteration reached")') 1
             exit
          end if
       end do !while
       write(*, *) "counter:", counter, "errorsum:", errorsum
    end if

    !Update the motion_t_cotrec(output)******************************
    do j = 1 + idxoffset0, dy - idxoffset1
       motion_t_cotrec(1, j, 1, 1, 1) &
            & = (motion_t(1 + 1, j, 1, 1, 1) + 2.0d0 * motion_t(1, j, 1, 1, 1) + motion_t(1 - 1, j, 1, 1, 1) &
            &    + tmplambda(1 + 1, j - idxoffset0) &
            &    - tmplambda(dx,    j - idxoffset0)) * 0.25d0
       motion_t_cotrec(i, j, 1, 1, 2) &
            & = (motion_t(i, j + 1, 1, 1, 2) + 2.0d0 * motion_t(i, j, 1, 1, 2) + motion_t(i, j - 1, 1, 1, 2) &
            &    + tmplambda(1, j + 1 - idxoffset0) &
            &    - tmplambda(1, j - 1 - idxoffset0)) * 0.25d0
       do i = 2, dx - 1
          motion_t_cotrec(i, j, 1, 1, 1) &
               & = (motion_t(i + 1, j, 1, 1, 1) + 2.0d0 * motion_t(i, j, 1, 1, 1) + motion_t(i - 1, j, 1, 1, 1) &
               &    + tmplambda(i + 1, j - idxoffset0) &
               &    - tmplambda(i - 1, j - idxoffset0)) * 0.25d0
          motion_t_cotrec(i, j, 1, 1, 2) &
               & = (motion_t(i, j + 1, 1, 1, 2) + 2.0d0 * motion_t(i, j, 1, 1, 2) + motion_t(i, j - 1, 1, 1, 2) &
               &    + tmplambda(i, j + 1 - idxoffset0) &
               &    - tmplambda(i, j - 1 - idxoffset0)) * 0.25d0
       end do !i
       motion_t_cotrec(dx, j, 1, 1, 1) &
            & = (motion_t(dx + 1, j, 1, 1, 1) + 2.0d0 * motion_t(dx, j, 1, 1, 1) + motion_t(dx - 1, j, 1, 1, 1) &
            &    + tmplambda(1,      j - idxoffset0) &
            &    - tmplambda(dx - 1, j - idxoffset0)) * 0.25d0
       motion_t_cotrec(dx, j, 1, 1, 2) &
            & = (motion_t(dx, j + 1, 1, 1, 2) + 2.0d0 * motion_t(dx, j, 1, 1, 2) + motion_t(dx, j - 1, 1, 1, 2) &
            &    + tmplambda(dx, j + 1 - idxoffset0) &
            &    - tmplambda(dx, j - 1 - idxoffset0)) * 0.25d0
    end do !j
    motion_t(:, 1:dy, 1:dz, 1, 1:2) = motion_t_cotrec(:, 1:dy, 1:dz, 1, 1:2)
    deallocate(tmplambda, tmpupsilon0_x_2, motion_t_cotrec)
  end subroutine cotrec2D_2d_cyclic_x

  subroutine matvec_cyclic_x(nn, x, y)
    integer, intent(in) :: nn
    double precision, intent(in) :: x(1:nn)
    double precision, intent(inout) :: y(1:nn)
    integer ii, jj, nnx, nny
    integer(8) index

    nnx = dx
    nny = dy - (frame_num + search_num * 2 - 1) + 2

!$omp parallel
!$omp do private(ii, jj, index)
    do jj = 2, nny - 1
       do ii = 2, nnx - 1
          index = ii + (jj - 1) * nnx
          y(index) = x(index) * 4.0d0 &
               & - x(index + 1)   - x(index - 1) &
               & - x(index + nnx) - x(index - nnx)
       end do !ii
    end do !jj
!$omp end do

    ii = 1
!$omp do private(jj, index)
    do jj = 2, nny - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - x(index + 1)   - x(index + nnx - 1) &
            & - x(index + nnx) - x(index - nnx)
    end do !jj
!$omp end do

    ii = nnx
!$omp do private(jj, index)
    do jj = 2, nny - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - x(index - nnx + 1) - x(index - 1) &
            & - x(index + nnx)     - x(index - nnx)
    end do !jj
!$omp end do

    jj = 1
!$omp do private(ii, index)
    do ii = 2, nnx - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - x(index + 1)   - x(index - 1) !&
       !& - x(index + nnx) - 0.0d0
    end do !ii
!$omp end do

    jj = nny
!$omp do private(ii, index)
    do ii = 2, nnx - 1
       index = ii + (jj - 1) * nnx
       y(index) = x(index) * 4.0d0 &
            & - x(index + 1)   - x(index - 1) !&
       !& - 0.0d0          - x(index - nnx)
    end do !ii
!$omp end do
!$omp end parallel

    ii = 1
    jj = 1
    index = ii + (jj - 1) * nnx
    y(index) = x(index) * 4.0d0 &
         & - x(index + 1)   - x(index + nnx - 1) !&
         !& - x(index + nnx) - 0.0d0

    ii = 1
    jj = nny
    index = ii + (jj - 1) * nnx
    y(index) = x(index) * 4.0d0 &
         & - x(index + 1)   - x(index + nnx - 1) !&
         !& - 0.0d0          - x(index - nnx)

    ii = nnx
    jj = 1
    index = ii + (jj - 1) * nnx
    y(index) = x(index) * 4.0d0 &
         & - x(index - nnx + 1) - x(index - 1) !&
         !& - x(index + nnx) - 0.0d0

    ii = nnx
    jj = nny
    index = ii + (jj - 1) * nnx
    y(index) = x(index) * 4.0d0 &
         & - x(index - nnx + 1) - x(index - 1) !&
         !& - 0.0d0          - x(index - nnx)
  end subroutine matvec_cyclic_x
end module module_cotrec
