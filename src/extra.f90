module extra
  use variables
  implicit none

contains

  subroutine extrap_motion_at_boundaries
    integer i, j, k
    integer k_limb1, k_limb2

    k_limb1 = frame_numz_2 + search_numz
    k_limb2 = frame_numz_2a + search_numz

    !TOP
!$omp parallel
!$omp do private(i, j, k) collapse(2)
    do k = dz - k_limb2 + 1, dz
       do j = 1, dy
          do i = 1, dx
             motion_t(i, j, k, 1, 1) = motion_t(i, j, dz - k_limb2, 1, 1)
             motion_t(i, j, k, 1, 2) = motion_t(i, j, dz - k_limb2, 1, 2)
             if(motion_t(i, j, dz - k_limb2, 1, 3) < 0.0d0) then  !SUPPRESS DOWNDRAFT AT THE TOP
                motion_t(i, j, k, 1, 3) = motion_t(i, j, dz - k_limb2, 1, 3) * (dz - k) / k_limb2 !!! LINEAR DAMPING
             else
                motion_t(i, j, k, 1, 3) = motion_t(i, j, dz - k_limb2, 1, 3)
             end if
          end do !i
       end do !j
    end do !k
!$omp end do

    !BOTTOM
!$omp do private(i, j, k) collapse(2)
    do k = 1, k_limb1
       do j = 1, dy
          do i = 1, dx
             motion_t(i, j, k, 1, 1) = motion_t(i, j, k_limb1 + 1, 1, 1)
             motion_t(i, j, k, 1, 2) = motion_t(i, j, k_limb1 + 1, 1, 2)
             if(motion_t(i, j, k_limb1 + 1, 1, 3) > 0.0d0) then !SUPPRESS UPDRAFT AT THE BOTTOM
                motion_t(i, j, k, 1, 3) = motion_t(i, j, k_limb1 + 1, 1, 3) * (k - 1) / k_limb1 !!! LINEAR DAMPING
             else
                motion_t(i, j, k, 1, 3) = motion_t(i, j, k_limb1 + 1, 1, 3)
             end if
          end do !i
       end do !j
    end do !k
!$omp end do
!$omp end parallel
  end subroutine extrap_motion_at_boundaries
end module extra
