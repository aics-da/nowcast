subroutine cotrec
  use variables
  implicit none
  real(4), allocatable :: motion_t_cotrec(:, :, :, :, :)
  double precision, allocatable, dimension(:, :, :, :) :: lambda
  double precision, allocatable, dimension(:, :, :, :) :: upsilon0_x_2
  real(4),allocatable,dimension(:,:,:)::error
  integer errorsum
  integer i, j, k, counter
  
  !READ ME**********************************************************
  !This subroutine convert TREC data to COTREC one.
  !Input matrix ... [motion_t] calculated by vector.f90
  !Output matrix ... [motion_t_cotrec]
  !All methods follow Li et al.(1994)
  !Almost all variables also follow it.(ex: upsolon0, lambda ...)
  !*****************************************************************
  
  !initialize*******************************************************
  allocate(motion_t_cotrec((1 - margin_x):(dx + margin_x), dy, dz, 1, 3))
  allocate(lambda(dx, dy, 1, 2))
  allocate(upsilon0_x_2(dx, dy, 1, 1))
  allocate(error(dx, dy, 1))
  motion_t_cotrec(:, 1:dy, k, 1, 1:3) = motion_t(:, 1:dy, k, 1, 1:3)
  error(:,:,:)=9999.9d0
  !motion_t_cotrec(:,:,:,1,1)=0.0d0!Final output of this subroutine
  !motion_t_cotrec(:,:,:,1,2)=0.0d0!Final output of this subroutine
  !motion_t_cotrec(:,:,:,1,3)=0.0d0!Final output of this subroutine
  lambda(:,:,:,1)=0.0d0!Lagrangian multiplier (ref: Li et al. 1994)
  !*****************************************************************
  
  !Calcurate upsilon0 (ref: Li et al. 1994)*************************
  do k=(frame_num/2)+search_num+1,dz-(frame_num/2)-search_num
     do j=(frame_num/2)+search_num+1,dy-(frame_num/2)-search_num
        do i=(frame_num/2)+search_num+1,dx-(frame_num/2)-search_num
           upsilon0_x_2(i,j,k,1) = (motion_t(i+1,j,k,1,1)-motion_t(i-1,j,k,1,1))/2.0d0+ &
                &                  (motion_t(i,j+1,k,1,2)-motion_t(i,j-1,k,1,2))/2.0d0+ &
                &                  (motion_t(i,j,k+1,1,3)-motion_t(i,j,k-1,1,3))
        enddo
     enddo
  enddo
  !*****************************************************************
  
  !put original value of TREC on output matrix.(uploaded later)****
  !do k=1,dz
  !	do j=1,dy
  !		do i=1,dx
  !			motion_t_cotrec(i,j,k,1,1)=motion_t(i,j,k,1,1)
  !			motion_t_cotrec(i,j,k,1,2)=motion_t(i,j,k,1,2)
  !		enddo
  !	enddo
  !enddo
  !****************************************************************
  
  !Solve lambda by Gauss-Seidel method*****************************
  !Meaning of variables
  ![error]...Difference between updated lambda and previous lambda.
  ![threshold]...Thereshold of error.
  ![errorsum]...The number of NON-error grids.
  ![counter]...How many times loops did
  errorsum=0
  counter=1
  do while (errorsum<(dz-(frame_num/2)-search_num-((frame_num/2)+search_num+1)+1) * &
       &             (dy-(frame_num/2)-search_num-((frame_num/2)+search_num+1)+1) * &
       &             (dx-(frame_num/2)-search_num-((frame_num/2)+search_num+1)+1))
     errorsum=0
     do k=(frame_num/2)+search_num+1,dz-(frame_num/2)-search_num
        do j=(frame_num/2)+search_num+1,dy-(frame_num/2)-search_num
           do i=(frame_num/2)+search_num+1,dx-(frame_num/2)-search_num
              lambda(i,j,k,2) = (upsilon0_x_2(i,j,k,1) + &
                   &             lambda(i+1,j,k,1) + lambda(i-1,j,k,1) + &
                   &             lambda(i,j+1,k,1) + lambda(i,j-1,k,1) + &
                   &             lambda(i,j,k+1,1) + lambda(i,j,k-1,1))/6.0d0
              error(i,j,k)=abs(lambda(i,j,k,2)-lambda(i,j,k,1))
              if ( error(i,j,k) < threshold  ) then
                 errorsum=errorsum+1
              endif
              lambda(i,j,k,1)=lambda(i,j,k,2)
           end do
        end do
     end do
     counter=counter+1
     write(*,*) "counter..."
     write(*,*) counter
     write(*,*) "errorsum..."
     write(*,*) errorsum
  end do
  !****************************************************************
  
  !Update the motion_t_cotrec(output)******************************
  do k=(frame_num/2)+search_num+1,dz-(frame_num/2)-search_num
     do j=(frame_num/2)+search_num+1,dy-(frame_num/2)-search_num
        do i=(frame_num/2)+search_num+1,dx-(frame_num/2)-search_num
           motion_t_cotrec(i,j,k,1,1)=(motion_t(i+1,j,k,1,1)+2.0d0*motion_t(i,j,k,1,1)+motion_t(i-1,j,k,1,1))/4+&
                &                     (lambda(i+1,j,k,1)-lambda(i-1,j,k,1))/4.0d0
           motion_t_cotrec(i,j,k,1,2)=(motion_t(i,j+1,k,1,2)+2.0d0*motion_t(i,j,k,1,2)+motion_t(i,j-1,k,1,2))/4+&
                &                     (lambda(i,j+1,k,1)-lambda(i,j-1,k,1))/4.0d0
           motion_t_cotrec(i,j,k,1,3)=(motion_t(i,j,k+1,1,3)+2.0d0*motion_t(i,j,k,1,3)+motion_t(i,j,k-1,1,3))/4+&
                &                     (lambda(i,j,k+1,1)-lambda(i,j,k-1,1))/4.0d0
        enddo
     enddo
  enddo
  motion_t(:, 1:dy, k, 1, 1:3) = motion_t_cotrec(:, 1:dy, k, 1, 1:3)
  
  !output
  open(17,file='vector_u_cotrec.bin', form='unformatted')
  do k=1,dz
     do j=1,dy
        do i=1,dx
           write(17) real(motion_t_cotrec(i,j,k,1,1))
        end do
     end do
  end do
  close(17)
  
  open(18,file='vector_v_cotrec.bin', form='unformatted')
  do k=1,dz
     do j=1,dy
        do i=1,dx
           write(18) real(motion_t_cotrec(i,j,k,1,2))
        end do
     end do
  end do
  close(18)
  
  open(19,file='vector_w_cotrec.bin', form='unformatted')
  do k=1,dz
     do j=1,dy
        do i=1,dx
           write(19) real(motion_t_cotrec(i,j,k,1,3))
        end do
     end do
  end do
  close(19)
  
  deallocate(motion_t_cotrec)
  
end subroutine cotrec

