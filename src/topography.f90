subroutine topography
  use variables
  implicit none
  integer i, j, k
  integer(4), allocatable :: topo_mask(:, :)
  logical file_exist

  !READ ME**********************************************************
  !This subroutine make matrix of topography data.
  !CALL BEFORE DATA CONVERSION OF frame_t
  !*****************************************************************

  if(read_topo_mask .ge. 1) then
     allocate(topo_mask(ddx, ddy))
     inquire(file = 'topo_mask.dat', exist = file_exist)
     if(file_exist) then
        if(myrank == 0) write(*, *) "reading topo_mask.dat"
        open(13, file = 'topo_mask.dat', access = 'stream', form = 'unformatted', convert = "little_endian")
        read(13) topo_mask
        close(13)
     else
        if(myrank == 0) write(*, *) "topo_mask.dat not found"
!$omp parallel do private(i, j)
        do j = 1, dy
           do i = 1, dx
              topo_mask(i, j) = ddz
           end do !i
        end do !j
!$omp end parallel do
     end if
  end if

  if(read_topo_mask == 0) then
     if(myrank == 0) write(*, *) "computing topo mask (clip), threshold = ", topo_norain_threshold
!$omp parallel do private(i, j, k)
     do j = 1, dy
        do i = 1, dx
           do k = 1, dz
              if (true_t(i + posix, j + posiy, k, 1, 1) .gt. topo_norain_threshold) then
                 topo(i, j) = k - 1
                 exit
              end if
           end do !k
        end do !i
     end do !j
!$omp end parallel do
     if(myrank == 0) then
        open(13, file = 'topo.bin', access = 'stream', form = 'unformatted', convert = "little_endian")
        write(13) real(topo)
        close(13)
     end if
  else if(read_topo_mask == 2) then
     if(myrank == 0) write(*, *) "computing topo mask (all), threshold = ", topo_norain_threshold
!$omp parallel do private(i, j, k)
     do j = 1, ddy
        do i = 1, ddx
           do k = 1, topo_mask(i, j)
              if (true_t(i, j, k, 1, 1) .gt. topo_norain_threshold) then
                 topo_mask(i, j) = k
                 exit
              end if
           end do !k
        end do !i
     end do !j
!$omp end parallel do
     if(myrank == 0) then
        open(13, file = 'topo_mask.dat', access = 'stream', form = 'unformatted', convert = "little_endian")
        write(13) topo_mask
        close(13)
        write(*, *) "topo_mask.dat is updated"
     end if
  end if

  if(read_topo_mask .ge. 1) then
     topo(:, :) = topo_mask((posix + 1):(posix + dx), (posiy + 1):(posiy + dy)) - 1
     deallocate(topo_mask)
  end if
end subroutine topography

