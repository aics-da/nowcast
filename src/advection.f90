subroutine advection
  use variables
  implicit none

  !1st order upwind scheme

  CFL = maxval(motion_t(1:dx, 1:dy, 1:dz, it - 1, 1:3)) * dt / gridx
  write(*, *) "CFL", CFL, maxval(rain_t(1:dx, 1:dy, 1:dz, it - 1, 1))

  !boundary condition!

  !x
  do k=1,dz
     do i=1,dx
        rain_t(i,1,k,it-1,1)=rain_t(i,2,k,it-1,1) !Neumann
        rain_t(i,dy,k,it-1,1)=rain_t(i,dy-1,k,it-1,1) !Neumann
!		rain_t(i,1,k,it-1,1)=rain_t(i,2,k,1,1) !Dirichlet
!		rain_t(i,dy,k,it-1,1)=rain_t(i,dy-1,k,1,1) !Dirichlet
		
     end do
  end do

  !y
  do k=1,dz
     do j=1,dy
        rain_t(1,j,k,it-1,1)=rain_t(2,j,k,it-1,1) !Neumann
        rain_t(dx,j,k,it-1,1)=rain_t(dx-1,j,k,it-1,1) !Neumann
!		rain_t(1,j,k,it-1,1)=rain_t(2,j,k,1,1) !Dirichlet
!		rain_t(dx,j,k,it-1,1)=rain_t(dx-1,j,k,1,1) !Dirichlet
     end do
  end do

  !z
  do j=1,dy
     do i=1,dx
        rain_t(i,j,1,it-1,1)=rain_t(i,j,2,it-1,1) !Neumann
        rain_t(i,j,dz,it-1,1)=rain_t(i,j,dz-1,it-1,1) !Neumann
!		rain_t(i,j,k,it-1,1)=rain_t(i,j,k,1,1) !Dirichlet
!		rain_t(i,j,k,it-1,1)=rain_t(i,j,k,1,1) !Dirichlet
     end do
  end do


  do k=2,dz-1
     do j=2,dy-1
	do i=2,dx-1

           if(motion_t(i,j,k,it-1,1)>0.0d0) then
              adv_a=0 !1st upwind
              adv_b=-1 !1st upwind
       
           else
              adv_a=1 !1st upwind
              adv_b=0 !1st upwind
           end if

           if(motion_t(i,j,k,it-1,2)>0.0d0) then
              adv_c=0 !1st upwind
              adv_d=-1 !1st upwind

           else
              adv_c=1 !1st upwind
              adv_d=0 !1st upwind
           end if

           if(motion_t(i,j,k,it-1,3)>0.0d0) then
              adv_e=0 !1st upwind
              adv_f=-1 !1st upwind

           else
              adv_e=1 !1st upwind
              adv_f=0 !1st upwind
           end if


           !adams
           adams_t(i,j,k,1)=-motion_t(i,j,k,it-1,1)*(dt/gridx)*(rain_t(i+adv_a,j,k,it-1,1)-rain_t(i+adv_b,j,k,it-1,1))-&
                &motion_t(i,j,k,it-1,2)*(dt/gridy)*(rain_t(i,j+adv_c,k,it-1,1)-rain_t(i,j+adv_d,k,it-1,1))-& 
                &motion_t(i,j,k,it-1,3)*(dt/gridz)*(rain_t(i,j,k+adv_e,it-1,1)-rain_t(i,j,k+adv_f,it-1,1))

!	if(it<4) then	

           !1st upwind
           rain_t(i,j,k,it,1)=rain_t(i,j,k,it-1,1)-motion_t(i,j,k,it-1,1)*(dt/gridx)*(rain_t(i+adv_a,j,k,it-1,1)-rain_t(i+adv_b,j,k,it-1,1))-&
                &motion_t(i,j,k,it-1,2)*(dt/gridy)*(rain_t(i,j+adv_c,k,it-1,1)-rain_t(i,j+adv_d,k,it-1,1))-& 
                &motion_t(i,j,k,it-1,3)*(dt/gridz)*(rain_t(i,j,k+adv_e,it-1,1)-rain_t(i,j,k+adv_f,it-1,1))
!	else
!	rain_t(i,j,k,it,1)=rain_t(i,j,k,it-1,1)+(23.0d0*adams_t(i,j,k,1)-16.0d0*adams_t(i,j,k,2)+5.0d0*adams_t(i,j,k,3))/12.0d0
!	end if	
	
           adams_t(i,j,k,3)=adams_t(i,j,k,2)
           adams_t(i,j,k,2)=adams_t(i,j,k,1)

           rain_t(i,j,k,it,1)=rain_t(i,j,k,it,1)+growth_t(i,j,k,it-1,1)*dt

           if(rain_t(i,j,k,it,1)<0) then
              rain_t(i,j,k,it,1)=0.0d0
           end if

	end do
     end do
  end do

end subroutine advection
