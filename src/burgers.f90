module burgers
  use variables
  use nowcast_mpi_utils
  implicit none

  integer, parameter, private :: halo_vars = 3
  integer, parameter, private :: halo_len = 1
  real(4), save, allocatable :: mpi_buf(:, :, :, :, :)
  integer i_start, i_end, j_start2, j_end2, k_start, k_end
  integer jt1, jt2
  integer, private :: time1, time2, time3, time4, timerate, timemax

  public burgers_fluid, burgers_fluid_free_mpi_buf

contains

  subroutine burgers_fluid(j0, j1, motion, last)
    integer, intent(in) :: j0, j1
    real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    logical, intent(in) :: last
    integer :: jsbody, jebody

    !INDEX CONVERTER
    jt1 = it2jt(it, 1) !it
    if(it == 2) then
       jt2 = 1 !INITIAL
    else
       jt2 = it2jt(it, 2) !it - 1
    end if

    !1st order upwind scheme

    if(boundary_type == 1) then !CYCLIC IN X
       i_start = 1
       i_end   = dx
    else
       i_start = 2
       i_end   = dx - 1
    end if
    j_start2 = j_mpi(1, myrank)
    j_end2   = j_mpi(2, myrank)
    if(j_start2 < 2)    j_start2 = 2
    if(j_end2 > dy - 1) j_end2   = dy - 1
    k_start = 2
    k_end   = dz - 1

    jsbody = j_start2 + halo_len
    jebody = j_end2   - halo_len
    if(myrank == 0) jsbody = j_start2
    if(myrank == mpiprocs - 1) jebody = j_end2
    if(jsbody .le. jebody) then
       !computation except halo
       if(it < 3 .or. mod(it, 10) .eq. 0) then
          call burgers_fluid_euler(j0, j1, motion, jsbody, jebody)
       else
          if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
             call burgers_fluid_AB3(j0, j1, motion, jsbody, jebody)
          elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
             call burgers_fluid_AB2(j0, j1, motion, jsbody, jebody)
          else
             write(*, *) "switching_time_integration is wrong"
             stop 999
          end if
       end if
    end if

    if(mpiprocs > 1) then
       !receive halo
       call system_clock(time1, timerate, timemax)
       call exchange_real4_halo_by_mpi_sendrecv_wait( &
            & j0, j1, motion, i_start, i_end, j_start2, j_end2, k_start, k_end, &
            & jt2, halo_vars, halo_len, mpi_buf, mpi_req_wind_halo)
       call system_clock(time2, timerate, timemax)
       time_adv_wind_mpi = time_adv_wind_mpi + (time2 - time1) / dble(timerate)

       !computation for halo
       if(it < 3 .or. mod(it, 10) .eq. 0) then
          if(j_start2 < jsbody) call burgers_fluid_euler(j0, j1, motion, j_start2, jsbody - 1)
          if(jebody   < j_end2) call burgers_fluid_euler(j0, j1, motion, jebody + 1, j_end2)
       else
          if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
             if(j_start2 < jsbody) call burgers_fluid_AB3(j0, j1, motion, j_start2, jsbody - 1)
             if(jebody   < j_end2) call burgers_fluid_AB3(j0, j1, motion, jebody + 1, j_end2)
          elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
             if(j_start2 < jsbody) call burgers_fluid_AB2(j0, j1, motion, j_start2, jsbody - 1)
             if(jebody   < j_end2) call burgers_fluid_AB2(j0, j1, motion, jebody + 1, j_end2)
          else
             write(*, *) "switching_time_integration is wrong"
             stop 999
          end if
       end if

       !send halo
       if(.not. last) then
          call system_clock(time1, timerate, timemax)
          call exchange_real4_halo_by_mpi_sendrecv_start( &
               & j0, j1, motion, i_start, i_end, j_start2, j_end2, k_start, k_end, &
               & jt1, halo_vars, halo_len, mpi_buf, mpi_req_wind_halo)
          call system_clock(time2, timerate, timemax)
          time_adv_wind_mpi = time_adv_wind_mpi + (time2 - time1) / dble(timerate)
       end if
    end if

    return
  end subroutine burgers_fluid

  subroutine burgers_fluid_calc_adams(j0, j1, motion, j, k)
    integer, intent(in) :: j0, j1
    real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    integer, intent(in) :: j, k
    double precision :: tmp(i_start:i_end, 3, 3)
    integer i

    do i = i_start, i_end
       if(motion(i, j, k, jt2, 1) > 0.0d0) then
          tmp(i, 1, 1) = motion(i, j, k, jt2, 1) - motion(i - 1, j, k, jt2, 1)
          tmp(i, 2, 1) = motion(i, j, k, jt2, 2) - motion(i - 1, j, k, jt2, 2)
          tmp(i, 3, 1) = motion(i, j, k, jt2, 3) - motion(i - 1, j, k, jt2, 3)
       else
          tmp(i, 1, 1) = motion(i + 1, j, k, jt2, 1) - motion(i, j, k, jt2, 1)
          tmp(i, 2, 1) = motion(i + 1, j, k, jt2, 2) - motion(i, j, k, jt2, 2)
          tmp(i, 3, 1) = motion(i + 1, j, k, jt2, 3) - motion(i, j, k, jt2, 3)
       end if
       if(motion(i, j, k, jt2, 2) > 0.0d0) then
          tmp(i, 1, 2) = motion(i, j, k, jt2, 1) - motion(i, j - 1, k, jt2, 1)
          tmp(i, 2, 2) = motion(i, j, k, jt2, 2) - motion(i, j - 1, k, jt2, 2)
          tmp(i, 3, 2) = motion(i, j, k, jt2, 3) - motion(i, j - 1, k, jt2, 3)
       else
          tmp(i, 1, 2) = motion(i, j + 1, k, jt2, 1) - motion(i, j, k, jt2, 1)
          tmp(i, 2, 2) = motion(i, j + 1, k, jt2, 2) - motion(i, j, k, jt2, 2)
          tmp(i, 3, 2) = motion(i, j + 1, k, jt2, 3) - motion(i, j, k, jt2, 3)
       end if
       if(motion(i, j, k, jt2, 3) > 0.0d0) then
          tmp(i, 1, 3) = motion(i, j, k, jt2, 1) - motion(i, j, k - 1, jt2, 1)
          tmp(i, 2, 3) = motion(i, j, k, jt2, 2) - motion(i, j, k - 1, jt2, 2)
          tmp(i, 3, 3) = motion(i, j, k, jt2, 3) - motion(i, j, k - 1, jt2, 3)
       else
          tmp(i, 1, 3) = motion(i, j, k + 1, jt2, 1) - motion(i, j, k, jt2, 1)
          tmp(i, 2, 3) = motion(i, j, k + 1, jt2, 2) - motion(i, j, k, jt2, 2)
          tmp(i, 3, 3) = motion(i, j, k + 1, jt2, 3) - motion(i, j, k, jt2, 3)
       end if
    end do !i

    do i = i_start, i_end
       adams_t(i, j, k, i_adams(1), 1) = -dt * (motion(i, j, k, jt2, 1) * inv_gridx * tmp(i, 1, 1) + &
            &                                   motion(i, j, k, jt2, 2) * inv_gridy * tmp(i, 1, 2) + &
            &                                   motion(i, j, k, jt2, 3) * inv_gridz * tmp(i, 1, 3))
       adams_t(i, j, k, i_adams(1), 2) = -dt * (motion(i, j, k, jt2, 1) * inv_gridx * tmp(i, 2, 1) + &
            &                                   motion(i, j, k, jt2, 2) * inv_gridy * tmp(i, 2, 2) + &
            &                                   motion(i, j, k, jt2, 3) * inv_gridz * tmp(i, 2, 3))
       adams_t(i, j, k, i_adams(1), 3) = -dt * (motion(i, j, k, jt2, 1) * inv_gridx * tmp(i, 3, 1) + &
            &                                   motion(i, j, k, jt2, 2) * inv_gridy * tmp(i, 3, 2) + &
            &                                   motion(i, j, k, jt2, 3) * inv_gridz * tmp(i, 3, 3))
    end do !i
  end subroutine burgers_fluid_calc_adams

  subroutine burgers_fluid_euler(j0, j1, motion, js, je)
    integer, intent(in) :: j0, j1, js, je
    real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    integer i, j, k

!$omp parallel do private(i, j, k)
    do k = k_start, k_end
       do j = js, je
!DIR$ FORCEINLINE
          call burgers_fluid_calc_adams(j0, j1, motion, j, k)
          do i = i_start, i_end
             motion(i, j, k, jt1, 1) = motion(i, j, k, jt2, 1) + adams_t(i, j, k, i_adams(1), 1)
             motion(i, j, k, jt1, 2) = motion(i, j, k, jt2, 2) + adams_t(i, j, k, i_adams(1), 2)
             motion(i, j, k, jt1, 3) = motion(i, j, k, jt2, 3) + adams_t(i, j, k, i_adams(1), 3)
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine burgers_fluid_euler

  subroutine burgers_fluid_AB3(j0, j1, motion, js, je)
    integer, intent(in) :: j0, j1, js, je
    real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    double precision :: factor(3)
    integer i, j, k
    factor(i_adams(1)) =  23.0d0 / 12.0d0
    factor(i_adams(2)) = -16.0d0 / 12.0d0
    factor(i_adams(3)) =   5.0d0 / 12.0d0

!$omp parallel do private(i, j, k)
    do k = k_start, k_end
       do j = js, je
!DIR$ FORCEINLINE
          call burgers_fluid_calc_adams(j0, j1, motion, j, k)
          do i = i_start, i_end
             motion(i, j, k, jt1, 1) = motion(i, j, k, jt2, 1) &
                  & + factor(1) * adams_t(i, j, k, 1, 1) &
                  & + factor(2) * adams_t(i, j, k, 2, 1) &
                  & + factor(3) * adams_t(i, j, k, 3, 1)
             motion(i, j, k, jt1, 2) = motion(i, j, k, jt2, 2) &
                  & + factor(1) * adams_t(i, j, k, 1, 2) &
                  & + factor(2) * adams_t(i, j, k, 2, 2) &
                  & + factor(3) * adams_t(i, j, k, 3, 2)
             motion(i, j, k, jt1, 3) = motion(i, j, k, jt2, 3) &
                  & + factor(1) * adams_t(i, j, k, 1, 3) &
                  & + factor(2) * adams_t(i, j, k, 2, 3) &
                  & + factor(3) * adams_t(i, j, k, 3, 3)
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine burgers_fluid_AB3

  subroutine burgers_fluid_AB2(j0, j1, motion, js, je)
    integer, intent(in) :: j0, j1, js, je
    real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    double precision :: factor(2)
    integer i, j, k
    factor(i_adams(1)) =  3.0d0 * 0.5d0
    factor(i_adams(2)) = -1.0d0 * 0.5d0

!$omp parallel do private(i, j, k)
    do k = k_start, k_end
       do j = js, je
!DIR$ FORCEINLINE
          call burgers_fluid_calc_adams(j0, j1, motion, j, k)
          do i = i_start, i_end
             motion(i, j, k, jt1, 1) = motion(i, j, k, jt2, 1) &
                  & + factor(1) * adams_t(i, j, k, 1, 1) + factor(2) * adams_t(i, j, k, 2, 1)
             motion(i, j, k, jt1, 2) = motion(i, j, k, jt2, 2) &
                  & + factor(1) * adams_t(i, j, k, 1, 2) + factor(2) * adams_t(i, j, k, 2, 2)
             motion(i, j, k, jt1, 3) = motion(i, j, k, jt2, 3) &
                  & + factor(1) * adams_t(i, j, k, 1, 3) + factor(2) * adams_t(i, j, k, 2, 3)
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine burgers_fluid_AB2

  subroutine burgers_fluid_free_mpi_buf
    if(allocated(mpi_buf)) deallocate(mpi_buf)
  end subroutine burgers_fluid_free_mpi_buf

end module burgers
