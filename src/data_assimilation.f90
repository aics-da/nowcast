module data_assimilation
  use netcdf
  use variables
  use module_input
  use, intrinsic :: ieee_arithmetic
  implicit none

  public

contains

  subroutine exec_data_assimilation()

    integer i, j, k, a
    integer dat_count, datsize
    character citer*3
    real(4) kalmangain
    real(4), dimension(:, :, :, :), allocatable :: motion_a, motion_f
    real(4), allocatable :: mean_motion_t(:, :)
    integer, allocatable :: mean_count(:)
    integer js, je
    integer time1, time2, timerate, timemax
    logical isnan

    js = j_mpi(1, myrank)
    je = j_mpi(2, myrank)

    allocate(motion_a(1:dx, js:je, 1:dz, 1:3), &
         &   motion_f(1:dx, 1:dy,  1:dz, 1:3))

    !READ FIRST GUESS
    call system_clock(time1)
    call read_da_first_guess(motion_f)
    call system_clock(time2, timerate, timemax)
    if(myrank == 0) write(*, *) "reading first guess", (time2 - time1) / dble(timerate)
    time1 = time2

    isnan = .false.
!$omp parallel do private(j, k) collapse(2)
    do k = 1, dz
       do j = js, je !MPI
          do i = 1, dx
             if(ieee_is_nan(motion_f(i, j, k, 1))) isnan = .true.
             if(ieee_is_nan(motion_f(i, j, k, 2))) isnan = .true.
             if(ieee_is_nan(motion_f(i, j, k, 3))) isnan = .true.
          end do !i
       end do !j
    end do !k
!$omp end parallel do
    if(isnan) write(*, *) "myrank ", myrank, " NaN detected"
    call system_clock(time2, timerate, timemax)
    if(myrank == 0) write(*, *) "check NaN in first guess", (time2 - time1) / dble(timerate)
    time1 = time2

    !CONVERT burgers[UVW] to vector_[uvw]
    if(.not. isnan) then
!$omp parallel do private(j, k) collapse(2)
       do k = 1, dz
          do j = js, je !MPI
             motion_f(:, j, k, 1) = motion_f(:, j, k, 1) / real(gridx) * input_interval
             motion_f(:, j, k, 2) = motion_f(:, j, k, 2) / real(gridy) * input_interval
             motion_f(:, j, k, 3) = motion_f(:, j, k, 3) / real(gridz) * input_interval
          end do !j
       end do !k
!$omp end parallel do
    end if

    !SET/UPDATE FORECAST VARIANCE
    if(iter == da_start_from) then
!$omp parallel do private(j, k) collapse(2)
       do k = 1, dz
          do j = js, je !MPI
             variance_f(:, j, k, 1) = variance_f_init
             variance_f(:, j, k, 2) = variance_f_init
             variance_f(:, j, k, 3) = variance_f_init
          end do !j
       end do !k
!$omp end parallel do
    else
!$omp parallel do private(i, j, k) collapse(2)
       do k = 1, dz
          do j = js, je !MPI
             do i = 1, dx
                variance_f(i, j, k, 1) = variance_f(i, j, k, 1) * variance_f_inflation
                variance_f(i, j, k, 2) = variance_f(i, j, k, 2) * variance_f_inflation
                variance_f(i, j, k, 3) = variance_f(i, j, k, 3) * variance_f_inflation
                variance_f(i, j, k, 1) = min(variance_f(i, j, k, 1), variance_f_max)
                variance_f(i, j, k, 2) = min(variance_f(i, j, k, 2), variance_f_max)
                variance_f(i, j, k, 3) = min(variance_f(i, j, k, 3), variance_f_max)
             end do !i
          end do !j
       end do !k
!$omp end parallel do
    end if

    call system_clock(time2, timerate, timemax)
    if(myrank == 0) write(*, *) "setup DA", (time2 - time1) / dble(timerate)
    time1 = time2

    !GROSS ERROR QUALITY CONTROL
    dat_count = 0
!$omp parallel do private(i, j, k) collapse(2)
    do k = 1, dz
       do j = js, je !MPI
          do i = 1, dx
             if((motion_t(i, j, k, 1, 1) < motion_undef) .and. &
                  & ((abs(motion_t(i, j, k, 1, 1) - motion_f(i, j, k, 1)) .gt. da_qc_threshold) .or. &
                  &  (abs(motion_t(i, j, k, 1, 2) - motion_f(i, j, k, 2)) .gt. da_qc_threshold) .or. &
                  &  (abs(motion_t(i, j, k, 1, 3) - motion_f(i, j, k, 3)) .gt. da_qc_threshold))) then
                motion_t(i, j, k, 1, 1) = motion_undef !QC REJECTION
                motion_t(i, j, k, 1, 2) = motion_undef !QC REJECTION
                motion_t(i, j, k, 1, 3) = motion_undef !QC REJECTION
             end if
          end do !i
       end do !j
    end do !k
!$omp end parallel do
    call system_clock(time2, timerate, timemax)
    if(myrank == 0) write(*, *) "gross error QC", (time2 - time1) / dble(timerate)
    time1 = time2

    if(isnan) then
       allocate(mean_motion_t(dz, 3), mean_count(dz))
       mean_motion_t(:, :) = 0
!$omp parallel do private(j, k) collapse(2)
       do k = 1, dz
          do j = js, je !MPI
             do i = 1, dx
                if(    (motion_t(i, j, k, 1, 1) < motion_undef) .and. &
                     & (motion_t(i, j, k, 1, 2) < motion_undef) .and. &
                     & (motion_t(i, j, k, 1, 3) < motion_undef)) then
                   mean_motion_t(k, 1) = mean_motion_t(k, 1) + motion_t(i, j, k, 1, 1)
                   mean_motion_t(k, 2) = mean_motion_t(k, 2) + motion_t(i, j, k, 1, 2)
                   mean_motion_t(k, 3) = mean_motion_t(k, 3) + motion_t(i, j, k, 1, 3)
                   mean_count(k) = mean_count(k) + 1
                end if
             end do !i
          end do !j
       end do !k
!$omp end parallel do
       do k = 1, dz
          if(mean_count(k) > 0) then
             mean_motion_t(k, 1) = mean_motion_t(k, 1) / mean_count(k)
             mean_motion_t(k, 2) = mean_motion_t(k, 2) / mean_count(k)
             mean_motion_t(k, 3) = mean_motion_t(k, 3) / mean_count(k)
          end if
       end do !k
!$omp parallel do private(j, k) collapse(2)
       do k = 1, dz
          do j = js, je !MPI
             motion_f(:, j, k, 1) = mean_motion_t(k, 1)
             motion_f(:, j, k, 2) = mean_motion_t(k, 2)
             motion_f(:, j, k, 3) = mean_motion_t(k, 3)
          end do !j
       end do !k
!$omp end parallel do
       deallocate(mean_motion_t, mean_count)
    end if
    call system_clock(time2, timerate, timemax)
    if(myrank == 0) write(*, *) "recovery from NaN", (time2 - time1) / dble(timerate)
    time1 = time2

    !KALMAN FILTER WITHOUT SPATIAL CORRELATION
!$omp parallel do private(i, j, k, a, kalmangain) reduction(+ : dat_count) collapse(3)
    do a = 1, 3
       do k = 1, dz
          do j = js, je !MPI
             do i = 1, dx
                if(motion_t(i, j, k, 1, a) < motion_undef) then
                   if(isnan) then
                      motion_a(i, j, k, a) = motion_t(i, j, k, 1, a)
                   else
                      kalmangain = variance_f(i, j, k, a) / (variance_f(i, j, k, a) + variance_o)
                      motion_a(i, j, k, a) = motion_f(i, j, k, a) + kalmangain * (motion_t(i, j, k, 1, a) - motion_f(i, j, k, a))
                      variance_f(i, j, k, a) = (1 - kalmangain) * variance_f(i, j, k, a)
                   end if
                else
                   motion_a(i, j, k, a) = motion_f(i, j, k, a)
                end if
             end do !i

             do i = 1, dx
                if(motion_t(i, j, k, 1, a) < motion_undef) dat_count = dat_count + 1
             end do !i
          end do !j
       end do !k
    end do !a
!$omp end parallel do
    if(myrank == 0) write(*, *) "iter ", iter, ", num of obs assimilated: ", dat_count
    call system_clock(time2, timerate, timemax)
    if(myrank == 0) write(*, *) "DA", (time2 - time1) / dble(timerate)
    time1 = time2

!$omp parallel do private(j, k) collapse(2)
    do k = 1, dz
       do j = js, je !MPI
          motion_t(1:dx, j, k, 1, 1) = motion_a(1:dx, j, k, 1)
          motion_t(1:dx, j, k, 1, 2) = motion_a(1:dx, j, k, 2)
          motion_t(1:dx, j, k, 1, 3) = motion_a(1:dx, j, k, 3)
       end do !j
    end do !k
!$omp end parallel do
    call system_clock(time2, timerate, timemax)
    if(myrank == 0) write(*, *) "DA post processing", (time2 - time1) / dble(timerate)
    time1 = time2

    !output
    if(myrank == 0 .and. output_da_anal == 1) then
       inquire(iolength = datsize) motion_t(1:dx, 1:dy, 1:dz, 1, 1)
       write(citer, '(I3.3)') start + iter - 1
       open(555, file = 'vector_u_anl_' // citer // '.bin', access = "direct", form = 'unformatted', recl = datsize, convert = "little_endian")
       write(555, rec = 1) motion_t(1:dx, 1:dy, 1:dz, 1, 1)
       close(555)

       open(666, file = 'vector_v_anl_' // citer // '.bin', access = "direct", form = 'unformatted', recl = datsize, convert = "little_endian")
       write(666, rec = 1) motion_t(1:dx, 1:dy, 1:dz, 1, 2)
       close(666)

       open(777, file = 'vector_w_anl_' // citer // '.bin', access = "direct", form = 'unformatted', recl = datsize, convert = "little_endian")
       write(777, rec = 1) motion_t(1:dx, 1:dy, 1:dz, 1, 3)
       close(777)

       write(citer, '(I3.3)') start + iter - 1
       open(555, file = 'vector_u_var_' // citer // '.bin', access = "direct", form = 'unformatted', recl = datsize, convert = "little_endian")
       write(555, rec = 1) variance_f(1:dx, 1:dy, 1:dz, 1)
       close(555)

       open(666, file = 'vector_v_var_' // citer // '.bin', access = "direct", form = 'unformatted', recl = datsize, convert = "little_endian")
       write(666, rec = 1) variance_f(1:dx, 1:dy, 1:dz, 2)
       close(666)

       open(777, file = 'vector_w_var_' // citer // '.bin', access = "direct", form = 'unformatted', recl = datsize, convert = "little_endian")
       write(777, rec = 1) variance_f(1:dx, 1:dy, 1:dz, 3)
       close(777)
    end if

    deallocate(motion_a, motion_f)
  end subroutine exec_data_assimilation
end module data_assimilation
