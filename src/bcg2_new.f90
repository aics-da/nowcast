module bcg2
  implicit none

  public

  contains

    subroutine bicgstab_new(n, x, rhs, matvec, tol, mxmv, info)
      !To solve a linear system Ax = b, BiCGSTAB starts with an initial guess x0 and proceeds as follows:
      !r(0) = b − A x(0)
      !Choose an arbitrary vector r0hat such that (r0hat, r0) .ne. 0, e.g., r0hat = r0
      !rho(0) = alpha = omega(0) = 1
      !v(0) = p(0) = 0
      !For i = 1, 2, 3, ...
      !  rho(i) = (r0hat, r(i − 1))
      !  beta = (rho(i) / rho(i − 1)) (alpha / omega(i − 1))
      !  p(i) = r(i − 1) + beta(p(i − 1) − omega(i − 1)v(i − 1))
      !  v(i) = A p(i)
      !  alpha = rho(i) / (r0hat, v(i))
      !  s = r(i − 1) − alpha v(i)
      !  t = A s
      !  omega(i) = (t, s) / (t, t)
      !  x(i) = x(i − 1) + alpha p(i) + omega(i) s
      !  If xi is accurate enough then quit
      !  r(i) = s − omega(i) t

      interface
         subroutine matvec(n, x, y)
           integer, intent(in) :: n
           double precision, intent(in) :: x(n)
           double precision, intent(inout) :: y(n)
         end subroutine matvec
      end interface

      integer, intent(in) :: n
      integer, intent(inout) :: mxmv
      integer, intent(out) :: info
      double precision, intent(inout) :: x(n), tol
      double precision, intent(in) :: rhs(n)

      integer :: counter, m
      double precision :: r(n), r0hat(n), v(n), p(n), t(n)
      double precision :: rho, alpha, beta, omega, err, tmprho
      double precision :: t_s_sum, t_t_sum

      r = 0.0d0
      v = 0.0d0
      p = 0.0d0
      t = 0.0d0

      info = 0

      !!! r(0) = b - A x(0)
      call matvec(n, x, r)
!$omp parallel do
      do m = 1, n
         r(m) = rhs(m) - r(m)
         r0hat(m) = r(m)
      end do
!$omp end parallel do

      err = maxval(abs(r)) !CHECK CONVERGENCE

      if(err .lt. tol) then
         mxmv = 0
         tol = err
         return
      end if

      rho = 1.0d0
      alpha = 1.0d0
      omega = 1.0d0

      counter = 1
      do
         beta = alpha / omega / rho
         tmprho = 0
!$omp parallel do reduction(+: tmprho)
         do m = 1, n
            tmprho = tmprho + r0hat(m) * r(m)
         end do
!$omp end parallel do
         rho = tmprho
         beta = beta * rho
!$omp parallel do
         do m = 1, n
            p(m) = r(m) + beta * (p(m) - omega * v(m))
         end do
!$omp end parallel do
         call matvec(n, p, v) ! v(i) = A p(i)

         alpha = rho / dot_product(r0hat, v)

!$omp parallel do
         do m = 1, n
            r(m) = r(m) - alpha * v(m)
         end do
!$omp end parallel do
         call matvec(n, r, t) ! t = A s (note: s is replaced by r)
         t_s_sum = 0
         t_t_sum = 0
!$omp parallel do reduction(+: t_s_sum, t_t_sum)
         do m = 1, n
            t_s_sum = t_s_sum + t(m) * r(m)
            t_t_sum = t_t_sum + t(m) * t(m)
         end do
!$omp end parallel do
         omega = t_s_sum / t_t_sum

!$omp parallel do
         do m = 1, n
            x(m) = x(m) + alpha * p(m) + omega * r(m)
            r(m) = r(m) - omega * t(m)
         end do
!$omp end parallel do

         err = maxval(abs(r)) !CHECK CONVERGENCE

         if(err .lt. tol) exit
         if(counter .eq. mxmv) then
            info = 1
            exit
         end if
         counter = counter + 1

      end do
      mxmv = counter
      tol = err
    end subroutine bicgstab_new

end module bcg2
