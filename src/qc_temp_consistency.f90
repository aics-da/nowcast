module module_qc_temp_consistency
  use variables
  implicit none

  public qc_temp_consistency

contains

  subroutine qc_temp_consistency
    double precision mean_vect_mean(3, dz), mean_vect_var(3, dz), factor2
    integer mean_vect_count(dz)
    integer num_valid_lev, num_reject_lev
    double precision, allocatable :: mean_vect_history(:, :, :)
    integer, allocatable :: num_valid_vect_history(:, :)
    integer, parameter :: num_history = 10
    logical qc_temp_consistency_flag
    logical file_exist
    integer k, a

    !GLOBAL QUALITY CONTROL BY TEMPORAL CONSISTENCY
    allocate(mean_vect_history(3, dz, num_history), num_valid_vect_history(dz, num_history))
    inquire(file = trim(da_gues_path) // 'mean_vect_history.bin', exist = file_exist)
    if(file_exist) then
       open(26, file = trim(da_gues_path) // 'mean_vect_history.bin', access = 'stream', form = 'unformatted', convert = "little_endian")
       read(26) num_valid_vect_history, mean_vect_history
       close(26)
    else
       num_valid_vect_history = 0
       mean_vect_history = 0.0d0
    end if
    mean_vect_count = 0
    mean_vect_mean = 0
    mean_vect_var = 0
    do a = 1, num_history
       where(num_valid_vect_history(:, a) > 0)
          mean_vect_count = mean_vect_count + 1
          mean_vect_mean(1, :) = mean_vect_mean(1, :) + mean_vect_history(1, :, a)
          mean_vect_mean(2, :) = mean_vect_mean(2, :) + mean_vect_history(2, :, a)
          mean_vect_mean(3, :) = mean_vect_mean(3, :) + mean_vect_history(3, :, a)
       end where
    end do !a
    where(mean_vect_count == num_history)
       mean_vect_mean(1, :) = mean_vect_mean(1, :) / num_history
       mean_vect_mean(2, :) = mean_vect_mean(2, :) / num_history
       mean_vect_mean(3, :) = mean_vect_mean(3, :) / num_history
    end where
    do a = 1, num_history
       where(num_valid_vect_history(:, a) > 0 .and. mean_vect_count == num_history)
          mean_vect_var(1, :) = mean_vect_var(1, :) + (mean_vect_history(1, :, a) - mean_vect_mean(1, :)) ** 2
          mean_vect_var(2, :) = mean_vect_var(2, :) + (mean_vect_history(2, :, a) - mean_vect_mean(2, :)) ** 2
          mean_vect_var(3, :) = mean_vect_var(3, :) + (mean_vect_history(3, :, a) - mean_vect_mean(3, :)) ** 2
       end where
    end do !a
    where(mean_vect_count == num_history)
       mean_vect_var(1, :) = mean_vect_var(1, :) / (num_history - 1)
       mean_vect_var(2, :) = mean_vect_var(2, :) / (num_history - 1)
       mean_vect_var(3, :) = mean_vect_var(3, :) / (num_history - 1)
    end where
    qc_temp_consistency_flag = .false.
    factor2 = da_qc_temp_consistency_sigma ** 2
    do a = 1, 3
       num_valid_lev = 0
       num_reject_lev = 0
       do k = 1, dz
          if(mean_vect_count(k) == num_history) then
             num_valid_lev = num_valid_lev + 1
             if((mean_vect (a, k) - mean_vect_mean(a, k)) ** 2 > mean_vect_var(a, k) * factor2) then
                num_reject_lev = num_reject_lev + 1
             end if
          end if
       end do !k
       if(num_valid_lev > 0 .and. dble(num_reject_lev) / dble(num_valid_lev) > da_qc_temp_consistency_threshold) then
          if(myrank == 0) write(*, *) "global quality control by temporal consistency: reject"
          qc_temp_consistency_flag = .true.
          exit
       end if
    end do !a
    if(qc_temp_consistency_flag) then
!$omp parallel do private(k)
       do k = 1, dz
          motion_t(:, :, k, 1, 1:3) = motion_undef !QC REJECTION
       end do
!$omp end parallel do
    end if
    do a = num_history, 2, -1
       num_valid_vect_history(:, a) = num_valid_vect_history(:, a - 1)
       mean_vect_history(:, :, a) = mean_vect_history(:, :, a - 1)
    end do !a
    num_valid_vect_history(:, 1) = num_valid_vect
    mean_vect_history(:, :, 1) = mean_vect
    if(myrank == 0) then
       open(26, file = 'mean_vect_history.bin', access = 'stream', form = 'unformatted', convert = "little_endian")
       write(26) num_valid_vect_history, mean_vect_history
       close(26)
    end if
    deallocate(mean_vect_history, num_valid_vect_history)
  end subroutine qc_temp_consistency
end module module_qc_temp_consistency
