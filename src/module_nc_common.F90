module module_nc_common
  use netcdf
#ifdef MPI
  use mpi_f08
#endif
  !use h5e
  implicit none

  public

contains

  subroutine check_nc_err(stat, err_count, max_err_count)
    integer, intent(in) :: stat
    integer, intent(inout), optional :: err_count
    integer, intent(in), optional :: max_err_count
    integer count, maxerr
    !integer hdferr
    if(present(err_count)) then
       count = err_count
    else
       count = 0
    end if
    if(present(max_err_count)) then
       maxerr = max_err_count
    else
       maxerr = 1
    end if
    if(stat .ne. nf90_noerr) then
       write(*, *) stat, trim(nf90_strerror(stat))
       if(stat .eq. NF90_EHDFERR) then
          open(99, file = "HDF5_ERROR", access = 'stream', form = 'unformatted', convert = "little_endian")
          close(99)
       !   call h5eprint_f(hdferr)
       end if
       count = count + 1
       if(count .ge. maxerr) then
#ifdef MPI
          call MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER)
#else
          stop 999
#endif
       end if
    else
       count = 0 !!! RESET ERROR COUNT
    end if
    if(present(err_count)) err_count = count
  end subroutine check_nc_err

  subroutine define_nc(ncname, ncid, varname, varunit, varid, ncix, ncy0, ncy1, nciz)
    character(*), intent(in) :: ncname, varname, varunit
    integer, intent(in) :: ncix, ncy0, ncy1, nciz
    integer, intent(out) :: ncid, varid
    integer x_dimid, y_dimid, z_dimid, t_dimid
    integer dimids(4), nciy, varid_y, j
    integer :: yval(ncy0:ncy1)

    nciy = ncy1 - ncy0 + 1
    do j = ncy0, ncy1
       yval(j) = j
    end do !j
    call check_nc_err(nf90_create(ncname, NF90_HDF5, ncid))
    call check_nc_err(nf90_def_dim(ncid, "x", ncix, x_dimid))
    call check_nc_err(nf90_def_dim(ncid, "y", nciy, y_dimid))
    call check_nc_err(nf90_def_dim(ncid, "z", nciz, z_dimid))
    call check_nc_err(nf90_def_dim(ncid, "t", NF90_UNLIMITED, t_dimid))
    dimids = (/ x_dimid, y_dimid, z_dimid, t_dimid /)
    call check_nc_err(nf90_def_var(ncid, varname, NF90_REAL, dimids, varid))
    call check_nc_err(nf90_def_var_deflate(ncid, varid, 0, 1, 2))
    call check_nc_err(nf90_put_att(ncid, varid, "units", varunit))
    !Y AXIS
    call check_nc_err(nf90_def_var(ncid, "y", NF90_INT, dimids(2:2), varid_y))
    call check_nc_err(nf90_def_var_deflate(ncid, varid_y, 0, 1, 2))
    call check_nc_err(nf90_enddef(ncid))
    !WRITE Y AXIS
    call check_nc_err(nf90_put_var(ncid, varid_y, yval))
  end subroutine define_nc

end module module_nc_common
