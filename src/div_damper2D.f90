subroutine div_damper2d
  use variables
  implicit none
  integer i, j
  real(real_size), allocatable :: div(:, :)
  real(real_size) :: dt_div_damper_param
  integer jt1, jt2

  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  allocate(div(dx, dy))

  dt_div_damper_param = dt * div_damper_param

!$omp parallel
!$omp do private(i, j)
  do j = 2, dy - 1
     do i = 2 - margin_x_adv, dx - 1 + margin_x_adv
        div(i, j) = (motion_t(i + 1, j, 1, jt2, 1) - motion_t(i - 1, j, 1, jt2, 1) &
             + motion_t(i, j + 1, 1, jt2, 2) - motion_t(i, j - 1, 1, jt2, 2)) * 0.5d0
     end do
  end do
!$omp end do
!$omp do private(i, j)
  do j = 3, dy - 2
     do i = 3 - margin_x_adv, dx - 2 + margin_x_adv
        motion_t(i, j, 1, jt1, 1) = motion_t(i, j, 1, jt1, 1) + dt_div_damper_param * (div(i + 1, j) - div(i - 1, j)) * 0.5d0
     end do
  end do
!$omp end do
!$omp do private(i, j)
  do j = 3, dy - 2
     do i = 3 - margin_x_adv, dx - 2 + margin_x_adv
        motion_t(i, j, 1, jt1, 2) = motion_t(i, j, 1, jt1, 2) + dt_div_damper_param * (div(i, j + 1) - div(i, j - 1)) * 0.5d0
     end do
  end do
!$omp end do
!$omp end parallel
end subroutine div_damper2d
