module module_output_each_proc
  use variables
  use module_io_common
  implicit none

  public

contains

  subroutine open_output_files_each_proc(j_start, j_end)
    integer, intent(in) :: j_start, j_end
    character(14) fname_ext

    write(fname_ext, '(I3.3, "_", I6.6, ".bin")') start + iter - 1, myrank
    fname1 = 'raindata_' // fname_ext
    fname2 = 'burgersU_' // fname_ext
    fname3 = 'burgersV_' // fname_ext
    fname4 = 'burgersW_' // fname_ext
    fname5 = 'growth_'   // fname_ext
    open(111, file = trim(fname1), access = 'stream', form = 'unformatted', convert = "little_endian")
    open(222, file = trim(fname2), access = 'stream', form = 'unformatted', convert = "little_endian")
    open(333, file = trim(fname3), access = 'stream', form = 'unformatted', convert = "little_endian")
    open(444, file = trim(fname4), access = 'stream', form = 'unformatted', convert = "little_endian")
    if(switching_growth == 1) &
         & open(555, file = trim(fname5), access = 'stream', form = 'unformatted', convert = "little_endian")
  end subroutine open_output_files_each_proc

  subroutine close_output_files_each_proc
    close(111)
    close(222)
    close(333)
    close(444)
    if(switching_growth == 1) close(555)
  end subroutine close_output_files_each_proc

  subroutine output_each_proc(l_out_rain, l_out_uvw, l_out_growth, j_start, j_end)
    logical, intent(in) :: l_out_rain, l_out_uvw, l_out_growth
    integer, intent(in) :: j_start, j_end
    integer tmptime1, tmptime2, timerate, timemax

    write(*, *) "output t = ", (it - 1) * dt + 1
    write(*, '("output start = ", I4, ", iter = ", I4, ", it = ", I6, ", t = ", F10.1)') start, iter, it, (it - 1) * dt + 1

    call system_clock(tmptime1, timerate, timemax)
    if(l_out_rain) write(111) rain_t_single(1:dx, j_start:j_end, 1:dz) !CROP NOT IMPLEMENTED YET
    if(l_out_uvw)  write(222) motion_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 1)
    if(l_out_uvw)  write(333) motion_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 2)
    if(l_out_uvw)  write(444) motion_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 3)
    if(switching_growth == 1 .and. l_out_growth) &
         &         write(555) growth_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 1)
    call system_clock(tmptime2, timerate, timemax)
    time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)
  end subroutine output_each_proc
end module module_output_each_proc
