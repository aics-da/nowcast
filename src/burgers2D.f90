module burgers2d
  use variables
  use nowcast_mpi_utils
  implicit none

  real(4), save, allocatable :: mpi_buf(:, :, :, :, :)

  public burgers_fluid2d, burgers_fluid2d_free_mpi_buf

contains
  subroutine burgers_fluid2d(j0, j1, motion, last)
    integer, intent(in) :: j0, j1
    real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
    logical, intent(in) :: last
    integer i, j
    integer i_start, i_end, j_start2, j_end2
    integer jt1, jt2
    real(real_size), allocatable :: div(:, :)
    real(real_size) :: dt_div_damper_param

    !INDEX CONVERTER
    jt1 = it2jt(it, 1) !it
    if(it == 2) then
       jt2 = 1 !INITIAL
    else
       jt2 = it2jt(it, 2) !it - 1
    end if

    !1st order upwind scheme

    if(boundary_type == 1) then !CYCLIC IN X
       i_start = 1
       i_end = dx
    else
       i_start = 2
       i_end = dx - 1
    end if
    j_start2 = j_mpi(1, myrank)
    j_end2   = j_mpi(2, myrank)
    if(j_start2 < 2)    j_start2 = 2
    if(j_end2 > dy - 1) j_end2   = dy - 1

    if(mpiprocs > 1) &
         & call exchange_real4_halo_by_mpi_sendrecv_wait( &
         & j0, j1, motion, i_start, i_end, j_start2, j_end2, 1, 1, jt2, 3, 1, mpi_buf, mpi_req_wind_halo)

    if(switching_div_damper == 1) then ! for divergence damper
       allocate(div((i_start - 1):(i_end + 1), (j_start2 - 1):(j_end2 + 1)))
       dt_div_damper_param = dt * div_damper_param
!$omp parallel do private(i, j)
       do j = j_start2, j_end2
          do i = i_start, i_end
             div(i, j) = (motion(i + 1, j, 1, jt2, 1) - motion(i - 1, j, 1, jt2, 1)) * inv_gridx * 0.5d0 &
                  &    + (motion(i, j + 1, 1, jt2, 2) - motion(i, j - 1, 1, jt2, 2)) * inv_gridy * 0.5d0
          end do
          div(i_start - 1, j) = div(i_start, j)
          div(i_end + 1, j)   = div(i_end, j)
       end do
!$omp end parallel do
       div(i_start:i_end, j_start2 - 1) = div(i_start:i_end, j_start2)
       div(i_start:i_end, j_end2 + 1) = div(i_start:i_end, j_end2)
    end if

!$omp parallel do private(i, j, adv_a, adv_b, adv_c, adv_d)
    do j = j_start2, j_end2
       do i = i_start, i_end

          ! 1st-order upwind advection
          if(motion(i, j, 1, jt2, 1) > 0.0d0) then
             adv_a = 0
             adv_b = -1
          else
             adv_a = 1
             adv_b = 0
          end if
          if(motion(i, j, 1, jt2, 2) > 0.0d0) then
             adv_c = 0 
             adv_d = -1
          else
             adv_c = 1
             adv_d = 0
          end if
          adams_t(i, j, 1, i_adams(1), 1:2) = -dt * ( &
               &   motion(i, j, 1, jt2, 1) * inv_gridx * (motion(i + adv_a, j, 1, jt2, 1:2) - motion(i + adv_b, j, 1, jt2, 1:2)) &
               & + motion(i, j, 1, jt2, 2) * inv_gridy * (motion(i, j + adv_c, 1, jt2, 1:2) - motion(i, j + adv_d, 1, jt2, 1:2)))

          ! divergence damper
          if(switching_div_damper == 1) then
             adams_t(i, j, 1, i_adams(1), 1) = adams_t(i, j, 1, i_adams(1), 1) + dt_div_damper_param * (div(i + 1, j) - div(i - 1, j)) * inv_gridx * 0.5d0
             adams_t(i, j, 1, i_adams(1), 2) = adams_t(i, j, 1, i_adams(1), 2) + dt_div_damper_param * (div(i, j + 1) - div(i, j - 1)) * inv_gridy * 0.5d0
          end if
       end do

       if(it < 3 .or. mod(it, 10) .eq. 0) then
          motion(i_start:i_end, j, 1, jt1, 1:2) = motion(i_start:i_end, j, 1, jt2, 1:2) + adams_t(i_start:i_end, j, 1, i_adams(1), 1:2)
       else
          if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
             motion(i_start:i_end, j, 1, jt1, 1:2) = motion(i_start:i_end, j, 1, jt2, 1:2) &
                  & + 23.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), 1:2) &
                  & - 16.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(2), 1:2) &
                  & +  5.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(3), 1:2)
          elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
             motion(i_start:i_end, j, 1, jt1, 1:2) = motion(i_start:i_end, j, 1, jt2, 1:2) &
                  & + 0.5d0 * (3.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), 1:2) - adams_t(i_start:i_end, j, 1, i_adams(2), 1:2))
          end if
       end if
    end do
!$omp end parallel do

    if(mpiprocs > 1 .and. (.not. last)) &
         & call exchange_real4_halo_by_mpi_sendrecv_start( &
         & j0, j1, motion, i_start, i_end, j_start2, j_end2, 1, 1, jt1, 3, 1, mpi_buf, mpi_req_wind_halo)

    if(allocated(div)) deallocate(div)
    return
  end subroutine  burgers_fluid2d

  subroutine burgers_fluid2d_free_mpi_buf
    if(allocated(mpi_buf)) deallocate(mpi_buf)
  end subroutine burgers_fluid2d_free_mpi_buf
end module burgers2d
