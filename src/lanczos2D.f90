subroutine lanczos_filtering2d()
  use variables
  implicit none
  integer i, j, a
  double precision new_x
  double precision, allocatable, dimension(:, :, :, :) :: motion_t_filter, tmp_motion_t_filter
  double precision, dimension(1, 2) :: motion_t_filter_mean
  integer aa1, MM1
  double precision fc   ! critical angular frequency in [0,pi]
  double precision, allocatable, dimension(:) :: weight_lan
  integer i_start, i_end

  allocate(motion_t_filter((1 - margin_x):(dx + margin_x), dy, 1, 2), &
       &   tmp_motion_t_filter((1 - margin_x):(dx + margin_x), dy, 1, 2))
  do a = 1, 2
     motion_t_filter_mean(1, a) = sum(motion_t(1:dx, 1:dy, 1, 1, a)) / (dx * dy)
     !compute anomaly
     motion_t_filter(:, :, 1, a) = motion_t(:, :, 1, 1, a) - motion_t_filter_mean(1, a)
  end do

  !---------------------------------lanczos filtering--------------------------------------------------------
  ! lanczos filtering on motion vectors
  MM1 = lanczos_window
  fc = (4.0d0 * atan(1.0d0)) / lanczos_critical_wavelength ! PI/(wavelength)
  write(*, *) "lanczos_window = ", lanczos_window
  write(*, *) "lanczos_critical_wavelength = ", lanczos_critical_wavelength

  allocate(weight_lan(2 * MM1 + 1))
  call lanczos_filter_weight(weight_lan, fc, MM1)
  write(*, *) "lanczos sum of weight: ", sum(weight_lan)

  i_start = 1 - margin_x + MM1
  i_end = dx + margin_x - MM1

!!! X
!$omp parallel do private(i, j, a, aa1, new_x)
  do j = 1 + MM1, dy - MM1
     do a = 1, 2 !!! U, V
        do i = i_start, i_end
           new_x = 0.0d0
           do aa1 = -MM1, MM1
              new_x = new_x + motion_t_filter(i + aa1, j, 1, a) * weight_lan(MM1 + aa1 + 1)
           end do
           tmp_motion_t_filter(i, j, 1, a) = new_x
        end do
     end do
  end do
!$omp end parallel do

!$omp parallel do private(j)
  do j = 1 + MM1, dy - MM1
     motion_t_filter(i_start:i_end, j, 1, 1) = tmp_motion_t_filter(i_start:i_end, j, 1, 1)
     motion_t_filter(i_start:i_end, j, 1, 2) = tmp_motion_t_filter(i_start:i_end, j, 1, 2)
  end do
!$omp end parallel do

!!! Y
!$omp parallel do private(i, j, a, aa1, new_x)
  do j = 1 + MM1, dy - MM1
     do a = 1, 2 !!! U, V
        do i = i_start, i_end
           new_x = 0.0d0
           do aa1 = -MM1, MM1
              new_x = new_x + motion_t_filter(i, j + aa1, 1, a) * weight_lan(MM1 + aa1 + 1)
           end do
           tmp_motion_t_filter(i, j, 1, a) = new_x
        end do
     end do
  end do
!$omp end parallel do

!$omp parallel do private(j)
  do j = 1 + MM1, dy - MM1
     motion_t_filter(i_start:i_end, j, 1, 1) = tmp_motion_t_filter(i_start:i_end, j, 1, 1)
     motion_t_filter(i_start:i_end, j, 1, 2) = tmp_motion_t_filter(i_start:i_end, j, 1, 2)
  end do
!$omp end parallel do

!!! ANOMALY TO FULL FIELD
!$omp parallel do private(j)
  do j = 1, dy
     motion_t_filter(:, j, 1, 1) = motion_t_filter(:, j, 1, 1) + motion_t_filter_mean(1, 1)
     motion_t_filter(:, j, 1, 2) = motion_t_filter(:, j, 1, 2) + motion_t_filter_mean(1, 2)
  end do
!$omp end parallel do
  !write(*, *) "max change:", maxval(abs(motion_t_filter(:, :, :, 1) - motion_t(:, :, :, 1, 1)))  

  !------------------------use average nearest neighbours on margins----------------------------------
  i_start = 2 - margin_x
  i_end = dx - 1 + margin_x

  !SOUTH
  call lanczos_filtering2d_5point_mean(i_start, i_end, 2, MM1)
  !NORTH
  call lanczos_filtering2d_5point_mean(i_start, i_end, dy - MM1 + 1, dy - 1)

  if(boundary_type .ne. 1) then !NOT CYCLIC IN X
     !WEST
     call lanczos_filtering2d_5point_mean(2, MM1, MM1 + 1, dy - MM1)
     !EAST
     call lanczos_filtering2d_5point_mean(dx - MM1 + 1, dx - 1, MM1 + 1, dy - MM1)
  end if

!$omp parallel do private(j)
  do j = 1, dy
     motion_t(:, j, 1, 1, 1:2) = motion_t_filter(:, j, 1, 1:2)
  end do
!$omp end parallel do

  deallocate(motion_t_filter, tmp_motion_t_filter, weight_lan)

  contains
    subroutine lanczos_filtering2d_5point_mean(i0, i1, j0, j1)
      integer, intent(in) :: i0, i1, j0, j1
!$omp parallel do private(i, j, a)
      do j = j0, j1
         do a = 1, 2 !!! U, V
            do i = i0, i1
               motion_t_filter(i, j, 1, a) = (motion_t(i, j, 1, 1, a) &
                    &                         + motion_t(i - 1, j, 1, 1, a) + motion_t(i + 1, j, 1, 1, a) &
                    &                         + motion_t(i, j - 1, 1, 1, a) + motion_t(i, j + 1, 1, 1, a)) / 5.0d0
            end do
         end do
      end do
!$omp end parallel do
    end subroutine lanczos_filtering2d_5point_mean

end subroutine lanczos_filtering2d

