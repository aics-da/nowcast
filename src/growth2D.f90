subroutine growth_rain2d(j0, j1, motion)
  use variables
  implicit none

  integer, intent(in) :: j0, j1
  real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
  integer i, j
  integer jt1, jt2
  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  !1st order upwind scheme
  !boundary condition!

  growthmax = maxval(growth_t(1:dx, 1:dy, 1:dz, jt2, 1))
  write(114, *) it, growthmax

!$omp parallel
!$omp do private(i)
  do i = 1, dx
     !x
     growth_t(i, 1,  1, jt2, 1) = growth_t(i, 2,      1, jt2, 1)
     growth_t(i, dy, 1, jt2, 1) = growth_t(i, dy - 1, 1, jt2, 1)
  end do
!$omp end do

!$omp do private(j)
  do j = 1, dy
     !y
     growth_t(1,  j, 1, jt2, 1) = growth_t(2,      j, 1, jt2, 1)
     growth_t(dx, j, 1, jt2, 1) = growth_t(dx - 1, j, 1, jt2, 1)
  end do
!$omp end do

!$omp do private(i, j, adv_a, adv_b, adv_c, adv_d)
  do j = 2, dy - 1
     do i = 2, dx - 1

        if(motion(i, j, 1, jt2, 1) > 0.0d0) then
           adv_a = 0  !1st upwind
           adv_b = -1 !1st upwind
        else
           adv_a = 1 !1st upwind
           adv_b = 0 !1st upwind
        end if

        if(motion(i, j, 1, jt2, 2) > 0.0d0) then
           adv_c = 0  !1st upwind
           adv_d = -1 !1st upwind
        else
           adv_c = 1 !1st upwind
           adv_d = 0 !1st upwind
        end if

        !adams
        adams_t(i, j, 1, i_adams(1), 5) = -dt * ( &
             &   motion(i, j, 1, jt2, 1) * inv_gridx * (growth_t(i + adv_a, j, 1, jt2, 1) - growth_t(i + adv_b, j, 1, jt2, 1)) &
             & + motion(i, j, 1, jt2, 2) * inv_gridy * (growth_t(i, j + adv_c, 1, jt2, 1) - growth_t(i, j + adv_d, 1, jt2, 1)))
     end do !i

     if(it < 3 .or. mod(it, 10) .eq. 0) then
        growth_t(3:(dx - 2), j, 1, jt1, 1) = growth_t(3:(dx - 2), j, 1, jt2, 1) + adams_t(3:(dx - 2), j, 1, i_adams(1), 5)
     else
        if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
           growth_t(3:(dx - 2), j, 1, jt1, 1) = growth_t(3:(dx - 2), j, 1, jt2, 1) &
                & + 23.0d0 / 12.0d0 * adams_t(3:(dx - 2), j, 1, i_adams(1), 5) &
                & - 16.0d0 / 12.0d0 * adams_t(3:(dx - 2), j, 1, i_adams(2), 5) &
                & +  5.0d0 / 12.0d0 * adams_t(3:(dx - 2), j, 1, i_adams(3), 5)
        elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
           growth_t(3:(dx - 2), j, 1, jt1, 1) = growth_t(3:(dx - 2), j, 1, jt2, 1) &
                & + 0.5d0 * (3.0d0 * adams_t(3:(dx - 2), j, 1, i_adams(1), 5) - adams_t(3:(dx - 2), j, 1, i_adams(2), 5))
        end if
     end if
  end do !j
!$omp end do
!$omp end parallel
end subroutine  growth_rain2d
