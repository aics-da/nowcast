module module_output
  use netcdf
  use variables
  use module_io_common
  use module_output_nc
  implicit none

  public

contains

  subroutine open_output_files(counter, l_rain, l_uvw, l_growth)
    integer, intent(in), optional :: counter
    logical, intent(in), optional :: l_rain, l_uvw, l_growth
    character(3) citer
    character(4) split
    logical :: l_rain_l = .true.
    logical :: l_uvw_l = .true.
    logical :: l_growth_l = .true.

    if(split_history == 1 .and. .not. present(counter)) then
       write(*, *) "error in open_output_files: counter is not specified"
       stop 999
    end if
    if(present(l_rain)) l_rain_l = l_rain
    if(present(l_uvw)) l_uvw_l = l_uvw
    if(present(l_growth)) l_growth_l = l_growth

    write(citer, '(I3.3)') start + iter - 1
    if(split_history == 1) then
       write(split, '("_", I3.3)') counter
    else
       split = ""
    end if
    fname1 = 'raindata_' // citer // trim(split) // '.bin'
    fname2 = 'burgersU_' // citer // trim(split) // '.bin'
    fname3 = 'burgersV_' // citer // trim(split) // '.bin'
    fname4 = 'burgersW_' // citer // trim(split) // '.bin'
    fname5 = 'growth_'   // citer // trim(split) // '.bin'
    if(io_proc_num(0) == myrank .and. l_rain_l) &
         & open(111, file = trim(fname1), access = 'stream', form = 'unformatted', convert = "little_endian")
    if(io_proc_num(1) == myrank .and. l_uvw_l) &
         & open(222, file = trim(fname2), access = 'stream', form = 'unformatted', convert = "little_endian")
    if(io_proc_num(2) == myrank .and. l_uvw_l) &
         & open(333, file = trim(fname3), access = 'stream', form = 'unformatted', convert = "little_endian")
    if(io_proc_num(3) == myrank .and. l_uvw_l) &
         & open(444, file = trim(fname4), access = 'stream', form = 'unformatted', convert = "little_endian")
    if(switching_growth == 1 .and. io_proc_num(4) == myrank .and. l_growth_l) &
         & open(555, file = trim(fname5), access = 'stream', form = 'unformatted', convert = "little_endian")
  end subroutine open_output_files

  subroutine close_output_files(l_rain, l_uvw, l_growth)
    logical, intent(in), optional :: l_rain, l_uvw, l_growth
    logical :: l_rain_l = .true.
    logical :: l_uvw_l = .true.
    logical :: l_growth_l = .true.
    if(present(l_rain)) l_rain_l = l_rain
    if(present(l_uvw)) l_uvw_l = l_uvw
    if(present(l_growth)) l_growth_l = l_growth
    if(io_proc_num(0) == myrank .and. l_rain_l) close(111)
    if(io_proc_num(1) == myrank .and. l_uvw_l) close(222)
    if(io_proc_num(2) == myrank .and. l_uvw_l) close(333)
    if(io_proc_num(3) == myrank .and. l_uvw_l) close(444)
    if(switching_growth == 1 .and. io_proc_num(4) == myrank .and. l_growth_l) close(555)
  end subroutine close_output_files

  subroutine output(l_out_rain, l_out_uvw, l_out_growth)
    logical, intent(in) :: l_out_rain, l_out_uvw, l_out_growth
    integer j, k
    integer tmptime1, tmptime2, timerate, timemax

    if(myrank == 0) then
       write(*, *) "output t = ", (it - 1) * dt + 1
       write(*, '("output start = ", I4, ", iter = ", I4, ", it = ", I6, ", t = ", F10.1)') start, iter, it, (it - 1) * dt + 1
    end if

    if(l_out_rain .and. io_proc_num(0) == myrank) then
       call system_clock(tmptime1, timerate, timemax)
       write(111) rain_t_single((o_offset_x + 1):(o_offset_x + ox), &
            &                   (o_offset_y + 1):(o_offset_y + oy), &
            &                   (o_offset_z + 1):(o_offset_z + oz))
       call system_clock(tmptime2, timerate, timemax)
       time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)
    end if

    if(l_out_uvw .and. io_proc_num(1) == myrank) then
       do k = 1, dz
          do j = 1, dy
             write(222) motion_t(1:dx, j, k, it2jt(it, 1), 1)
          end do !j
       end do !k
    end if
    if(l_out_uvw .and. io_proc_num(2) == myrank) then
       do k = 1, dz
          do j = 1, dy
             write(333) motion_t(1:dx, j, k, it2jt(it, 1), 2)
          end do !j
       end do !k
    end if
    if(l_out_uvw .and. io_proc_num(3) == myrank) then
       do k = 1, dz
          do j = 1, dy
             write(444) motion_t(1:dx, j, k, it2jt(it, 1), 3)
          end do !j
       end do !k
    end if
    if(switching_growth == 1 .and. l_out_growth .and. io_proc_num(4) == myrank) then
       do k = 1, dz
          do j = 1, dy
             write(555) growth_t(1:dx, j, k, it2jt(it, 1), 1)
          end do !j
       end do !k
    end if
  end subroutine output

  subroutine output_vector_file(label, output_uvw, output_growth, output_quality, output_corr_diff)
    character(*), intent(in) :: label
    logical, intent(in) :: output_uvw, output_growth, output_quality, output_corr_diff

    if(output_vector == 1) then
       call output_vector_single(label, output_uvw, output_growth, output_quality, output_corr_diff)
    else if(output_vector == 4) then
       call output_vector_nc(label, output_uvw, output_growth, output_quality, output_corr_diff)
    end if
  end subroutine output_vector_file

  subroutine output_vector_single(label, output_uvw, output_growth, output_quality, output_corr_diff)
    character(*), intent(in) :: label
    logical, intent(in) :: output_uvw, output_growth, output_quality, output_corr_diff
    character(3) citer
    integer k
    !OUTPUT VECTOR
    if(myrank .ne. 0) return
    write(citer, '(I3.3)') start + iter - 1
    if(output_uvw .eqv. .true.) then
       open(33, file = 'vector_u_' // trim(label) // citer //'.bin', access = "stream", form = 'unformatted', convert = "little_endian")
       do k = 1, dz
          write(33) motion_t(1:dx, 1:dy, k, 1, 1)
       end do
       close(33)
       open(33, file = 'vector_v_' // trim(label) // citer //'.bin', access = "stream", form = 'unformatted', convert = "little_endian")
       do k = 1, dz
          write(33) motion_t(1:dx, 1:dy, k, 1, 2)
       end do
       close(33)
       open(33, file = 'vector_w_' // trim(label) // citer //'.bin', access = "stream", form = 'unformatted', convert = "little_endian")
       do k = 1, dz
          write(33) motion_t(1:dx, 1:dy, k, 1, 3)
       end do
       close(33)
    end if
    if(output_growth .eqv. .true.) then
       open(66, file = 'initial_growth_' // trim(label) // citer //'.bin', access = "stream", form = 'unformatted', convert = "little_endian")
       do k = 1, dz
          write(66) growth_t(1:dx, 1:dy, k, 1, 1)
       end do
       close(66)
    end if
    if(output_quality .eqv. .true.) then
       open(66, file = 'quality_' // trim(label) // citer //'.bin', access = "stream", form = 'unformatted', convert = "little_endian")
       do k = 1, dz
          write(66) quality(1:dx, 1:dy, k)
       end do
       close(66)
    end if
    if(output_corr_diff .eqv. .true.) then
       open(66, file = 'corrdiff_' // trim(label) // citer //'.bin', access = "stream", form = 'unformatted', convert = "little_endian")
       do k = 1, dz
          write(66) corr_diff(1:dx, 1:dy, k)
       end do
       close(66)
    end if
  end subroutine output_vector_single
end module module_output
