#include <stdlib.h>
#include <stdio.h>
#include <lz4.h>

int write_lz4_file(char* fname, float* dat, size_t size){
  int srcsize, bufsize, stat;
  char *outbuf;
  FILE *fd;
  const int acceleration = 10;

  stat = 0;
  srcsize = sizeof(float) * size;
  bufsize = LZ4_compressBound(srcsize);
  if((outbuf = (char*)malloc(bufsize)) != NULL){
    bufsize = LZ4_compress_fast((char*)dat, outbuf, srcsize, bufsize, acceleration);
    if(bufsize != 0){
      printf("writing %s\n", fname);
      if((fd = fopen(fname, "wb")) != NULL){
        if(fwrite(outbuf, sizeof(char), bufsize, fd) == bufsize){
          stat = 1;
        } else {
          printf("write_lz4_file: fwrite failed\n");
        }
        fclose(fd);
      } else {
        printf("write_lz4_file: fopen failed\n");
      }
    } else {
      printf("write_lz4_file: LZ4_compress_fast failed\n");
    }
  } else {
    printf("write_lz4_file: malloc failed\n");
  }

  free(outbuf);
  return stat;
}

int read_lz4_file(char* fname, float* dat, size_t size){
  int destsize, bufsize, stat;
  char *inbuf;
  FILE *fd;

  stat = 0;
  destsize = sizeof(float) * size;
  bufsize = LZ4_compressBound(destsize);
  if((inbuf = (char*)malloc(bufsize)) != NULL){
    printf("reading %s\n", fname);
    if((fd = fopen(fname, "rb")) != NULL){
      if((bufsize = fread(inbuf, sizeof(char), bufsize, fd)) > 0){
        destsize = LZ4_decompress_safe(inbuf, (char*)dat, bufsize, destsize);
        if(destsize > 0){
          stat = 1;
        } else {
          printf("read_lz4_file: LZ4_decompress_safe failed\n");
        }
      } else {
        printf("read_lz4_file: fread failed\n");
      }
      fclose(fd);
    } else {
      printf("read_lz4_file: fopen failed\n");
    }
  } else {
    printf("read_lz4_file: malloc failed\n");
  }

  free(inbuf);
  return stat;
}
