subroutine burgers_weno(j0, j1, motion, last)
  use variables
  use weno_core
  use nowcast_mpi_utils
  implicit none

  integer, intent(in) :: j0, j1
  real(4), intent(inout) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
  logical, intent(in) :: last
  real(4), save, allocatable :: mpi_buf(:, :, :, :, :)
  integer i, j, k, a
  integer :: i_start, i_end, j_start, j_end, k_start, k_end
  integer jt1, jt2
  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  i_start = 3 - margin_x_adv
  i_end = dx - 2 + margin_x_adv
  j_start = j_mpi(1, myrank)
  j_end   = j_mpi(2, myrank)
  if(myrank == 0) j_start = 3
  if(myrank == mpiprocs - 1) j_end = dy - 2
  k_start = 3
  k_end = dz - 2
  if(mpiprocs > 1) &
     & call exchange_real4_halo_by_mpi_sendrecv_wait( &
     & j0, j1, motion, i_start, i_end, j_start, j_end, k_start, k_end, jt2, 3, 1, mpi_buf, mpi_req_wind_halo)

  do a = 1, 3
     call burgers_weno_core(a)
  end do

  i_start = 3 - margin_x_adv
  i_end = dx - 2 + margin_x_adv
  j_start = j_mpi(1, myrank)
  j_end   = j_mpi(2, myrank)
  if(myrank == 0) j_start = 3
  if(myrank == mpiprocs - 1) j_end = dy - 2
  k_start = 3
  k_end = dz - 2
  if(mpiprocs > 1 .and. (.not. last)) &
       & call exchange_real4_halo_by_mpi_sendrecv_start( &
       & j0, j1, motion, i_start, i_end, j_start, j_end, k_start, k_end, jt1, 3, 2, mpi_buf, mpi_req_wind_halo)

contains

  subroutine burgers_weno_core(iuvw)
    integer, intent(in) :: iuvw

    !5th order weno
    !Dweno	
    i_start = 2 - margin_x_adv
    i_end = dx - 1 + margin_x_adv
    j_start = j_mpi(1, myrank) - 2
    j_end   = j_mpi(2, myrank) + 2
    if(j_start < 2) j_start = 2
    if(j_end > dy - 1) j_end = dy - 1
!$omp parallel do private(i, j, k)
    do k = 2, dz - 1
       do j = j_start, j_end
          do i = i_start, i_end
             if(motion(i, j, k, jt2, 1) > 0) then
                Dweno(i, j, k, 1) = (motion(i,     j, k, jt2, iuvw) - motion(i - 1, j, k, jt2, iuvw)) * inv_gridx
             else
                Dweno(i, j, k, 1) = (motion(i + 1, j, k, jt2, iuvw) - motion(i,     j, k, jt2, iuvw)) * inv_gridx
             end if
             if(motion(i, j, k, jt2, 2) > 0) then
                Dweno(i, j, k, 2) = (motion(i, j,     k, jt2, iuvw) - motion(i, j - 1, k, jt2, iuvw)) * inv_gridy
             else
                Dweno(i, j, k, 2) = (motion(i, j + 1, k, jt2, iuvw) - motion(i, j,     k, jt2, iuvw)) * inv_gridy
             end if
             if(motion(i, j, k, jt2, 3) > 0) then
                Dweno(i, j, k, 3) = (motion(i, j, k,     jt2, iuvw) - motion(i, j, k - 1, jt2, iuvw)) * inv_gridz
             else
                Dweno(i, j, k, 3) = (motion(i, j, k + 1, jt2, iuvw) - motion(i, j, k,     jt2, iuvw)) * inv_gridz
             end if
          end do
       end do
    end do
!$omp end parallel do

    call weno_dweno_boundary_condition

    i_start = 3 - margin_x_adv
    i_end = dx - 2 + margin_x_adv
    j_start = j_mpi(1, myrank)
    j_end   = j_mpi(2, myrank)
    if(myrank == 0) j_start = 3
    if(myrank == mpiprocs - 1) j_end = dy - 2
    k_start = 3
    k_end = dz - 2

    call weno_calc_adams(j0, j1, motion, i_start, i_end, j_start, j_end, k_start, k_end, jt2, iuvw)

!$omp parallel do private(i, j, k)
    do k = k_start, k_end
       if(it < 3 .or. mod(it, 10) .eq. 0) then
          motion(i_start:i_end, j_start:j_end, k, jt1, iuvw) &
               & = motion(i_start:i_end, j_start:j_end, k, jt2, iuvw) + adams_t(i_start:i_end, j_start:j_end, k, i_adams(1), iuvw)
       else
          if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
             motion(i_start:i_end, j_start:j_end, k, jt1, iuvw) = motion(i_start:i_end, j_start:j_end, k, jt2, iuvw) &
                  & + 23.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(1), iuvw) &
                  & - 16.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(2), iuvw) &
                  & +  5.0d0 / 12.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(3), iuvw)
          elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
             motion(i_start:i_end, j_start:j_end, k, jt1, iuvw) = motion(i_start:i_end, j_start:j_end, k, jt2, iuvw) &
                  & + 0.5d0 * (3.0d0 * adams_t(i_start:i_end, j_start:j_end, k, i_adams(1), iuvw) - adams_t(i_start:i_end, j_start:j_end, k, i_adams(2), iuvw))
          end if
       end if
    end do !k
!$omp end parallel do
  end subroutine burgers_weno_core

end subroutine burgers_weno
