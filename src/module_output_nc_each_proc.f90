module module_output_nc_each_proc
  use netcdf
  use variables
  use module_io_common
  use module_nc_common
  implicit none

  integer ncid_rain, ncid_u, ncid_v, ncid_w, ncid_growth
  integer varid_rain, varid_u, varid_v, varid_w, varid_growth

  private ncid_rain, ncid_u, ncid_v, ncid_w, ncid_growth
  private varid_rain, varid_u, varid_v, varid_w, varid_growth

  public

contains

  subroutine open_output_files_nc_each_proc(j_start, j_end, counter, l_rain, l_uvw, l_growth)
    integer, intent(in) :: j_start, j_end
    integer, intent(in), optional :: counter
    logical, intent(in), optional :: l_rain, l_uvw, l_growth
    character(3) citer
    character(6) crank
    character(4) split
    logical :: l_rain_l = .true.
    logical :: l_uvw_l = .true.
    logical :: l_growth_l = .true.
    integer oj_start, oj_end

    if(split_history == 1 .and. .not. present(counter)) then
       write(*, *) "error in open_output_files: counter is not specified"
       stop 999
    end if
    if(present(l_rain)) l_rain_l = l_rain
    if(present(l_uvw)) l_uvw_l = l_uvw
    if(present(l_growth)) l_growth_l = l_growth

    write(citer, '(I3.3)') start + iter - 1
    write(crank, '(I6.6)') myrank
    if(split_history == 1) then
       write(split, '("_", I3.3)') counter
    else
       split = ""
    end if
    fname1 = 'raindata_' // citer // '_' // crank // trim(split) // '.nc'
    fname2 = 'burgersU_' // citer // '_' // crank // trim(split) // '.nc'
    fname3 = 'burgersV_' // citer // '_' // crank // trim(split) // '.nc'
    fname4 = 'burgersW_' // citer // '_' // crank // trim(split) // '.nc'
    fname5 = 'growth_'   // citer // '_' // crank // trim(split) // '.nc'
    if(l_rain_l) then
       oj_start = max(j_start, o_offset_y + 1) !CROP OUTPUT DOMAIN
       oj_end   = min(j_end,   o_offset_y + oy)
       call define_nc(fname1, ncid_rain, "rain", "", varid_rain, ox, oj_start - o_offset_y, oj_end - o_offset_y, oz)
    end if
    if(l_uvw_l)  call define_nc(fname2, ncid_u, "u", "m s-1", varid_u, dx, j_start, j_end, dz)
    if(l_uvw_l)  call define_nc(fname3, ncid_v, "v", "m s-1", varid_v, dx, j_start, j_end, dz)
    if(l_uvw_l)  call define_nc(fname4, ncid_w, "w", "m s-1", varid_w, dx, j_start, j_end, dz)
    if(switching_growth == 1 .and. l_growth_l) &
         &       call define_nc(fname5, ncid_growth, "growth", "", varid_growth, dx, j_start, j_end, dz)
  end subroutine open_output_files_nc_each_proc

  subroutine close_output_files_nc_each_proc(l_rain, l_uvw, l_growth)
    logical, intent(in), optional :: l_rain, l_uvw, l_growth
    logical :: l_rain_l = .true.
    logical :: l_uvw_l = .true.
    logical :: l_growth_l = .true.
    if(present(l_rain)) l_rain_l = l_rain
    if(present(l_uvw)) l_uvw_l = l_uvw
    if(present(l_growth)) l_growth_l = l_growth
    if(l_rain_l) call check_nc_err(nf90_close(ncid_rain))
    if(l_uvw_l)  call check_nc_err(nf90_close(ncid_u))
    if(l_uvw_l)  call check_nc_err(nf90_close(ncid_v))
    if(l_uvw_l)  call check_nc_err(nf90_close(ncid_w))
    if(switching_growth == 1 .and. l_growth_l) call check_nc_err(nf90_close(ncid_growth))
  end subroutine close_output_files_nc_each_proc

  subroutine output_nc_each_proc(l_out_rain, l_out_uvw, l_out_growth, output_count, j_start, j_end)
    logical, intent(in) :: l_out_rain, l_out_uvw, l_out_growth
    integer, intent(in) :: output_count, j_start, j_end
    integer tmptime1, tmptime2, timerate, timemax
    integer nc_count(4), nc_start(4)
    integer oj_start, oj_end

    if(split_history == 1) then
       nc_start = (/ 1, 1, 1, 1 /)
    else
       nc_start = (/ 1, 1, 1, output_count /)
    end if

    call system_clock(tmptime1, timerate, timemax)
    if(l_out_rain) then
       oj_start = max(j_start, o_offset_y + 1) !CROP OUTPUT DOMAIN
       oj_end   = min(j_end,   o_offset_y + oy)
       nc_count = (/ ox, oj_end - oj_start + 1, oz, 1 /)
       write(*, '("myrank = ", I10, ", output nc rain, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_rain, varid_rain, &
            &                         rain_t_single((o_offset_x + 1):(o_offset_x + ox), &
            &                                       oj_start:oj_end, &
            &                                       (o_offset_z + 1):(o_offset_z + oz)), &
            &                         start = nc_start, count = nc_count))
    end if

    nc_count = (/ dx, j_end - j_start + 1, dz, 1 /)
    if(l_out_uvw) then
       write(*, '("myrank = ", I10, ", output nc uvw, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_u, varid_u, &
            &                         motion_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 1), &
            &                         start = nc_start, count = nc_count))
       call check_nc_err(nf90_put_var(ncid_v, varid_v, &
            &                         motion_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 2), &
            &                         start = nc_start, count = nc_count))
       call check_nc_err(nf90_put_var(ncid_w, varid_w, &
            &                         motion_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 3), &
            &                         start = nc_start, count = nc_count))
    end if
    if(switching_growth == 1 .and. l_out_growth) then
       write(*, '("myrank = ", I10, ", output nc growth, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_growth, varid_growth, &
            &                         growth_t(1:dx, j_start:j_end, 1:dz, it2jt(it, 1), 1), &
            &                         start = nc_start, count = nc_count))
    end if
    call system_clock(tmptime2, timerate, timemax)
    time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)
  end subroutine output_nc_each_proc
end module module_output_nc_each_proc
