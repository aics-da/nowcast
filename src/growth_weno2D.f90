subroutine growth_rain_weno2d(j0, j1, motion)
  use variables
  use weno_core
  implicit none

  integer, intent(in) :: j0, j1
  real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 3)
  integer i, j, a, i_start, i_end
  integer jt1, jt2
  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  !5th order weno

  !Dweno
  i_start = 2 - margin_x_adv
  i_end = dx - 1 + margin_x_adv
!$omp parallel do private(i, j)
  do j = 2, dy - 1
     do i = i_start, i_end
        if(motion(i, j, 1, jt2, 1) > 0) then
           Dweno(i, j, 1, 1) = growth_t(i, j, 1, jt2, 1) - growth_t(i - 1, j, 1, jt2, 1)
        else
           Dweno(i, j, 1, 1) = growth_t(i + 1, j, 1, jt2, 1) - growth_t(i, j, 1, jt2, 1)
        end if

        if(motion(i, j, 1, jt2, 2) > 0) then
           Dweno(i, j, 1, 2) = growth_t(i, j, 1, jt2, 1) - growth_t(i, j - 1, 1, jt2, 1)
        else
           Dweno(i, j, 1, 2) = growth_t(i, j + 1, 1, jt2, 1) - growth_t(i, j, 1, jt2, 1)
        end if
     end do !i

     Dweno(i_start:i_end, j, 1, 1) = Dweno(i_start:i_end, j, 1, 1) * inv_gridx
     Dweno(i_start:i_end, j, 1, 2) = Dweno(i_start:i_end, j, 1, 2) * inv_gridy
  end do !j
!$omp end parallel do

!$omp parallel do private(j)
  do j = 1, dy
     Dweno(1,  j, 1, 1:3) = Dweno(2,      j, 1, 1:3)
     Dweno(dx, j, 1, 1:3) = Dweno(dx - 1, j, 1, 1:3)
  end do
!$omp end parallel do

  Dweno(1:dx, 1,  1, 1:3) = Dweno(1:dx, 2,      1, 1:3)
  Dweno(1:dx, dy, 1, 1:3) = Dweno(1:dx, dy - 1, 1, 1:3)
  
  i_start = 3 - margin_x_adv
  i_end = dx - 2 + margin_x_adv
!$omp parallel do private(i, j)
  do j = 3, dy - 2
     call weno_calc_adams_2D(j0, j1, motion, i_start, i_end, j, jt2, 5)

     if(it < 3 .or. mod(it, 10) .eq. 0) then
        growth_t(i_start:i_end, j, 1, jt1, 1) = growth_t(i_start:i_end, j, 1, jt2, 1) + adams_t(i_start:i_end, j, 1, i_adams(1), 5)
     else
        if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
           growth_t(i_start:i_end, j, 1, jt1, 1) = growth_t(i_start:i_end, j, 1, jt2, 1) &
                & + 23.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), 5) &
                & - 16.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(2), 5) &
                & +  5.0d0 / 12.0d0 * adams_t(i_start:i_end, j, 1, i_adams(3), 5)
        elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
           growth_t(i_start:i_end, j, 1, jt1, 1) = growth_t(i_start:i_end, j, 1, jt2, 1) &
                & + 0.5d0 * (3.0d0 * adams_t(i_start:i_end, j, 1, i_adams(1), 5) - adams_t(i_start:i_end, j, 1, i_adams(2), 5))
        end if
     end if
  end do
!$omp end parallel do

end subroutine growth_rain_weno2d
