module advection_weno
  use variables
  use weno_core
  use nowcast_mpi_utils

  implicit none
 
  integer, private :: i, j, k, a
  integer, private :: jt1, jt2
  integer, private :: time1, time2, time3, time4, timerate, timemax
  integer, private, parameter :: halo_len = 3

  public

contains
  subroutine advection(j0, j1, rain, jj0, jj1, motion, last)
    integer, intent(in) :: j0, j1, jj0, jj1
    double precision, intent(inout) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)
    real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), jj0:jj1, dz, maxnt, 3)
    logical, intent(in) :: last
    integer :: is, ie, js, je, ks, ke, jsbody, jebody

    !real rainmin, rainmax, wmax

    !INDEX CONVERTER
    jt1 = it2jt(it, 1) !it
    if(it == 2) then
       jt2 = 1 !INITIAL
    else
       jt2 = it2jt(it, 2) !it - 1
    end if

  !5th order weno
!!$omp parallel workshare
  !CFL = maxval(abs(motion(1:dx, 1:dy, 1:dz, jt2, 1:3))) * dt * inv_gridx
  !rainmin = minval(rain(1:dx, 1:dy, 1:dz, jt2, 1))
  !rainmax = maxval(rain(1:dx, 1:dy, 1:dz, jt2, 1))
  !wmax = maxval(motion(1:dx, 1:dy, 1:dz, jt2, 3))
!!$omp end parallel workshare
  !write(*, *) it, CFL, maxval(rain(1:dx, 1:dy, 1:dz, jt2, 1)), maxval(motion(1:dx, 1:dy, 1:dz, jt2, 3))

  !if(myrank == 0) &
  !     & write(*, '("myrank:", I5, ", it = ", I6, " CFL ", F10.5, " rain max ", F10.5, " min ", F10.5, " w max ", F10.5)') &
  !     !& myrank, it, CFL, rainmax, rainmin, wmax
  !     & myrank, it, CFL, maxval(rain(1:dx, 1:dy, 1:dz, jt2, 1)), minval(rain(1:dx, 1:dy, 1:dz, jt2, 1)), maxval(motion(1:dx, 1:dy, 1:dz, jt2, 3))

    is = 3 - margin_x_adv
    ie = dx - 2 + margin_x_adv
    js = max(j_mpi(1, myrank), 3)
    je = min(j_mpi(2, myrank), dy - 2)
    ks = 3
    ke = dz - 2

    jsbody = js + halo_len
    jebody = je - halo_len
    if(myrank == 0) jsbody = js
    if(myrank == mpiprocs - 1) jebody = je
    if(jsbody .le. jebody) then
       !computation except halo
       call advection_weno_main(j0, j1, rain, jj0, jj1, motion, &
            &                   is, ie, jsbody, jebody, ks, ke)
    end if

    if(mpiprocs > 1) then
       !exchange halo
       call system_clock(time1, timerate, timemax)
       call exchange_rain_halo_by_mpi_sendrecv_wait(j0, j1, rain, &
            & 3 - margin_x_adv, dx - 2 + margin_x_adv, & !x range
            & j_mpi(1, myrank), j_mpi(2, myrank),      & !y range (MPI)
            & 3,                dz - 2,                & !z range
            & jt2, halo_len)
       call system_clock(time2, timerate, timemax)
       time_adv_weno_mpi = time_adv_weno_mpi + (time2 - time1) / dble(timerate)

       !computation for halo
       if(js < jsbody) &
            & call advection_weno_main(j0, j1, rain, jj0, jj1, motion, &
            &                          is, ie, js, jsbody - 1, ks, ke)
       if(jebody < je) &
            & call advection_weno_main(j0, j1, rain, jj0, jj1, motion, &
            &                          is, ie, jebody + 1, je, ks, ke)

       !send halo
       if(.not. last) then
          call system_clock(time1, timerate, timemax)
          call exchange_rain_halo_by_mpi_sendrecv_start(j0, j1, rain, &
               & 3 - margin_x_adv, dx - 2 + margin_x_adv, & !x range
               & j_mpi(1, myrank), j_mpi(2, myrank),      & !y range (MPI)
               & 3,                dz - 2,                & !z range
               & jt1, halo_len)
          call system_clock(time2, timerate, timemax)
          time_adv_weno_mpi = time_adv_weno_mpi + (time2 - time1) / dble(timerate)
       end if
    end if

    return
  end subroutine advection

  subroutine advection_weno_main(j0, j1, rain, jj0, jj1, motion, is, ie, js, je, ks, ke)
    integer, intent(in) :: j0, j1, jj0, jj1, is, ie, js, je, ks, ke
    double precision, intent(inout) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)
    real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), jj0:jj1, dz, maxnt, 3)

    !Dweno
    call system_clock(time1, timerate, timemax)
    call advection_weno_calc_dweno(j0, j1, rain, jj0, jj1, motion, &
         & max(is - 2, 2 - margin_x_adv), min(ie + 2, dx - 1 + margin_x_adv), &  !x range
         & max(js - 2, 2),                min(je + 2, dy - 1), &                 !y range (MPI)
         & max(ks - 2, 2),                min(ke + 2, dz - 1))                   !z range
    call weno_dweno_boundary_condition
    call system_clock(time2, timerate, timemax)
    time_adv_weno_dweno = time_adv_weno_dweno + (time2 - time1) / dble(timerate)

    call weno_calc_adams(jj0, jj1, motion, is, ie, js, je, ks, ke, jt2, 4)

    if(it < 3 .or. mod(it, 10) .eq. 0) then
       call advection_weno_euler(j0, j1, rain, is, ie, js, je, ks, ke)
    else
       if(switching_time_integration .eq. 1) then !3RD-ORDER ADAMS-BASHFORTH
          call advection_weno_AB3(j0, j1, rain, is, ie, js, je, ks, ke)
       elseif(switching_time_integration .eq. 0) then !2ND-ORDER ADAMS-BASHFORTH
          call advection_weno_AB2(j0, j1, rain, is, ie, js, je, ks, ke)
       end if
    end if
  end subroutine advection_weno_main

  subroutine advection_weno_calc_dweno(j0, j1, rain, jj0, jj1, motion, is, ie, js, je, ks, ke)
    integer, intent(in) :: j0, j1, jj0, jj1, is, ie, js, je, ks, ke
    double precision, intent(in) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)
    real(4), intent(in) :: motion((1 - margin_x):(dx + margin_x), jj0:jj1, dz, maxnt, 3)
    double precision d_rain_dx((1 - margin_x_adv):(dx - 1 + margin_x_adv))

!$omp parallel do private(i, j, k, d_rain_dx)
    do k = ks, ke
       do j = js, je
          d_rain_dx((is - 1):ie) = rain(is:(ie + 1), j, k, jt2, 1) - rain((is - 1):ie, j, k, jt2, 1)
          !!!d_rain_dx(i_start - 1) = rain(i_start) - rain(i_start - 1)
          !!!d_rain_dx(i_end) = rain(i_end + 1) - rain(i_end)

          do i = is, ie
             if(motion(i, j, k, jt2, 1) > 0) then
                Dweno(i, j, k, 1) = d_rain_dx(i - 1)
             else
                Dweno(i, j, k, 1) = d_rain_dx(i)
             end if
             if(motion(i, j, k, jt2, 2) > 0) then
                Dweno(i, j, k, 2) = rain(i, j, k, jt2, 1) - rain(i, j - 1, k, jt2, 1)
             else
                Dweno(i, j, k, 2) = rain(i, j + 1, k, jt2, 1) - rain(i, j, k, jt2, 1)
             end if
             if(motion(i, j, k, jt2, 3) > 0) then
                Dweno(i, j, k, 3) = rain(i, j, k, jt2, 1) - rain(i, j, k - 1, jt2, 1)
             else
                Dweno(i, j, k, 3) = rain(i, j, k + 1, jt2, 1) - rain(i, j, k, jt2, 1)
             end if
          end do

          Dweno(is:ie, j, k, 1) = Dweno(is:ie, j, k, 1) * inv_gridx
          Dweno(is:ie, j, k, 2) = Dweno(is:ie, j, k, 2) * inv_gridy
          Dweno(is:ie, j, k, 3) = Dweno(is:ie, j, k, 3) * inv_gridz
       end do !!! j
    end do !!! k
!$omp end parallel do
  end subroutine advection_weno_calc_dweno

  subroutine advection_weno_calc_growth(j0, j1, rain, ii, jj, kk)
    integer, intent(in) :: j0, j1
    double precision, intent(inout) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)
    integer, intent(in) :: ii, jj, kk

    if(tmpgrowth == 1) then
       rain(ii, jj, kk, jt1, 1) = rain(ii, jj, kk, jt1, 1) + growth_t(ii, jj, kk, jt2, 1)
       if((rain(ii, jj, kk, jt1, 1) .le. rain_missing) .and. (growth_t(ii, jj, kk, jt2, 1) .lt. 0.0d0)) then
          growth_t(ii, jj, kk, jt2, 1) = 0.0d0
       end if
    end if
  end subroutine advection_weno_calc_growth

  subroutine advection_weno_euler(j0, j1, rain, is, ie, js, je, ks, ke)
    integer, intent(in) :: j0, j1, is, ie, js, je, ks, ke
    double precision, intent(inout) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)

!$omp parallel do private(i, j, k)
    do k = ks, ke
       do j = js, je
          do i = is, ie
             rain(i, j, k, jt1, 1) = rain(i, j, k, jt2, 1) + adams_t(i, j, k, i_adams(1), 4)
!dir$ forceinline
             call advection_weno_calc_growth(j0, j1, rain, i, j, k)
             if(rain(i, j, k, jt1, 1) < rain_missing) rain(i, j, k, jt1, 1) = rain_missing
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine advection_weno_euler

  subroutine advection_weno_AB3(j0, j1, rain, is, ie, js, je, ks, ke)
    integer, intent(in) :: j0, j1, is, ie, js, je, ks, ke
    double precision, intent(inout) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)

    double precision :: factor(3)
    factor(i_adams(1)) =  23.0d0 / 12.0d0
    factor(i_adams(2)) = -16.0d0 / 12.0d0
    factor(i_adams(3)) =   5.0d0 / 12.0d0

!$omp parallel do private(i, j, k)
    do k = ks, ke
       do j = js, je
          do i = is, ie
             rain(i, j, k, jt1, 1) = rain(i, j, k, jt2, 1) &
                  & + factor(1) * adams_t(i, j, k, 1, 4) &
                  & + factor(2) * adams_t(i, j, k, 2, 4) &
                  & + factor(3) * adams_t(i, j, k, 3, 4)
!dir$ forceinline
             call advection_weno_calc_growth(j0, j1, rain, i, j, k)
             if(rain(i, j, k, jt1, 1) < rain_missing) rain(i, j, k, jt1, 1) = rain_missing
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine advection_weno_AB3

  subroutine advection_weno_AB2(j0, j1, rain, is, ie, js, je, ks, ke)
    integer, intent(in) :: j0, j1, is, ie, js, je, ks, ke
    double precision, intent(inout) :: rain((1 - margin_x):(dx + margin_x), j0:j1, dz, maxnt, 1)

    double precision :: factor(2)
    factor(i_adams(1)) =  3.0d0 * 0.5d0
    factor(i_adams(2)) = -1.0d0 * 0.5d0

!$omp parallel do private(i, j, k)
    do k = ks, ke
       do j = js, je
          do i = is, ie
             rain(i, j, k, jt1, 1) = rain(i, j, k, jt2, 1) &
                  & + factor(1) * adams_t(i, j, k, 1, 4) + factor(2) * adams_t(i, j, k, 2, 4)
          end do !i

          do i = is, ie
!dir$ forceinline
             call advection_weno_calc_growth(j0, j1, rain, i, j, k)
             if(rain(i, j, k, jt1, 1) < rain_missing) rain(i, j, k, jt1, 1) = rain_missing
          end do !i
       end do !j
    end do !k
!$omp end parallel do
  end subroutine advection_weno_AB2
end module advection_weno
