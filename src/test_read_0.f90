subroutine reading
  use variables
  implicit none
  integer it_offset
  
  it_offset = start + iter - 2
  do it = 1, nt
     write(ex1, '(I4.4)') it + it_offset
     
     fname = '/data2/gulan/real_data/rain_3D_real_data' // ex1 // '.bin'
     write(*, *) "reading", fname
     open(10, file = fname, access = 'direct', form = 'binary', recl = ddx * ddy * ddz* 4)
     read(10, rec = 1) true_t(1:ddx, 1:ddy, 1:ddz, it, 1)
     close(10)
     
     do k = 1, ddz
        do j = 1, ddy
           do i = 1, ddx
              if (true_t(i, j, k, it, 1) < 0) then
                 !change -9999.000 to 0
                 true_t(i, j, k, it, 1) = 0 
              endif
           enddo
        enddo
     enddo

  enddo
end subroutine reading
