subroutine boundary
  use variables
  implicit none
  double precision, parameter :: relax_const1 = 0.9
  double precision, parameter :: relax_const2 = 0.1

  integer i, j, k, j_start, j_end
  integer jt1, jt2
  !INDEX CONVERTER
  jt1 = it2jt(it, 1) !it
  if(it == 2) then
     jt2 = 1 !INITIAL
  else
     jt2 = it2jt(it, 2) !it - 1
  end if

  j_start = j_mpi(1, myrank)
  j_end   = j_mpi(2, myrank)

  !1st order upwind scheme
  !boundary condition!

  if (switching_boundary_motion == 0) then
!$omp parallel do private(k)
     do k = 1, dz
        !x
        if(myrank == 0) then
           motion_t(:, 2,      k, jt1, 1:2) = motion_t(:, 3,      k, 1, 1:2) ! FIXED IN TIME
           motion_t(:, 2,      k, jt1, 3)   = 0.0d0! motion_t(i,3,k,1,3)
           motion_t(:, 1,      k, jt1, 1:2) = motion_t(:, 2,      k, 1, 1:2)
           motion_t(:, 1,      k, jt1, 3)   = 0.0d0! motion_t(i,2,k,1,3)
        end if
        if(myrank == mpiprocs - 1) then
           motion_t(:, dy - 1, k, jt1, 1:2) = motion_t(:, dy - 2, k, 1, 1:2)
           motion_t(:, dy - 1, k, jt1, 3)   = 0.0d0! motion_t(i,dy-2,k,1,3)
           motion_t(:, dy,     k, jt1, 1:2) = motion_t(:, dy - 1, k, 1, 1:2)
           motion_t(:, dy,     k, jt1, 3)   = 0.0d0! motion_t(i,dy-1,k,1,3)
        end if

        if(boundary_type == 1) then !CYCLIC IN X
           motion_t((1 - margin_x):0,         j_start:j_end, k, jt1, 1:3) = motion_t((dx - margin_x + 1):dx, j_start:j_end, k, jt1, 1:3)
           motion_t((dx + 1):(dx + margin_x), j_start:j_end, k, jt1, 1:3) = motion_t(1:margin_x,             j_start:j_end, k, jt1, 1:3)
        else
           !y
           motion_t(2,      j_start:j_end, k, jt1, 1:2) = motion_t(3,      j_start:j_end, k, 1, 1:2)
           motion_t(2,      j_start:j_end, k, jt1, 3)   = 0.0d0! motion_t(3,j,k,1,3)
           motion_t(1,      j_start:j_end, k, jt1, 1:2) = motion_t(2,      j_start:j_end, k, 1, 1:2)
           motion_t(1,      j_start:j_end, k, jt1, 3)   = 0.0d0! motion_t(2,j,k,1,3)
           motion_t(dx - 1, j_start:j_end, k, jt1, 1:2) = motion_t(dx - 2, j_start:j_end, k, 1, 1:2)
           motion_t(dx - 1, j_start:j_end, k, jt1, 3)   = 0.0d0! motion_t(dx-2,j,k,1,3)
           motion_t(dx,     j_start:j_end, k, jt1, 1:2) = motion_t(dx - 1, j_start:j_end, k, 1, 1:2)
           motion_t(dx,     j_start:j_end, k, jt1, 3)   = 0.0d0! motion_t(dx-1,j,k,1,3)
        end if
     end do
!$omp end parallel do

     if(dz > 1) then
!$omp parallel do private(j)
        do j = j_start, j_end
           !z
           motion_t(:, j, 1, jt1, 1:2) = motion_t(:, j, 3, jt2, 1:2) !halo = 2 grids
           motion_t(:, j, 2, jt1, 1:2) = motion_t(:, j, 3, jt2, 1:2)
           where(motion_t(:, j, 1, jt1, 3) > 0)
              motion_t(:, j, 1, jt1, 3) = 0.0d0
              motion_t(:, j, 2, jt1, 3) = motion_t(:, j, 3, jt2, 3) * 0.5d0
           elsewhere
              motion_t(:, j, 1, jt1, 3) = motion_t(:, j, 3, jt2, 3)
              motion_t(:, j, 2, jt1, 3) = motion_t(:, j, 3, jt2, 3)
           end where
           motion_t(:, j, dz - 1,jt1, 1:3) = motion_t(:, j, dz - 2, 1,   1:3) ! FIXED IN TIME
           motion_t(:, j, dz,    jt1, 1:3) = motion_t(:, j, dz - 2, 1,   1:3)
        end do
!$omp end parallel do
     end if
  else if (switching_boundary_motion == 1) then ! RELAXATION TO MEAN WIND
     !x
     do k = 1, dz
        do i = 1, dx
           if(motion_t(i, 1, k, jt1, 2) > 0) then
              motion_t(i, 2, k, jt1, 1:3) = motion_t(i, 2, k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
              motion_t(i, 1, k, jt1, 1:3) = motion_t(i, 1, k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
           else
              motion_t(i, 2, k, jt1, 1:3) = motion_t(i, 3, k, jt1, 1:3) !Neumann
              motion_t(i, 1, k, jt1, 1:3) = motion_t(i, 2, k, jt1, 1:3) !Neumann
           end if
           if(motion_t(i, dy, k, jt1, 2) >= 0) then
              motion_t(i, dy - 1,k, jt1, 1:3) = motion_t(i, dy - 2, k, jt1, 1:3) !Neumann
              motion_t(i, dy,    k, jt1, 1:3) = motion_t(i, dy - 1, k, jt1, 1:3) !Neumann
           else
              motion_t(i, dy - 1, k, jt1, 1:3) = motion_t(i, dy - 1, k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
              motion_t(i, dy,     k, jt1, 1:3) = motion_t(i, dy,     k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
           end if
        end do
     end do
  
     !y
     do k = 1, dz
        do j = j_start, j_end
           if(motion_t(1, j, k, jt1, 1) > 0) then
              motion_t(2, j, k, jt1, 1:3) = motion_t(2, j, k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
              motion_t(1, j, k, jt1, 1:3) = motion_t(1, j, k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
           else
              motion_t(2, j, k, jt1, 1:3) = motion_t(3, j, k, jt1, 1:3) !Neumann
              motion_t(1, j, k, jt1, 1:3) = motion_t(2, j, k, jt1, 1:3) !Neumann
           end if
           if(motion_t(dx, j, k, jt1, 1) >= 0) then
              motion_t(dx - 1, j, k, jt1, 1:3) = motion_t(dx - 2, j, k, jt1, 1:3) !Neumann
              motion_t(dx,     j, k, jt1, 1:3) = motion_t(dx - 1, j, k, jt1, 1:3) !Neumann
           else
              motion_t(dx - 1, j, k, jt1, 1:3) = motion_t(dx - 1, j, k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
              motion_t(dx,     j, k, jt1, 1:3) = motion_t(dx,     j, k, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
           end if
        end do
     end do

     !z
     do j = j_start, j_end
        do i = 1, dx
           if(motion_t(i, j, 1, jt1, 3) > 0) then
              motion_t(i, j, 2, jt1, 1:3) = motion_t(i, j, 2, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
              motion_t(i, j, 1, jt1, 1:3) = motion_t(i, j, 1, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
           else
              motion_t(i, j, 2, jt1, 1:3) = motion_t(i, j, 3, jt1, 1:3) !Neumann
              motion_t(i, j, 1, jt1, 1:3) = motion_t(i, j, 2, jt1, 1:3) !Neumann
           end if
           if(motion_t(i, j, 1, jt1, 3) >= 0) then
              motion_t(i, j, dz - 1, jt1, 1:3) = motion_t(i, j, dz - 2, jt1, 1:3) !Neumann
              motion_t(i, j, dz,     jt1, 1:3) = motion_t(i, j, dz - 1, jt1, 1:3) !Neumann
           else
              motion_t(i, j, dz - 1, jt1, 1:3) = motion_t(i, j, dz - 1, jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
              motion_t(i, j, dz,     jt1, 1:3) = motion_t(i, j, dz,     jt2, 1:3) * relax_const1 + mean_motion(1:3) * relax_const2
           end if
        end do
     end do
  endif
  
  !boundary condition (for adaptive boundary condition)
!$omp parallel do private(i, j, k)
  do k = 1, dz
     !x
     do i = 1, dx
        if(myrank == 0) then
           if(motion_t(i, 1, k, jt1, 2) > 0) then
              rain_t(i, 1:2, k, jt1, 1) = rain_missing 
           else
              rain_t(i, 1:2, k, jt1, 1) = rain_t(i, 3, k, jt1, 1) !Neumann
           end if
        end if
        if(myrank == mpiprocs - 1) then
           if(motion_t(i, dy, k, jt1, 2) >= 0) then
              rain_t(i, (dy - 1):dy, k, jt1, 1) = rain_t(i, dy - 2, k, jt1, 1) !Neumann
           else
              rain_t(i, (dy - 1):dy, k, jt1, 1) = rain_missing
           end if
        end if
     end do

     if(boundary_type == 1) then !CYCLIC IN X
        rain_t((1 - margin_x):0,         j_start:j_end, k, jt1, 1) = rain_t((dx - margin_x + 1):dx, j_start:j_end, k, jt1, 1)
        rain_t((dx + 1):(dx + margin_x), j_start:j_end, k, jt1, 1) = rain_t(1:margin_x,             j_start:j_end, k, jt1, 1)
     else
        !y
        do j = j_start, j_end
           if(motion_t(1, j, k, jt1, 1) > 0) then
              rain_t(1:2, j, k, jt1, 1) = rain_missing
           else
              rain_t(1:2, j, k, jt1, 1) = rain_t(3, j, k, jt1, 1) !Neumann
           end if
           if(motion_t(dx, j, k, jt1, 1) >= 0) then
              rain_t((dx - 1):dx, j, k, jt1, 1) = rain_t(dx - 2, j, k, jt1, 1) !Neumann
           else
              rain_t((dx - 1):dx, j, k, jt1, 1) = rain_missing
           end if
        end do
     end if
  end do
!$omp end parallel do
  
  if(dz > 1) then
!$omp parallel do private(i, j, k)
     do j = j_start, j_end
        !z
        do i = 1, dx
           if(motion_t(i, j, 1, jt1, 3) > 0) then
              rain_t(i, j, 1:2, jt1, 1) = rain_missing
           else
              rain_t(i, j, 1:2, jt1, 1) = rain_t(i, j, 3, jt1, 1) !Neumann
           end if
 
           if(motion_t(i, j, dz, jt1, 3) >= 0) then
              rain_t(i, j, (dz - 1):dz, jt1, 1) = rain_t(i, j, dz - 2, jt1, 1) !Neumann
           else
              rain_t(i, j, (dz - 1):dz, jt1, 1) = rain_missing
           end if
        end do
     end do
!$omp end parallel do
  end if

end subroutine  boundary
