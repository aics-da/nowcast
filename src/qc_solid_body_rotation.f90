module module_qc_solid_body_rotation
  use variables
  implicit none

  public qc_solid_body_rotation

contains

  subroutine qc_solid_body_rotation
    double precision rot_mean(dz), rot_var(dz), factor2, rot(dz), ax, ay, cx, cy, norm
    integer rot_count(dz)
    integer num_valid_lev, num_reject_lev
    double precision, allocatable :: rot_history(:, :)
    integer, allocatable :: num_valid_vect_history(:, :)
    integer, parameter :: num_history = 10
    logical qc_solid_body_rotation_flag
    logical file_exist
    integer i, j, k, a, num_point

    !GLOBAL QUALITY CONTROL BY TEMPORAL CONSISTENCY
    allocate(rot_history(dz, num_history), num_valid_vect_history(dz, num_history))
    inquire(file = trim(da_gues_path) // 'solid_body_rot_history.bin', exist = file_exist)
    if(file_exist) then
       if(myrank == 0) write(*, *) "reading solid_body_rot_history.bin ..."
       open(26, file = trim(da_gues_path) // 'solid_body_rot_history.bin', access = 'stream', form = 'unformatted', convert = "little_endian")
       read(26) num_valid_vect_history, rot_history
       close(26)
    else
       num_valid_vect_history = 0
       rot_history = 0.0d0
    end if
    rot_count = 0
    rot_mean = 0
    rot_var = 0
    do a = 1, num_history
       where(num_valid_vect_history(:, a) > 0)
          rot_count = rot_count + 1
          rot_mean(:) = rot_mean(:) + rot_history(:, a)
       end where
    end do !a
    where(rot_count == num_history)
       rot_mean = rot_mean / num_history
    end where
    do a = 1, num_history
       where(num_valid_vect_history(:, a) > 0 .and. rot_count == num_history)
          rot_var(:) = rot_var(:) + (rot_history(:, a) - rot_mean(:)) ** 2
       end where
    end do !a
    where(rot_count == num_history)
       rot_var(:) = rot_var(:) / (num_history - 1)
    end where
    qc_solid_body_rotation_flag = .false.
    factor2 = da_qc_temp_consistency_sigma ** 2

    cx = ddx * 0.5d0
    cy = ddy * 0.5d0
!$omp parallel do private(i, j, k, num_point, norm, ax, ay)
    do k = 1, dz
       num_point = 0
       rot(k) = 0
       norm = 0.0d0
       do j = 1, dy
          ax = -(j + posiy - cy)
          do i = 1, dx
             if(motion_t(i, j, k, 1, 1) .ne. motion_undef) then
                ay = i + posix - cx
                rot(k) = rot(k) + motion_t(i, j, k, 1, 1) * ax + motion_t(i, j, k, 1, 2) * ay
                norm = norm + ax * ax + ay * ay
             end if
          end do
       end do
       if(num_point > 0) rot(k) = rot(k) / sqrt(norm) / num_point
    end do
!$omp end parallel do

    num_valid_lev = 0
    num_reject_lev = 0
    do k = 1, dz
       if(rot_count(k) == num_history) then
          num_valid_lev = num_valid_lev + 1
          if((rot(k) - rot_mean(k)) ** 2 > rot_var(k) * factor2) then
             num_reject_lev = num_reject_lev + 1
          end if
       end if
    end do !k
    if(num_valid_lev > 0 .and. dble(num_reject_lev) / dble(num_valid_lev) > da_qc_temp_consistency_threshold) then
       if(myrank == 0) write(*, *) "global quality control by temporal consistency: reject"
       qc_solid_body_rotation_flag = .true.
    end if

    if(qc_solid_body_rotation_flag) then
!$omp parallel do private(k)
       do k = 1, dz
          motion_t(:, :, k, 1, 1:3) = motion_undef !QC REJECTION
       end do
!$omp end parallel do
    end if
    if((.not. qc_solid_body_rotation_flag) .or. (da_qc_temp_consistency_hist_save_rejected == 1)) then
       do a = num_history, 2, -1
          num_valid_vect_history(:, a) = num_valid_vect_history(:, a - 1)
          rot_history(:, a) = rot_history(:, a - 1)
       end do !a
       num_valid_vect_history(:, 1) = num_valid_vect
       rot_history(:, 1) = rot
    end if
    if(myrank == 0) then
       open(26, file = 'solid_body_rot_history.bin', access = 'stream', form = 'unformatted', convert = "little_endian")
       write(26) num_valid_vect_history, rot_history
       close(26)
    end if
    deallocate(rot_history, num_valid_vect_history)
  end subroutine qc_solid_body_rotation
end module module_qc_solid_body_rotation
