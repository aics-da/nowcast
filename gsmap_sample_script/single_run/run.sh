#!/usr/bin/bash
set -ex

ln -sf ../data/gsmap_nrt.20230301.0000.dat ./input_0001.bin
ln -sf ../data/gsmap_nrt.20230301.0100.dat ./input_0002.bin

ruby gen_variable_frame_num.rb
ruby gen_variable_search_num.rb

if [ -e ../../src/nowcast-mpi-lwatch.exe ] ; then
    ../../src/nowcast-mpi-lwatch.exe
elif [ -e ../../src/nowcast-mpi.exe ] ; then
    ../../src/nowcast-mpi.exe
else
    ../../src/nowcast.exe
fi
