require "narray"

#fn = NArray.int(1200).fill!(92)
fn = NArray.int(1200)

fnmin = 46
fnmax = 92

fn[0..299] = fnmax

i0 = 349.5
iwid = 100.0
for i in 300..399
  fn[i] = (fnmax - (fnmax - fnmin) * 0.5 - Math.sin((i - i0) / iwid * Math::PI) * (fnmax - fnmin) * 0.5).round
end

fn[400..799] = fnmin

i0 = 849.5
iwid = 100.0
for i in 800..899
  fn[i] = (fnmax - (fnmax - fnmin) * 0.5 + Math.sin((i - i0) / iwid * Math::PI) * (fnmax - fnmin) * 0.5).round
end

fn[900..1199] = fnmax

File.open("variable_frame_num_y.txt", "w"){|f| f.puts fn.to_a.join("\n")}
