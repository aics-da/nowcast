require "narray"

#fn = NArray.int(1200).fill!(92)
sn = NArray.float(1200)

sn = 1.0 / NMath.cos(sn.indgen!.mul!(-0.1).add!(59.95).mul!(Math::PI / 180.0))
sn *= 6

p [sn.min, sn.max]

sn = sn.round

p [sn.min, sn.max]

File.open("variable_search_num_x.txt", "w"){|f| f.puts sn.to_a.join("\n")}
