require "numru/ggraph"
include NumRu

p u = GPhys::IO.open("vector_u_001.nc", "u")
p v = GPhys::IO.open("vector_v_001.nc", "v")
p g = GPhys::IO.open("raindata_001.nc", "rain")
GGraph.tone(u)
DCLExt.color_bar
GGraph.tone(v)
DCLExt.color_bar
for i in 0...g.shape[-1]
  p g[false, i].val
  GGraph.tone(g[false, i], true, "tonc" => true, "levels" => [0, 0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100])
  DCLExt.color_bar("constwidth" => true)
end
DCL::grcls
