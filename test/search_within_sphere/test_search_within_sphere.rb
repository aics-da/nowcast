require "narray"

topdir = File.expand_path(File.join(File.dirname(__FILE__), "..", ".."))

na1 = NArray.sfloat(30, 30)
na2 = na1.dup

na1[true, true] = 0
for j in 0..29
  for i in 0..29
    if((i - 14.5) ** 2 + (j - 14.5) ** 2 <= 5 ** 2)
      na1[i, j] = 100.0 * (5 ** 2 - (i - 14.5) ** 2 + (j - 14.5) ** 2) / (5 ** 2)
    end
  end
end

na2[1..-1, true] = na1[0..-2, true]
na2[0, true] = na1[-1, true]

#na2[10..15, 10..15] = -99

File.open("test0001.bin", "w"){|f| f.print na1.to_s}
File.open("test0002.bin", "w"){|f| f.print na2.to_s}

ENV["OMP_NUM_THREADS"] = "4"
puts `#{topdir}/nowcast.exe`

system("ruby ../../tools/create_ctl_from_namelist.rb")
system("ruby ../../tools/u_orig_cotrec.rb")
