require "narray"

topdir = File.expand_path(File.join(File.dirname(__FILE__), "..", ".."))

na1 = NArray.sfloat(30, 30)
na2 = na1.dup

for j in 0..29
  for i in 0..29
    na1[i, j] = 100.0 * Math.exp(-((i - 14).to_f ** 2 + (j - 14).to_f ** 2) / 5.0)
  end
end

na2[1..-1, true] = na1[0..-2, true]
na2[0, true] = na1[-1, true]

File.open("test0001.bin", "w"){|f| f.print na1.to_s}
File.open("test0002.bin", "w"){|f| f.print na2.to_s}

ENV["OMP_NUM_THREADS"] = "4"
puts `#{topdir}/nowcast.exe`

#File.unlink("test0001.bin")
#File.unlink("test0002.bin")
