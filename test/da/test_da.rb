require "narray"

topdir = File.expand_path(File.join(File.dirname(__FILE__), "..", ".."))

na = []
iter = 10

na[0] = NArray.sfloat(30, 30)
iter.times{|i| na[i + 1] = na[0].dup}

for j in 0..29
  for i in 0..29
    na[0][i, j] = 100.0 * Math.exp(-((i - 14).to_f ** 2 + (j - 14).to_f ** 2) / 5.0)
  end
end

iter.times{|i|
  na[i + 1][1..-1, true] = na[i][0..-2, true]
  na[i + 1][0, true] = na[i][-1, true]
}

(iter + 1).times{|i|
  na[i].add!(na[i].dup.randomn.mul!(0.1))
  File.open("test#{format('%04d', i + 1)}.bin", "w"){|f| f.print na[i].to_s}
}

ENV["OMP_NUM_THREADS"] = "4"
puts `#{topdir}/nowcast.exe`

system("ruby ../../tools/create_ctl_from_namelist.rb")
iter.times{|i|
  system("ruby ../../tools/u_orig_cotrec.rb #{i + 1}")
}
iter.times{|i|
  system("ruby ../../tools/u.rb #{i + 1}")
}

