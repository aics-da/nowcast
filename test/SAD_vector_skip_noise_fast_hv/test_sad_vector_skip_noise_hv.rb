require "narray"

topdir = File.expand_path(File.join(File.dirname(__FILE__), "..", ".."))

na1 = NArray.sfloat(30, 30, 30)
na2 = na1.dup

ci = cj = ck = 14
for k in 0..29
  for j in 0..29
    for i in 0..29
      na1[i, j, k] = 100.0 * Math.exp(-((i - ci).to_f ** 2 + (j - cj).to_f ** 2 + (k - ck).to_f ** 2) / 5.0)
    end
  end
end

na2[1..-1, true, 1..-1] = na1[0..-2, true, 0..-2]
na2[0, true, 1..-1] = na1[-1, true, 0..-2]
na2[1..-1, true, 0] = na1[0..-2, true, -1]
na2[0, true, 0] = na1[-1, true, -1]

na2[10..15, 10..15, 10..15] = -99

File.open("test0001.bin", "w"){|f| f.print na1.to_s}
File.open("test0002.bin", "w"){|f| f.print na2.to_s}

ENV["OMP_NUM_THREADS"] = "4"
t1 = Time.now
raise unless system "#{topdir}/nowcast.exe"
t2 = Time.now

system("ruby ../../tools/create_ctl_from_namelist.rb")
system("ruby ../../tools/u.rb 1 true true 14 1e-5 1e-4 1e-3 1e-2 0.1, 1, 10, 100")
system("ruby ../../tools/u.rb 1 true true 8 1e-5 1e-4 1e-3 1e-2  0.1, 1, 10, 100")
system("ruby ../../tools/w.rb 1 true true 14 1e-5 1e-4 1e-3 1e-2 0.1, 1, 10, 100")

puts "TIme: #{t2 - t1}"

require "numru/ggraph"
include NumRu
DCL::gropn(4)

DCL::grfrm
DCL::grswnd(0, 29, 0, NArray[na1[true, 14, 19].max, na2[true, 14, 19].max].max)
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grstrf
DCL::usdaxs
DCL::uulin(NArray.sfloat(30).indgen!, na1[true, 14, 19])
DCL::uuslni(31)
DCL::uulin(NArray.sfloat(30).indgen!, na2[true, 14, 19])

dat = []
(-5..5).each{|i|
  dat.push (na2[10..18, 14, 19] - na1[(10 + i)..(18 + i), 14, 19]).abs.sum
}
dat = NArray.to_na(dat)

DCL::grfig
DCL::grswnd(0, 29, 0, dat.max)
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grstrf
DCL::uuslni(21)
DCL::uulin(NArray.sfloat(dat.length).indgen!.add!(14 - 5), dat)


DCL::grfrm
DCL::grswnd(0, 29, 0, NArray[na1[9, 14, true].max, na2[9, 14, true].max].max)
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grstrf
DCL::usdaxs
DCL::uulin(NArray.sfloat(30).indgen!, na1[9, 14, true])
DCL::uuslni(31)
DCL::uulin(NArray.sfloat(30).indgen!, na2[9, 14, true])

dat = []
(-5..5).each{|i|
  dat.push (na2[9, 14, 10..18] - na1[9, 14, (10 + i)..(18 + i)]).abs.sum
}
dat = NArray.to_na(dat)

DCL::grfig
DCL::grswnd(0, 29, 0, dat.max)
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grstrf
DCL::uuslni(21)
DCL::uulin(NArray.sfloat(dat.length).indgen!.add!(14 - 5), dat)

DCL::grcls
