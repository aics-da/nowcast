require "narray"

wavelength = 4.0

topdir = File.expand_path(File.join(File.dirname(__FILE__), "..", "..", ".."))

na1 = NArray.sfloat(32, 30)
na2 = na1.dup
na3 = na1.dup

for j in 0..29
  for i in 0..31
    na1[i, j] = 100.0 * Math.exp(-((i - 14).to_f ** 2 + (j - 14).to_f ** 2) / 5.0)
  end
end

na2[2..-1, true] = na1[0..-3, true]
na2[0..1, true] = na1[-2..-1, true]

File.open("test0001.bin", "w"){|f| f.print na1.to_s}
File.open("test0002.bin", "w"){|f| f.print na2.to_s}

for j in 0..29
  for i in 0..31
    na3[i, j] = 10.0 * Math.sin(2 * Math::PI * i / wavelength)
  end
end
p na3[true, 15].to_a

File.open("vector_u_001.bin", "w"){|f| f.print na3.to_s}
File.open("vector_v_001.bin", "w"){|f| f.print na3.to_s}
File.open("vector_w_001.bin", "w"){|f| f.print na3.to_s}

puts `#{topdir}/nowcast.exe`

#File.unlink("test0001.bin")
#File.unlink("test0002.bin")

system("ruby ../../../tools/create_ctl_from_namelist.rb")
system("ruby ../../../tools/u_orig_cotrec.rb 1 1")
