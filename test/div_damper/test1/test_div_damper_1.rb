require "narray"

topdir = File.expand_path(File.join(File.dirname(__FILE__), "..", "..", ".."))

na1 = NArray.sfloat(30, 30)
na2 = na1.dup

for j in 0..29
  for i in 0..29
    na1[i, j] = 100.0 * Math.exp(-((i - 14).to_f ** 2 + (j - 14).to_f ** 2) / 5.0)
  end
end

na2[2..-1, true] = na1[0..-3, true]
na2[0..1, true] = na1[-2..-1, true]

File.open("test0001.bin", "w"){|f| f.print na1.to_s}
File.open("test0002.bin", "w"){|f| f.print na2.to_s}

u = NArray.sfloat(30, 30)
u[true, 0] = NMath.sin(u[true, 0].indgen! / (u.shape[0] - 1) * 2.0 * Math::PI)
u[true, 1..-1] = u[true, 0..0]

p u[true, 0].to_a

v = NArray.sfloat(30, 30).fill!(0)
w = NArray.sfloat(30, 30).fill!(0)

File.open("vector_u_001.bin", "w"){|f| f.print u.to_s}
File.open("vector_v_001.bin", "w"){|f| f.print v.to_s}
File.open("vector_w_001.bin", "w"){|f| f.print w.to_s}

puts `#{topdir}/nowcast.exe`

File.unlink("test0001.bin")
File.unlink("test0002.bin")

system("ruby ../../../tools/create_ctl_from_namelist.rb")
system("ruby ../../../tools/u.rb")
