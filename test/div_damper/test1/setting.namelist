&input_config
input_basename = "test" ! input file basename (should be quoted, time step and suffix such as 0001.bin will be added to this) /
input_interval = 30.0d0 ! input file interval (seconds) /
output_interval = 30.0d0 ! output file interval (seconds) /
switching_input_missing = 0 ! 0:do not replace missing value, 1:replace missing value with input_missing_newval, 2: replace missing value before vector computation /
input_missing_newval = 0.0d0 ! substitute missing value with this /
expconv = 0 ! 0:no conversion, 1: convert from dBZ to Z, 2: convert from dBZ to rain rate /
read_topo_mask = 0 ! 0: internally generate topographic info, 1: read topographic info from file /
topo_max = 20 ! max layer number for topo filling /
use_precomputed_vector = 1 ! 0:compute motion vector, 1:read in motion vector from file /
/

&time_config
dt = 1.0d0 !Time step (seconds) /
calnt = 601 !maximum time steps /
start = 1 !initial run number /
iteration_all = 1 !number of runs /
nt = 2 !number of observation frames for each run /
/

&domain_config
ddx = 30 ! input data size (x) /
ddy = 30 ! input data size (y) /
ddz = 1 ! input data size (z) /
dx = 30 ! output data size (x) /
dy = 30 ! output data size (y) /
dz = 1 ! output data size (z) /
gridx = 100 ! resolution (x) /
gridy = 100 ! resolution (y) /
gridz = 100 ! resolution (z) /
posix = 0 ! offset of origin for the output field (x) /
posiy = 0 ! offset of origin for the output field (y) /
posiz = 0 ! offset of origin for the output field (z) /
boundary_type = 0 ! 0:standard, 1:cyclic in x /
/

&search_config
frame_num = 10 ! search box size (xy)
frame_numz = 10 ! search box size (z)
search_num = 5 ! max search area (xy)
search_numz = 5 ! max search area (z)
switching_vector = 4 ! 0:corration, 1:SSD, 2:fuzzy, 3:fuzzy2, 4:SSD_skip_missing, 5:corr_skip_noise /
switching_vector_centroid = 1 ! 0:use single point, 1:use centroid /
corr_qc_threshold = 0.6
switching_vector_skip_missing = 0
vector_missing_threshold = -999.0d0 ! values smaller than this will be ignored if switching_vector_skip_missing == 1 /
min_valid_grid_ratio = 0.05 ! minimum valid grid ratio within the matching box /
min_valid_direction_ratio = 0.99 ! minimum valid direction ratio within the search area /
fill_missing = 2 ! 0:filling domain-average motions, 1:filling latitude-average motions, 2:filling by diffusion /
fill_missing_threshold = 0.0001 ! threshold value for filling by diffusion /
match_by_sphere = 1 ! 0: box, 1: sphere /
search_within_sphere = 1 ! 0: box, 1: sphere /
/

&cotrec_config
swiching_cotrec = 0 ! 0:No cotrec, 1:2D cotrec, 2:3D cotrec /
threshold = 0.001 ! threshold for cotrec /
countermax = 100000 ! max iteration for cotrec /
sor_accel = 1.5 ! for cotrec Gauss-Seidel SOR (fallback) /
/

&driver_config
swiching_burgers = 1 ! advection of motion vectors 0:1st order upwind, 1:5th order WENO /
swiching_growth = 0 ! growth rate 0:not consider 1:consider /
switching_growth_weno = 0 ! growth rate advection method 0:1st order upwind 1:5th order WENO /
growth_rate_max_it = 181 ! time step to turn off growth rate /
switching_boundary_motion = 0 ! boundary motion 0:fixed, 1:upwind_mean_downwind_Neumann /
switching_time_integration = 0 ! 0: 2nd-order Adams-Bashforth, 1: 3rd-order Adams-Bashfort /
switching_div_damper = 1
div_damper_param = 0.5
/

&noise_config
rain_noise = -29.99 ! rain noise level /
fuzzy_sad_min = 0 ! fuzzy SAD min /
fuzzy_sad_max = 30 ! fuzzy SAD max /
switching_lanczos = 0 ! 0:no filter, 1:Lanczos filter /
lanczos_window = 10 ! Lanczos filter window (grids) /
lanczos_critical_wavelength = 3.0d0 ! Lanczos filter critical wavelength (grids) /
/

&da_config
use_data_assimilation = 0 ! 0: no DA, 1: intrinsic DA subroutine, 2: external DA program /
variance_f_init = 10.0 ! initial forecast variance /
variance_f_inflation = 1.1 ! forecast variance inflation parameter /
variance_f_max = 10000.0 ! max forecast variance /
variance_o = 5.0 ! observation variance /
da_start_from = 2 ! start assimilation from this cycle /
da_qc_threshold = 5.0 ! QC threshold /
/
