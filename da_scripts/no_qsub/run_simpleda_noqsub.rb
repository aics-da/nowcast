require "fileutils"
require "time"


use_shm = false
shm_suffix = "nc"
split_history = true
history_length = 21
use_pigz = false

$qsub_job_ids = []

def qsub(nodes, cpus, nmpi, dir, prg, nowait = false)
  host = `uname -n`
  mpiconf = "select=#{nodes}:ncpus=#{cpus}:mpiprocs=#{nmpi}:ompthreads=#{cpus / nmpi}"
  ompconf = "select=#{nodes}:ncpus=#{cpus}"
  p resource = nmpi > 1 ? mpiconf : ompconf
  if /hakushu/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l #{resource}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel/2017.1.132
ulimit -u 200
ulimit -a
umask 0022
  cd #{dir}
  #{prg}
EOF

  elsif /yamazaki/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l nodes=#{nodes}:ppn=#{cpus.to_i / nodes.to_i}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
module load mpt/2.05
  cd #{dir}
  #{prg}
EOF
  else
    raise "configuration for #{host} not found"
  end

  File.open("run-simpleda2-#{$$}.sh", "w"){|f| f.puts script}
  puts qsub = `qsub run-simpleda2-#{$$}.sh`
  raise unless /^([0-9]+)/ =~ qsub
  jobid = $1
  if nowait
    $qsub_job_ids.push jobid
  else
    while /#{jobid}/ =~ `qstat`
      sleep 2
    end
    puts "job #{jobid} finished"
  end
end

def qsub_wait
  waitflag = true
  p $qsub_job_ids
  while true
    sleep 5
    qstat = `qstat`
    $qsub_job_ids.dup.each{|jobid|
      unless /#{jobid}/ =~ qstat
        puts "job #{jobid} finished"
        $qsub_job_ids.delete(jobid)
      end
    }
    p $qsub_job_ids
    break if $qsub_job_ids.length == 0
  end
end

pwd = Dir.pwd

first_cycle = true
disable_lanczos_in_da = true

unless File.exist?("lock")
  File.open("lock", "w"){|f| }
  tfile = File.open("elapse_time.txt", "w")
  begin
    #nodes = 2
    #ncpus = 40
    #nmpi = 4
    #model = "/data13/otsuka/nowcast-mpi-20170427.exe"
    #model = "/data13/otsuka/nowcast-mpi-20170427b.exe"
    #model = "/data13/otsuka/nowcast-mpi-20170504.exe"
    #model = "/data13/otsuka/nowcast-mpi-20170505.exe"
    model = "/data13/otsuka/nowcast-mpi-20170511.exe"
    zfile  = "/data13/otsuka/pawr-osaka/%Y-%m%d/%H/%M/%Y%m%d-%H%M%S.all.10000000.dat"
    vrfile = "/data13/otsuka/pawr-osaka/%Y-%m%d/%H/%M/%Y%m%d-%H%M%S.all.20000000.dat"

    obsfiles = Dir.glob("/data13/otsuka/pawr-osaka/*/*/*/*.10000000.dat").sort

    (obsfiles.length - 1).times{|i|
      /([0-9]{4})([0-9]{2})([0-9]{2})-([0-9]{2})([0-9]{2})([0-9]{2})\.all\.10000000\.dat/ =~ obsfiles[i]
      date1 = Time.new($1, $2, $3, $4, $5, $6)
      /([0-9]{4})([0-9]{2})([0-9]{2})-([0-9]{2})([0-9]{2})([0-9]{2})\.all\.10000000\.dat/ =~ obsfiles[i + 1]
      date2 = Time.new($1, $2, $3, $4, $5, $6)
      diff = date2 - date1
      if diff > 20 and diff < 40 # assume they are consecutive observations
        next unless File.exist?(date1.strftime(vrfile)) and File.exist?(date2.strftime(vrfile))
        dir1 = date1.strftime("/data13/otsuka/simpleda/%Y/%m/%d/%H/%M/%S")
        dir2 = date2.strftime("/data13/otsuka/simpleda/%Y/%m/%d/%H/%M/%S")
        next if File.exist?(File.join(dir2, "raindata_001.bin"))

        t0 = Time.now

        p dir2
        FileUtils.mkdir_p(dir2)
        if File.exist?(File.join(dir1, "rain_cart_0002.nc"))
          FileUtils.ln_sf(File.join(dir1, "rain_cart_0002.nc"), File.join(dir2, "rain_cart_0001.nc"))
        elsif File.exist?(File.join(dir1, "input_0002.bin"))
          FileUtils.ln_sf(File.join(dir1, "input_0002.bin"), File.join(dir2, "input_0001.bin"))
          FileUtils.ln_sf(File.join(dir1, "vr_0002.bin"), File.join(dir2, "vr_0001.bin"))
        else
          FileUtils.cp(date1.strftime(zfile), File.join(dir2, "input_0001.bin"), :preserve => true)
          FileUtils.cp(date1.strftime(vrfile), File.join(dir2, "vr_0001.bin"), :preserve => true)
        end
        FileUtils.cp(date2.strftime(zfile), File.join(dir2, "input_0002.bin"), :preserve => true)
        FileUtils.cp(date2.strftime(vrfile), File.join(dir2, "vr_0002.bin"), :preserve => true)
        FileUtils.cp("topo_mask.dat", dir2, :preserve => true) if File.exist?("topo_mask.dat")
        FileUtils.cp(Dir.glob("*.namelist"), dir2, :preserve => true)
        FileUtils.cp(Dir.glob("*.txt"), dir2, :preserve => true)


        ### MODIFY setting.namelist
        File.open(File.join(dir2, "setting.namelist"), "w"){|fout|
          File.open("setting.namelist", "r"){|fin|
            fin.readlines.each{|l|
              case l
              when /use_data_assimilation/
                if(first_cycle)
                  fout.puts "use_data_assimilation = 0"
                else
                  fout.print l
                end
              when /da_gues_path/
                unless(first_cycle)
                  fout.puts "da_gues_path = \"#{dir1}/\""
                end
              when /switching_lanczos/
                if(disable_lanczos_in_da && !first_cycle)
                  fout.puts "switching_lanczos = 0"
                else
                  fout.print l
                end
              else
                fout.print l
              end
            }
          }
        }

        Dir.chdir(dir2)
        if use_shm
          %w(b01 b02 b03 b04).each{|node|
            system("ssh #{node} 'mkdir -p /dev/shm/otsuka/#{dir2}'") || raise
          }
          %w(raindata_001 burgersU_001 burgersV_001 burgersW_001).each{|ncfile|
            if split_history
              history_length.times{|ht|
                system("ln -sf /dev/shm/otsuka/#{dir2}/#{ncfile}_#{format('%03d', ht + 1)}.#{shm_suffix} .") || raise
              }
            else
              system("ln -sf /dev/shm/otsuka/#{dir2}/#{ncfile}.#{shm_suffix} .") || raise
            end
          }
        end
        #qsub(nodes, ncpus, nmpi, Dir.pwd, "mpiexec --hostfile $PBS_NODEFILE --bind-to socket #{model}")
        raise unless system("mpiexec --hostfile $PBS_NODEFILE --bind-to socket #{model}")
        if use_shm
          %w(raindata_001 burgersU_001 burgersV_001 burgersW_001).each{|ncfile|
            if split_history
              history_length.times{|ht|
                system("rm #{dir2}/#{ncfile}_#{format('%03d', ht + 1)}.#{shm_suffix}") || raise
              }
            else
              system("rm #{dir2}/#{ncfile}.#{shm_suffix}") || raise
            end
          }
          if use_pigz
            system("parallel ssh ::: b01 b02 b03 b04 ::: 'pigz -f /dev/shm/otsuka/#{dir2}/*.bin > /dev/null ; mv -f /dev/shm/otsuka/#{dir2}/* #{dir2}'") || raise
          else
            system("parallel ssh ::: b01 b02 b03 b04 ::: 'mv /dev/shm/otsuka/#{dir2}/* #{dir2}'") || raise
          end
        end
        Dir.chdir(pwd)

        FileUtils.cp(File.join(dir2, "topo_mask.dat"), pwd, :preserve => true) if File.exist?(File.join(dir2, "topo_mask.dat"))

        tfile.puts "#{Time.now - t0}"
        tfile.flush
      end

      first_cycle = false
    }
  ensure
    File.unlink(File.join(pwd, "lock"))
    tfile.close
  end
else
  raise "lock file detected"
end
