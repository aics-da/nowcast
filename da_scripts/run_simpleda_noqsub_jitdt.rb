require "fileutils"
require "time"


use_shm = false
shm_suffix = "nc"
split_history = true
history_length = 21
use_pigz = false
fname_date = true

$qsub_job_ids = []

def qsub(nodes, cpus, nmpi, dir, prg, nowait = false)
  host = `uname -n`
  mpiconf = "select=#{nodes}:ncpus=#{cpus}:mpiprocs=#{nmpi}:ompthreads=#{cpus / nmpi}"
  ompconf = "select=#{nodes}:ncpus=#{cpus}"
  p resource = nmpi > 1 ? mpiconf : ompconf
  if /hakushu/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l #{resource}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel/2017.1.132
ulimit -u 200
ulimit -a
umask 0022
  cd #{dir}
  #{prg}
EOF

  elsif /yamazaki/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l nodes=#{nodes}:ppn=#{cpus.to_i / nodes.to_i}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
module load mpt/2.05
  cd #{dir}
  #{prg}
EOF
  else
    raise "configuration for #{host} not found"
  end

  File.open("run-simpleda2-#{$$}.sh", "w"){|f| f.puts script}
  puts qsub = `qsub run-simpleda2-#{$$}.sh`
  raise unless /^([0-9]+)/ =~ qsub
  jobid = $1
  if nowait
    $qsub_job_ids.push jobid
  else
    while /#{jobid}/ =~ `qstat`
      sleep 2
    end
    puts "job #{jobid} finished"
  end
end

def qsub_wait
  waitflag = true
  p $qsub_job_ids
  while true
    sleep 5
    qstat = `qstat`
    $qsub_job_ids.dup.each{|jobid|
      unless /#{jobid}/ =~ qstat
        puts "job #{jobid} finished"
        $qsub_job_ids.delete(jobid)
      end
    }
    p $qsub_job_ids
    break if $qsub_job_ids.length == 0
  end
end

pwd = Dir.pwd

first_cycle = true
disable_lanczos_in_da = true

unless File.exist?("lock")
  File.open("lock", "w"){|f| }
  tfile = File.open("elapse_time.txt", "w")
  begin
    #nodes = 2
    #ncpus = 40
    #nmpi = 4
    #model = "/data13/otsuka/nowcast-mpi-20170605.exe"
    #model = "/data13/otsuka/nowcast-mpi-20170606.exe"
    model = "/data13/otsuka/nowcast-mpi-20170607.exe"

    dir1 = nil
    dir2 = nil
    ncycle = 0
    while true
      t0 = Time.now

      dir2 = "/data13/otsuka/simpleda_jitdt/run"

      p dir2
      FileUtils.mkdir_p(dir2)
      if (!first_cycle) && File.exist?(File.join(dir1, "rain_cart_0002.nc"))
        FileUtils.ln_sf(File.join(dir1, "rain_cart_0002.nc"), File.join(dir2, "rain_cart_0001.nc"))
      end
      FileUtils.cp("topo_mask.dat", dir2, :preserve => true) if File.exist?("topo_mask.dat")
      FileUtils.cp(Dir.glob("*.namelist"), dir2, :preserve => true)
      FileUtils.cp(Dir.glob("*.txt"), dir2, :preserve => true)


      ### MODIFY setting.namelist
      File.open(File.join(dir2, "setting.namelist"), "w"){|fout|
        File.open("setting.namelist", "r"){|fin|
          fin.readlines.each{|l|
            case l
            when /use_data_assimilation/
              if(first_cycle)
                fout.puts "use_data_assimilation = 0"
              else
                fout.print l
              end
            when /da_gues_path/
              unless(first_cycle)
                fout.puts "da_gues_path = \"#{dir1}/\""
              end
            when /switching_lanczos/
              if(disable_lanczos_in_da && !first_cycle)
                fout.puts "switching_lanczos = 0"
              else
                fout.print l
              end
            else
              fout.print l
            end
          }
        }
      }

      Dir.chdir(dir2)
      if use_shm
        %w(b01 b02 b03 b04).each{|node|
          system("ssh #{node} 'mkdir -p /dev/shm/otsuka/#{dir2}'") || raise
        }
        %w(raindata_001 burgersU_001 burgersV_001 burgersW_001).each{|ncfile|
          if split_history
            history_length.times{|ht|
              system("ln -sf /dev/shm/otsuka/#{dir2}/#{ncfile}_#{format('%03d', ht + 1)}.#{shm_suffix} .") || raise
            }
          else
            system("ln -sf /dev/shm/otsuka/#{dir2}/#{ncfile}.#{shm_suffix} .") || raise
          end
        }
      end
      #qsub(nodes, ncpus, nmpi, Dir.pwd, "mpiexec --hostfile $PBS_NODEFILE --bind-to socket #{model}")
      raise unless system("mpiexec --hostfile $PBS_NODEFILE --bind-to socket #{model}")
      #raise unless system("mpiexec --hostfile $PBS_NODEFILE valgrind #{model}")

      if use_shm
        %w(raindata_001 burgersU_001 burgersV_001 burgersW_001).each{|ncfile|
          if split_history
            history_length.times{|ht|
              system("rm #{dir2}/#{ncfile}_#{format('%03d', ht + 1)}.#{shm_suffix}") || raise
            }
          else
            system("rm #{dir2}/#{ncfile}.#{shm_suffix}") || raise
          end
        }
        if use_pigz
          system("parallel ssh ::: b01 b02 b03 b04 ::: 'pigz -f /dev/shm/otsuka/#{dir2}/*.bin > /dev/null ; mv -f /dev/shm/otsuka/#{dir2}/* #{dir2}'") || raise
        else
          system("parallel ssh ::: b01 b02 b03 b04 ::: 'mv /dev/shm/otsuka/#{dir2}/* #{dir2}'") || raise
        end
      end

      date2 = Time.parse(Dir.glob("[0-9]*\.txt").sort[-1].gsub(".txt", ""))

      if fname_date
        suff = use_pigz ? "#{shm_suffix}.gz" : shm_suffix
        Dir.glob("raindata*").sort.each_with_index{|f, i|
          system("ln -s #{f} #{date2.strftime('%Y%m%d%H%M%S')}_#{(date2 + i * 30).strftime('%Y%m%d%H%M%S')}.#{suff}")
        }
      end

      Dir.chdir(pwd)

      FileUtils.cp(File.join(dir2, "topo_mask.dat"), pwd, :preserve => true) if File.exist?(File.join(dir2, "topo_mask.dat"))

      tfile.puts "#{Time.now - t0}"
      tfile.flush

      first_cycle = false

      dir2_new = "/data13/otsuka/simpleda_jitdt/out/#{date2.strftime('%Y%m%d')}/#{date2.strftime('%H%M%S')}"
      FileUtils.mkdir_p(dir2_new)
      system("mv #{dir2}/* #{dir2_new}")
      dir1 = dir2_new
      ncycle = ncycle + 1
    end
  ensure
    File.unlink(File.join(pwd, "lock"))
    tfile.close
  end
else
  raise "lock file detected"
end
