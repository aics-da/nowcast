require "narray"
require "fileutils"
require "yaml"

$qsub_job_ids = []

def qsub(nodes, cpus, nmpi, dir, prg, nowait = false)
  host = `uname -n`
  if /hakushu/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l select=#{nodes}:ncpus=#{cpus}:mpiprocs=#{nmpi}:ompthreads=#{cpus / nmpi}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
ulimit -u 200
ulimit -a
umask 0022
  cd #{dir}
  #{prg}
EOF

  elsif /yamazaki/ =~ host
    script = <<"EOF"
#!/bin/sh
#PBS -l nodes=#{nodes}:ppn=#{cpus}
. /usr/share/modules/init/sh
module unload pgi-12.10
module load intel-2013.1.117
module load mpt/2.05

  cd #{dir}
  #{prg}
EOF
  else
    raise "configuration for #{host} not found"
  end

  File.open("run-cycle-#{$$}.sh", "w"){|f| f.puts script}
  puts qsub = `qsub run-cycle-#{$$}.sh`
  raise unless /^([0-9]+)/ =~ qsub
  jobid = $1
  if nowait
    $qsub_job_ids.push jobid
  else
    while /#{jobid}/ =~ `qstat`
      sleep 2
    end
    puts "job #{jobid} finished"
  end
end

def qsub_wait
  waitflag = true
  p $qsub_job_ids
  while true
    sleep 5
    qstat = `qstat`
    $qsub_job_ids.dup.each{|jobid|
      unless /#{jobid}/ =~ qstat
        puts "job #{jobid} finished"
        $qsub_job_ids.delete(jobid)
      end
    }
    p $qsub_job_ids
    break if $qsub_job_ids.length == 0
  end
end

raise "locked by another run" if File.exist?("run_cycle.lock")
system("touch run_cycle.lock")

begin
  p yml = YAML.load(File.read("run_cycle.yml"))

  st = yml["start"].utc
  et = yml["end"].utc
  input = File.expand_path(yml["input"])
  input_vr = File.expand_path(yml["input_vr"]) if yml["input_vr"]
  intv = yml["interval"]

  outdir = yml["outdir"]
  confdir = yml["confdir"]

  model = File.expand_path(yml["model"])

  nodes = yml["nodes"] || raise("parameter nodes is not specified")
  ncpus = yml["ncpus_per_node"] || raise("parameter ncpus_per_node is not specified")
  nmpi  = yml["nmpi_per_node"] || raise("parameter nmpi_per_node is not specified")

  disable_lanczos_in_da = yml["disable_lanczos_in_da"]

  first_cycle = yml["first_cycle"]
  outd_old = nil
  old_infile2cp = nil
  old_vrfile2cp = nil

  pwd = Dir.pwd
  while st + intv <= et
    p st

    ### SETUP INPUT FILES FOR ENSEMBLE FORECAST
    p infile1 = st.strftime(input)
    p vrfile1 = st.strftime(input_vr) if input_vr
    st += intv
    p infile2 = st.strftime(input)
    p vrfile2 = st.strftime(input_vr) if input_vr
    outd = File.join(outdir, st.strftime("%Y%m%d%H%M%S"), "fcst")

    ### RUN TREC
    infile1cp = File.join(outd, "input_0001.bin")
    infile2cp = File.join(outd, "input_0002.bin")
    vrfile1cp = File.join(outd, "vr_0001.bin") if input_vr
    vrfile2cp = File.join(outd, "vr_0002.bin") if input_vr
    unless File.exist?(File.join(outd, "raindata_001.bin"))
      puts "start model ..."
      FileUtils.mkdir_p(outd)
      if old_infile2cp
        FileUtils.ln_sf(old_infile2cp, infile1cp)
        FileUtils.ln_sf(old_vrfile2cp, vrfile1cp) if input_vr
      else
        FileUtils.cp(infile1, infile1cp, :preserve => true) unless File.exist?(infile1cp)
        FileUtils.cp(vrfile1, vrfile1cp, :preserve => true) if input_vr and !File.exist?(vrfile1cp)
      end
      FileUtils.cp(infile2, infile2cp, :preserve => true) unless File.exist?(infile2cp)
      FileUtils.cp(vrfile2, vrfile2cp, :preserve => true) if input_vr and !File.exist?(vrfile2cp)
      if(File.exist?(File.join(confdir, "topo_mask.dat")))
        FileUtils.ln_sf(File.join(confdir, "topo_mask.dat"), outd)
      end
      FileUtils.ln_sf(model, outd)
      FileUtils.cp(File.join(confdir, "setting.namelist"), File.join(outd, "setting.namelist.org"))
      FileUtils.cp(File.join(confdir, "radar_qc.namelist"), outd) if File.exist?(File.join(confdir, "radar_qc.namelist"))
      if(File.exist?(File.join(confdir, "topo_mask.dat")))
        FileUtils.ln_sf(File.join(confdir, "topo_mask.dat"), outd)
      end
      %w(ref_vgrad swam tref).each{|qcparam|
        %w(clutter weather).each{|type|
          txt = "#{qcparam}pdf_#{type}.txt"
          FileUtils.ln_sf(File.join(confdir, txt), outd) if File.exist?(File.join(confdir, txt))
        }
      }
      Dir.chdir(outd)

      ### MODIFY setting.namelist
      File.open("setting.namelist", "w"){|fout|
        File.open("setting.namelist.org", "r"){|fin|
          fin.readlines.each{|l|
            case l
            when /use_data_assimilation/
              if(first_cycle)
                fout.puts "use_data_assimilation = 0"
              else
                fout.print l
              end
            when /da_gues_path/
              unless(first_cycle)
                fout.puts "da_gues_path = \"#{outd_old}/\""
              end
            when /switching_lanczos/
              if(disable_lanczos_in_da && !first_cycle)
                fout.puts "switching_lanczos = 0"
              else
                fout.print l
              end
            else
              fout.print l
            end
          }
        }
      }

      #if nmpi == 1 ### ASSUME NON-MPI VERSION
      #  qsub(nodes, ncpus, nmpi, Dir.pwd, "./#{File.basename(model)}")
      #else
        qsub(nodes, ncpus, nmpi, Dir.pwd, "mpiexec --hostfile $PBS_NODEFILE --bind-to socket ./#{File.basename(model)}")
      #end
    else
      puts "skip model"
    end
    old_infile2cp = infile2cp if File.exist?(infile2cp)
    old_vrfile2cp = vrfile2cp if input_vr and File.exist?(vrfile2cp)

    outd_old = outd
    first_cycle = false

    Dir.chdir(pwd)
  end
ensure
  system("rm -f run_cycle.lock")
end
