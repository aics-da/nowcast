# -*- coding: utf-8 -*-
require "numru/ggraph"
require "json"
require "parallel"
include NumRu

$use_parallel = false

def dbz2rr(dat)
  (10.0 ** (dat * 0.1) / 200.0) ** (1.0 / 1.6)
end

#puts "usage: ruby rainrate-seamless-mercator-gmap.rb [imgpath [jsonpath]]"

id = format("%03d", 1)
xrange = true
yrange = true

### PARAMETERS
kz = 8
nobs = 20
tmax_fcst = 20

imgpath = ARGV[0] || "/nowcast/pawr/raw"
jsonpath = ARGV[1] || ARGV[0] || "/nowcast/pawr/json"

datefile = Dir.glob("[0-9]*.txt").sort[-1]
inittime = Time.parse(datefile.gsub(".txt", ""))
validtime = inittime.dup

timeflag = []
timetext = []
timetext_en = []
unixtime = []

### READ OBSERVATION
now = Time.now
pwd = Dir.pwd
obs = []
obstime = [inittime - 30.0]
nobs.times{|i|
  d = obstime[i].strftime("../../../../../../%Y/%m/%d/%H/%M/%S")
  if File.exist?(d)
    Dir.chdir(d)
  elsif File.exist?(d = (obstime[i] + 1).strftime("../../../../../../%Y/%m/%d/%H/%M/%S"))
    obstime[i] += 1
    Dir.chdir(d)
  elsif File.exist?(d = (obstime[i] - 1).strftime("../../../../../../%Y/%m/%d/%H/%M/%S"))
    obstime[i] -= 1
    Dir.chdir(d)
  else
    #break
    obs.unshift false
    obstime.push(obstime[i] - 30.0)
    next
  end

  if File.exist?(png = format("rainrate-gmap_%04d.png", nobs + 1)) && File.exist?(format("rainrate-gmap_%04d.png", nobs + tmax_fcst + 1))
    puts "image for #{obstime[i]} already exists"
    obs.unshift File.expand_path(File.join(Dir.pwd, png))
  elsif File.exist?("raindata_#{id}_001.nc")
    nc = NetCDF.open("raindata_#{id}_001.nc")
    obs.unshift GPhys::IO.open(nc, "rain")[xrange, yrange, kz, 0].copy
    nc.close
  elsif File.exist?("raindata_#{id}.nc")
    nc = NetCDF.open("raindata_#{id}.nc")
    obs.unshift GPhys::IO.open(nc, "rain")[xrange, yrange, kz, 0].copy
    nc.close
  elsif File.exit?("raindata_#{id}.bin")
    obs.unshift GPhys::IO.open("raindata_#{id}.ctl", "var")[xrange, yrange, kz, 0].copy
  else
    #break
    obs.unshift false
  end

  obstime.push(obstime[i] - 30.0)
}
obstime.reverse!.shift
Dir.chdir(pwd)
puts "Find observation: #{Time.now - now}"

### FILL MISSING OBS
(obs.length - 1).times{|i|
  unless obs[i + 1]
    obs[i + 1] = obs[i]
    obstime[i + 1] = obstime[i]
  end
}
(obs.length - 1).times{|i|
  unless obs[nobs - i - 2]
    obs[nobs - i - 2] = obs[nobs - i - 1]
    obstime[nobs - i - 2] = obstime[nobs - i - 1]
  end
}

obstime.each{|ot|
  timetext.push ot.strftime('観測 %Y/%m/%d %H:%M:%S')
  timetext_en.push ot.strftime('Obs %Y/%m/%d %H:%M:%S')
  unixtime.push ot.to_i
  timeflag.push false
}

### READ FORECASTS
now = Time.now
split = false
if File.exist?("raindata_#{id}_001.nc")
  split = true
  g = Dir.glob("raindata_#{id}_*.nc").sort.map!{|m|
    nc = NetCDF.open(m)
    gg = GPhys::IO.open(nc, "rain")[xrange, yrange, kz, 0].copy
    nc.close
    gg
  }
elsif File.exist?("raindata_#{id}.nc")
  nc = NetCDF.open("raindata_#{id}.nc")
  p g = GPhys::IO.open(nc, "rain")[xrange, yrange, kz, true].copy
  nc.close
else
  p g = GPhys::IO.open("raindata_#{id}.ctl", "var")[xrange, yrange, kz, true].copy
end

p lon = NArray.to_na(File.read("lon.bin"), NArray::FLOAT, *((split ? g[0] : g).shape[0..1]))
p lat = NArray.to_na(File.read("lat.bin"), NArray::FLOAT, *((split ? g[0] : g).shape[0..1]))

lon1d = lon[true, 0]
lat1d = lat[0, true]
v_lon = VArray.new(lon1d, {"units" => "deg"}, "lon")
v_lat = VArray.new(lat1d, {"units" => "deg"}, "lat")

(split ? g.length : g.shape[3]).times{|i|
  timetext.push (inittime + i * 30).strftime("#{i == 0 ? '観測' : '予測'} %Y/%m/%d %H:%M:%S")
  timetext_en.push (inittime + i * 30).strftime("#{i == 0 ? 'Obs' : 'Fcst'} %Y/%m/%d %H:%M:%S")
  unixtime.push (inittime + i * 30).to_i
  timeflag.push (i != 0)
}

puts "Read forecasts: #{Time.now - now}"

#p [lon1d.min, lon1d.max, lat1d.min, lat1d.max]

now = Time.now

vpt = [0, 1, 0, 1]

iwidth = (split ? g[0] : g).shape[0] * 2
iheight = iwidth

def open_dcl(iwidth, iheight, vpt)
  DCL::swpset("iwidth", iwidth)
  DCL::swpset("iheight", iheight)
  if $use_parallel
    DCL::swpset("ldump", true)
    DCL::swpset("lwnd", false)
    DCL::swpset("lwait", false)
    DCL::swpset("lwait1", false)
  end
  DCL::gropn(1)
  DCL::glrset("rmiss", -9e20)
  DCL::gllset("lmiss", true)
  DCL::sgpset("lfull", true)
  DCL::sgscmn(56)
  DCL::sglset("lcntl", false)

  GGraph.set_fig("viewport" => vpt, "itr" => 11, "map_fit" => true)
  GGraph.set_map("coast_japan" => false, "grid" => false, "lim" => false)
end

levels   = [0, 0.1,   0.5,   1,     2,     5,     10,    20,    30,    50,    80, DCL::glrget("rmiss")]
patterns = [0, 15999, 20999, 30999, 40999, 50999, 60999, 70999, 80999, 90999, 99999]


puts "open graphic device: #{Time.now - now}"

now = Time.now
open_dcl(iwidth, iheight, vpt) unless $use_parallel
($use_parallel ? Parallel.each_with_index(obs) : obs.each_with_index){|o, i|
  p obstime[i]

  if $use_parallel
    imgname = "rainrate-gmap_#{format('%04d', i + 1)}"
    if o.is_a?(String)
      system("cp -p #{o} #{imgname}.png")
      next
    end

    DCL::swpset("fname", imgname)
    open_dcl(iwidth, iheight, vpt)
  end

  GGraph.next_axes("xtitle" => "x grid num", "ytitle" => "y grid num", "xunits" => "", "yunits" => "")
  tmpdat = o

  if tmpdat
    rr = tmpdat.copy.replace_val(dbz2rr(tmpdat.val))
    rr.axis(0).set_pos(v_lon)
    rr.axis(1).set_pos(v_lat)

    param = DCLExt.uz_set_params("labelxb" => false, "labelyl" => false)
    GGraph.tone(rr, true, "min" => 0, "max" => 150, "levels" => levels, "patterns" => patterns, "tonc" => true, "auto" => false, "title" => "", "annotate" => false)
    DCL::uxsttl("T", "Valid: #{obstime[i].strftime('%Y/%m/%d %H:%M:%S')}", 0)
    DCL::uxsttl("T", "Observation", 0)
    DCL::uxsttl("T", "Rain rate (#{kz * 0.25}km)", 0)
    DCLExt.uz_set_params(param)
    #DCLExt.color_bar("tickintv" => 0, "constwidth" => true, "units" => "mm/h")

    #DCL::sgpset("lclip", true)
    #DCL::umpmap("GSHHS_h")
    DCL::umpmap("mlit_kansai_coast")
    DCL::umpmap("mlit_lake")
    #DCL::sgpset("lclip", false)
  else
    ### MISSING
    DCL::grsvpt(*vpt)
    DCL::grswnd(0, 1, 0, 1)
    DCL::grstrf
  end
  if $use_parallel
    DCL::grcls
    system("mv #{imgname}_0001.png #{imgname}.png && OMP_NUM_THREADS=1 convert -transparent white -crop #{iwidth}x#{iheight}+2+2 #{imgname}.png #{imgname}.png")
  end
}
puts "Plot obs: #{Time.now - now}"

now = Time.now
($use_parallel ? Parallel.each((split ? g.length : g.shape[3]).times.to_a) : (split ? g.length : g.shape[3]).times){|i|
  if $use_parallel
    imgname = "rainrate-gmap_#{format('%04d', obs.length + i + 1)}"
    DCL::swpset("fname", imgname)
    open_dcl(iwidth, iheight, vpt)
  end

  GGraph.next_axes("xtitle" => "x grid num", "ytitle" => "y grid num", "xunits" => "", "yunits" => "")
  tmpdat = g[i]

  rr = tmpdat.copy.replace_val(dbz2rr(tmpdat.val))
  rr.axis(0).set_pos(v_lon)
  rr.axis(1).set_pos(v_lat)

  param = DCLExt.uz_set_params("labelxb" => false, "labelyl" => false)
  GGraph.tone(rr, true, "min" => 0, "max" => 150, "levels" => levels, "patterns" => patterns, "tonc" => true, "auto" => false, "title" => "", "annotate" => false)
  DCL::uxsttl("T", "Valid: #{validtime.strftime('%Y/%m/%d %H:%M:%S')}", 0)
  DCL::uxsttl("T", "Init:  #{inittime.strftime('%Y/%m/%d %H:%M:%S')}", 0)
  DCL::uxsttl("T", "Rain rate (#{kz * 0.25}km)", 0)
  DCLExt.uz_set_params(param)
  #DCLExt.color_bar("tickintv" => 0, "constwidth" => true, "units" => "mm/h")

  #DCL::sgpset("lclip", true)
  #DCL::umpmap("coast_japan")
  #DCL::umpmap("GSHHS_h")
  #DCL::umpmap("GSHHS_i_L1")
  DCL::umpmap("mlit_kansai_coast")
  DCL::umpmap("mlit_lake")
  #DCL::sgpset("lclip", false)

  validtime += 30
  if $use_parallel
    DCL::grcls
    system("mv #{imgname}_0001.png #{imgname}.png && OMP_NUM_THREADS=1 convert -transparent white -crop #{iwidth}x#{iheight}+2+2 #{imgname}.png #{imgname}.png")
  end
}
puts "Plot fcst: #{Time.now - now}"
DCL::grcls


def deg2rad(x); x * Math::PI / 180.0; end
def rad2deg(x); x / Math::PI * 180.0; end

if $use_parallel
  lon_center = (lon1d.min + lon1d.max) * 0.5
  ymin = Math::log(Math::tan(Math::PI / 4 + deg2rad(lat1d.min) / 2))
  ymax = Math::log(Math::tan(Math::PI / 4 + deg2rad(lat1d.max) / 2))
  lat_center = rad2deg(2 * Math::atan(Math::exp((ymin + ymax) / 2)) - Math::PI / 2)
  offset = 34.805 - 34.82300186157227

  case Dir.pwd 
  when /suita/, /simpleda_20170510/, /simpleda_20130713/, /simpleda_20140911/
    observatory = "OSAKA"
  else
    observatory = "KOBE"
  end

  config = {
    "NUM_IMAGES" => tmax_fcst ? (timeflag.count(false) + tmax_fcst) : timetext.length,
    "GRAYOUT_COLOR_OBS" => "#000000",
    "GRAYOUT_COLOR_FCST" => "#0000FF",
    "GRAYOUT_COLOR_INIT" => "#000000",
    "GRAYOUT_OPACITY" => 0.2,
    "RAINRATE_IMAGE_PREFIX" => "#{imgpath}/rainrate-gmap_",
    "COORDINATES_SW" => [lat1d.min, lon1d.min],
    "COORDINATES_NE" => [lat1d.max, lon1d.max],
    "MAP_CENTER" => [lat_center + offset, lon_center],
    "TURNOFF_AUTO_UPDATE" => 1800000,
    "AUTO_UPDATE_FREQENCY" => 30000,
    "INITTIME" => inittime.strftime("%Y/%m/%d %H:%M:%S"),
    "INITUNIXTIME" => inittime.to_i,
    "OBSERVATORY" => observatory
  }

  if File.exist?("comment.txt")
    config["comment"] = File.read("comment.txt")
  end

  hash = {"forecast_flag_list" => timeflag, "image_timestamp_list" => timetext, "image_unixtime" => unixtime}
  hash["config"] = config
  File.open("dataformat.json", "w"){|f| f.puts hash.to_json}

  hash = {"forecast_flag_list" => timeflag, "image_timestamp_list" => timetext_en}
  hash["config"] = config
  File.open("dataformat.json_en", "w"){|f| f.puts hash.to_json}
end

=begin
system "scp -p rainrate*png weather.riken.jp:/srv/www/html/#{imgpath}/"
system "scp -p dataformat.json* weather.riken.jp:/srv/www/html/#{jsonpath}/"

system "ssh weather.riken.jp mkdir -p /srv/www_rw/data/pawr/archive/#{inittime.to_i}"
system "scp -p rainrate*png weather.riken.jp:/srv/www_rw/data/pawr/archive/#{inittime.to_i}/"
system "scp -p dataformat.json* weather.riken.jp:/srv/www_rw/data/pawr/archive/#{inittime.to_i}/"

system "ssh weather.riken.jp 'echo #{inittime.to_i} > /srv/www_rw/data/pawr/latestInitUnixTime'"
=end
