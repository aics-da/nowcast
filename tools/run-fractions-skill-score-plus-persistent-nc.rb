require "numru/gphys"

/(\d{4})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})/ =~ Dir.pwd || raise

year = $1
month = $2
day = $3
hour = $4
min = $5
sec = $6

time = Time.new(year, month, day, hour, min, sec)
dt = NumRu::GPhys::IO.open("rain_cart_0001.nc", "rain").get_att("date")
p time2 = Time.new(*dt)

#exit if time > time2
time = time2 if time2 > time

base = "/lfs01/otsuka/_OLD_DATA12_/nowcast_pawr/kobe_hindcast/out"

ary = []
21.times{|i|
  #ofile = time.strftime("/data13/nowcast_pawr/suita/out/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc")

  ofile = time.strftime(File.join(base, "%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc"))
  ofile = (time + 1).strftime(File.join(base, "%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc")) unless File.exist?(ofile)
  ofile = (time - 1).strftime(File.join(base, "%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc")) unless File.exist?(ofile)

  #ofile = time.strftime("/data13/nowcast_pawr/suita/obs/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)
  #ofile = (time + 1).strftime("/data13/nowcast_pawr/suita/obs/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)
  #ofile = (time - 1).strftime("/data13/nowcast_pawr/suita/obs/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)

  raise unless File.exist?(ofile)

  if /data13/ =~ ofile
    ofile_cache_dir = File.dirname(ofile).gsub!("/data13", "/dev/shm")
    system("mkdir -p #{ofile_cache_dir} && cp #{ofile} #{ofile_cache_dir}") || raise
    ofile = File.join(ofile_cache_dir, "rain_cart_0002.nc")
  end

  ary.push ofile
  time += 30
}

system("/home/otsuka/bitbucket/nowcast/tools/fractions-skill-score-plus-persistent-nc.exe #{ary.join(' ')}") || raise

