require "numru/gphys"

/(\d{4})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})/ =~ Dir.pwd || raise

year = $1
month = $2
day = $3
hour = $4
min = $5
sec = $6

time = Time.new(year, month, day, hour, min, sec)
dt = NumRu::GPhys::IO.open("rain_cart_0001.nc", "rain").get_att("date")
p time2 = Time.new(*dt)

#exit if time > time2
time = time2 if time2 > time

ary = []
21.times{|i|
  #ofile = time.strftime("/data13/nowcast_pawr/suita/out/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc")
  ofile = time.strftime("/data13/nowcast_pawr/suita/obs/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc")
  ofile = (time + 1).strftime("/data13/nowcast_pawr/suita/obs/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)
  ofile = (time - 1).strftime("/data13/nowcast_pawr/suita/obs/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)

  raise unless File.exist?(ofile)
  ary.push ofile
  time += 30
}

system("/home/otsuka/bitbucket/nowcast/tools/fractions-skill-score-persistent-nc.exe #{ary.join(' ')}") || raise

