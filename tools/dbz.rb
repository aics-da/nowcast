require "numru/ggraph"
include NumRu

puts "usage: ruby u.rb [id [xrange [yrange [kz [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

xrange = ARGV[1] ? eval(ARGV[1]) : true
yrange = ARGV[1] ? eval(ARGV[2]) : true

kz = 19
#kz = 59
#kz = 99
kz = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]

split = false
if File.exist?("raindata_#{id}_001.nc")
  split = true
  g = Dir.glob("raindata_#{id}_[0-9][0-9][0-9].nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
elsif File.exist?("raindata_#{id}.nc")
  p g = GPhys::IO.open("raindata_#{id}.nc", "rain")
else
  p g = GPhys::IO.open("raindata_#{id}.ctl", "var")
end
#if File.exist?("burgersU_#{id}.nc")
#  p u = GPhys::IO.open("burgersU_#{id}.nc", "u")
#else
#  p u = GPhys::IO.open("burgersU_#{id}.ctl", "var")
#end
kz = 0 if (split ? g[0] : g).shape[2] == 1

if split
else
  p g.axis(3).pos.units
  p g.axis(3).pos.val
end

DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
#DCL::sgscmn(14)
DCL::sgscmn(56)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.15, 0.75, 0.2, 0.8])
(split ? g.length : g.shape[3]).times{|i|
  #val = u[xrange, yrange, kz, i].val
  #puts "u[#{xrange}, #{yrange}, #{kz}, #{i}] min: #{val.min}, max: #{val.max}"
  #if val.is_a?(NArrayMiss)
  #  val.set_mask(val.get_mask * val.to_na.lt(1e4))
  #else
  #  val = NArrayMiss.to_nam_no_dup(val, val.lt(1e4))
  #end
  #p [val.min, val.max]
  GGraph.next_axes("xtitle" => "x grid num", "ytitle" => "y grid num", "xunits" => "", "yunits" => "")
  tmpdat = split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i]
  GGraph.tone(tmpdat, true, "min" => -20, "max" => 70, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "tonc" => true, "auto" => false, "title" => "dBZ(kz = #{kz}) FT = #{i * 30}s", "annotate" => false)
  DCLExt.color_bar("tickintv" => 0)
}

DCL::grcls

