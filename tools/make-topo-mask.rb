require "numru/ggraph"
require File.expand_path(File.dirname(__FILE__)) + "/parse_namelist"
include NumRu

max_topo = 30 #level
zrange = 0..(max_topo - 1)
min_val = 0

if File.exist?("setting.namelist")
  yml = parse_namelist("setting.namelist")
  p nt = yml["time_config"]["calnt"].to_i #setting[1].to_i
  p st = yml["time_config"]["start"].to_i #setting[2].to_i
  p iter = yml["time_config"]["iteration_all"].to_i #setting[3].to_i
  p nx = yml["domain_config"]["dx"].to_i #setting[9].to_i
  p ny = yml["domain_config"]["dy"].to_i #setting[10].to_i
  p nz = yml["domain_config"]["dz"].to_i #setting[11].to_i
  p posix = yml["domain_config"]["posix"].to_i #setting[15].to_i
  p posiy = yml["domain_config"]["posiy"].to_i #setting[16].to_i
  p posiz = yml["domain_config"]["posiz"].to_i #setting[17].to_i
  p yml["input_config"]["input_basename"] #setting[47]
  raise unless /\"(.*)\"/ =~ yml["input_config"]["input_basename"] #setting[47]
  path = File.dirname($1)
  puts "path = #{path}"
else
  setting = File.open("setting.dat"){|f| f.readlines}
  p nt = setting[1].to_i
  p st = setting[2].to_i
  p iter = setting[3].to_i
  p nx = setting[9].to_i
  p ny = setting[10].to_i
  p nz = setting[11].to_i
  p posix = setting[15].to_i
  p posiy = setting[16].to_i
  p posiz = setting[17].to_i
  p setting[47]
  raise unless /\"(.*)\"/ =~ setting[47]
  path = File.dirname($1)
  puts "path = #{path}"
end

truth = GPhys::IO.open(File.join(path, "radar3d.ctl"), "ref")

dat = nil
total = nil
truth.shape[-1].times{|it|
#(0..240).each{|it|
#(0..10).each{|it|
#(0..20).each{|it|
  p it
  val = truth[false, zrange, it].val
  mask = val.get_mask
  mask *= val.gt(min_val).to_na!(0) if min_val
  unless dat
    dat = mask
  else
    dat = dat.or(mask)
  end
  total ||= dat.total.to_f
  p dat.count_true / total
}

#p dat, dat.min, dat.max

imiss = -999
p idx = NArray.int(1, 1, dat.shape[2]).indgen!(1, 1)
valid_mask = dat.max(2).to_type(NArray::INT)
topo = (dat.to_type(NArray::INT) * idx)
topo = NArrayMiss.to_nam_no_dup(topo, topo.gt(0)).min(2).to_na!(0)

#p topo, topo.max, dat[700, 700, true], (dat.to_type(NArray::INT) * idx)[700, 700, true]

topo = topo * valid_mask + (1 - valid_mask) * imiss

#p topo, topo.max

#p (dat.to_type(NArray::INT) * idx).max

File.open("topo_mask.dat", "w"){|f| f.print topo.to_s}

DCL::gropn(1)
DCL::grfrm
DCL::grswnd(0, topo.shape[0] - 1, 0, topo.shape[1] - 1)
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grstrf

DCL::glrset("rmiss", imiss.to_f)
DCL::gllset("lmiss", true)

DCL::uegtla(1, max_topo, -max_topo)
DCL::uwsgxa(NArray.sfloat(topo.shape[0]).indgen!)
DCL::uwsgya(NArray.sfloat(topo.shape[1]).indgen!)
DCL::uetonc(topo.to_f)
DCL::usdaxs
#DCL::udcntz(topo.to_f)
DCLExt.color_bar

DCL::grcls
