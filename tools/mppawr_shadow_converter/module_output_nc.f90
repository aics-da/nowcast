module module_output_nc
  use netcdf
  use variables
  use module_io_common
  use module_nc_common
  implicit none

  integer ncid_rain, ncid_u, ncid_v, ncid_w, ncid_growth
  integer varid_rain, varid_u, varid_v, varid_w, varid_growth

  private ncid_rain, ncid_u, ncid_v, ncid_w, ncid_growth
  private varid_rain, varid_u, varid_v, varid_w, varid_growth

  public

contains

  subroutine open_output_files_nc(counter, l_rain, l_uvw, l_growth)
    integer, intent(in), optional :: counter
    logical, intent(in), optional :: l_rain, l_uvw, l_growth
    character(3) citer
    character(4) split
    logical :: l_rain_l = .true.
    logical :: l_uvw_l = .true.
    logical :: l_growth_l = .true.

    if(split_history == 1 .and. .not. present(counter)) then
       write(*, *) "error in open_output_files: counter is not specified"
       stop 999
    end if
    if(present(l_rain)) l_rain_l = l_rain
    if(present(l_uvw)) l_uvw_l = l_uvw
    if(present(l_growth)) l_growth_l = l_growth

    write(citer, '(I3.3)') start + iter - 1
    if(split_history == 1) then
       write(split, '("_", I3.3)') counter
    else
       split = ""
    end if
    fname1 = 'raindata_' // citer // trim(split) // '.nc'
    fname2 = 'burgersU_' // citer // trim(split) // '.nc'
    fname3 = 'burgersV_' // citer // trim(split) // '.nc'
    fname4 = 'burgersW_' // citer // trim(split) // '.nc'
    fname5 = 'growth_'   // citer // trim(split) // '.nc'
    if(io_proc_num(0) == myrank .and. l_rain_l) call define_nc(fname1, ncid_rain, "rain", "", varid_rain, ox, 1, oy, oz)
    if(io_proc_num(1) == myrank .and. l_uvw_l) call define_nc(fname2, ncid_u, "u", "m s-1", varid_u, dx, 1, dy, dz)
    if(io_proc_num(2) == myrank .and. l_uvw_l) call define_nc(fname3, ncid_v, "v", "m s-1", varid_v, dx, 1, dy, dz)
    if(io_proc_num(3) == myrank .and. l_uvw_l) call define_nc(fname4, ncid_w, "w", "m s-1", varid_w, dx, 1, dy, dz)
    if(switching_growth == 1 .and. io_proc_num(4) == myrank .and. l_growth_l) &
         &                       call define_nc(fname5, ncid_growth, "growth", "", varid_growth, dx, 1, dy, dz)
  end subroutine open_output_files_nc

  subroutine close_output_files_nc(l_rain, l_uvw, l_growth)
    logical, intent(in), optional :: l_rain, l_uvw, l_growth
    logical :: l_rain_l = .true.
    logical :: l_uvw_l = .true.
    logical :: l_growth_l = .true.
    if(present(l_rain)) l_rain_l = l_rain
    if(present(l_uvw)) l_uvw_l = l_uvw
    if(present(l_growth)) l_growth_l = l_growth
    if(io_proc_num(0) == myrank .and. l_rain_l) call check_nc_err(nf90_close(ncid_rain))
    if(io_proc_num(1) == myrank .and. l_uvw_l) call check_nc_err(nf90_close(ncid_u))
    if(io_proc_num(2) == myrank .and. l_uvw_l) call check_nc_err(nf90_close(ncid_v))
    if(io_proc_num(3) == myrank .and. l_uvw_l) call check_nc_err(nf90_close(ncid_w))
    if(switching_growth == 1 .and. io_proc_num(4) == myrank .and. l_growth_l) call check_nc_err(nf90_close(ncid_growth))
  end subroutine close_output_files_nc

  subroutine output_nc(l_out_rain, l_out_uvw, l_out_growth, output_count)
    logical, intent(in) :: l_out_rain, l_out_uvw, l_out_growth
    integer, intent(in) :: output_count
    integer tmptime1, tmptime2, timerate, timemax
    integer nc_count(4), nc_start(4)

    if(split_history == 1) then
       nc_start = (/ 1, 1, 1, 1 /)
    else
       nc_start = (/ 1, 1, 1, output_count /)
    end if

    if(l_out_rain .and. io_proc_num(0) == myrank) then
       nc_count = (/ ox, oy, oz, 1 /)
       call system_clock(tmptime1, timerate, timemax)
       write(*, '("myrank = ", I10, ", output nc rain, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_rain, varid_rain, &
            &            rain_t_single((o_offset_x + 1):(o_offset_x + ox), &
            &                          (o_offset_y + 1):(o_offset_y + oy), &
            &                          (o_offset_z + 1):(o_offset_z + oz)), &
            &            start = nc_start, count = nc_count))
       call system_clock(tmptime2, timerate, timemax)
       time_output_main = time_output_main + (tmptime2 - tmptime1) / dble(timerate)
    end if

    nc_count = (/ dx, dy, dz, 1 /)
    if(l_out_uvw .and. io_proc_num(1) == myrank) then
       write(*, '("myrank = ", I10, ", output nc u, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_u, varid_u, motion_t(1:dx, 1:dy, 1:dz, it2jt(it, 1), 1), start = nc_start, count = nc_count))
    end if
    if(l_out_uvw .and. io_proc_num(2) == myrank) then
       write(*, '("myrank = ", I10, ", output nc v, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_v, varid_v, motion_t(1:dx, 1:dy, 1:dz, it2jt(it, 1), 2), start = nc_start, count = nc_count))
    end if
    if(l_out_uvw .and. io_proc_num(3) == myrank) then
       write(*, '("myrank = ", I10, ", output nc w, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_w, varid_w, motion_t(1:dx, 1:dy, 1:dz, it2jt(it, 1), 3), start = nc_start, count = nc_count))
    end if
    if(switching_growth == 1 .and. l_out_growth .and. io_proc_num(4) == myrank) then
       write(*, '("myrank = ", I10, ", output nc growth, it = ", I10)') myrank, it
       call check_nc_err(nf90_put_var(ncid_growth, varid_growth, growth_t(1:dx, 1:dy, 1:dz, it2jt(it, 1), 1), start = nc_start, count = nc_count))
    end if
  end subroutine output_nc

  subroutine output_vector_nc(label, output_uvw, output_growth, output_quality, output_corr_diff)
    character(*), intent(in) :: label
    logical, intent(in) :: output_uvw, output_growth, output_quality, output_corr_diff
    character(3) citer
    integer ncid, varid
    integer nc_count(4), nc_start(4)

    !OUTPUT VECTOR to NetCDF
    if(myrank .ne. 0) return
    write(citer, '(I3.3)') start + iter - 1
    nc_count = (/ dx, dy, dz, 1 /)
    nc_start = (/ 1, 1, 1, 1 /)

    if(output_uvw .eqv. .true.) then
       call define_nc('vector_u_' // trim(label) // citer //'.nc', ncid, "u", "", varid, dx, 1, dy, dz)
       call check_nc_err(nf90_put_var(ncid, varid, motion_t(1:dx, 1:dy, 1:dz, 1, 1), start = nc_start, count = nc_count))
       call check_nc_err(nf90_close(ncid))

       call define_nc('vector_v_' // trim(label) // citer //'.nc', ncid, "v", "", varid, dx, 1, dy, dz)
       call check_nc_err(nf90_put_var(ncid, varid, motion_t(1:dx, 1:dy, 1:dz, 1, 2), start = nc_start, count = nc_count))
       call check_nc_err(nf90_close(ncid))

       call define_nc('vector_w_' // trim(label) // citer //'.nc', ncid, "w", "", varid, dx, 1, dy, dz)
       call check_nc_err(nf90_put_var(ncid, varid, motion_t(1:dx, 1:dy, 1:dz, 1, 3), start = nc_start, count = nc_count))
       call check_nc_err(nf90_close(ncid))
    end if
    if(output_growth .eqv. .true.) then
       call define_nc('initial_growth_' // trim(label) // citer //'.nc', ncid, "growth", "", varid, dx, 1, dy, dz)
       call check_nc_err(nf90_put_var(ncid, varid, growth_t(1:dx, 1:dy, 1:dz, 1, 1), start = nc_start, count = nc_count))
       call check_nc_err(nf90_close(ncid))
    end if
    if(output_quality .eqv. .true.) then
       call define_nc('quality_' // trim(label) // citer //'.nc', ncid, "quality", "", varid, dx, 1, dy, dz)
       call check_nc_err(nf90_put_var(ncid, varid, quality(1:dx, 1:dy, 1:dz), start = nc_start, count = nc_count))
       call check_nc_err(nf90_close(ncid))
    end if
    if(output_corr_diff .eqv. .true.) then
       call define_nc('corrdiff_' // trim(label) // citer //'.nc', ncid, "corrdiff", "", varid, dx, 1, dy, dz)
       call check_nc_err(nf90_put_var(ncid, varid, corr_diff(1:dx, 1:dy, 1:dz), start = nc_start, count = nc_count))
       call check_nc_err(nf90_close(ncid))
    end if
  end subroutine output_vector_nc

  subroutine output_cartesian_rain_nc(label, it, date)
    character(*), intent(in) :: label
    integer, intent(in) :: it, date(6)
    integer ncid, varid
    integer nc_count(4), nc_start(4)

    if(myrank .ne. 0) return
    write(*, '("myrank: ", I5.5, ", writing ", A)') myrank, 'rain_cart_' // trim(label) // '.nc'
    nc_count = (/ ddx, ddy, ddz, 1 /)
    nc_start = (/ 1, 1, 1, 1 /)
    call define_nc('rain_cart_' // trim(label) // '.nc', ncid, "rain", "", varid, ddx, 1, ddy, ddz)
    call check_nc_err(nf90_redef(ncid))
    call check_nc_err(nf90_put_att(ncid, varid, "date", date))
    call check_nc_err(nf90_enddef(ncid))
    call check_nc_err(nf90_put_var(ncid, varid, true_t(1:ddx, 1:ddy, 1:ddz, it, 1), start = nc_start, count = nc_count))
    call check_nc_err(nf90_close(ncid))
  end subroutine output_cartesian_rain_nc

  subroutine output_cartesian2_rain_nc(it, date)
    integer, intent(in) :: it, date(6)
    integer ncid, varid
    integer nc_count(4), nc_start(4)
    character(32) :: cart_name

    if(myrank .ne. 0) return
    write(cart_name, '(I4.4, 5I2.2, "-", I4.4, 5I2.2, ".nc")') date, date
    write(*, '("myrank: ", I5.5, ", writing ", A)') myrank, trim(cart_name)
    nc_count = (/ ox, oy, oz, 1 /)
    nc_start = (/ 1, 1, 1, 1 /)
    call define_nc(trim(cart_name), ncid, "rain", "", varid, ox, 1, oy, oz)
    call check_nc_err(nf90_redef(ncid))
    call check_nc_err(nf90_put_att(ncid, varid, "date", date))
    call check_nc_err(nf90_enddef(ncid))
    call check_nc_err(nf90_put_var(ncid, varid, &
         & true_t((posix + o_offset_x + 1):(posix + o_offset_x + ox), &
         &        (posiy + o_offset_y + 1):(posiy + o_offset_y + oy), &
         &        (posiz + o_offset_z + 1):(posiz + o_offset_z + oz), it, 1), &
         & start = nc_start, count = nc_count))
    call check_nc_err(nf90_close(ncid))
  end subroutine output_cartesian2_rain_nc
end module module_output_nc
