module variables
  integer, parameter :: real_size = 8

  integer dummy, l, m, n
  integer it, num, output_count, iter

!input/output file info
  character(1024) input_basename, wind_basename, qcf_basename
  character(1024) :: input_suffix = ".bin.gz"
  integer switching_input_missing
  double precision input_missing_newval
  double precision :: input_valid_min = -200.0d0
  double precision :: input_valid_max = 500.0d0
  double precision :: input_interval = 30.0 ! [seconds]
  double precision :: max_input_interval = huge(0.0d0)
  double precision :: output_interval = 30.0 ! [seconds]
  integer :: use_precomputed_vector = 0
  integer :: output_rain_cartesian = 0
  integer :: output_rain_cartesian2 = 0 !cropped to the output domain, with file name using date
  integer :: output_latlon = 0
  integer :: output_vector = 1
  integer :: output_init_growth = 1
  integer :: output_vector_quality = 1
  integer :: output_history = 1
  integer :: split_history = 0
  integer :: history_rain_max_it = -1
  integer :: history_uvw_max_it = -1
  integer :: history_growth_max_it = -1
  integer :: io_proc_permutation = 1
  integer :: output_da_anal = 0
  integer :: input_type = 0 !0: Cartesian, 1: PAWR QCed, 2: TOSHIBA, 3: NetCDF
  character(1024) :: input_nc_var = ""
  integer :: read_from_jit_dt = 0
  character(1024) :: jit_monitor_path
  integer :: ensemble_size = 1
  integer :: ensemble_share_input = 1
  integer :: monitor_mean_vector = 0
  integer, allocatable :: date(:, :)
  integer :: radar_qcf_mask(8) = 0

!vector missing value
  double precision :: vector_missing_threshold = -huge(0.0d0)
  double precision min_valid_grid_ratio, min_valid_direction_ratio
  real(4), parameter :: motion_undef = 123456.0

!parameter
  real dt ! Time step
  integer calnt !calculation time
  integer start !start time
  integer iteration_all 
  integer nt
  integer, parameter :: maxnt = 2 !max time steps stored on memory (including initial condition and one or more time steps for time integration) 
  integer, allocatable, dimension(:, :) :: it2jt

!grid
  integer :: ddx, ddy, ddz !INPUT DOMAIN SIZE
  integer :: dx, dy, dz !COMPUTATIONAL DOMAIN SIZE
  integer :: posix, posiy, posiz !OFFSET OF COMPUTATIONAL DOMAIN RELATIVE TO INPUT DOMAIN
  integer :: ox, oy, oz !OUTPUT DOMAIN SIZE
  integer :: o_offset_x, o_offset_y, o_offset_z !OFFSET OF OUTPUT DOMAIN RELATIVE TO COMPUTATIONAL DOMAIN
  real :: gridx, gridy, gridz !GRID SPASING
  real :: inv_gridx, inv_gridy, inv_gridz !INVERSE OF GRID SPACING
  integer :: boundary_type = 0
  integer margin_x, margin_x_adv
  integer dx_w_margin
!map projection
  integer :: proj_type = 0 !0: Lambert Conformal, 1: Mercator
  double precision, parameter :: map_undef = -999.9d9
  double precision :: base_x = map_undef
  double precision :: base_y = map_undef
  double precision :: base_lon = map_undef
  double precision :: base_lat = map_undef
  double precision :: std_lon = map_undef
  double precision :: std_lat1 = map_undef
  double precision :: std_lat2 = map_undef

  integer(4), parameter :: amax=1

  double precision, allocatable :: rain_t(:, :, :, :, :) !Rain
  real(4), allocatable :: rain_t_single(:, :, :) !Rain
  real(4), allocatable :: motion_t(:, :, :, :, :) !velocity
  real(4), allocatable :: true_t(:, :, :, :, :) !truth

!time integration method
  integer adv_a,adv_b,adv_c,adv_d,adv_e,adv_f
  double precision CFL, time, error_t
  double precision, allocatable :: adams_t(:, :, :, :, :) !time integration
  integer, dimension(3) :: i_adams = (/ 1, 2, 3 /)
  integer :: max_i_adams = 3
  integer :: it_switch_3d_to_2d = 1

!advection weno
  double precision, parameter :: ipu = 1.0d-18
  integer UVW
  double precision rain_missing
	
!vector
  integer frame_numz, frame_numz_2, frame_numz_2a
  integer frame_num, frame_num_2, frame_num_2a
  integer :: use_variable_frame_num = 0
  integer, allocatable :: variable_frame_num_y(:)
  integer search_num, search_numz, search_numx_max, search_numy_max
  integer :: use_variable_search_num = 0
  integer, allocatable :: search_numx(:), search_numy(:)
  integer :: adjust_search_num = 0
  real(4), allocatable :: frame_t(:, :, :, :, :)
  double precision :: rain_noise = -huge(0.0d0)
  real(4), allocatable :: quality(:, :, :) , corr_diff(:, :, :)
  double precision :: corr_qc_threshold
  double precision :: corr_diff_qc_threshold = 0.0d0
  integer :: corr_qc_normalize_diff_by_autocorr = 1
  integer :: match_by_sphere = 0
  integer :: search_within_sphere = 0
  integer :: x_skip = 1
  integer :: y_skip = 1
  integer :: vector_align_skip = 0
  integer :: switching_vector = 4
  integer :: switching_vector_centroid = 0
  double precision :: vector_centroid_variable_range = 0.1d0
  integer :: vector_centroid_global = 0
  integer :: horizontally_uniform_vector = 0
  integer :: vector_time_assignment = 0 ! 0: former, 1: latter
  double precision, allocatable :: mean_vect(:, :)
  integer, allocatable :: num_valid_vect(:)
  integer :: vector_output_corr_diff = 0
  !vector-set-core-minval
  integer :: vector_set_core_minval = 0
  double precision vector_core_radius, vector_core_minval

!noise
  real(4) sigma_noise,noise1,noise2,gnoise
  real(4) RMSE

!fuzzy 
  real(4),allocatable,dimension(:,:,:,:,:)::vector_fuzzy !velocity
  integer fuzzy_member
  double precision fuzzy_sad_min, fuzzy_sad_max

!3D
  character(4)  ex1
  character(1024) fname,fname1,fname2,fname3,fname4,fname5 

!growth rate
  real(4),allocatable,dimension(:,:,:,:,:)::growth_t 
  real(4) growthmax
  integer growth_rate_max_it

!mountain
  real(4), allocatable, dimension(:, :) :: topo, topo_fill
  integer read_topo_mask, topo_max
  real(4) :: topo_norain_threshold = -999.0 !value before conversion

!cotrec kitano
  real threshold
  integer countermax
  integer swiching_cotrec
  double precision sor_accel

!lanczos
  integer lanczos_window
  double precision lanczos_critical_wavelength

!swiching
  integer swiching_burgers
  integer :: switching_growth = 0
  integer tmpgrowth, switching_growth_weno
  integer switching_boundary_motion
  double precision mean_motion(3)
  integer switching_lanczos, switching_time_integration
  integer switching_vector_skip_missing

  integer expconv

  integer :: smooth_rain_field = 0
  integer :: smooth_rain_field_ratio_to_frame_num = 10

!filling missing
  integer :: fill_missing = 0
  real :: fill_missing_threshold = -1 !UNDEFINED
  double precision :: fill_missing_w = 1.0d0
  integer :: vect_interp_z_if_all_missing = 0

  !DATA ASSIMILATION
  integer :: use_data_assimilation = 0
  integer da_start_from
  real(4), allocatable, dimension(:, :, :, :) :: variance_f
  real(4) variance_f_init, variance_f_inflation, variance_o, da_qc_threshold
  real(4) :: variance_f_max = 10000.0
  integer :: da_gues_iter = -1 ! -1: use previous iter, >=0: use specified iter
  integer :: da_gues_rec_num = 2
  character(1024) :: da_gues_path
  integer :: da_qc_temp_consistency = 0
  double precision :: da_qc_temp_consistency_sigma = 5.0d0
  double precision :: da_qc_temp_consistency_threshold = 0.9d0
  integer :: da_qc_temp_consistency_hist_save_rejected = 0
  integer :: da_assimilate_boundary = 1
  integer :: da_assimilate_filled_wind = 0

  !divergence damper
  integer :: switching_div_damper = 0
  real(real_size) :: div_damper_param = 0.0001d0

  integer :: myrank = 0
  integer :: mpiprocs = 1
  integer, allocatable :: j_mpi(:, :)

  !radar qc
  integer :: use_radar_qc = 0
  integer :: radar_smooth_radial = 0
  integer :: radar_smooth_radial_wid = 2 + 1
  integer :: radar_smooth_azimuth = 0
  integer :: radar_smooth_elev = 0

  double precision time_adv_weno_mpi, time_adv_weno_dweno, time_adv_weno_mpi_fence, time_adv_weno_mpi_pack, time_adv_weno_mpi_unpack, time_adv_weno_mpi_put, time_adv_weno_mpi_init
  double precision time_adv_wind_mpi
end module variables
