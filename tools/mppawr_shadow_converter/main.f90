!///////////////////////////////////////////////////////////////
!/                                                             /
!/   Short-term Rain Forecasting utilizing Radar Data for 3D   /
!/                                                             /
!/   First release version: October 18, 2014                   /
!/                                                             /
!/   Developed by                                              /
!/    ---- Ryota Kikuchi (Tohoku University)                   /
!/    ---- Yoshikazu Kitano (Hokkaido University)              /
!/    ---- Yusuke Taniguchi (University of Hyogo)              /
!/    ---- Shigenori Otsuka (RIKEN AICS)                       /
!/    ---- Gulanbaier Tuerhong (RIKEN AICS)                    /
!/                                                             /
!/   References:                                               /
!/   Otsuka, S., G. Tuerhong, R. Kikuchi, Y. Kitano,           /
!/     Y. Taniguchi, J. J. Ruiz, S. Satoh, T. Ushio,           /
!/     and T. Miyoshi, 2016:                                   /
!/     Precipitation nowcasting with three-dimensional         /
!/     space-time extrapolation of dense and frequent          /
!/     phased-array weather radar observations.                /
!/     Wea. Forecasting, 31, 329-340.                          /
!/     doi:10.1175/WAF-D-15-0063.1                             /
!/                                                             /
!/   Otsuka, S., S. Kotsuki, and T. Miyoshi, 2016:             /
!/     Nowcasting with data assimilation:                      /
!/     a case of Global Satellite Mapping of Precipitation.    /
!/     Wea. Forecasting, 31, 1409-1416.                        /
!/     doi:10.1175/WAF-D-16-0039.1                             /
!/                                                             /
!/   Otsuka, S., S. Kotsuki, M. Ohhigashi, and T. Miyoshi,     /
!/     2019: GSMaP RIKEN Nowcast: Global precipitation         /
!/     nowcasting with data assimilation.                      /
!/     J. Meteor. Soc. Japan., 97, 1099-1117.                  /
!/     doi:10.2151/jmsj.2019-061                               /
!/                                                             /
!/   Contacts: shigenori.otsuka@riken.jp (Otsuka)              /
!/             rkikuchi@edge.ifs.tohoku.ac.jp (Kikuchi)        /
!/             appukin@gmail.com         (Kitano)              /
!/             taniguchi.y.jp@ieee.org   (Taniguchi)           /
!/                                                             /
!/   This software is distributed under New BSD License.       /
!/   (See LICENSE.txt for details.)                            /
!/                                                             /
!///////////////////////////////////////////////////////////////
program main
  use omp_lib
  use variables
  use module_io_common
  use module_input
  use module_output
  use module_output_nc
  implicit none
  
  !http://stackoverflow.com/questions/26730836/change-of-directory-in-fortran-in-a-non-compiler-specific-way
  interface
    integer function c_chdir(path) bind(C,name="chdir")
      use iso_c_binding
      character(kind=c_char) :: path(*)
    end function

    double precision function c_diff_date(date1, date2) bind(C,name="diff_date")
      use iso_c_binding
      integer(kind=c_int) :: date1(6), date2(6)
    end function c_diff_date
  end interface

  integer i, j, k, j_start, j_end, j_start_halo, j_end_halo, j_start_uvw, j_end_uvw, true_t_1, true_t_2
  integer :: member = 1
  integer time1, time2, time3, time4, time5, timerate, timemax
  double precision time_adv_rain, time_adv_wind, time_boundary, time_output, time_mpi, time_mpi_barrier, time_output_close
  integer mpi_roots(6)
  double precision output_target
  logical, volatile :: exchange_dat_to_output = .true.
  character(6) :: dir_prefix = "      "

  if(myrank == 0) then
     write(*, *) '======================================================================='
     write(*, *) '  Short-term Rain Forecasting utilizing Radar Data for 3D              '
     write(*, *) '                                    by Kikuchi, Kitano, and Taniguchi  '
     write(*, *) '                                    updated by Otsuka and Tuerhong     '
     write(*, *) '======================================================================='
  end if

  !setting matrix and switching
  call setting

  !INITIALIZE PARAMETERS
  true_t_1 = 1
  true_t_2 = 2

  if(ensemble_size > 1) then
     do member = 1, ensemble_size
        if(myrank == 0) write(*, *) "starting member", member
        write(dir_prefix, '(I5, "/")') member
        call chdir(trim(adjustl(dir_prefix)))
        call setting
        call main_main
        call chdir("..")
     end do
  else
     call main_main
  end if

  stop

contains

  !http://stackoverflow.com/questions/26730836/change-of-directory-in-fortran-in-a-non-compiler-specific-way
  subroutine chdir(path, err)
    use iso_c_binding
    character(*) :: path
    integer, optional, intent(out) :: err
    integer :: loc_err

    loc_err = c_chdir(path // c_null_char)

    if (present(err)) err = loc_err
  end subroutine chdir

  double precision function diff_date(date1, date2)
    use iso_c_binding
    integer :: date1(6), date2(6)
    integer(kind=c_int) :: cdate1(6), cdate2(6)
    cdate1 = date1
    cdate2 = date2
    diff_date = c_diff_date(cdate1, cdate2)
  end function diff_date

  subroutine main_main
    double precision input_interval_new, input_interval_ratio

    !clock time 
    if(myrank == 0) open(11, file = "time.dat")
    call system_clock(time1)
  
    !MAKE INDEX CONVERTER
    do j = 1, maxnt
       do i = 1, max(maxnt, calnt)
          it2jt(i, j) = mod((i - 1) - (j - 1) + maxnt, maxnt) + 1
       end do
    end do
    !it2jt = 1

    !MPI DIVISION
    allocate(j_mpi(2, 0:(mpiprocs - 1)))
    j_mpi(2, :) = dy / mpiprocs
    if(mod(dy, mpiprocs) > 0) j_mpi(2, 0:(mod(dy, mpiprocs) - 1)) = j_mpi(2, 0:(mod(dy, mpiprocs) - 1)) + 1
    j_mpi(1, 0) = 1
    do i = 1, mpiprocs - 1
       j_mpi(1, i) = j_mpi(2, i - 1) + 1
       j_mpi(2, i) = j_mpi(2, i - 1) + j_mpi(2, i)
    end do
    j_start = j_mpi(1, myrank)
    j_end = j_mpi(2, myrank)
    j_start_halo = j_start - 3
    j_end_halo = j_end + 3
    if(j_start_halo < 1) j_start_halo = 1
    if(j_end_halo > dy) j_end_halo = dy
    !write(*, '("OMP_GET_MAX_THREADS=", I3)') omp_get_max_threads() 
    !write(*, '("OMP_GET_NUM_PROCS=", I3)') omp_get_num_procs() 
    write(*, '("myrank=", I5, ", y=", I4, ":", I4)') myrank, j_mpi(1, myrank), j_mpi(2, myrank)
  
    if(.not. allocated(io_proc_num)) call create_io_proc_table

    do iter = 1, iteration_all
       tmpgrowth = switching_growth
     
       write(*, *) "start", iter
       allocate(motion_t((1 - margin_x):(dx + margin_x), dy, dz, maxnt, 3), &
            &   growth_t((1 - margin_x):(dx + margin_x), dy, dz, maxnt, 1))
!$omp parallel workshare
       motion_t = 0.0d0
       growth_t = 0.0d0
!$omp end parallel workshare

       !input
       call system_clock(time2)
       if(member == 1 .or. ensemble_share_input == 0) then
          allocate(true_t(ddx, ddy, ddz, nt, 1))
          call read_data
          if(date(2, 1) > 0 .and. date(2, 2) > 0) then !DATE IN FILE HEADER IS USED
             input_interval_new = diff_date(date(:, 2), date(:, 1)) !OVERRIDE
             if(input_interval_new .le. 0) then
                if(myrank == 0) write(*, *) "!!! input_interval is negative !!!"
                input_interval_new = -input_interval_new
                true_t_1 = 2
                true_t_2 = 1
             else
                true_t_1 = 1
                true_t_2 = 2
             end if
             input_interval_ratio = input_interval_new / input_interval
             input_interval = input_interval_new
             if(myrank == 0) write(*, *) "input_interval is updated to ", input_interval
             if(adjust_search_num == 1) then
                search_num = ceiling(dble(search_num) * input_interval_ratio)
                search_numz = ceiling(dble(search_numz) * input_interval_ratio)
                if(myrank == 0) then
                   write(*, *) "search_num is updated to ", search_num
                   write(*, *) "search_numz is updated to ", search_numz
                end if
             end if
          end if
       end if

       call system_clock(time3, timerate, timemax)
       if(myrank == 0) then
          write(11, *) "reading", (time3 - time2) / dble(timerate)
          !flush(11)
       end if

       if(allocated(frame_t)) deallocate(frame_t)
       if(allocated(motion_t)) deallocate(motion_t)
       if(allocated(growth_t)) deallocate(growth_t)
    end do !iter

    deallocate(j_mpi)
    if(allocated(io_proc_num)) deallocate(io_proc_num)

    call system_clock(time3, timerate, timemax)
    if(myrank == 0) then
       write(11, *) "all_time", (time3 - time1) / dble(timerate)
       close(11)
    end if
  end subroutine main_main

  subroutine convert_rain_to_single_for_output
    integer j, k
    integer tmptime1, tmptime2, timerate, timemax
    call system_clock(tmptime1, timerate, timemax)

    ! CONVERT TO SINGLE PRECISION
    if(expconv == 1) then
!$omp parallel do private(j, k) collapse(2)
       do k = 1, dz
          do j = j_start, j_end
             where(rain_t(1:dx, j, k, it2jt(it, 1), 1) .le. rain_missing)
                rain_t_single(1:dx, j, k) = log10(rain_missing) * 10.0
             elsewhere
                rain_t_single(1:dx, j, k) = log10(rain_t(1:dx, j, k, it2jt(it, 1), 1)) * 10.0
             end where
          end do !j
       end do !k
!$omp end parallel do
    elseif(expconv == 2) then
!$omp parallel do private(j, k) collapse(2)
       do k = 1, dz
          do j = j_start, j_end
             where(rain_t(1:dx, j, k, it2jt(it, 1), 1) .le. rain_missing)
                rain_t_single(1:dx, j, k) = log10((rain_missing ** 1.6d0) * 200.0d0) * 10.0d0
             elsewhere
                rain_t_single(1:dx, j, k) = log10((rain_t(1:dx, j, k, it2jt(it, 1), 1) ** 1.6d0) * 200.0d0) * 10.0d0
             end where
          end do !j
       end do !k
!$omp end parallel do
    else
!$omp parallel do private(j, k) collapse(2)
       do k = 1, dz
          do j = j_start, j_end
             rain_t_single(1:dx, j, k) = rain_t(1:dx, j, k, it2jt(it, 1), 1)
          end do !j
       end do !k
!$omp end parallel do
    end if
    call system_clock(tmptime2, timerate, timemax)
    time_output_datconv = time_output_datconv + (tmptime2 - tmptime1) / dble(timerate)
  end subroutine convert_rain_to_single_for_output

  subroutine rain_field_smoother
    real(real_size), allocatable :: tmpframe(:, :)
    integer ix, jy, kz, tt, xx, yy, box_x, box_y, xxx, yyy, jy0

    box_x = frame_num / smooth_rain_field_ratio_to_frame_num
    box_y = frame_num / smooth_rain_field_ratio_to_frame_num

    xx = box_x / 2
    yy = box_y / 2

    allocate(tmpframe((1 - frame_num_2 - search_numx_max + xx):(dx + frame_num_2a + search_numx_max - xx), &
         &            (1 - frame_num_2 - search_numy_max + yy):(dy + frame_num_2a + search_numy_max - yy)))

    do tt = 1, 2
       do kz = 1, dz
!$omp parallel
!$omp do private(ix, jy, jy0, xxx, yyy)
          do jy = 1 - frame_num_2 - search_numy_max + yy, dy + frame_num_2a + search_numy_max - yy
             if(use_variable_frame_num == 1) then
                jy0 = jy
                jy0 = max(jy0, 1)
                jy0 = min(jy0, dy)
                xxx = variable_frame_num_y(jy0) / (smooth_rain_field_ratio_to_frame_num * 2)
                yyy = variable_frame_num_y(jy0) / (smooth_rain_field_ratio_to_frame_num * 2)
             else
                xxx = xx
                yyy = yy
             end if
             do ix = 1 - frame_num_2 - search_numx_max + xx, dx + frame_num_2a + search_numx_max - xx
                tmpframe(ix, jy) = sum(frame_t((ix - xxx):(ix + xxx), (jy - yyy):(jy + yyy), kz, tt, 1)) / ((xxx * 2 + 1) * (yyy * 2 + 1))
             end do !ix
          end do !jy
!$omp end do
!$omp do private(jy)
          do jy = 1 - frame_num_2 - search_numy_max + yy, dy + frame_num_2a + search_numy_max - yy
             frame_t((1 - frame_num_2 - search_numx_max + xx):(dx + frame_num_2a + search_numx_max - xx), jy, kz, tt, 1) = tmpframe(:, jy)
          end do !jy
!$omp end do
!$omp end parallel
          if(boundary_type == 1) then !CYCLIC IN X
             frame_t((1 - frame_num_2 - search_numx_max):0, 1:dy, kz, tt, 1) &
                  & = frame_t((dx + 1 - frame_num_2 - search_numx_max):dx, 1:dy, kz, tt, 1)
             frame_t((dx + 1):(dx + frame_num_2a + search_numx_max), 1:dy, kz, tt, 1) &
                  & = frame_t(1:(frame_num_2a + search_numx_max), 1:dy, kz, tt, 1)
          end if
       end do !kz
    end do !tt

  end subroutine rain_field_smoother
end program main
