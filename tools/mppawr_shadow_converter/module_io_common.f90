module module_io_common
  use variables
  implicit none

  integer, allocatable :: io_proc_num(:)
  double precision time_output_datconv, time_output_main

contains

  subroutine create_io_proc_table
    integer i_file
    integer, parameter :: max_files = 5

    if(allocated(io_proc_num)) then
       write(*, *) "error in create_io_proc_table: io_proc_table is already allocated"
       stop 999
    end if
    allocate(io_proc_num(0:(max_files - 1)))
    if(mpiprocs > io_proc_permutation) then
       do i_file = 0, max_files - 1
          io_proc_num(i_file) = mod((i_file * io_proc_permutation), mpiprocs) + (i_file * io_proc_permutation) / mpiprocs 
       end do
    else
       do i_file = 0, max_files - 1
          io_proc_num(i_file) = mod(i_file, mpiprocs)
       end do
    end if
  end subroutine create_io_proc_table
end module module_io_common
