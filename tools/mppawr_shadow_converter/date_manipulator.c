#include <time.h>

double diff_date(int *date1, int *date2){
  struct tm tm1, tm2;
  time_t t1, t2;

  tm1.tm_year  = date1[0] - 1900;
  tm1.tm_mon   = date1[1];
  tm1.tm_mday  = date1[2];
  tm1.tm_hour  = date1[3];
  tm1.tm_min   = date1[4];
  tm1.tm_sec   = date1[5];
  tm1.tm_isdst = -1;

  tm2.tm_year  = date2[0] - 1900;
  tm2.tm_mon   = date2[1];
  tm2.tm_mday  = date2[2];
  tm2.tm_hour  = date2[3];
  tm2.tm_min   = date2[4];
  tm2.tm_sec   = date2[5];
  tm2.tm_isdst = -1;

  t1 = mktime(&tm1);
  t2 = mktime(&tm2);

  return difftime(t1, t2);
}
