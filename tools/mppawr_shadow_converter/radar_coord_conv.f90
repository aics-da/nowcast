module radar_coord_conv
  !use radar_qc
  use iso_c_binding
  use read_toshiba_mpr_f

  implicit none

  integer, parameter :: r_size = 4
  double precision, parameter :: Re = 6371000.0d0 ! radius of the earth [m]
  double precision, parameter :: ke = 4.0d0 / 3.0d0 !FACTOR FOR THE EFFECTIVE RADIUS OF THE EARTH
  double precision, parameter :: ke_Re = ke * Re ! effective radius of the earth [m]
  double precision, parameter :: PI = 3.1415926535897932384626d0
  double precision, parameter :: PI_over_2 = 3.1415926535897932384626d0 / 2.0d0
  double precision, parameter :: deg2rad = 3.1415926535897932384626d0 / 180.0d0
  double precision, parameter :: rad2deg = 180.0d0 / 3.1415926535897932384626d0
  double precision, parameter :: eps = tiny(0.0d0)
  integer, parameter :: int1 = selected_int_kind(1) !1-BYTE INT
  integer, parameter :: int2 = selected_int_kind(3) !2-BYTE INT
  integer, parameter :: int4 = selected_int_kind(5) !4-BYTE INT

  integer time1, time2, time3, time4, timerate, timemax
  double precision time_read_qced, time_radar_georef, time_lambert_conf, time_visibility, time_smooth, time_smooth_azim, time_interp, time_radar_qc
  !MPI
  integer :: mpiprocs, myrank, mpi_ierr

  private int1, int2, int4
  private time1, time2, time3, time4, timerate, timemax
  private time_read_qced, time_radar_georef, time_lambert_conf, time_visibility, time_smooth, time_smooth_azim, time_interp
  private mpiprocs, myrank, mpi_ierr
  private radar_mpi_init, radar_mpi_set_k_div

  public

contains

  subroutine radar_mpi_init
    mpiprocs = 1
    myrank = 0
  end subroutine radar_mpi_init

  subroutine radar_mpi_set_k_div(nr, k0, k1, k_start, k_end, sendcount, recvcounts, rdispls)
    integer, intent(in) :: nr, k0, k1
    integer, intent(out) :: k_start, k_end, sendcount, recvcounts(0:(mpiprocs - 1)), rdispls(0:(mpiprocs - 1))
    integer tmpmod, i, nz1

    nz1 = k1 - k0 + 1

    if(mpiprocs > nz1) then
       if(myrank < nz1) then
          k_start = k0 + myrank
          k_end   = k0 + myrank
       else
          k_start = -1
          k_end = -1
       end if
    else
       if(mod(nz1, mpiprocs) > myrank) then
          k_end   = k0 - 1 + (myrank + 1) * (nz1 / mpiprocs + 1)
          k_start = k_end - (nz1 / mpiprocs + 1) + 1
       else
          k_end   = k0 - 1 + (myrank + 1) * (nz1 / mpiprocs) + mod(nz1, mpiprocs)
          k_start = k_end - (nz1 / mpiprocs) + 1
       end if
    end if

    if(mpiprocs > nz1) then
       recvcounts(0:(nz1 - 1)) = nr
       recvcounts(nz1:(mpiprocs - 1)) = 0
    else
       recvcounts = nz1 / mpiprocs
       tmpmod = mod(nz1, mpiprocs)
       if(tmpmod > 0) recvcounts(0:(tmpmod - 1)) = recvcounts(0:(tmpmod - 1)) + 1
       recvcounts = recvcounts * nr
    end if
    recvcounts = recvcounts * 4
    rdispls = 0
    do i = 1, mpiprocs - 1
       rdispls(i) = rdispls(i - 1) + recvcounts(i - 1)
    end do
    sendcount = recvcounts(myrank)
  end subroutine radar_mpi_set_k_div

  subroutine rm_overlapped_elevation_angles(r_dim, ze, wind, qcf, qcf_mask, elevation, missing, missing_vr, missing_qcf)
    integer, intent(inout) :: r_dim(3)
    real(r_size), intent(inout) :: ze(r_dim(1), r_dim(2), r_dim(3)), elevation(r_dim(3))
    real(r_size), intent(inout), allocatable :: wind(:, :, :), qcf(:, :, :)
    integer, intent(in) :: qcf_mask(8)
    real(r_size), intent(in) :: missing, missing_vr, missing_qcf
    integer :: ne, ia, ir, ie, ii
    integer(kind = int1) :: tmp, valid

    !MERGE OVERLAPPED ELEVATION ANGLES (ASSUME MAX TWO BEAMS OVERLAPPED)

    valid = 0
    do ii = 1, 8
       if(qcf_mask(ii) > 0) valid = ibset(valid, ii - 1)
    end do

    ne = 1
    ie = 1
    do while(ie < r_dim(3))
       if(elevation(ie) .ge. elevation(ie + 1)) then
          !write(*, *) ie, elevation(ie)
          elevation(ne) = elevation(ie)
          if(allocated(qcf)) then
!$omp parallel do private(ia, ir, tmp)
             do ir = 1, r_dim(2)
                do ia = 1, r_dim(1)
                   tmp = int(qcf(ia, ir, ie), int1)
                   if((iand(valid, tmp) .eq. 0) .and. (ze(ia, ir, ie) .ne. missing)) then
                      tmp = int(qcf(ia, ir, ie + 1), int1)
                      if((iand(valid, tmp) .eq. 0) .and. (ze(ia, ir, ie + 1) .ne. missing)) then
                         ze(ia, ir, ne) = (ze(ia, ir, ie) + ze(ia, ir, ie + 1)) * 0.5d0
                         !qcf(ia, ir, ne) = ior(qcf(ia, ir, ie), qcf(ia, ir, ie + 1))
                         qcf(ia, ir, ne) = qcf(ia, ir, ie)
                      else
                         ze(ia, ir, ne) = ze(ia, ir, ie)
                         qcf(ia, ir, ne) = qcf(ia, ir, ie)
                      end if
                   else
                      tmp = int(qcf(ia, ir, ie + 1), int1)
                      if((iand(valid, tmp) .eq. 0) .and. (ze(ia, ir, ie + 1) .ne. missing)) then
                         ze(ia, ir, ne) = ze(ia, ir, ie + 1)
                         qcf(ia, ir, ne) = qcf(ia, ir, ie + 1)
                      else
                         ze(ia, ir, ne) = missing
                         qcf(ia, ir, ne) = missing_qcf
                      end if
                   end if
                end do !ia
             end do !ir
!$omp end parallel do
          else if(allocated(wind)) then !EXCLUSIVE
!$omp parallel do private(ia, ir, tmp)
             do ir = 1, r_dim(2)
                do ia = 1, r_dim(1)
                   if(ze(ia, ir, ie) .ne. missing) then
                      if(ze(ia, ir, ie + 1) .ne. missing) then
                         ze(ia, ir, ne) = (ze(ia, ir, ie) + ze(ia, ir, ie + 1)) * 0.5d0
                      else
                         ze(ia, ir, ne) = ze(ia, ir, ie)
                      end if
                   else
                      if(ze(ia, ir, ie + 1) .ne. missing) then
                         ze(ia, ir, ne) = ze(ia, ir, ie + 1)
                      else
                         ze(ia, ir, ne) = missing
                      end if
                   end if
                   if(wind(ia, ir, ie) .ne. missing_vr) then
                      if(wind(ia, ir, ie + 1) .ne. missing_vr) then
                         wind(ia, ir, ne) = (wind(ia, ir, ie) + wind(ia, ir, ie + 1)) * 0.5d0
                      else
                         wind(ia, ir, ne) = wind(ia, ir, ie)
                      end if
                   else
                      if(wind(ia, ir, ie + 1) .ne. missing_vr) then
                         wind(ia, ir, ne) = wind(ia, ir, ie + 1)
                      else
                         wind(ia, ir, ne) = missing_vr
                      end if
                   end if
                end do !ia
             end do !ir
!$omp end parallel do
          else
!$omp parallel do private(ia, ir, tmp)
             do ir = 1, r_dim(2)
                do ia = 1, r_dim(1)
                   if(ze(ia, ir, ie) .ne. missing) then
                      if(ze(ia, ir, ie + 1) .ne. missing) then
                         ze(ia, ir, ne) = (ze(ia, ir, ie) + ze(ia, ir, ie + 1)) * 0.5d0
                      else
                         ze(ia, ir, ne) = ze(ia, ir, ie)
                      end if
                   else
                      if(ze(ia, ir, ie + 1) .ne. missing) then
                         ze(ia, ir, ne) = ze(ia, ir, ie + 1)
                      else
                         ze(ia, ir, ne) = missing
                      end if
                   end if
                end do !ia
             end do !ir
!$omp end parallel do
          end if
          ie = ie + 1
       else
          if(ie .gt. ne) then !SHRINK ARRAY
             elevation(ne) = elevation(ie)
!$omp parallel do private(ia, ir)
             do ir = 1, r_dim(2)
                do ia = 1, r_dim(1)
                   ze(ia, ir, ne) = ze(ia, ir, ie)
                end do !ia
             end do !ir
!$omp end parallel do
             if(allocated(wind)) then
!$omp parallel do private(ia, ir)
                do ir = 1, r_dim(2)
                   do ia = 1, r_dim(1)
                      wind(ia, ir, ne) = wind(ia, ir, ie)
                   end do !ia
                end do !ir
!$omp end parallel do
             end if
             if(allocated(qcf)) then
!$omp parallel do private(ia, ir)
                do ir = 1, r_dim(2)
                   do ia = 1, r_dim(1)
                      qcf(ia, ir, ne) = qcf(ia, ir, ie)
                   end do !ia
                end do !ir
!$omp end parallel do
             end if
          end if
       end if
       ie = ie + 1
       ne = ne + 1
    end do
    if(ie .eq. r_dim(3)) then
       if(ie .gt. ne) then !SHRINK ARRAY
          elevation(ne) = elevation(ie)
!$omp parallel do private(ia, ir)
          do ir = 1, r_dim(2)
             do ia = 1, r_dim(1)
                ze(ia, ir, ne) = ze(ia, ir, ie)
             end do !ia
          end do !ir
!$omp end parallel do
          if(allocated(wind)) then
!$omp parallel do private(ia, ir)
             do ir = 1, r_dim(2)
                do ia = 1, r_dim(1)
                   wind(ia, ir, ne) = wind(ia, ir, ie)
                end do !ia
             end do !ir
!$omp end parallel do
          end if
          if(allocated(qcf)) then
!$omp parallel do private(ia, ir)
             do ir = 1, r_dim(2)
                do ia = 1, r_dim(1)
                   qcf(ia, ir, ne) = qcf(ia, ir, ie)
                end do !ia
             end do !ir
!$omp end parallel do
          end if
       end if
       ie = ie + 1
       ne = ne + 1
    end if
    r_dim(3) = ne - 1
    !write(*, *) "#elev: ", r_dim(3)

  end subroutine rm_overlapped_elevation_angles

  subroutine read_toshiba_mppawr_and_proj_to_map( &
       & reflectivity_file, read_from_jit_dt, jit_monitor_path, &
       & nx, k0, k1, dx, dy, dz, &
       & proj_type, bx, by, blon, blat, slon, slat1, slat2, map_undef, &
       & qc, qcf_mask, smooth_radial, smooth_radial_wid, smooth_azimuth, smooth_elev, &
       & output, lon, lat, z, date, error)
    use iso_c_binding
    character(len=*), intent(in) :: reflectivity_file, jit_monitor_path
    integer, intent(in) :: read_from_jit_dt, nx(3), k0, k1, proj_type, qc, qcf_mask(8)
    integer, intent(in) :: smooth_radial, smooth_radial_wid, smooth_azimuth, smooth_elev
    real(r_size), intent(in) :: dx, dy, dz
    double precision, intent(inout) :: bx, by, blon, blat, slon, slat1, slat2
    double precision, intent(in) :: map_undef
    real(r_size), intent(out) :: output(nx(1), nx(2), nx(3))
    double precision, intent(out) :: lon(nx(1), nx(2)), lat(nx(1), nx(2)), z(nx(3))
    integer, intent(out) :: date(6)
    integer, intent(inout) :: error

    real(r_size), allocatable :: azimuth(:), rrange(:), elevation(:)
    real(r_size), allocatable :: reflectivity(:, :, :), wind(:, :, :), qcf(:, :, :)
    real(r_size) :: lon0, lat0, z0, range_resolution, lambda, missing, missing_vr, missing_qcf
    integer :: r_dim(3), i, ia, ir, ie, ret
    integer :: nretry = 10
    real(c_float) :: c_azimuth(AZDIM, ELDIM), c_elevation(AZDIM, ELDIM)
    real(c_float), allocatable :: c_ref(:, :, :)
    real(c_float) :: tmpaz
    type(c_mppawr_header) :: hd
    integer, parameter :: root = 0
    real(4) :: tmpmpi(14)
    integer :: access !FILE INQUIRY
    character(33), parameter :: shadow_fname = "saitama-shadow-mask.dat"
    integer(4) :: shadow_na, shadow_ne, tmpshadow
    integer(2), allocatable :: shadow(:, :)
    real(8) :: shadow_del_az

    call radar_mpi_init

    allocate(c_ref(RDIM, AZDIM, ELDIM))
    !READ RADAR OBS
    if(myrank == 0) then
       call system_clock(time1, timerate, timemax)
       do i = 1, nretry
          error = 0
          write(*, *) "calling read_toshiba_mppawr for ze..."
          ret = read_toshiba_mpr(reflectivity_file, 0, hd, c_azimuth(:, :), c_elevation(:, :), c_ref)
          if(ret .ne. 0) then
             write(*, *) "error in decode_toshiba_mpr", i
             error = 1
             cycle !ERROR IN read_toshiba_mpr
          end if

          r_dim = (/ hd%ray_num, hd%range_num, hd%el_num /)
          !CHECK UNIFORMITY
          do ir = 1, r_dim(1)
             tmpaz = c_azimuth(ir, 1)
             if(r_dim(3) > 1) then
                do ie = 2, r_dim(3)
                   if(c_azimuth(ir, ie) .ne. tmpaz) then
                      write(*, *) "azimuth angle not uniform in a ray", i
                      error = 1
                      exit
                   end if
                end do !ie
                if(error .ne. 0) exit
             end if
          end do !ir
          if(error .ne. 0) cycle

          allocate(reflectivity(r_dim(1), r_dim(2), r_dim(3)), azimuth(r_dim(1)), rrange(r_dim(2)), elevation(r_dim(3)))
          azimuth(:) = c_azimuth(1:r_dim(1), 1) !ASSUME UNIFORMITY
          elevation(:) = maxval(c_elevation(:, 1:r_dim(3)), 1)

!$omp parallel do private(ia, ir, ie) collapse(3)
          do ie = 2, r_dim(3)
             do ir = 1, r_dim(2)
                do ia = 1, r_dim(1)
                   reflectivity(ia, ir, ie) = 1.0d0 !c_ref(ir, ia, ie) !!! SPECIAL TREATMENT FOR PRODUCING MASK DATA
                end do !ia
             end do !ir
          end do !ie
!$omp end parallel do
          date = (/ hd%s_yr, hd%s_mn, hd%s_dy, hd%s_hr, hd%s_mi, hd%s_sc /)
          lat0 = hd%latitude
          lon0 = hd%longitude
          z0   = hd%altitude
          range_resolution = hd%range_res
          missing = hd%mesh_offset

          lambda = 0
          if(error == 0) exit
       end do !i
       if(error .ne. 0 .and. i .ge. nretry) then
          write(*, *) "Reached to max error count. Abort."
          stop 999
       end if

       if(access(trim(shadow_fname), ' ') == 0) then
          write(*, '("reading ", A)') trim(shadow_fname)
          open(99, file = trim(shadow_fname), status = "old", access = "stream", form = "unformatted", convert = "little_endian")
          read(99) shadow_na, shadow_ne
          allocate(shadow(shadow_na, shadow_ne))
          read(99) shadow
          shadow_del_az = 360.0d0 / shadow_na
!$omp parallel do private(ia, ie, tmpshadow)
          do ie = 1, min(shadow_ne, r_dim(3))
             do ia = 1, r_dim(1)
                tmpshadow = shadow(mod(nint(azimuth(ia) / shadow_del_az), shadow_na) + 1, ie)
                if(tmpshadow .ne. 0) then
                   !write(*, *) "shadow: ", ia, ie
                   reflectivity(ia, tmpshadow:, ie) = missing
                end if
             end do !ia
          end do !ie
!$omp end parallel do
          deallocate(shadow)
          close(99)
       end if

       call system_clock(time2, timerate, timemax)
       write(11, *) "(read_toshiba_mppawr dbz: ", (time2 - time1) / dble(timerate), ")"
    end if

!$omp parallel do private(i)
    do ir = 1, r_dim(2)
       rrange(ir) = (real(ir, r_size) - 0.5) * range_resolution !MAYBE BETTER
    end do !ir
!$omp end parallel do

    call rm_overlapped_elevation_angles(r_dim, reflectivity, wind, qcf, qcf_mask, elevation, missing, missing_vr, missing_qcf)

    !CONVERT
    call convert_radar_obs_to_map_proj(nx, k0, k1, dx, dy, dz, &
         & proj_type, bx, by, blon, blat, slon, slat1, slat2, map_undef, &
         & lon0, lat0, z0, missing, &
         & r_dim, azimuth, rrange, elevation, &
         & reflectivity, wind, qcf, &
         & 0, & !FORCE QC = 0
         & qcf_mask, &
         & smooth_radial, smooth_radial_wid, smooth_azimuth, smooth_elev, &
         & output, lon, lat, z)

    if(allocated(azimuth)) deallocate(azimuth)
    if(allocated(rrange)) deallocate(rrange)
    if(allocated(elevation)) deallocate(elevation)
    if(allocated(reflectivity)) deallocate(reflectivity)
    if(allocated(wind)) deallocate(wind)
    if(allocated(qcf)) deallocate(qcf)

    return
  end subroutine read_toshiba_mppawr_and_proj_to_map

  subroutine convert_radar_obs_to_map_proj(nx, k0, k1, dx, dy, dz, &
       & proj_type, bx, by, blon, blat, slon, slat1, slat2, map_undef, &
       & lon0, lat0, z0, missing, &
       & r_dim, azimuth, rrange, elevation, reflectivity, wind, qcf, qc, qcf_mask, smooth_radial, smooth_radial_wid, smooth_azimuth, smooth_elev, &
       & output, lon, lat, z)
    integer, intent(in) :: nx(3), k0, k1, proj_type, qcf_mask(8)
    real(r_size), intent(in) :: dx, dy, dz
    double precision, intent(inout) :: bx, by, blon, blat, slon, slat1, slat2
    double precision, intent(in) :: map_undef
    real(r_size), intent(in) :: lon0, lat0, z0, missing
    integer, intent(in) :: r_dim(3)
    real(r_size), intent(in) :: azimuth(r_dim(1)), rrange(r_dim(2)), elevation(r_dim(3))
    real(r_size), intent(inout) :: reflectivity(r_dim(1), r_dim(2), r_dim(3))
    real(r_size), allocatable, intent(inout) :: wind(:, :, :), qcf(:, :, :)
    integer, intent(in) :: qc, smooth_radial, smooth_radial_wid, smooth_azimuth, smooth_elev
    real(r_size), intent(out) :: output(nx(1), nx(2), nx(3))
    double precision, intent(out) :: lon(nx(1), nx(2)), lat(nx(1), nx(2)), z(nx(3))

    double precision, allocatable :: radz(:, :, :), distance(:, :), lon_cyl(:, :), lat_cyl(:, :)
    logical, allocatable ::  visibility(:, :)
    double precision, allocatable :: distmap(:, :)
    integer k

    if(myrank == 0) write(*, *) "convert_radar_obs_to_map_proj"

    !GET RADAR GEOREFERENCE
    allocate(radz(r_dim(1), r_dim(2), r_dim(3)), distance(r_dim(2), r_dim(3)))
    call radar_georeference(z0, r_dim, rrange, elevation, radz, distance)

    !GENERATE NEW COORDINATE
    if(bx == map_undef) bx = dble(nx(1) + 1) * 0.5d0
    if(by == map_undef) by = dble(nx(2) + 1) * 0.5d0
    if(blon == map_undef) blon = dble(lon0)
    if(blat == map_undef) blat = dble(lat0)
    if(slon == map_undef) slon = dble(lon0)
    if(slat1 == map_undef .or. slat2 == map_undef) then
       slat1 = 30.0d0
       slat2 = 60.0d0
    end if
    if(proj_type == 0) then
       call lambert_conformal(dble(dx), dble(dy), bx, by, blon, blat, slon, slat1, slat2, nx(1), nx(2), lon, lat)
    else if(proj_type == 1) then
       call mercator(dble(dx), dble(dy), bx, by, blon, blat, nx(1), nx(2), lon, lat)
    else
       write(*, *) "Map projection type is invalid"
       stop 999
    end if
    do k = 1, nx(3)
       z(k) = (k - 1) * dz !VERTICALLY UNIFORM GRID
    end do

    !GET VISIBILITY
    allocate(visibility(nx(1), nx(2)), distmap(nx(1), nx(2)))
    call get_visibility(nx(1), nx(2), lon, lat, lon0, lat0, real(distance(r_dim(2), 1), r_size), visibility, distmap)

    call system_clock(time1, timerate, timemax)
    if(qc == 1) then
       !call radar_qc_spherical_coord(r_dim(1), r_dim(2), r_dim(3), rrange, radz, reflectivity, wind, missing)
    else if(qc == 2) then
       !call apply_qcf(r_dim, reflectivity, qcf, qcf_mask, missing)
    end if
    call system_clock(time2, timerate, timemax)
    time_radar_qc = (time2 - time1) / dble(timerate)

    call radar_fill_hole(r_dim, missing, reflectivity)
    call radar_fill_hole(r_dim, missing, reflectivity) !CALL TWICE
    call radar_fill_hole(r_dim, missing, reflectivity) !CALL THREE TIMES

    if(smooth_azimuth == 1) &
         & call radar_smooth_by_lanczos_azimuth(r_dim, missing, reflectivity, azimuth, rrange, (/ dx, dy, dz /))

    if(smooth_radial == 1) then
       if(myrank == 0) write(*, *) "radar_smooth_by_lancoz, manual", smooth_radial_wid
       call radar_smooth_by_lanczos(r_dim, missing, reflectivity, smooth_radial_wid)
    else if(minval((/ dx, dy, dz /)) > (rrange(2) - rrange(1)) * 1.5d0) then
       if(myrank == 0) write(*, *) "radar_smooth_by_lancoz, auto ", ceiling(dx / (rrange(2) - rrange(1))) + 1
       call radar_smooth_by_lanczos(r_dim, missing, reflectivity, ceiling(dx / (rrange(2) - rrange(1))) + 1)
    end if

    if(smooth_elev == 1) call radar_smooth_by_lanczos_elev(r_dim, missing, reflectivity, elevation, rrange, (/ dx, dy, dz /))

    !LAT-LON FOR INTERMEDIATE CYLINDRICAL COORDINATE
    allocate(lon_cyl(r_dim(1), r_dim(2)), lat_cyl(r_dim(1), r_dim(2)))
    call cylinder_georeference(lon0, lat0, r_dim(1), r_dim(2), azimuth, rrange, lon_cyl, lat_cyl)

    !INTERPOLATE (MPI PARALLELIZED)
    call radar_interpolate_3d(r_dim(1), r_dim(2), r_dim(3), rrange, radz, distance, lon_cyl, lat_cyl, &
         &                    nx(1), nx(2), nx(3), k0, k1, lon, lat, z, visibility, distmap, missing, reflectivity, output)

    deallocate(radz, distance, visibility, distmap, lon_cyl, lat_cyl)

    if(myrank == 0) then
       write(11, *) "(radar_georeference: ", time_radar_georef, ")"
       write(11, *) "(map_proj: ", time_lambert_conf, ")"
       write(11, *) "(get_visibility: ", time_visibility, ")"
       write(11, *) "(radar_qc: ", time_radar_qc, ")"
       write(11, *) "(smooth_by_lanczos_azimuth: ", time_smooth_azim, ")"
       write(11, *) "(smooth_by_lanczos: ", time_smooth, ")"
       write(11, *) "(radar_interpolate_3d: ", time_interp, ")"
    end if
    return
  end subroutine convert_radar_obs_to_map_proj

  subroutine radar_georeference(z0, r_dim, rrange, elevation, z, distance)
    real(r_size), intent(in) :: z0
    integer, intent(in) :: r_dim(3)
    real(r_size), intent(in) :: rrange(r_dim(2)), elevation(r_dim(3))
    double precision, intent(out) :: z(r_dim(1), r_dim(2), r_dim(3)), distance(r_dim(2), r_dim(3))

    double precision sin_elev_ke_Re_2, cos_elev_div_ke_Re, tmpdist
    integer ir, ie

    !THIS CODE IS COPIED FROM JUAN RUIZ'S QC CODE AND MODIFIED
    call system_clock(time1, timerate, timemax)
    if(myrank == 0) write(*, *) "radar_georeference"

!$omp parallel do private(ir, ie, sin_elev_ke_Re_2, cos_elev_div_ke_Re, tmpdist)
    do ie = 1, r_dim(3)
       sin_elev_ke_Re_2 = sin(elevation(ie) * deg2rad) * ke_Re * 2
       cos_elev_div_ke_Re = cos(elevation(ie) * deg2rad) / ke_Re
       do ir = 1, r_dim(2)
          !Perform standard height beam heigth computation.
          z(:, ir, ie) = z0 + sqrt(rrange(ir) * (rrange(ir) + sin_elev_ke_Re_2) + ke_Re * ke_Re) - ke_Re
          tmpdist = ke * asin(rrange(ir) * cos_elev_div_ke_Re)
          distance(ir, ie) = Re * tmpdist
       end do !ir
    end do !ie
!$omp end parallel do

    call system_clock(time2, timerate, timemax)
    time_radar_georef = (time2 - time1) / dble(timerate)

    return
  end subroutine radar_georeference

  subroutine cylinder_georeference(lon0, lat0, na, nr, azimuth, rrange, lon_cyl, lat_cyl)
    real(r_size), intent(in) :: lon0, lat0
    integer, intent(in) :: na, nr
    real(r_size), intent(in) :: azimuth(na), rrange(nr)
    double precision, intent(out) :: lon_cyl(na, nr), lat_cyl(na, nr)
    double precision cdist, sdist, sinll1, cosll1, sinll1_cdist, cosll1_sdist, cosll1_cdist, sinll1_sdist, tmpdist
    integer ia, ir
    double precision :: azimuth_rad, sin_azim(na), cos_azim(na)

    sinll1 = sin(lat0 * deg2rad)
    cosll1 = cos(lat0 * deg2rad)

!$omp parallel
!$omp do private(ia, azimuth_rad)
    do ia = 1, na
       azimuth_rad = azimuth(ia) * deg2rad
       sin_azim(ia) = sin(azimuth_rad)
       cos_azim(ia) = cos(azimuth_rad)
    end do !ia
!$omp end do

!$omp do private(ia, ir, cdist, sdist, sinll1_cdist, cosll1_sdist, cosll1_cdist, sinll1_sdist, tmpdist)
    do ir = 1, nr
       !THIS CODE IS COPIED FROM LETKF AND MODIFIED
       tmpdist = rrange(ir) / Re
       if (tmpdist .eq. 0d0) then
          lon_cyl(1:na, ir) = lon0
          lat_cyl(1:na, ir) = lat0
       else
          cdist = cos(tmpdist)
          sdist = sin(tmpdist)
          sinll1_cdist = sinll1 * cdist
          cosll1_sdist = cosll1 * sdist
          cosll1_cdist = cosll1 * cdist
          sinll1_sdist = sinll1 * sdist
          do ia = 1, na
             lat_cyl(ia, ir) = asin(sinll1_cdist + cosll1_sdist * cos_azim(ia)) * rad2deg
             lon_cyl(ia, ir) = lon0 + atan2(sdist * sin_azim(ia), cosll1_cdist - sinll1_sdist * cos_azim(ia)) * rad2deg
          end do !ia
       end if
    end do !ir
!$omp end do
!$omp end parallel
  end subroutine cylinder_georeference

  subroutine lambert_conformal(dx, dy, bi, bj, bx, by, sx, sy, sy2, imax, jmax, lon, lat)
    double precision, intent(in) :: dx, dy, bi, bj, bx, by, sx, sy, sy2
    integer, intent(in) :: imax, jmax
    double precision, intent(out) :: lon(imax, jmax), lat(imax, jmax)

    double precision :: bxr, byr, sxr, syr, sy2r
    double precision :: x, y, n, n_inv, f, rho0, xslon, x0, y0, theta, rho
    integer i, j

    !THIS CODE IS ORIGINALLY COPIED FROM ruby-NuSDaS AND MODIFIED
    call system_clock(time1, timerate, timemax)
    if(myrank == 0) then
       write(*, *) "creating grids on a Lambert Conformal projection"
       write(*, *) "bx, by: ", bx, by
       write(*, '("sx, sy, sy2: ", F10.6, ", ", F10.6, ", ", F10.6)') sx, sy, sy2
    end if

    if(abs(bx) .ge. 180.0d0) then
       write(*, *) "parameter for basepoint is invalide"
       stop 999
    end if
    if((dx .le. 0.0d0) .or. (dy .le. 0.0d0)) then
       write(*, *) "parameter for distance is invalide"
       stop 999
    end if
    if(abs(sx) .ge. 180.0d0) then
       write(*, *) "standard longitude is invalide"
       stop 999
    end if
    if(abs(sy) <= 0.0d0 .or. (abs(sy) .ge. abs(sy2)) .or. (abs(sy2) .ge. 90.0d0) .or. (sy * sy2 .le. 0.0d0)) then
       write(*, *) "standard latitude is invalide"
       stop 999
    end if

    bxr = bx * deg2rad
    byr = by * deg2rad
    sxr = sx * deg2rad
    syr = sy * deg2rad
    sy2r = sy2 * deg2rad

    n = log(cos(syr) / cos(sy2r)) / log(tan(PI / 4.0d0 + sy2r / 2.0d0) / tan(PI / 4.0d0 + syr / 2.0d0))
    n_inv = 1.0d0 / n
    f = cos(syr) * tan(PI / 4.0d0 + syr / 2.0d0) ** n * Re * n_inv
    rho0 = f / tan(PI / 4.0d0 + byr / 2.0d0) ** n

    xslon = bxr - sxr
    xslon = mod(xslon + 2.5d0 * PI, 2.0d0 * PI) - 0.5d0 * PI
    x0 = rho0 * sin(xslon * n)
    y0 = rho0 * cos(xslon * n)

!$omp parallel do private(i, j, x, y, theta, rho)
    do j = 1, jmax
       y = y0 - (j - bj) * dy
       do i = 1, imax
          x = x0 + (i - bi) * dx

          theta = atan(x / y)
          rho = sqrt(x ** 2 + y ** 2)

          if(n < 0) rho = -rho 
          lon(i, j) = (sxr + theta * n_inv) * rad2deg
          lat(i, j) = (2.0d0 * atan((f / rho) ** n_inv) - PI_over_2) * rad2deg
       end do
    end do
!$omp end parallel do

    call system_clock(time2, timerate, timemax)
    time_lambert_conf = (time2 - time1) / dble(timerate)
    return
  end subroutine lambert_conformal

  subroutine mercator(dx, dy, bi, bj, bx, by, imax, jmax, lon, lat)
    double precision, intent(in) :: dx, dy, bi, bj, bx, by
    integer, intent(in) :: imax, jmax
    double precision, intent(out) :: lon(imax, jmax), lat(imax, jmax)

    double precision :: bxr, byr
    double precision :: y0, factor
    integer i, j

    call system_clock(time1, timerate, timemax)
    if(myrank == 0) then
       write(*, *) "creating grids on a Mercator projection"
       write(*, *) "dx, dy: ", dx, dy
       write(*, *) "bi, bj: ", bi, bj
       write(*, *) "bx, by: ", bx, by
    end if

    if(abs(bx) .ge. 180.0d0) then
       write(*, *) "parameter for basepoint is invalide"
       stop 999
    end if
    if((dx .le. 0.0d0) .or. (dy .le. 0.0d0)) then
       write(*, *) "parameter for distance is invalide"
       stop 999
    end if

    bxr = bx * deg2rad
    byr = by * deg2rad

    y0 = Re * log(tan(PI * 0.25d0 + byr * 0.5d0))

    factor = 1.0d0 / sin(byr + PI_over_2)

!$omp parallel do private(i)
    do i = 1, imax
       lon(i, :) = bx + ((dble(i) - bi) * dx * factor / Re) * rad2deg
    end do
!$omp end parallel do

!$omp parallel do private(j)
    do j = 1, jmax
       lat(:, j) = (2.0d0 * atan(exp((y0 + (dble(j) - bj) * dy * factor) / Re)) - PI_over_2) * rad2deg
    end do
!$omp end parallel do

    call system_clock(time2, timerate, timemax)
    time_lambert_conf = (time2 - time1) / dble(timerate)
    return
  end subroutine mercator

  subroutine get_visibility(nx, ny, lon, lat, lon0, lat0, distance, visibility, distmap)
    integer, intent(in) :: nx, ny
    double precision, intent(in) :: lon(nx, ny), lat(nx, ny)
    real(r_size), intent(in) :: lon0, lat0, distance
    logical, intent(out) :: visibility(nx, ny)
    double precision, intent(out) :: distmap(nx, ny)

    double precision com_distll_lon2, lat2, com_distll_sin_lat2, com_distll_cos_lat2, lon1, lat1, cosd
    integer i, j

    call system_clock(time1, timerate, timemax)

    com_distll_lon2 = lon0 * deg2rad
    lat2 = lat0 * deg2rad

    com_distll_sin_lat2 = sin(lat2)
    com_distll_cos_lat2 = cos(lat2)

!$omp parallel do private(i, j, lon1, lat1, cosd)
    do j = 1, ny
       do i = 1, nx
          lon1 = lon(i, j) * deg2rad
          lat1 = lat(i, j) * deg2rad

          cosd = sin(lat1) * com_distll_sin_lat2 + cos(lat1) * com_distll_cos_lat2 * cos(com_distll_lon2 - lon1)
          cosd = min( 1.d0, cosd)
          cosd = max(-1.d0, cosd)

          distmap(i, j) = acos(cosd) * Re ! * ke
          if(distmap(i, j) > distance) then
             visibility(i, j) = .false.
          else
             visibility(i, j) = .true.
          end if
       end do
    end do
!$omp end parallel do

    call system_clock(time2, timerate, timemax)
    time_visibility = (time2 - time1) / dble(timerate)
    return
  end subroutine get_visibility

  subroutine radar_smooth_by_lanczos_azimuth(r_dim, missing, dat, azimuth, rrange, dx)
    integer, intent(in) :: r_dim(3)
    real(r_size), intent(in) :: missing, azimuth(r_dim(1)), rrange(r_dim(2)), dx(3)
    real(r_size), intent(inout) :: dat(r_dim(1), r_dim(2), r_dim(3))
    double precision :: fc, tmp, mindx, minda
    double precision, allocatable :: tmpdat(:, :), weight(:, :)
    integer ia, ir, ie, j, wavelength
    integer, allocatable :: wid(:)
    logical flag
    integer, parameter :: maxwid = 20
    integer, parameter :: wid_margin = 2

    call system_clock(time1, timerate, timemax)

    write(*, *) "lanczos azimuth before:", minval(dat), maxval(dat)

    !AZIMUTHAL FILTERING
    mindx = minval(dx)
    minda = minval(abs(azimuth(2:r_dim(1)) - azimuth(1:(r_dim(1) - 1)))) * deg2rad
    allocate(weight(-maxwid:maxwid, r_dim(2)), wid(r_dim(2)))
    do ir = 1, r_dim(2)
       !write(*, *) mindx, minda, minda * rrange(ir)
       wavelength = max(2 + 1, min(maxwid - wid_margin, 2 * int(mindx / (minda * rrange(ir)))))
       wid(ir) = wavelength + wid_margin
       fc = 2.0d0 * PI / dble(wavelength)
       do j = -wid(ir), wid(ir)
          if (j == 0) then
             weight(j, ir) = fc / PI
          else
             weight(j, ir) = (sin(j * fc) / (PI * j)) * (sin(j * PI / dble(wid(ir))) / (j * PI / dble(wid(ir))))
          end if
       end do !j
    end do
    write(*, *) "wid: ", wid
    write(*, *) weight(:, r_dim(2))
    write(*, *) sum(weight(:, r_dim(2)))

    allocate(tmpdat((1 - maxwid - 1):(r_dim(1) + maxwid + 1), 1))
!$omp parallel do private(ia, ir, ie, j, tmp, flag, tmpdat)
    do ie = 1, r_dim(3)
       do ir = 1, r_dim(2)
          tmpdat(1:r_dim(1), 1) = dat(:, ir, ie)
          tmpdat((1 - wid(ir) - 1):0, 1) = tmpdat((r_dim(1) - wid(ir) - 1):(r_dim(1) - 1), 1)
          tmpdat((r_dim(1) + 1):(r_dim(1) + wid(ir) + 1), 1) = tmpdat(1:(wid(ir) + 1), 1)
          do ia = 1, r_dim(1)
             if(tmpdat(ia, 1) == missing) cycle
             tmp = 0.0d0
             flag = .true.
             do j = -wid(ir), wid(ir)
                if(tmpdat(ia + j, 1) == missing) then
                   if(tmpdat(ia + j - 1, 1) == missing .or. tmpdat(ia + j + 1, 1) == missing) then
                      flag = .false.
                      exit
                   else
                      tmp = tmp + (tmpdat(ia + j - 1, 1) + tmpdat(ia + j + 1, 1)) * 0.5d0 * weight(j, ir)
                   end if
                else
                   tmp = tmp + tmpdat(ia + j, 1) * weight(j, ir)
                end if
             end do !j
             if(flag) dat(ia, ir, ie) = tmp
          end do !ia
       end do !ir
    end do !ie
!$omp end parallel do
    deallocate(tmpdat, weight)
    call system_clock(time2, timerate, timemax)
    time_smooth_azim = (time2 - time1) / dble(timerate)

    write(*, *) "lanczos azimuth after:", minval(dat), maxval(dat)

    return
  end subroutine radar_smooth_by_lanczos_azimuth

  subroutine radar_smooth_by_lanczos(r_dim, missing, dat, wid)
    integer, intent(in) :: r_dim(3), wid
    real(r_size), intent(in) :: missing
    real(r_size), intent(inout) :: dat(r_dim(1), r_dim(2), r_dim(3))
    double precision :: fc, tmp
    double precision, allocatable :: tmpdat(:, :), weight(:)
    integer ia, ir, ie, j
    logical flag
    integer, parameter :: wid_margin = 2

    call system_clock(time1, timerate, timemax)

    write(*, *) "lanczos before:", minval(dat), maxval(dat)
    write(*, *) wid

    if(wid .le. wid_margin) then
       write(*, *) "radar_smooth_by_lanczos: wid <= wid_margin"
       stop 999
    end if
    fc = 2.0d0 * PI / dble(wid - wid_margin)
    allocate(weight(-wid:wid))
    do j = -wid, wid
       if (j == 0) then
          weight(j) = fc / PI
       else
          weight(j) = (sin(j * fc) / (PI * j)) * (sin(j * PI / dble(wid)) / (j * PI / dble(wid)))
       end if
    end do !j

    write(*, *) weight
    write(*, *) sum(weight)

    allocate(tmpdat(r_dim(1), r_dim(2)))
!$omp parallel do private(ia, ir, ie, j, tmp, flag, tmpdat)
    do ie = 1, r_dim(3)
       tmpdat(:, :) = dat(:, :, ie)
       do ir = 1 + wid, r_dim(2) - wid
          do ia = 1, r_dim(1)
             if(tmpdat(ia, ir) == missing) cycle
             tmp = 0.0d0
             flag = .true.
             do j = -wid, wid
                if(tmpdat(ia, ir + j) == missing) then
                   if(ir == 1 + wid .or. ir == r_dim(2) - wid) then
                      flag = .false.
                      exit
                   else if(tmpdat(ia, ir + j - 1) == missing .or. tmpdat(ia, ir + j + 1) == missing) then
                      flag = .false.
                      exit
                   else
                      tmp = tmp + (tmpdat(ia, ir + j - 1) + tmpdat(ia, ir + j + 1)) * 0.5d0 * weight(j)
                   end if
                else
                   tmp = tmp + tmpdat(ia, ir + j) * weight(j)
                end if
             end do !j
             if(flag) dat(ia, ir, ie) = tmp
          end do !ia
       end do !ir
    end do !ie
!$omp end parallel do
    deallocate(tmpdat, weight)
    call system_clock(time2, timerate, timemax)
    time_smooth = (time2 - time1) / dble(timerate)

    write(*, *) "lanczos after:", minval(dat), maxval(dat)

    return
  end subroutine radar_smooth_by_lanczos

  subroutine radar_smooth_by_lanczos_elev(r_dim, missing, dat, elev, rrange, dx)
    integer, intent(in) :: r_dim(3)
    real(r_size), intent(in) :: missing, elev(r_dim(3)), rrange(r_dim(2)), dx(3)
    real(r_size), intent(inout) :: dat(r_dim(1), r_dim(2), r_dim(3))
    double precision :: fc, tmp, mindx, maxde
    double precision, allocatable :: tmpdat(:, :), weight(:, :)
    integer ia, ir, ie, k, wavelength
    integer, allocatable :: wid(:)
    logical flag
    integer, parameter :: maxwid = 20
    integer, parameter :: wid_margin = 2

    call system_clock(time1, timerate, timemax)

    write(*, *) "lanczos elev before:", minval(dat), maxval(dat)

    mindx = minval(dx)
    maxde = maxval(abs(elev(2:r_dim(3)) - elev(1:(r_dim(3) - 1)))) * deg2rad
    allocate(weight(-maxwid:maxwid, r_dim(2)), wid(r_dim(2)))
    do ir = 1, r_dim(2)
       !write(*, *) mindx, maxde, maxde * rrange(ir)
       wavelength = max(2 + 1, min(maxwid - wid_margin, 2 * int(mindx / (maxde * rrange(ir)))))
       wid(ir) = wavelength + wid_margin
       fc = 2.0d0 * PI / dble(wavelength)
       do k = -wid(ir), wid(ir)
          if (k == 0) then
             weight(k, ir) = fc / PI
          else
             weight(k, ir) = (sin(k * fc) / (PI * k)) * (sin(k * PI / dble(wid(ir))) / (k * PI / dble(wid(ir))))
          end if
       end do !k
    end do

    allocate(tmpdat(r_dim(1), r_dim(3)))
!$omp parallel do private(ia, ir, ie, k, tmp, flag, tmpdat)
    do ir = 1, r_dim(2)
       tmpdat = dat(:, ir, :)
       do ie = 1 + wid(ir), r_dim(3) - wid(ir)
          do ia = 1, r_dim(1)
             if(tmpdat(ia, ie) == missing) cycle
             tmp = 0.0d0
             flag = .true.
             do k = -wid(ir), wid(ir)
                if(tmpdat(ia, ie + k) == missing) then
                   if(ie == 1 + wid(ir) .or. ie == r_dim(3) - wid(ir)) then
                      flag = .false.
                      exit
                   else if(tmpdat(ia, ie + k - 1) == missing .or. tmpdat(ia, ie + k + 1) == missing) then
                      flag = .false.
                      exit
                   else
                      tmp = tmp + (tmpdat(ia, ie + k - 1) + tmpdat(ia, ie + k + 1)) * 0.5d0 * weight(k, ir)
                   end if
                else
                   tmp = tmp + tmpdat(ia, ie + k) * weight(k, ir)
                end if
             end do !j
             if(flag) dat(ia, ir, ie) = tmp
          end do !ia
       end do !ie
    end do !ir
!$omp end parallel do
    deallocate(tmpdat, weight)
    call system_clock(time2, timerate, timemax)
    time_smooth = (time2 - time1) / dble(timerate)

    write(*, *) "lanczos elev after:", minval(dat), maxval(dat)

    return
  end subroutine radar_smooth_by_lanczos_elev

  subroutine radar_fill_hole(r_dim, missing, dat)
    integer, intent(in) :: r_dim(3)
    real(r_size), intent(in) :: missing
    real(r_size), intent(inout) :: dat(r_dim(1), r_dim(2), r_dim(3))
    double precision :: tmp
    integer ia, ir, ie, ia0, ia1, count
    integer(1), allocatable :: flag(:, :, :)

    allocate(flag(r_dim(1), r_dim(2), r_dim(3)))

    write(*, *) "radar_fill_hole"
!$omp parallel do private(ia, ir, ie, ia0, ia1) collapse(2)
    do ie = 2, r_dim(3) - 1
       do ir = 2, r_dim(2) - 1
          do ia = 1, r_dim(1)
             flag(ia, ir, ie) = 0
             if(dat(ia, ir, ie) .ne. missing) cycle

             ia0 = mod(ia - 1 - 1, r_dim(1)) + 1
             ia1 = mod(ia - 1 + 1, r_dim(1)) + 1

             if((dat(ia0, ir, ie) .ne. missing) .and. dat(ia1, ir, ie) .ne. missing) then
                flag(ia, ir, ie) = ibset(flag(ia, ir, ie), 0)
             end if
             if((dat(ia, ir - 1, ie) .ne. missing) .and. dat(ia, ir + 1, ie) .ne. missing) then
                flag(ia, ir, ie) = ibset(flag(ia, ir, ie), 1)
             end if
             if((dat(ia, ir, ie - 1) .ne. missing) .and. dat(ia, ir, ie + 1) .ne. missing) then
                flag(ia, ir, ie) = ibset(flag(ia, ir, ie), 2)
             end if
          end do
       end do
    end do
!$omp end parallel do

!$omp parallel do private(ia, ir, ie, ia0, ia1, tmp, count) collapse(2)
    do ie = 2, r_dim(3) - 1
       do ir = 2, r_dim(2) - 1
          do ia = 1, r_dim(1)
             if(flag(ia, ir, ie) == 0) cycle

             ia0 = mod(ia - 1 - 1, r_dim(1)) + 1
             ia1 = mod(ia - 1 + 1, r_dim(1)) + 1

             tmp = 0
             count = 0
             if(btest(flag(ia, ir, ie), 0)) then
                tmp = tmp + dat(ia0, ir, ie) + dat(ia1, ir, ie)
                count = count + 2
             end if
             if(btest(flag(ia, ir, ie), 1)) then
                tmp = tmp + dat(ia, ir - 1, ie) + dat(ia, ir + 1, ie)
                count = count + 2
             end if
             if(btest(flag(ia, ir, ie), 2)) then
                tmp = tmp + dat(ia, ir, ie - 1) + dat(ia, ir, ie + 1)
                count = count + 2
             end if
             dat(ia, ir ,ie) = tmp / count
          end do
       end do
    end do
!$omp end parallel do

    deallocate(flag)

  end subroutine radar_fill_hole

  subroutine radar_interpolate_3d(na, nr, ne, rrange, radz, distance, lon_cyl, lat_cyl, &
       &                          nx, ny, nz, k0, k1, lon, lat, z, visibility, distmap, missval, input, output)
    integer, intent(in) :: na, nr, ne, nx, ny, nz, k0, k1 !MAX GRID NUMBER FOR EACH AXIS
    real(r_size), intent(in) :: rrange(nr)
    double precision, intent(in) :: radz(na, nr, ne), distance(nr, ne), lon_cyl(na, nr), lat_cyl(na, nr)
    double precision, intent(in) :: lon(nx, ny), lat(nx, ny), z(nz)
    logical, intent(in) :: visibility(nx, ny)
    double precision, intent(in) :: distmap(nx, ny)
    real(r_size), intent(in) :: missval
    real(r_size), intent(in) :: input(na, nr, ne)
    real(r_size), intent(out) :: output(nx, ny, nz)

    real(r_size) :: aidx(na + 1), ridx(nr), eidx(ne)
    real(r_size), allocatable :: tmpdat(:, :, :)
    double precision, allocatable :: tmpradz(:, :), tmplon0(:, :), tmplat0(:, :)
    double precision, allocatable :: ux2d(:, :), uy2d(:, :), px2d(:, :), py2d(:, :)
    double precision missval_dble, wx1, wx2, wy1, wy2, dist_axis(nr)
    integer ux0, ux1, uy0, uy1
    integer ia, ir, ie, id, kz, idkz
    integer k_start, k_end
    integer sendcount, recvcounts(0:(mpiprocs - 1)), rdispls(0:(mpiprocs - 1))
    logical :: debug = .false.

    write(*, *) "stage 0:", minval(input), maxval(input)

    call system_clock(time1, timerate, timemax)
    !write(*, *) myrank, " radar_interpolate_3d"

    call radar_mpi_set_k_div(nr, k0, k1, k_start, k_end, sendcount, recvcounts, rdispls)

!$omp parallel
!$omp do private(ia)
    do ia = 1, na + 1
       aidx(ia) = real(ia, r_size)
    end do
!$omp end do
!$omp do private(ir)    
    do ir = 1, nr
       ridx(ir) = real(ir, r_size)
       dist_axis(ir) = rrange(ir) !NO NEED TO MAKE A NEW VARIABLE
    end do
!$omp end do
!$omp do private(ie)
    do ie = 1, ne
       eidx(ie) = real(ie, r_size)
    end do
!$omp end do
!$omp do private(kz)
    do kz = k0, k1
       output(:, :, kz) = missval
    end do
!$omp end do
!$omp end parallel

    missval_dble = dble(missval)
!if(debug) write(*, *) "missval_dble", missval_dble
    !INTERPOLATION AT EACH AZIMUTH
    call system_clock(time3, timerate, timemax)
    !write(*, *) myrank, " radar_interpolate_3d: each azimuth"
    allocate(ux2d(1:nr, k0:k1), uy2d(1:nr, k0:k1), px2d(1:nr, k0:k1), py2d(1:nr, k0:k1))
    if(sendcount > 0) then
       allocate(tmpradz(nr, ne))
       tmpradz(:, :) = radz(1, :, :)
!$omp parallel do private(idkz, id, kz)
       do idkz = 0, nr * (k_end - k_start + 1) - 1
          id = mod(idkz, nr) + 1
          kz = idkz / nr + k_start !MPI PARALLELIZED
          !call g2ictr_new2(nr, ne, distance(id, 1), z(kz), ridx, eidx, distance, tmpradz, &
          !     &           ux2d(id, kz), uy2d(id, kz), px2d(id, kz), py2d(id, kz))
          call g2ictr_non_monotonic(nr, ne, dist_axis(id), z(kz), ridx, eidx, distance, tmpradz, &
               &                    ux2d(id, kz), uy2d(id, kz), px2d(id, kz), py2d(id, kz))
       end do !idkz
!$omp end parallel do
       deallocate(tmpradz)
    end if !sendcount > 0
    call system_clock(time4, timerate, timemax)
    !write(*,*) myrank, " each azimuth ", (time4 - time3) / dble(timerate)

    !if(sendcount > 0) write(*, *) myrank, " tmpdat_l max: ", maxval(tmpdat_l(:, :, k_start:k_end))

    time4 = time3
    allocate(tmpdat(na + 1, nr, k0:k1), tmplon0(na + 1, nr), tmplat0(na + 1, nr))
!$omp parallel do private(ia, id, kz, ux0, ux1, uy0, uy1, wx1, wx2, wy1, wy2)
    do kz = k0, k1
       do id = 1, nr
          if(ux2d(id, kz) < 1.0d0 .or. uy2d(id, kz) < 1.0d0) then !MISSING
             tmpdat(:, id, kz) = missval
          else
             ux0 = floor(ux2d(id, kz))
             ux1 = ceiling(ux2d(id, kz))
             uy0 = floor(uy2d(id, kz))
             uy1 = ceiling(uy2d(id, kz))

             !USE THE SAME POSITION INFO FOR ALL AZIMUTH
             wx1 = 1.0d0 - px2d(id, kz)
             wx2 = px2d(id, kz)
             wy1 = 1.0d0 - py2d(id, kz)
             wy2 = py2d(id, kz)

             do ia = 1, na
                if(input(ia, ux0, uy0) .ne. missval) then
                   if(input(ia, ux1, uy0) .ne. missval) then
                      if(input(ia, ux0, uy1) .ne. missval) then
                         if(input(ia, ux1, uy1) .ne. missval) then !all valid
                            tmpdat(ia, id, kz) = ((input(ia, ux0, uy0) * wx1 + &
                                 &                 input(ia, ux1, uy0) * wx2) * wy1 + &
                                 &                (input(ia, ux0, uy1) * wx1 + &
                                 &                 input(ia, ux1, uy1) * wx2) * wy2)
                         else !(ux0, uy0), (ux1, uy0), (ux0, uy1) valid
                            if(wx1 .ge. 0.5d0) then
                               if(wy1 .ge. 0.5d0) then
                                  tmpdat(ia, id, kz) = input(ia, ux0, uy0)
                               else
                                  tmpdat(ia, id, kz) = input(ia, ux0, uy1)
                               end if
                            else if(wy1 .ge. 0.5d0) then
                               tmpdat(ia, id, kz) = input(ia, ux1, uy0)
                            else
                               tmpdat(ia, id, kz) = missval
                            end if
                         end if
                      else if(input(ia, ux1, uy1) .ne. missval) then !(ux0, uy0), (ux1, uy0), (ux1, uy1) valid
                         if(wx1 .lt. 0.5d0) then
                            if(wy1 .ge. 0.5d0) then
                               tmpdat(ia, id, kz) = input(ia, ux1, uy0)
                            else
                               tmpdat(ia, id, kz) = input(ia, ux1, uy1)
                            end if
                         else if(wy1 .ge. 0.5d0) then
                            tmpdat(ia, id, kz) = input(ia, ux0, uy0)
                         else
                            tmpdat(ia, id, kz) = missval
                         end if
                      else
                         tmpdat(ia, id, kz) = missval
                      end if
                   else if(input(ia, ux0, uy1) .ne. missval .and. input(ia, ux1, uy1) .ne. missval) then !(ux0, uy0), (ux0, uy1), (ux1, uy1) valid
                      if(wx1 .ge. 0.5d0) then
                         if(wy1 .lt. 0.5d0) then
                            tmpdat(ia, id, kz) = input(ia, ux0, uy1)
                         else
                            tmpdat(ia, id, kz) = input(ia, ux0, uy0)
                         end if
                      else if(wy1 .lt. 0.5d0) then
                         tmpdat(ia, id, kz) = input(ia, ux1, uy1)
                      else
                         tmpdat(ia, id, kz) = missval
                      end if
                   else
                      tmpdat(ia, id, kz) = missval
                   end if
                else if(input(ia, ux1, uy0) .ne. missval .and. input(ia, ux0, uy1) .ne. missval .and. input(ia, ux1, uy1) .ne. missval) then !(ux0, uy1), (ux1, uy0), (ux1, uy1) valid
                   if(wx1 .lt. 0.5d0) then
                      if(wy1 .lt. 0.5d0) then
                         tmpdat(ia, id, kz) = input(ia, ux1, uy1)
                      else
                         tmpdat(ia, id, kz) = input(ia, ux1, uy0)
                      end if
                   else if(wy1 .lt. 0.5d0) then
                      tmpdat(ia, id, kz) = input(ia, ux0, uy1)
                   else
                      tmpdat(ia, id, kz) = missval
                   end if
                else
                   tmpdat(ia, id, kz) = missval
                end if
             end do !ia

             !CYCLIC BOUNDARY
             tmpdat(na + 1, id, kz) = tmpdat(1, id, kz)
          end if
       end do !id
    end do !kz
!$omp end parallel do
    deallocate(ux2d, uy2d, px2d, py2d)

!$omp parallel do private(id)
    do id = 1, nr
       tmplon0(1:na, id) = lon_cyl(1:na, id)
       tmplat0(1:na, id) = lat_cyl(1:na, id)

       tmplon0(na + 1, id) = tmplon0(1, id)
       tmplat0(na + 1, id) = tmplat0(1, id)
    end do
!$omp end parallel do

    call system_clock(time3, timerate, timemax)
    !write(*,*) myrank, " tmpdat, tmplon0, tmplat0 ", (time3 - time4) / dble(timerate)
    write(*, *) "stage 1:", minval(tmpdat), maxval(tmpdat)

    time3 = time4

    !INTERPOLATION AT EACH Z
    call radar_interp_cyl2cart_continuous( &
         & na, nr, aidx, ridx, tmpdat, tmplon0, tmplat0, dist_axis, &
         & nx, ny, nz, k0, k1, lon, lat, visibility, distmap, missval, missval_dble, output, debug)

    deallocate(tmpdat, tmplon0, tmplat0)

    write(*, *) "stage 2:", minval(output), maxval(output)

    call system_clock(time2, timerate, timemax)
    time_interp = (time2 - time1) / dble(timerate)

    return
  end subroutine radar_interpolate_3d

  subroutine radar_interp_cyl2cart_continuous( &
       & na, nr, aidx, ridx, tmpdat, tmplon0, tmplat0, dist_axis, &
       & nx, ny, nz, k0, k1, lon, lat, visibility, distmap, missval, missval_dble, output, debug)
    integer, intent(in) :: na, nr, nx, ny, nz, k0, k1 !MAX GRID NUMBER FOR EACH AXIS
    real(r_size), intent(in) :: aidx(na + 1), ridx(nr)
    double precision, intent(in) :: dist_axis(nr)
    double precision, intent(in) :: lon(nx, ny), lat(nx, ny)
    real(r_size), intent(in) :: tmpdat(na + 1, nr, k0:k1)
    double precision, intent(in) :: tmplon0(na + 1, nr), tmplat0(na + 1, nr)
    logical, intent(in) :: visibility(nx, ny)
    double precision, intent(in) :: distmap(nx, ny)
    real(r_size), intent(in) :: missval
    double precision, intent(in) :: missval_dble
    real(r_size), intent(inout) :: output(nx, ny, nz)
    logical, intent(in) :: debug

    double precision ux, uy, px, py
    integer ux0, ux1, uy0, uy1
    integer ir, ix, jy, kz, i0, i1, j1, ij, m
    real(4), allocatable :: sendbuf(:, :, :), recvbuf(:, :, :)
    integer jy0(0:(mpiprocs - 1)), jy1(0:(mpiprocs - 1)), djy(0:(mpiprocs - 1))

    call system_clock(time3, timerate, timemax)

    djy = ny / mpiprocs
    if(mod(ny, mpiprocs) > 0) djy(0:(mod(ny, mpiprocs) - 1)) = ny / mpiprocs + 1
    jy0(0) = 1
    do m = 1, mpiprocs - 1
       jy0(m) = jy0(m - 1) + djy(m - 1)
    end do
    jy1 = jy0 + djy - 1

    i0 = 1
!$omp parallel do private(ij, ix, jy, kz, ir, ux, uy, px, py, ux0, ux1, uy0, uy1, i1, j1, debug) firstprivate(i0)
    do ij = 0, nx * djy(myrank) - 1 !MPI PARALLELIZED
       jy = ij / nx + jy0(myrank)
       ix = mod(ij, nx) + 1

       if(.not. visibility(ix, jy)) then
          cycle
       end if
       do ir = 1, nr - 1
          if(distmap(ix, jy) >= dist_axis(ir) .and. distmap(ix, jy) <= dist_axis(ir + 1)) exit
       end do
       call g2ictr_pol2cart(na + 1, nr, lon(ix, jy), lat(ix, jy), aidx, ridx, tmplon0, tmplat0, i0, ir, &
            &                    missval_dble, ux, uy, px, py, i1, j1, debug)
       if(ux < 1.0d0 .or. uy < 1.0d0) then !MISSING
          i0 = 1
       else
          i0 = i1
          ux0 = floor(ux)
          ux1 = ceiling(ux)
          if(ux1 > na) ux1 = 1 !CYCLIC
          uy0 = floor(uy)
          uy1 = ceiling(uy)
          !USE THE SAME XY POSITION INFO FOR ALL Z
          do kz = k0, k1
             if(tmpdat(ux0, uy0, kz) == missval .or. tmpdat(ux1, uy0, kz) == missval .or. &
                  & tmpdat(ux0, uy1, kz) == missval .or. tmpdat(ux1, uy1, kz) == missval) then
             else
                output(ix, jy, kz) = ((tmpdat(ux0, uy0, kz) * (1.0d0 - px) + &
                     &                 tmpdat(ux1, uy0, kz) * px) * (1.0d0 - py) + &
                     &                (tmpdat(ux0, uy1, kz) * (1.0d0 - px) + &
                     &                 tmpdat(ux1, uy1, kz) * px) * py)
             end if
          end do !kz
       end if
    end do !ij
!$omp end parallel do
    call system_clock(time4, timerate, timemax)
    !write(*, *) myrank, " each z (cont) ", (time4 - time3) / dble(timerate)

    !MPI COMM
    if(allocated(sendbuf)) deallocate(sendbuf)
    if(allocated(recvbuf)) deallocate(recvbuf)

    !END OF MPI COMM
  end subroutine radar_interp_cyl2cart_continuous

  subroutine g2ictr_new2(nx, ny, cx, cy, uxs, uys, cxs, cys, ux, uy, px, py)
    integer, intent(in) :: nx, ny
    double precision, intent(in) :: cx, cy !TARGET VALUE
    real(r_size), intent(in) :: uxs(nx), uys(ny) !OLD AXES
    double precision, intent(in) :: cxs(nx, ny), cys(nx, ny) ! VALUES ON THE OLD COORDINATE
    double precision, intent(out) :: ux, uy !TARGET POSITION ON THE OLD COORDINATE
    double precision, intent(out) :: px, py !TARGET POSITION RELATIVE TO THE SURROUNDING OLD GRID POINTS (0-1)

    integer i, j
    integer :: sign_cxsx0 = 1
    integer :: sign_cysx0 = 1
    integer :: sign_cxsx1 = 1
    integer :: sign_cysx1 = 1

    ux = minval(uxs) - 1.0d0 !MISSING VALUE
    uy = minval(uys) - 1.0d0 !MISSING VALUE

    do j = 1, ny - 1
       !monotonicity assumed
       if(cxs(1, j) > cxs(nx, j)) sign_cxsx0 = -1
       if(cys(1, j) > cys(nx, j)) sign_cysx0 = -1
       if(cxs(1, j + 1) > cxs(nx, j + 1)) sign_cxsx1 = -1
       if(cys(1, j + 1) > cys(nx, j + 1)) sign_cysx1 = -1

       if((cx * sign_cxsx0 < cxs(1, j) * sign_cxsx0) .and. (cx * sign_cxsx1 < cxs(1, j + 1) * sign_cxsx1)) cycle
       if((cx * sign_cxsx0 > cxs(nx, j) * sign_cxsx0) .and. (cx * sign_cxsx1 > cxs(nx, j + 1) * sign_cxsx1)) cycle
       if((cy * sign_cysx0 < cys(1, j) * sign_cysx0) .and. (cy * sign_cysx1 < cys(1, j + 1) * sign_cysx1)) cycle
       if((cy * sign_cysx0 > cys(nx, j) * sign_cysx0) .and. (cy * sign_cysx1 > cys(nx, j + 1) * sign_cysx1)) cycle

       do i = 1, nx - 1
          if((cx * sign_cxsx0 < cxs(i, j) * sign_cxsx0) .and. (cx * sign_cxsx1 < cxs(i, j + 1) * sign_cxsx1)) cycle
          if((cx * sign_cxsx0 > cxs(i + 1, j) * sign_cxsx0) .and. (cx * sign_cxsx1 > cxs(i + 1, j + 1) * sign_cxsx1)) cycle
          if((cy * sign_cysx0 < cys(i, j) * sign_cysx0) .and. (cy * sign_cysx1 < cys(i, j + 1) * sign_cysx1)) cycle
          if((cy * sign_cysx0 > cys(i + 1, j) * sign_cysx0) .and. (cy * sign_cysx1 > cys(i + 1, j + 1) * sign_cysx1)) cycle

          call g2ibl2_new(cx, cy, &
               &          cxs(i, j), cxs(i + 1, j), cxs(i, j + 1), cxs(i + 1, j + 1), &
               &          cys(i, j), cys(i + 1, j), cys(i, j + 1), cys(i + 1, j + 1), &
               &          px, py, .false.)
          !px and py will be -1 when the location is not found
          if((-0.0d0 <= px) .and. (px <= 1.0d0) .and. (-0.0d0 <= py) .and. (py <= 1.0d0)) then
             ux = px * (uxs(i + 1) - uxs(i)) + uxs(i)
             uy = py * (uys(j + 1) - uys(j)) + uys(j)
             return
          end if
       end do !i
    end do !j

    !write(*, *) "g2ictr_new2: OUT OF THE WHOLE DOMAIN"
    return
  end subroutine g2ictr_new2

  subroutine g2ictr_non_monotonic(nx, ny, cx, cy, uxs, uys, cxs, cys, ux, uy, px, py)
    integer, intent(in) :: nx, ny
    double precision, intent(in) :: cx, cy !TARGET VALUE
    real(r_size), intent(in) :: uxs(nx), uys(ny) !OLD AXES
    double precision, intent(in) :: cxs(nx, ny), cys(nx, ny) ! VALUES ON THE OLD COORDINATE
    double precision, intent(out) :: ux, uy !TARGET POSITION ON THE OLD COORDINATE
    double precision, intent(out) :: px, py !TARGET POSITION RELATIVE TO THE SURROUNDING OLD GRID POINTS (0-1)

    integer i, j

    ux = minval(uxs) - 1.0d0 !MISSING VALUE
    uy = minval(uys) - 1.0d0 !MISSING VALUE

    do j = 1, ny - 1
       do i = 1, nx - 1
          call g2ibl2_new(cx, cy, &
               &          cxs(i, j), cxs(i + 1, j), cxs(i, j + 1), cxs(i + 1, j + 1), &
               &          cys(i, j), cys(i + 1, j), cys(i, j + 1), cys(i + 1, j + 1), &
               &          px, py, .false.)
          !px and py will be -1 when the location is not found
          if((-0.0d0 <= px) .and. (px <= 1.0d0) .and. (-0.0d0 <= py) .and. (py <= 1.0d0)) then
             ux = px * (uxs(i + 1) - uxs(i)) + uxs(i)
             uy = py * (uys(j + 1) - uys(j)) + uys(j)
             return
          end if
       end do !i
    end do !j

    !write(*, *) "g2ictr_new2: OUT OF THE WHOLE DOMAIN"
    return
  end subroutine g2ictr_non_monotonic

  subroutine g2ictr_pol2cart(nx, ny, cx, cy, uxs, uys, cxs, cys, i0, j, &
       &                          missval, ux, uy, px, py, i1, j1, debug)
    integer, intent(in) :: nx, ny
    double precision, intent(in) :: cx, cy !TARGET VALUE
    real(r_size), intent(in) :: uxs(nx), uys(ny) !OLD AXES
    double precision, intent(in) :: cxs(nx, ny), cys(nx, ny) !VALUES ON THE OLD COORDINATE
    integer, intent(in) :: i0 !PRIOR INDEX
    integer, intent(in) :: j !RADIAL RANGE
    double precision, intent(in) :: missval
    double precision, intent(out) :: ux, uy !TARGET POSITION ON THE OLD COORDINATE
    double precision, intent(out) :: px, py !TARGET POSITION RELATIVE TO THE SURROUNDING OLD GRID POINTS (0-1)
    integer, intent(out) :: i1, j1 !RESULTANT INDEX
    logical, intent(in) :: debug

    integer i00

    ux = minval(uxs) - 1.0d0 !MISSING VALUE
    uy = minval(uys) - 1.0d0 !MISSING VALUE

    i00 = i0 - 1
    if(i00 < 1) i00 = 1

    px = -1.0d0
    py = -1.0d0

    call g2ictr_pol2cart_core(nx, ny, cx, cy, cxs, cys, i00, nx - 1, 1, j, &
       &                          missval, px, py, i1, j1, debug)
    if(((px < -0.0d0) .or. (1.0d0 < px) .or. (py < -0.0d0) .or. (1.0d0 < py)) .and. i00 > 1) then
!if(debug) write(*, *) "call g2ictr_pol2cart_core again"
       call g2ictr_pol2cart_core(nx, ny, cx, cy, cxs, cys, i00 - 1, 1, -1, j, &
            &                          missval, px, py, i1, j1, debug)
    end if
!if(debug) write(*, *) "px, py", px, py
    if((-0.0d0 <= px) .and. (px <= 1.0d0) .and. (-0.0d0 <= py) .and. (py <= 1.0d0)) then
!if(debug) write(*, *) "i1, j1", i1, j1
!if(debug) write(*, *) "uxs(i1), uxs(i1 + 1)", uxs(i1), uxs(i1 + 1)
!if(debug) write(*, *) "uys(j1), uys(j1 + 1)", uys(j1), uys(j1 + 1)
       ux = px * (uxs(i1 + 1) - uxs(i1)) + uxs(i1)
       uy = py * (uys(j1 + 1) - uys(j1)) + uys(j1)
    end if

    return
  end subroutine g2ictr_pol2cart

  subroutine g2ictr_pol2cart_core(nx, ny, cx, cy, cxs, cys, i00, i01, di, j0, &
       &                          missval, px, py, i1, j1, debug)
    integer, intent(in) :: nx, ny
    double precision, intent(in) :: cx, cy !TARGET VALUE
    double precision, intent(in) :: cxs(nx, ny), cys(nx, ny) !VALUES ON THE OLD COORDINATE
    integer, intent(in) :: i00, i01, di !PRIOR INDEX
    integer, intent(in) :: j0 !RADIAL RANGE
    double precision, intent(in) :: missval
    double precision, intent(out) :: px, py !TARGET POSITION RELATIVE TO THE SURROUNDING OLD GRID POINTS (0-1)
    integer, intent(out) :: i1, j1 !RESULTANT INDEX
    logical, intent(in) :: debug

    integer i, j, j11, jmax, jj(3)

    px = -1.0d0
    py = -1.0d0

    if(j0 == 1) then
       jmax = 2
       jj(1:2) = (/ j0, j0 + 1 /)
    else if(j0 == ny) then
       jmax = 2
       jj(1:2) = (/ j0, j0 - 1 /)
    else
       jmax = 3
       jj = (/ j0, j0 - 1, j0 + 1 /)
    end if

    do j11 = 1, jmax
       j = jj(j11)
!if(debug) write(*, *) "search j =", j
!if(debug) write(*, *) "cx:", cx, mincx_y(j), maxcx_y(j)
!if(debug) write(*, *) "cy:", cy, mincy_y(j), maxcy_y(j)
       do i = i00, i01, di
          !if(mincx_x(i) > cx .or. maxcx_x(i) < cx .or. mincy_x(i) > cy .or. maxcy_x(i) < cy) cycle
          if(i == nx .or. j == ny) cycle

          if(cxs(i, j) .eq. missval) cycle
          if(cxs(i + 1, j) .eq. missval) cycle
          if(cxs(i, j + 1) .eq. missval) cycle
          if(cxs(i + 1, j + 1) .eq. missval) cycle
          if(cys(i, j) .eq. missval) cycle
          if(cys(i + 1, j) .eq. missval) cycle
          if(cys(i, j + 1) .eq. missval) cycle
          if(cys(i + 1, j + 1) .eq. missval) cycle

!if(debug) write(*, *) "4 points all valid", i, j
!if(debug) write(*, *) "cx, cy", cx, cy
!if(debug) write(*, *) "cxs", cxs(i, j), cxs(i + 1, j), cxs(i, j + 1), cxs(i + 1, j + 1)
!if(debug) write(*, *) "cys", cys(i, j), cys(i + 1, j), cys(i, j + 1), cys(i + 1, j + 1)
          if(cx < cxs(i, j) .and. cx < cxs(i + 1, j) .and. cx < cxs(i, j + 1) .and. cx < cxs(i + 1, j + 1)) cycle
          if(cx > cxs(i, j) .and. cx > cxs(i + 1, j) .and. cx > cxs(i, j + 1) .and. cx > cxs(i + 1, j + 1)) cycle
          if(cy < cys(i, j) .and. cy < cys(i + 1, j) .and. cy < cys(i, j + 1) .and. cy < cys(i + 1, j + 1)) cycle
          if(cy > cys(i, j) .and. cy > cys(i + 1, j) .and. cy > cys(i, j + 1) .and. cy > cys(i + 1, j + 1)) cycle

!if(debug) write(*, *) "call g2ibl2_new", i, j
          call g2ibl2_new(cx, cy, &
               &          cxs(i, j), cxs(i + 1, j), cxs(i, j + 1), cxs(i + 1, j + 1), &
               &          cys(i, j), cys(i + 1, j), cys(i, j + 1), cys(i + 1, j + 1), &
               &          px, py, debug)
          !px and py will be -1 when the location is not found
!if(debug) write(*, *) "px, py", px, py
          if((-0.0d0 <= px) .and. (px <= 1.0d0) .and. (-0.0d0 <= py) .and. (py <= 1.0d0)) then
             i1 = i
             j1 = j
             return
          end if
       end do !i
    end do !j

    return
  end subroutine g2ictr_pol2cart_core

  subroutine g2ibl2_new(cx, cy,  cx00, cx01, cx10, cx11, cy00, cy01, cy10, cy11, s, t, debug)
    double precision, intent(in) :: cx, cy !TARGET VALUE
    double precision, intent(in) :: cx00, cx01, cx10, cx11, cy00, cy01, cy10, cy11 !VALUES ON THE OLD GRID POINTS
    double precision, intent(out) :: s, t !TARGET POSITION RELATIVE TO THE SURROUNDING OLD GRID POINTS (0-1)
    double precision a, b, c, d, det
    logical, intent(in) :: debug

    s = -1.0d0 !MISSING VALUE
    t = -1.0d0 !MISSING VALUE

    a = cx01 - cx00 !df/dx
    b = cx10 - cx00 !df/dy
    c = cy01 - cy00 !dg/dx
    d = cy10 - cy00 !dg/dy
    det = a * d - b * c
    if(abs(det) < eps) then
       return !NO SOLUTION
    else
       s = (d * (cx - cx00) - b * (cy - cy00)) / det
       t = (-c * (cx - cx00) + a * (cy - cy00)) / det
!if(debug) write(*, *) "s, t:", s, t
       if ((s > -0.0d0) .and. (t > -0.0d0) .and. (s + t <= 1.0d0)) then
          return !COMPLETE
       end if
    end if

!if(debug) write(*, *) "g2ibl2_new: try again"
    a = cx10 - cx11 !-df/dx
    b = cx01 - cx11 !-df/dy
    c = cy10 - cy11 !-dg/dx
    d = cy01 - cy11 !-dg/dy
    det = a * d - b * c
    if(abs(det) < eps) then
       return !NO SOLUTION
    else
       s = (d * (cx - cx11) - b * (cy - cy11)) / det
       t = (-c * (cx - cx11) + a * (cy - cy11)) / det
!if(debug) write(*, *) "s, t:", s, t
       if ((s >= -0.0d0) .and. (t >= -0.0d0) .and. (s + t <= 1.0d0) ) then
          s = 1.0d0 - s
          t = 1.0d0 - t
          return !COMPLETE
       end if
    end if

    s = -1.0d0 !MISSING VALUE
    t = -1.0d0 !MISSING VALUE

    return !NO SOLUTION
  end subroutine g2ibl2_new

end module radar_coord_conv
