module module_nc_common
  use netcdf
  implicit none

  public

contains

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine define_nc(ncname, ncid, varname, varunit, varid, ncix, ncy0, ncy1, nciz)
    character(*), intent(in) :: ncname, varname, varunit
    integer, intent(in) :: ncix, ncy0, ncy1, nciz
    integer, intent(out) :: ncid, varid
    integer x_dimid, y_dimid, z_dimid, t_dimid
    integer dimids(4), nciy, varid_y, j
    integer :: yval(ncy0:ncy1)

    nciy = ncy1 - ncy0 + 1
    do j = ncy0, ncy1
       yval(j) = j
    end do !j
    call check_nc_err(nf90_create(ncname, NF90_HDF5, ncid))
    call check_nc_err(nf90_def_dim(ncid, "x", ncix, x_dimid))
    call check_nc_err(nf90_def_dim(ncid, "y", nciy, y_dimid))
    call check_nc_err(nf90_def_dim(ncid, "z", nciz, z_dimid))
    call check_nc_err(nf90_def_dim(ncid, "t", NF90_UNLIMITED, t_dimid))
    dimids = (/ x_dimid, y_dimid, z_dimid, t_dimid /)
    call check_nc_err(nf90_def_var(ncid, varname, NF90_REAL, dimids, varid))
    call check_nc_err(nf90_def_var_deflate(ncid, varid, 0, 1, 2))
    call check_nc_err(nf90_put_att(ncid, varid, "units", varunit))
    !Y AXIS
    call check_nc_err(nf90_def_var(ncid, "y", NF90_INT, dimids(2:2), varid_y))
    call check_nc_err(nf90_def_var_deflate(ncid, varid_y, 0, 1, 2))
    call check_nc_err(nf90_enddef(ncid))
    !WRITE Y AXIS
    call check_nc_err(nf90_put_var(ncid, varid_y, yval))
  end subroutine define_nc

end module module_nc_common
