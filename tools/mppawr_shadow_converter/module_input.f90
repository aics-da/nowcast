module module_input
  use iso_c_binding
  use, intrinsic :: ieee_arithmetic
  use netcdf
  use variables
  use radar_coord_conv
  use module_io_common
  use module_nc_common
  use module_output_nc
  implicit none

  private

  integer i, j, k, k_start, k_end
  integer it_offset
  character citer * 3
  integer datsize
  integer time1, time2, time3, time4, time5, timerate, timemax
  real(4), allocatable :: sendbuf(:, :, :)

  interface
    integer(c_int) function c_read_lz4_file(fname, dat, size) bind(C, name="read_lz4_file")
       use iso_c_binding
       character(c_char) :: fname(*)
       real(c_float) :: dat(*)
       integer(c_size_t), value :: size
     end function c_read_lz4_file
  end interface

  public read_data !, read_vector, read_da_first_guess

contains

  subroutine read_data
    it_offset = start - 2 + iter

    !if(input_type == 0) then
       !call read_rain_cartesian
    !else if(input_type == 1) then
       !call read_rain_pawr_qced
    !else if(input_type == 2) then
       !call read_rain_toshiba_pawr
    !else if(input_type == 3) then
       !call read_rain_jrc_netcdf
    !else
    if(input_type == 4) then
       call read_rain_toshiba_mppawr
    else
       write(*, *) "input_type is invalid: ", input_type
       stop 999
    end if
  end subroutine read_data

  subroutine read_rain_toshiba_mppawr
    double precision, allocatable :: lon(:, :), lat(:, :), z(:)
    character(1024) :: windname, qcfname
    integer :: error = 0
    integer :: access !FILE INQUIRY
    character(18) timecard

    allocate(lon(ddx, ddy), lat(ddx, ddy), z(ddz))
    do it = 1, nt
       call system_clock(time1, timerate, timemax)
       write(ex1, '(I4.4)') it + it_offset

       fname = 'rain_cart_' // ex1 // '.nc'
       if(access(trim(fname), ' ') == 0) then
          write(*, '("myrank: ", I5.5, ", reading ", A)') myrank, trim(fname)
          call read_cartesian_rain_nc(fname, "rain", it, .true., date(:, it))
       else
          fname = trim(input_basename) // ex1 // trim(input_suffix)
          write(*, '("myrank: ", I5.5, ", reading ", A, ", z:", I3.3, "->", I3.3)') myrank, trim(fname)
          if(use_radar_qc == 1) then
             if(len(wind_basename) > 0) then
                windname = trim(wind_basename) // ex1 // trim(input_suffix)
                write(*, '("myrank: ", I5.5, ", reading ", A, ", z:", I3.3, "->", I3.3)') myrank, trim(windname)
             end if
          else if(use_radar_qc == 2) then
             if(len(qcf_basename) > 0) then
                qcfname = trim(qcf_basename) // ex1 // trim(input_suffix)
                write(*, '("myrank: ", I5.5, ", reading ", A, ", z:", I3.3, "->", I3.3)') myrank, trim(qcfname)
             end if
          end if
          !flush(6)
          if(myrank == 0) write(*, *) "use_radar_qc = ", use_radar_qc
          call read_toshiba_mppawr_and_proj_to_map( &
               & trim(fname), read_from_jit_dt, jit_monitor_path, &
               & (/ ddx, ddy, ddz /), posiz + 1, posiz + dz, &
               & gridx, gridy, gridz, &
               & proj_type, base_x, base_y, base_lon, base_lat, std_lon, std_lat1, std_lat2, map_undef, &
               & use_radar_qc, radar_qcf_mask, radar_smooth_radial, radar_smooth_radial_wid, radar_smooth_azimuth, radar_smooth_elev, &
               & true_t(:, :, :, it, 1), lon, lat, z, date(:, it), error)
          if(output_rain_cartesian == 1) call output_cartesian_rain_nc(ex1, it, date(:, it)) !!!SHOULD BE MOVED TO MAIN
          if(output_rain_cartesian2 == 1) call output_cartesian2_rain_nc(it, date(:, it)) !!!SHOULD BE MOVED TO MAIN
          if(myrank == 0 .and. output_latlon == 1) then
             open(10, file = "lat.bin", status = "replace", access = 'stream', form = 'unformatted', convert = "little_endian")
             write(10) lat
             close(10)
             open(10, file = "lon.bin", status = "replace", access = 'stream', form = 'unformatted', convert = "little_endian")
             write(10) lon
             close(10)
          end if

          write(timecard, '(I4.4, 5I2.2, ".txt")') date(1, it), date(2, it), date(3, it), date(4, it), date(5, it), date(6, it)
          open(10, file = trim(timecard), status = "replace")
          close(10)
       end if

       call system_clock(time2, timerate, timemax)
       write(*, '("myrank:", I5.5, ", it:", I1.1, ", read:", F12.5)') myrank, it, (time2 - time1) / dble(timerate)

       !call read_rain_fill_missing(it, posiz + 1, posiz + dz, .false.)
    end do !it

    deallocate(lon, lat, z)
    return
  end subroutine read_rain_toshiba_mppawr

  subroutine read_cartesian_rain_nc(fname, vname, it, l_read_date, date)
    character(*), intent(in) :: fname, vname
    integer, intent(in) :: it
    logical, intent(in) :: l_read_date
    integer, intent(out) :: date(6)
    integer ncid, varid

    date = 0
    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, vname, varid))
    if(l_read_date) call check_nc_err(nf90_get_att(ncid, varid, "date", date))
    call check_nc_err(nf90_get_var(ncid, varid, true_t(:, :, :, it, 1)))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_cartesian_rain_nc

end module module_input
