program main
  use netcdf
  implicit none

  integer :: n_arg, length, status
  character(1024), allocatable :: args(:)
  integer, parameter :: ft_len = 13
  integer :: i, j, k, l
  real(4), allocatable :: fcst(:, :, :, :), obs(:, :, :), obs0(:, :, :), tmpfcst(:, :)
  integer, parameter :: nx = 3600
  integer, parameter :: ny = 1200
  integer, parameter :: nz = 1
  integer, parameter :: nt = 13
  integer, parameter :: kz = 1
  integer, parameter :: n_thresholds = 3
  real(4), parameter :: thresholds(n_thresholds) = (/ 0.1, 1.0, 5.0 /)
  integer, parameter :: n_region = 12
  real(4) :: ts(n_region, n_thresholds, ft_len)
  integer(4) :: tp(n_region, n_thresholds, ft_len), fp(n_region, n_thresholds, ft_len)
  integer(4) :: fn(n_region, n_thresholds, ft_len), tn(n_region, n_thresholds, ft_len), ms(n_region, n_thresholds, ft_len)
  integer(8) :: tpsum, tpfpfn
  character(3) :: c_ft
  character(5) :: c_th
  real(4), parameter :: missing = -1.0
  integer, parameter :: y_start(n_region) = (/ 1,   101, 201, 301, 401, 501, 601, 701, 801, 901,  1001, 1101 /)
  integer, parameter :: y_end(n_region)   = (/ 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200 /)
  integer access

  n_arg = command_argument_count()
  allocate(args(n_arg))
  do i = 1, n_arg
     call get_command_argument(i, args(i), length, status)
  end do !i

  allocate(fcst(nx, ny, nz, nt), obs(nx, ny, nz), obs0(nx, ny, nz))
  if(access('raindata_001.nc', ' ') == 0) then
     !write(*, *) "reading raindata_001.nc ..."
     call read_fcst_rain_nc("raindata_001.nc", nx, ny, nz, nt, fcst)
  else
     !write(*, *) "reading raindata_001.bin ..."
     call read_fcst_rain_bin("raindata_001.bin", nx, ny, nz, nt, fcst)
  end if
  obs0 = fcst(:, :, :, 1)
  do i = 1, ft_len
     !write(*, *) "reading " // trim(args(i)) // " ..."
     call read_obs_rain_bin(trim(args(i)), nx, ny, nz, obs)
     !if(i == 1) obs0 = obs
     !write(*, *) "computing scores ..."
     do k = 1, n_region !region
        do j = 1, n_thresholds !threshold
           allocate(tmpfcst(nx, y_start(k):y_end(k)))
           tmpfcst(:, y_start(k):y_end(k)) = fcst(:, y_start(k):y_end(k), kz, i)
           call get_score(nx, y_end(k) - y_start(k) + 1, &
                &         obs(:, y_start(k):y_end(k), kz), &
                &         tmpfcst, &
                &         thresholds(j), missing, &
                &         tp(k, j, i), fp(k, j, i), fn(k, j, i), tn(k, j, i), ms(k, j, i), ts(k, j, i))
           deallocate(tmpfcst)
        end do !j
     end do !k
  end do !i

  do j = 1, n_thresholds !threshold
     write(c_th, '(F3.1)') thresholds(j)
     open(1, file = "threat_score_each_lat_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do k = 1, n_region !region
        do i = 1, ft_len
           write(1, *) ts(k, j, i)
        end do !i
     end do !k
     close(1)
  end do !j

  deallocate(fcst, obs, obs0, args)
  stop
contains

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine read_obs_rain_bin(fname, nx, ny, nz, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz
    real(4), intent(out) :: rain(nx, ny, nz)

    open(10, file = trim(fname), access = "stream", form = "unformatted", convert = "little_endian")
    read(10) rain
    close(10)
  end subroutine read_obs_rain_bin

  subroutine read_fcst_rain_bin(fname, nx, ny, nz, nt, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz, nt
    real(4), intent(out) :: rain(nx, ny, nz, nt)

    open(10, file = trim(fname), access = "stream", form = "unformatted", convert = "little_endian")
    read(10) rain
    close(10)
  end subroutine read_fcst_rain_bin

  subroutine read_fcst_rain_nc(fname, nx, ny, nz, nt, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz, nt
    real(4), intent(out) :: rain(nx, ny, nz, nt)
    integer ncid, varid

    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_fcst_rain_nc

  subroutine get_score(nx, ny, obs, fcst, threshold, missing, tp, fp, fn, tn, ms, threat_score)
    integer, intent(in) :: nx, ny
    real(4), intent(in) :: obs(nx, ny), fcst(nx, ny), threshold, missing
    real(4), intent(out) :: threat_score
    integer, intent(out) :: tp, fp, fn, tn, ms
    real(4) :: orr, frr

    integer :: i, j

    tp = 0
    fp = 0
    fn = 0
    tn = 0
    ms = 0

!$omp parallel do private(i, j, orr, frr) reduction(+: tp, fp, fn, tn, ms)
    do j = 1, ny
       do i = 1, nx
          orr = obs(i, j)
          frr = fcst(i, j)
          if(orr .le. missing .or. frr .le. missing) then
             ms = ms + 1
          else
             if(orr .ge. threshold) then
                if(frr .ge. threshold) then
                   tp = tp + 1
                else
                   fn = fn + 1
                end if
             else if(frr .ge. threshold) then
                fp = fp + 1
             else
                tn = tn + 1
             end if
          end if
       end do !i
    end do !j
!$omp end parallel do

    threat_score = dble(tp) / dble(tp + fp + fn)
    !write(*, *) threshold, ": ", threat_score
  end subroutine get_score
end program main
