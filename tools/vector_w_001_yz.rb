require "numru/ggraph"
include NumRu

puts "usage: ruby u.rb [id [xrange [yrange [kz [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

yrange = ARGV[1] ? eval(ARGV[1]) : true
zrange = ARGV[2] ? eval(ARGV[2]) : true

ix = 160
#kz = 59
#kz = 99
ix = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]

#split = false
#if File.exist?("raindata_#{id}_001.nc")
  split = true
#  g = Dir.glob("raindata_#{id}_[0-9][0-9][0-9].nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
#  u = Dir.glob("burgersU_#{id}_[0-9][0-9][0-9].nc").sort.map!{|m|
#    begin
#      GPhys::IO.open(m, "u")
#    rescue
#      nil
#    end
#  }
#elsif File.exist?("raindata_#{id}.nc")
#  g = GPhys::IO.open("raindata_#{id}.nc", "rain")
#  u = GPhys::IO.open("burgersU_#{id}.nc", "u")
#else
#  g = GPhys::IO.open("raindata_#{id}.ctl", "var")
#  u = GPhys::IO.open("burgersU_#{id}.ctl", "var")
#end

u = [GPhys::IO.open("vector_w_001.nc", "w")]

#kz = 0 if (split ? u[0] : u).shape[2] == 1

#p g.axis(3).pos.units
#p g.axis(3).pos.val

#DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
#DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::swpset("fname", "vector_w")
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
DCL::sgscmn(14)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.1, 0.7, 0.2, 0.8])
(split ? u.length : u.shape[3]).times{|i|
  #val = u[xrange, yrange, kz, i].val
  #puts "u[#{xrange}, #{yrange}, #{kz}, #{i}] min: #{val.min}, max: #{val.max}"
  #if val.is_a?(NArrayMiss)
  #  val.set_mask(val.get_mask * val.to_na.lt(1e4))
  #else
  #  val = NArrayMiss.to_nam_no_dup(val, val.lt(1e4))
  #end
  #p [val.min, val.max]
  GGraph.tone(split ? u[i][ix, yrange, zrange, 0] : u[ix, yrange, zrange, i], true, "min" => -20, "max" => 20, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "tonc" => true, "auto" => false)
  #GGraph.contour(split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i], false, "levels" => levels)
  DCLExt.color_bar("tickintv" => 0)
}

DCL::grcls

