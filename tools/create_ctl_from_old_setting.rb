setting = File.open("setting.dat"){|f| f.readlines}
output_intv_s = 30 # [sec]
p dt = setting[0].to_f
output_intv_it = (output_intv_s / dt).round ## MIGHT BE PROBLEMATIC
p st = setting[2].to_i
p nx = setting[9].to_i
p ny = setting[10].to_i
p nz = setting[11].to_i
p nt = ((setting[1].to_i - 1) * dt).floor / 30 + 1
p iter = setting[3].to_i

fileinfo = [{:fname => "raindata", :title => "rain mixing ratio", :units => "kg/kg"},
            {:fname => "burgersU", :title => "u wind", :units => "m/s"},
            {:fname => "burgersV", :title => "v wind", :units => "m/s"},
            {:fname => "burgersW", :title => "w wind", :units => "m/s"},
            {:fname => "growth", :title => "rain growth rate", :units => ""}]

fileinfo.each{|info|
  iter.times{|i|
    File.open("#{info[:fname]}_#{format('%03d', st + i)}.ctl", "w"){|f|
      f.puts <<"EOF"
DSET ^#{info[:fname]}_#{format('%03d', st + i)}_%y4.bin
TITLE #{info[:title]}
options little_endian template
undef -9.99e+08
XDEF #{nx} LINEAR 1 1
YDEF #{ny} LINEAR 1 1
ZDEF #{nz} LINEAR 1 1
TDEF #{nt} LINEAR 01JAN0001 #{output_intv_it}yr
VARS 1
var #{nz} 99 #{info[:title]} #{info[:units]}
ENDVARS
EOF
    }
  }
}

fileinfo = [{:fname => "vector_u", :title => "u wind", :units => "m/s"},
            {:fname => "vector_v", :title => "v wind", :units => "m/s"},
            {:fname => "vector_w", :title => "w wind", :units => "m/s"},
            {:fname => "vector_u_filter", :title => "u wind", :units => "m/s"},
            {:fname => "vector_v_filter", :title => "v wind", :units => "m/s"},
            {:fname => "vector_w_filter", :title => "w wind", :units => "m/s"},
            {:fname => "vector_u_cotrec", :title => "u wind", :units => "m/s"},
            {:fname => "vector_v_cotrec", :title => "v wind", :units => "m/s"},
            {:fname => "vector_w_cotrec", :title => "w wind", :units => "m/s"},
            {:fname => "quality", :title => "corr", :units => "1"}]

fileinfo.each{|info|
  iter.times{|i|
    File.open("#{info[:fname]}_#{format('%03d', st + i)}.ctl", "w"){|f|
      f.puts <<"EOF"
DSET ^#{info[:fname]}_#{format('%03d', st + i)}.bin
TITLE #{info[:title]}
options little_endian template
undef -9.99e+08
XDEF #{nx} LINEAR 1 1
YDEF #{ny} LINEAR 1 1
ZDEF #{nz} LINEAR 1 1
TDEF 1 LINEAR 01JAN0001 30yr
VARS 1
var #{nz} 99 #{info[:title]} #{info[:units]}
ENDVARS
EOF
    }
  }
}
