require "numru/ggraph"
include NumRu

puts "usage: ruby u-yz.rb [id [yrange [zrange [ix [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

yrange = ARGV[1] ? eval(ARGV[1]) : true
zrange = ARGV[1] ? eval(ARGV[2]) : true

ix = 200
#jy = 100
#kz = 19
#kz = 59
#kz = 99
ix = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]


split = false
if File.exist?("raindata_#{id}_001.nc")
  split = true
  g = Dir.glob("raindata_#{id}_*.nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
  u = Dir.glob("burgersU_#{id}_*.nc").sort.map!{|m|
    begin
      GPhys::IO.open(m, "u")
    rescue
      nil
    end
  }
elsif File.exist?("raindata_#{id}.nc")
  g = GPhys::IO.open("raindata_#{id}.nc", "rain")
  u = GPhys::IO.open("burgersU_#{id}.nc", "u")
else
  g = GPhys::IO.open("raindata_#{id}.ctl", "var")
  u = GPhys::IO.open("burgersU_#{id}.ctl", "var")
end

#p g = GPhys::IO.open("raindata_#{id}.ctl", "var")
#p u = GPhys::IO.open("burgersU_#{id}.ctl", "var")
#raise if u.shape[2] == 1

#p g.axis(3).pos.units
#p g.axis(3).pos.val

#DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
#DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
DCL::sgscmn(14)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.1, 0.7, 0.2, 0.8])
(split ? u.length : u.shape[3]).times{|i|
  val = split ? u[i][ix, yrange, zrange, 0].val : u[ix, yrange, zrange, i].val
  puts "u[#{ix}, #{yrange}, #{zrange}, #{i}] min: #{val.min}, max: #{val.max}"
  if val.is_a?(NArrayMiss)
    val.set_mask(val.get_mask * val.to_na.lt(1e4))
  else
    val = NArrayMiss.to_nam_no_dup(val, val.lt(1e4))
  end
  p [val.min, val.max]
  GGraph.set_fig("yreverse" => "")
  GGraph.next_axes("xtitle" => "y grid num", "ytitle" => "z grid num", "xunits" => "", "yunits" => "")
  GGraph.tone(split ? u[i][ix, yrange, zrange, 0] : u[ix, yrange, zrange, i], true, "min" => -20, "max" => 20, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "tonc" => true, "auto" => false)
  GGraph.contour(split ? g[i][ix, yrange, zrange, 0] : g[ix, yrange, zrange, i], false, "levels" => levels)
  DCLExt.color_bar("tickintv" => 0)
}

DCL::grcls

