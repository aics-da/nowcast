require "numru/gphys"

if /(\d{4})(\d{2})\/(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd || /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd
  year = $1
  month = $2
  day = $3
  hour = $4
  min = $5
  sec = $6
elsif /(\d{4})(\d{2})\/(\d{2})(\d{2})/ =~ Dir.pwd
  year = $1
  month = $2
  day = $3
  hour = $4
  min = 0
  sec = 0
end

p time = Time.new(year, month, day, hour, min, sec)

ary = []
13.times{|i|
  ofile = Dir.glob(time.strftime("/data14-2/otsuka/gsmap/standard/v8/%Y%m/gsmap_mvk.%Y%m%d.%H%M.v8.*.dat"))

  raise "#{ofile} not found" unless ofile.length > 0
  ary.push ofile.sort[-1]
  time += 3600
}

system("/home/otsuka/bitbucket/nowcast/tools/gsmap-threat-score-plus-persistent-nc.exe #{ary.join(' ')}") || raise

datdir = "score_mvk_v8"
system("mkdir -p #{datdir} && mv *NH*.txt *TR*.txt *SH*.txt *GL*.txt #{datdir}") || raise
