require "numru/ggraph"
include NumRu

puts "usage ruby v_orig_octrec.rb [id] [kz] [0=nolanczos|1=lanczos]"

id = format("%03d", (ARGV[0] || 1).to_i)
lanczos = ARGV[2].to_i == 1


split = false
if File.exist?("raindata_#{id}_001.nc")
  split = true
  g = Dir.glob("raindata_#{id}_*.nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
elsif File.exist?("raindata_#{id}.nc")
  g = GPhys::IO.open("raindata_#{id}.nc", "rain")
elsif File.exist?("raindata_#{id}.bin")
  g = GPhys::IO.open("raindata_#{id}.ctl", "var")
end
g0 = GPhys::IO.open("rain_cart_0001.nc", "rain") if File.exist?("rain_cart_0001.nc")
if File.exist?("vector_v_#{id}.nc")
  uorg = GPhys::IO.open("vector_v_#{id}.nc", "v")
  ulan = GPhys::IO.open("vector_v_filter_#{id}.nc", "v") if lanczos
else
  uorg = GPhys::IO.open("vector_v_#{id}.ctl", "var")
  ulan = GPhys::IO.open("vector_v_filter_#{id}.ctl", "var") if lanczos
end
if Dir.glob("vector_v_cotrec_#{id}*.bin").length > 0
  cotrec = true
  uctr = GPhys::IO.open("vector_v_cotrec_#{id}.ctl", "var")
end
if Dir.glob("vector_v_anl_#{id}*.bin").length > 0
  anl = true
  uanl = GPhys::IO.open("vector_v_anl_#{id}.ctl", "var")
end
if File.exist?("quality_#{id}.nc")
  corr = GPhys::IO.open("quality_#{id}.nc", "quality")
elsif File.exist?("quality_#{id}.bin")
  corr = GPhys::IO.open("quality_#{id}.ctl", "var")
end

#xrange = 0..650 #true
xrange = true
yrange = true
#xrange = 1200..2000
#yrange = 500..1100
zrange = true
#zlev = 19
zlev = (ARGV[1] || 15).to_i
#zlev = 49
#zlev = 75
zlev = 0 if uorg.shape[2] == 1

dbz_lev = [10, 20, 30]

DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::sgscmn(14)
DCL::gllset("lmiss", true)
DCL::glrset("rmiss", -999999)
DCL::sglset("lcntl", false)
uorg.shape[3].times{|i|
  x = uorg[xrange, yrange, zrange, true].axis(0).pos.val
  y = uorg[xrange, yrange, zrange, true].axis(1).pos.val
  z = uorg[xrange, yrange, zrange, true].axis(2).pos.val
  GGraph.set_fig("window" => [x.min, x.max, y.min, y.max])
  tmpu = uorg[xrange, yrange, zlev, i]
  #tmpu += tmpu.random * 1e-5 * tmpu.max
  tmpul = ulan[xrange, yrange, zlev, i] if lanczos
  tmpuc = uctr[xrange, yrange, zlev, i] if cotrec
  tmpua = uanl[xrange, yrange, zlev, i] if anl
  if corr
    tmpc = corr[xrange, yrange, zlev, i]
    if tmpc.val.is_a?(NArray)
      tmpc.replace_val(NArrayMiss.to_nam_no_dup(tmpc.val, tmpc.val.gt(-9e8)))
    else
      tmpc.replace_val(tmpc.val.set_mask(tmpc.val.gt(-999000000.0)))
    end
p tmpc
    corr = false if tmpc.val.min == tmpc.val.max
  end
  tmpr = (split ? g[i][xrange, yrange, zlev, 0] : g[xrange, yrange, zlev, i]) if g
  GGraph.tone(tmpu, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "auto" => false, "tonc" => true, "title" => "v motion grids/intv") if corr
  GGraph.contour(tmpc, false, "nlev" => 10) if corr
  DCLExt.color_bar("tickintv" => 0) if corr
  puts "u original (#{zlev}) min = #{tmpu.min}, max = #{tmpu.max}"

  tmpr0 = g0[xrange, yrange, zlev, 0] if g0
  GGraph.tone(tmpu, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "title" => "v motion grids/intv")
  DCLExt.color_bar("tickintv" => 0)
  GGraph.contour(tmpr0, false, "levels" => dbz_lev, "index" => 31) if g0
  GGraph.contour(tmpr, false, "levels" => dbz_lev) if g

  GGraph.tone(mask = tmpu.dup.replace_val(tmpu.val.lt(1e3)), true, "title" => "mask")
  #p tmpu.val.to_na(1e4).min, tmpu.val.to_na(1e4).max, mask.min, mask.max

  if lanczos
    GGraph.tone(tmpul, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "title" => "lanczos v")
    GGraph.contour(tmpr, false, "levels" => dbz_lev) if g

    GGraph.next_axes("xside" => "", "yside" => "")
    GGraph.tone(tmpul - tmpu, true, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "title" => "lanczos v - orig v")
    GGraph.contour(tmpr, false, "levels" => dbz_lev) if g
    p (tmpul - tmpu).val.abs.max
  end

  if cotrec
    GGraph.tone(tmpuc, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "title" => "cotrec v")
    GGraph.contour(tmpr, false, "levels" => dbz_lev) if g
    DCLExt.color_bar("tickintv" => 0)
    puts "v cotrec (#{zlev}) min = #{tmpuc.min}, max = #{tmpuc.max}"
  end

  if anl
    GGraph.tone(tmpua, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "title" => "v analysis")
    GGraph.contour(tmpr, false, "levels" => dbz_lev) if g
    DCLExt.color_bar("tickintv" => 0)
    puts "v analysis (#{zlev}) min = #{tmpua.min}, max = #{tmpua.max}"

    GGraph.tone(tmpua - tmpu, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "title" => "v anal incl")
    GGraph.contour(tmpr, false, "levels" => dbz_lev) if g
    DCLExt.color_bar("tickintv" => 0)
    puts "v analysis incl (#{zlev}) min = #{tmpua.min}, max = #{tmpua.max}"
  end

  if corr
    puts "corr min: #{tmpc.min} max:#{tmpc.max}"
    #GGraph.tone(tmpc, true, "min" => tmpc.val.min, "max" => tmpc.val.max,  "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true) if corr
    GGraph.tone(tmpc, true, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "auto" => false, "tonc" => true, "title" => "quality")
    DCLExt.color_bar("tickintv" => 0)
    GGraph.contour(tmpr, false, "levels" => dbz_lev) if g
  end

  next if uorg.shape[2] == 1

  ### VERTICAL
  tmpu = uorg[xrange, uorg.shape[1] / 2, zrange, i]
  tmpuc = uctr[xrange, uorg.shape[1] / 2, zrange, i] if cotrec
  if corr  
    tmpc = corr[xrange, uorg.shape[1] / 2, zrange, i]
    if tmpc.val.is_a?(NArray)
      tmpc.replace_val(NArrayMiss.to_nam_no_dup(tmpc.val, tmpc.val.gt(-9e8)))
    else
      tmpc.replace_val(tmpc.val.set_mask(tmpc.val.gt(-999000000.0)))
    end
  end
  tmpr = (split ? g[i][xrange, uorg.shape[1] / 2, zrange, 0] : g[xrange, uorg.shape[1] / 2, zrange, i]) if g
  #p [uorg.min, uorg.max]
  p [tmpu.min, tmpu.max]
  p [tmpuc.min, tmpuc.max] if cotrec
  GGraph.set_fig("window" => [x.min, x.max, z.min, z.max])
  GGraph.tone(tmpu, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "title" => "v motion grids/intv")
  GGraph.contour(tmpc, false, "nlev" => 10) if corr

  GGraph.tone(tmpu, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "title" => "v motion grids/intv")
  #GGraph.contour(tmpu, false, "nlev" => 10)
  GGraph.contour(tmpr, false, "levels" => dbz_lev)

  GGraph.tone(tmpul, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "title" => "lanczos v", "auto" => false, "tonc" => true) if corr && lanczos
  GGraph.contour(tmpr, false, "levels" => [10, 40]) if corr and lanczos and g

  GGraph.tone(tmpuc, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "title" => "cotrec v", "auto" => false, "tonc" => true) if corr && cotrec
  GGraph.contour(tmpr, false, "levels" => [10, 40]) if corr and cotrec and g

  GGraph.tone(tmpuc, true, "min" => -10, "max" => 10, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "title" => "cotrec v") if cotrec
  GGraph.contour(tmpuc, false, "nlev" => 10) if cotrec

  if tmpc && (tmpc.val.is_a?(NArray) || tmpc.val.count_valid > 0)
    GGraph.tone(tmpc, true, "min" => tmpc.min, "max" => tmpc.max, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "auto" => false, "tonc" => true, "title" => "quality") if corr
    GGraph.contour(tmpr, false, "levels" => [10, 40]) if corr and g
  end
}

DCL::grcls

