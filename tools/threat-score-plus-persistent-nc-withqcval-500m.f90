program main
  use netcdf
  implicit none

  integer :: n_arg, length, status
  character(1024), allocatable :: args(:)
  integer :: ft_len
  integer :: i, j
  real(4), allocatable :: fcst(:, :), obs(:, :), obs0(:, :)
  integer, parameter :: nx = 161
  integer, parameter :: ny = 161
  integer, parameter :: nz = 29
  integer, parameter :: nt = 1
  integer, parameter :: kz = 4 + 1 !z = 2 km
  real(4), parameter :: thresholds(3) = (/ 1.0, 10.0, 30.0 /)

  real(4), allocatable :: ts(:, :, :)
  integer(4), allocatable :: tp(:, :, :), fp(:, :, :), fn(:, :, :), tn(:, :, :), ms(:, :, :)

  character(3) :: c_ft
  character(5) :: c_th
  real(4), parameter :: missing = -499.999 !-500.0 in the original program

  n_arg = command_argument_count()
  allocate(args(n_arg))
  do i = 1, n_arg
     call get_command_argument(i, args(i), length, status)
  end do !i

  ft_len = n_arg
  allocate(ts(3, ft_len, 2), tp(3, ft_len, 2), fp(3, ft_len, 2), &
       &  fn(3, ft_len, 2), tn(3, ft_len, 2), ms(3, ft_len, 2))


  write(*, *) "read files and calculate scores"
  allocate(fcst(nx, ny), obs(nx, ny), obs0(nx, ny))
  do i = 1, ft_len
     write(c_ft, '(I3.3)') i
     if(trim(args(i)) == "-") then
        tp(:, i, :) = -1
        fp(:, i, :) = -1
        fn(:, i, :) = -1
        tn(:, i, :) = -1
        ms(:, i, :) = -1
        ts(:, i, :) = -1
     else
        !write(*, *) "reading " // "raindata_001_" // c_ft // ".nc"
        call read_fcst_rain_nc("raindata_001_" // c_ft // "_500m.nc", nx, ny, nz, nt, kz, fcst)
        !write(*, *) "reading " // trim(args(i))
        call read_obs_rain_nc(trim(args(i)), nx, ny, nz, kz, obs)
        if(i == 1) then
           obs0 = fcst
        end if

        do j = 1, 3 !threshold
           call get_score(nx, ny, obs(:, :), fcst(:, :), thresholds(j), missing, &
                & tp(j, i, 1), fp(j, i, 1), fn(j, i, 1), tn(j, i, 1), ms(j, i, 1), ts(j, i, 1))

           call get_score(nx, ny, obs(:, :), obs0(:, :), thresholds(j), missing, &
                & tp(j, i, 2), fp(j, i, 2), fn(j, i, 2), tn(j, i, 2), ms(j, i, 2), ts(j, i, 2))
        end do !j
     end if
  end do !i

  write(*, *) "output results"
  do j = 1, 3
     write(c_th, '(I5)') int(thresholds(j))
     open(1, file = "threat_score_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) ts(j, i, 1)
     end do !i
     close(1)
     open(1, file = "true_positive_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) tp(j, i, 1)
     end do !i
     close(1)
     open(1, file = "false_positive_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) fp(j, i, 1)
     end do !i
     close(1)
     open(1, file = "false_negative_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) fn(j, i, 1)
     end do !i
     close(1)
     open(1, file = "true_negative_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) tn(j, i, 1)
     end do !i
     close(1)
     open(1, file = "missing_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) ms(j, i, 1)
     end do !i
     close(1)

     write(c_th, '(I5)') int(thresholds(j))
     open(1, file = "threat_score_persistent_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) ts(j, i, 2)
     end do !i
     close(1)
  end do !j

  deallocate(fcst, obs, obs0, args)
  stop
contains

  function rr2dbz(rr)
    real(4), intent(in) :: rr
    real(4) :: rr2dbz

    rr2dbz = log10((rr ** 1.6) * 200.0) * 10.0
  end function rr2dbz

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine read_obs_rain_nc(fname, nx, ny, nz, kz, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz, kz
    real(4), intent(out) :: rain(nx, ny)
    integer :: start(4), nccount(4)
    integer ncid, varid

    start = 1
    start(3) = kz
    nccount = 1
    nccount(1) = nx
    nccount(2) = ny
    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain, start, nccount))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_obs_rain_nc

  subroutine read_fcst_rain_nc(fname, nx, ny, nz, nt, kz, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz, nt, kz
    real(4), intent(out) :: rain(nx, ny)
    integer :: start(4), nccount(4)
    integer ncid, varid

    start = 1
    start(3) = kz
    nccount = 1
    nccount(1) = nx
    nccount(2) = ny
    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain, start, nccount))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_fcst_rain_nc

  subroutine get_score(nx, ny, obs, fcst, threshold, missing, tp, fp, fn, tn, ms, threat_score)
    integer, intent(in) :: nx, ny
    real(4), intent(in) :: obs(nx, ny), fcst(nx, ny), threshold, missing
    real(4), intent(out) :: threat_score
    integer, intent(out) :: tp, fp, fn, tn, ms
    real(4) :: o_dbz, f_dbz, threshold_dbz

    integer :: i, j, tpfpfn

    tp = 0
    fp = 0
    fn = 0
    tn = 0
    ms = 0

    threshold_dbz = rr2dbz(threshold)

!$omp parallel do private(i, j, o_dbz, f_dbz) reduction(+: tp, fp, fn, tn, ms) collapse(2)
    do j = 1, ny
       do i = 1, nx
          o_dbz = obs(i, j)
          f_dbz = fcst(i, j)
          if(o_dbz .le. missing .or. f_dbz .le. missing) then
             ms = ms + 1
          else
             if(o_dbz .ge. threshold_dbz) then
                if(f_dbz .ge. threshold_dbz) then
                   tp = tp + 1
                else
                   fn = fn + 1
                end if
             else if(f_dbz .ge. threshold_dbz) then
                fp = fp + 1
             else
                tn = tn + 1
             end if
          end if
       end do !i
    end do !j
!$omp end parallel do

    tpfpfn = tp + fp + fn
    if(tpfpfn > 0) then
       threat_score = real(tp) / real(tpfpfn)
    else
       threat_score = -1
    end if
    !write(*, *) threshold, ": ", threat_score
  end subroutine get_score
end program main
