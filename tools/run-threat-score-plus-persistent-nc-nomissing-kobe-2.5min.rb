require "numru/gphys"

/(\d{4})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})/ =~ Dir.pwd || raise

year = $1
month = $2
day = $3
hour = $4
min = $5
sec = $6

stop_if_obs_missing = ARGV[0]

enable_cache = false

p time = Time.new(year, month, day, hour, min, sec)
dt = NumRu::GPhys::IO.open("rain_cart_0001.nc", "rain").get_att("date")
p time2 = Time.new(*dt)

#exit if time > time2
time = time2 if time2 > time


#basedir = "/data13/nowcast_pawr/suita/obs"
basedir = "/data13/nowcast_pawr/test_kobe/obs"
path = "/"
basedir.split("/").each{|dd|
  path << "/" << dd
  path = File.readlink(path) if File.symlink?(path)
}
basedir = path

cachedir = "/dev/shm/otsuka/tmp_pawr_kobe"

count_missing = 0

ary = []
6.times{|i|
  cache_found = false
  if enable_cache
    ofile = time.strftime("#{cachedir}/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc")
    ofile = (time + 1).strftime("#{cachedir}/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)
    ofile = (time - 1).strftime("#{cachedir}/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)
    cache_found = true if File.exist?(ofile)
  end
  unless cache_found
    ofile = time.strftime("#{basedir}/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc")
    ofile = (time + 1).strftime("#{basedir}/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)
    ofile = (time - 1).strftime("#{basedir}/%Y/%m/%d/%H/%M/%S/rain_cart_0002.nc") unless File.exist?(ofile)
  end

  if !File.exist?(ofile)
    puts "#{time} not found"
    ofile = "-"
    count_missing += 1
    raise if stop_if_obs_missing
  elsif enable_cache && (!cache_found)
    puts "cache #{ofile}"
    newfile = ofile.gsub(basedir, cachedir)
    cachetarget = File.dirname(newfile)
    system("mkdir -p #{cachetarget} && cp -p #{ofile} #{cachetarget}") || raise
    ofile = newfile
  end

  ary.push ofile
  time += 30
}

if count_missing > 0
  system("touch some_obs_missing")
else
  system("rm -f some_obs_missing")
end
system("/home/otsuka/bitbucket/nowcast/tools/threat-score-plus-persistent-nc-nomissing.exe #{ary.join(' ')}") || raise

### CLEAN CACHE
if enable_cache
  puts "clean cache"
  cached = Dir.glob("#{cachedir}/*/*/*/*/*/*/rain_cart_0002.nc")
  ary.delete("-")
  p deleting = cached - ary
  system("rm -f #{deleting.join(' ')}") if deleting.length > 0
end
