require "numru/ggraph"
include NumRu

id = format("%03d", ARGV[0] ? ARGV[0].to_i : 1)

if File.exist?("vector_u_#{id}.bin")
  u = GPhys::IO.open("vector_u_#{id}.ctl", "var")
  v = GPhys::IO.open("vector_v_#{id}.ctl", "var")
  w = GPhys::IO.open("vector_w_#{id}.ctl", "var")
  puts "vector_*_#{id}.bin"
  puts "u min : #{u.min}, max: #{u.max}"
  puts "v min : #{v.min}, max: #{v.max}"
  puts "w min : #{w.min}, max: #{w.max}"
  p u.val[995 - 1, 664 - 1, 0, 0]
  p v.val[995 - 1, 664 - 1, 0, 0]
  p w.val[995 - 1, 664 - 1, 0, 0]
end

if File.exist?("burgersU_#{id}.bin")
  bu = GPhys::IO.open("burgersU_#{id}.ctl", "var")
  bv = GPhys::IO.open("burgersV_#{id}.ctl", "var")
  bw = GPhys::IO.open("burgersW_#{id}.ctl", "var")
  puts "burgers*_#{id}.bin"
  puts "u min : #{bu.min}, max: #{bu.max}"
  puts "v min : #{bv.min}, max: #{bv.max}"
  puts "w min : #{bw.min}, max: #{bw.max}"
  p bu.val[995 - 1, 664 - 1, 0, 0]
  p bv.val[995 - 1, 664 - 1, 0, 0]
  p bw.val[995 - 1, 664 - 1, 0, 0]
end
