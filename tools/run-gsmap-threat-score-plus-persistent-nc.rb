require "numru/gphys"

/(\d{4})(\d{2})\/(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd || /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd || raise

year = $1
month = $2
day = $3
hour = $4
min = $5
sec = $6

stop_if_obs_missing = ARGV[0]

p time = Time.new(year, month, day, hour, min, sec)

ary = []
13.times{|i|
  #ofile = time.strftime("/data14/otsuka/gsmap_rnc/bookers-production-2/out/%Y%m/%d%H%M%S/fcst/input_0002.bin")
  ofile = time.strftime("/data3/kotsuki/DATA/GSMaP/realtime/%Y%m/gsmap_nrt.%Y%m%d.%H%M.dat")
  
  unless File.exist?(ofile)
    ofile = "-"
    puts "obs file missing: #{ofile}"
    raise if stop_if_obs_missing
  end
  ary.push ofile
  time += 3600
}

system("/home/otsuka/bitbucket/nowcast/tools/gsmap-threat-score-plus-persistent-nc.exe #{ary.join(' ')}") || raise

system("mkdir -p score_nrt && mv *NH*.txt *TR*.txt *SH*.txt *GL*.txt score_nrt") || raise
