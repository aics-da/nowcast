require "numru/ggraph"
include NumRu

infile1 = ARGV[0] || raise
infile2 = ARGV[1] || raise

dat1 = NArray.to_na(File.read(infile1), NArray::INT)
dat2 = NArray.to_na(File.read(infile2), NArray::INT)
p dat1 = NArrayMiss.to_nam_no_dup(dat1, dat1.gt(-999)).to_na!(99999)
p dat2 = NArrayMiss.to_nam_no_dup(dat2, dat2.gt(-999)).to_na!(99999)

p mask = dat1.lt(dat2).to_type(NArray::INT)
p mask.min, mask.max
p newdat = dat1 * mask + dat2 * (1 - mask)
newdat = NArrayMiss.to_nam_no_dup(newdat, newdat.lt(99999))

p dx = Math.sqrt(dat1.shape[0]).to_i
newdat.reshape!(dx, dx)

File.open("topo_mask.merged.dat", "w"){|f| f.print newdat.to_na!(-999).to_s}

DCL::gropn(1)
DCL::grfrm
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grswnd(1, dx, 1, dx)
DCL::grstrf
DCL::uegtla(0, 30, -29)
DCL::uetonc(newdat.to_f)
DCL::usdaxs
DCLExt.color_bar
DCL::grcls
