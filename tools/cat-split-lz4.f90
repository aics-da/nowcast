program cat_split_lz4
  use iso_c_binding
  use netcdf
  implicit none

  interface
     integer(c_int) function c_read_lz4_file(fname, dat, size) bind(C, name="read_lz4_file")
       use iso_c_binding
       character(c_char) :: fname(*)
       real(c_float) :: dat(*)
       integer(c_size_t), value :: size
     end function c_read_lz4_file
  end interface

  real(c_float), allocatable :: buf(:, :, :)
  real(4), allocatable :: data(:, :, :, :)
  integer ncid, varid
  integer nx, ny, nz, st, et, ix, iy, iz, it, mpiprocs, ip
  integer i, k, ys, my
  integer :: n_arg, length, status
  character(1024), allocatable :: args(:)
  character(1024) fname0, vname

  n_arg = command_argument_count()
  allocate(args(n_arg))
  do i = 1, n_arg
     call get_command_argument(i, args(i), length, status)
  end do !i

  read(args(1), *) nx
  read(args(2), *) ny
  read(args(3), *) nz
  read(args(4), *) st
  read(args(5), *) et
  read(args(6), *) mpiprocs
  fname0 = trim(args(7))
  vname = trim(args(8))

  allocate(data(nx, ny, nz, 1))

  do it = st, et
     ys = 1
     do ip = 0, mpiprocs - 1
        call read_lz4(it, ip, my)
        !write(*, *) it, ip, my, ys, ys + my - 1
!$omp parallel do private(k)
        do k = 1, nz
           data(:, ys:(ys + my - 1), k, 1) = buf(:, 1:my, k)
        end do !k
!$omp end parallel do
        ys = ys + my
     end do !ip
     call write_nc(it)
  end do

contains

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine define_nc(ncname, ncid, varname, varunit, varid, ncix, nciy, nciz)
    character(*), intent(in) :: ncname, varname, varunit
    integer, intent(in) :: ncix, nciy, nciz
    integer, intent(out) :: ncid, varid
    integer x_dimid, y_dimid, z_dimid, t_dimid
    integer dimids(4)

    call check_nc_err(nf90_create(ncname, NF90_HDF5, ncid))
    call check_nc_err(nf90_def_dim(ncid, "x", ncix, x_dimid))
    call check_nc_err(nf90_def_dim(ncid, "y", nciy, y_dimid))
    call check_nc_err(nf90_def_dim(ncid, "z", nciz, z_dimid))
    call check_nc_err(nf90_def_dim(ncid, "t", NF90_UNLIMITED, t_dimid))
    dimids = (/ x_dimid, y_dimid, z_dimid, t_dimid /)
    call check_nc_err(nf90_def_var(ncid, varname, NF90_REAL, dimids, varid))
    call check_nc_err(nf90_def_var_deflate(ncid, varid, 0, 1, 2))
    call check_nc_err(nf90_put_att(ncid, varid, "units", varunit))
    call check_nc_err(nf90_enddef(ncid))
  end subroutine define_nc

  subroutine read_lz4(it, ip, my)
    integer, intent(in) :: it, ip
    integer, intent(out) :: my
    integer :: mx, mz, x0, x1, y0, y1, z0, z1, cx0, cx1, cy0, cy1, cz0, cz1
    character(6) crank
    character(4) split
    character(1024) fname, cfname0
    character(19) fname_info
    integer(c_size_t) c_size
    integer(c_int) stat

    if(allocated(buf)) deallocate(buf)
    write(crank, '(I6.6)') ip
    write(split, '("_", I3.3)') it

    fname_info = "lz4_" // crank // "_info.dat"
    open(111, file = fname_info, status = "old")
    read(111, *) x0, x1
    read(111, *) y0, y1
    read(111, *) z0, z1
    read(111, *) cfname0
    read(111, *) cx0, cx1
    read(111, *) cy0, cy1
    read(111, *) cz0, cz1
    close(111)
    if(trim(cfname0) == trim(fname0)) then
       mx = cx1 - cx0 + 1
       my = cy1 - cy0 + 1
       mz = cz1 - cz0 + 1
    else
       mx = x1 - x0 + 1
       my = y1 - y0 + 1
       mz = z1 - z0 + 1
    end if
    c_size = mx * my * mz
    allocate(buf(mx, my, mz))

    fname = trim(fname0) // '_001_' // crank // trim(split) // '.lz4' // c_null_char
    write(*, *) "reading " // trim(vname) // " in " // trim(fname)

    stat = c_read_lz4_file(fname, buf, c_size)
  end subroutine read_lz4

  subroutine write_nc(it)
    integer, intent(in) :: it
    character(4) split
    integer ncid_rain, varid_rain
    character(1024) fname
    integer nc_count(4), nc_start(4)

    nc_count = (/ nx, ny, nz, 1 /)
    nc_start = (/ 1, 1, 1, 1 /)

    write(split, '("_", I3.3)') it
    fname = trim(fname0) // '_001' // trim(split) // '.nc'
    write(*, *) "writing " // trim(fname)
    call define_nc(fname, ncid_rain, vname, "", varid_rain, nx, ny, nz)
    call check_nc_err(nf90_put_var(ncid_rain, varid_rain, data, start = nc_start, count = nc_count))
    call check_nc_err(nf90_close(ncid_rain))
  end subroutine write_nc

end program cat_split_lz4
