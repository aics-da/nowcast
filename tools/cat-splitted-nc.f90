program cat_splitted_nc
  use netcdf
  implicit none

  real(4), allocatable :: buf(:, :, :), data(:, :, :, :)
  integer ncid, varid
  integer nx, ny, nz, st, et, ix, iy, iz, it, mpiprocs, ip
  integer i, k, ys, my
  integer :: n_arg, length, status
  character(1024), allocatable :: args(:)
  character(1024) fname0, vname

  n_arg = command_argument_count()
  allocate(args(n_arg))
  do i = 1, n_arg
     call get_command_argument(i, args(i), length, status)
     !write(*, *) trim(args(i))
  end do !i

  read(args(1), *) nx
  read(args(2), *) ny
  read(args(3), *) nz
  read(args(4), *) st
  read(args(5), *) et
  read(args(6), *) mpiprocs
  fname0 = trim(args(7))
  vname = trim(args(8))

  allocate(data(nx, ny, nz, 1))

  do it = st, et
     ys = 1
     do ip = 0, mpiprocs - 1
        call read_nc(it, ip, my)
        !write(*, *) it, ip, my, ys, ys + my - 1
!$omp parallel do private(k)
        do k = 1, nz
           data(:, ys:(ys + my - 1), k, 1) = buf(:, 1:my, k)
        end do !k
!$omp end parallel do
        ys = ys + my
     end do !ip
     call write_nc(it)
  end do

contains

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine define_nc(ncname, ncid, varname, varunit, varid, ncix, nciy, nciz)
    character(*), intent(in) :: ncname, varname, varunit
    integer, intent(in) :: ncix, nciy, nciz
    integer, intent(out) :: ncid, varid
    integer x_dimid, y_dimid, z_dimid, t_dimid
    integer dimids(4)

    call check_nc_err(nf90_create(ncname, NF90_HDF5, ncid))
    call check_nc_err(nf90_def_dim(ncid, "x", ncix, x_dimid))
    call check_nc_err(nf90_def_dim(ncid, "y", nciy, y_dimid))
    call check_nc_err(nf90_def_dim(ncid, "z", nciz, z_dimid))
    call check_nc_err(nf90_def_dim(ncid, "t", NF90_UNLIMITED, t_dimid))
    dimids = (/ x_dimid, y_dimid, z_dimid, t_dimid /)
    call check_nc_err(nf90_def_var(ncid, varname, NF90_REAL, dimids, varid))
    call check_nc_err(nf90_def_var_deflate(ncid, varid, 0, 1, 2))
    call check_nc_err(nf90_put_att(ncid, varid, "units", varunit))
    call check_nc_err(nf90_enddef(ncid))
  end subroutine define_nc

  subroutine read_nc(it, ip, my)
    integer, intent(in) :: it, ip
    integer, intent(out) :: my
    integer :: jy
    character(6) crank
    character(4) split
    character(1024) fname

    character(1024) :: name
    integer :: xtype, ndims
    integer :: dimids(100)

    if(allocated(buf)) deallocate(buf)
    write(crank, '(I6.6)') ip
    write(split, '("_", I3.3)') it
    fname = trim(fname0) // '_001_' // crank // trim(split) // '.nc'
    write(*, *) "reading " // trim(vname) // " in " // trim(fname)
    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, vname, varid))

    call check_nc_err(nf90_inquire_variable(ncid, varid, name, xtype, ndims, dimids))
    call check_nc_err(nf90_inquire_dimension(ncid, dimids(2), len = my))
    !write(*, *) nx, my, nz
    allocate(buf(nx, my, nz))

    call check_nc_err(nf90_get_var(ncid, varid, buf(:, :, :)))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_nc

  subroutine write_nc(it)
    integer, intent(in) :: it
    character(4) split
    integer ncid_rain, varid_rain
    character(1024) fname
    integer nc_count(4), nc_start(4)

    nc_count = (/ nx, ny, nz, 1 /)
    nc_start = (/ 1, 1, 1, 1 /)

    write(split, '("_", I3.3)') it
    fname = trim(fname0) // '_001' // trim(split) // '.nc'
    write(*, *) "writing " // trim(fname)
    call define_nc(fname, ncid_rain, vname, "", varid_rain, nx, ny, nz)
    call check_nc_err(nf90_put_var(ncid_rain, varid_rain, data, start = nc_start, count = nc_count))
    call check_nc_err(nf90_close(ncid_rain))
  end subroutine write_nc

end program cat_splitted_nc
