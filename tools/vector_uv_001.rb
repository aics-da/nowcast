require "numru/ggraph"
include NumRu

puts "usage: ruby u.rb [id [xrange [yrange [kz [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

xrange = ARGV[1] ? eval(ARGV[1]) : true
yrange = ARGV[2] ? eval(ARGV[2]) : true

kz = 8
#kz = 19
#kz = 59
#kz = 99
kz = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]

split = true

g = [GPhys::IO.open("rain_cart_0002.nc", "rain")]
u = [GPhys::IO.open("vector_u_001.nc", "u")]
v = [GPhys::IO.open("vector_v_001.nc", "v")]

kz = 0 if (split ? u[0] : u).shape[2] == 1

DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::swpset("fname", "vector_uv")
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
#DCL::sgscmn(14)
DCL::sgscmn(56)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.1, 0.7, 0.2, 0.8])
(split ? u.length : u.shape[3]).times{|i|

  tmpdat = split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i]
  uu = split ? u[i][xrange, yrange, kz, 0] : u[xrange, yrange, kz, i]
  vv = split ? v[i][xrange, yrange, kz, 0] : v[xrange, yrange, kz, i]

  GGraph.tone(tmpdat, true, "min" => -20, "max" => 70, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "tonc" => true, "auto" => false, "title" => "dBZ(kz = #{kz}) FT = #{i * 30}s", "annotate" => false)
  GGraph.vector(uu, vv, false, "xintv" => 20, "yintv" => 20)
  #GGraph.contour(split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i], false, "levels" => levels)
  DCLExt.color_bar("tickintv" => 0)
}

DCL::grcls

