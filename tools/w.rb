require "numru/ggraph"
include NumRu

puts "usage: ruby w.rb [id [yrange [zrange [ix [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

yrange = ARGV[1] ? eval(ARGV[1]) : true
zrange = ARGV[1] ? eval(ARGV[2]) : true

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]

split = false
if File.exist?("raindata_#{id}_001.nc")
  split = true
  g = Dir.glob("raindata_#{id}_*.nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
elsif File.exist?("raindata_#{id}.nc")
  p g = GPhys::IO.open("raindata_#{id}.nc", "rain")
else
  p g = GPhys::IO.open("raindata_#{id}.ctl", "var")
end
if File.exist?("burgersW_#{id}_001.nc")
  w = Dir.glob("burgersW_#{id}_*.nc").sort.map!{|m| GPhys::IO.open(m, "w")}
elsif File.exist?("burgersW_#{id}.nc")
  p w = GPhys::IO.open("burgersW_#{id}.nc", "w")
else
  p w = GPhys::IO.open("burgersW_#{id}.ctl", "var")
end
exit if (split ? w.length : w.shape[2]) == 1

ix = (split ? w[0] : w).shape[0] / 2
ix = ARGV[3].to_i if ARGV[3]

#p g.axis(3).pos.units
#p g.axis(3).pos.val

#DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
#DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
DCL::sgscmn(14)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.1, 0.7, 0.2, 0.8])
(split ? w.length : w.shape[3]).times{|i|
  GGraph.set_fig("yreverse" => "")
  tmpw = split ? w[i][ix, yrange, zrange, 0] : w[ix, yrange, zrange, i]
  GGraph.tone(tmpw, true, "min" => -20, "max" => 20, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "tonc" => true, "auto" => false)
  tmpdat = split ? g[i][ix, yrange, zrange, 0] : g[ix, yrange, zrange, i]
  GGraph.contour(tmpdat, false, "levels" => levels)
}

DCL::grcls

