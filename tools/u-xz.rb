require "numru/ggraph"
include NumRu

puts "usage: ruby u-xz.rb [id [xrange [zrange [jy [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

xrange = ARGV[1] ? eval(ARGV[1]) : true
zrange = ARGV[1] ? eval(ARGV[2]) : true

jy = 100
#kz = 19
#kz = 59
#kz = 99
jy = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]

p g = GPhys::IO.open("raindata_#{id}.ctl", "var")
p u = GPhys::IO.open("burgersU_#{id}.ctl", "var")
raise if u.shape[2] == 1

p g.axis(3).pos.units
p g.axis(3).pos.val

#DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
#DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
DCL::sgscmn(14)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.1, 0.7, 0.2, 0.8])
u.shape[3].times{|i|
  val = u[xrange, jy, zrange, i].val
  puts "u[#{xrange}, #{jy}, #{zrange}, #{i}] min: #{val.min}, max: #{val.max}"
  if val.is_a?(NArrayMiss)
    val.set_mask(val.get_mask * val.to_na.lt(1e4))
  else
    val = NArrayMiss.to_nam_no_dup(val, val.lt(1e4))
  end
  p [val.min, val.max]
  GGraph.set_fig("yreverse" => "")
  GGraph.tone(u[xrange, jy, zrange, i], true, "min" => -20, "max" => 20, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "tonc" => true, "auto" => false)
  GGraph.contour(g[xrange, jy, zrange, i], false, "levels" => levels)
}

DCL::grcls

