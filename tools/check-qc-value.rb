require "numru/ggraph"
include NumRu

g = GPhys::IO.open("rain_cart_0002.nc", "rain")
GGraph.tone(g[true, true, 2, 0], true, "auto" => false, "tonc" => true,
            "levels"   => [-1001, -801,  -501,  -330,  -30, 100],
            "patterns" => [10999, 20999, 30999, 40999, 50999])
DCLExt.color_bar("constwidth" => true)
DCL::grcls
