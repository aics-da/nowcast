program main
  use netcdf
  implicit none

  integer :: n_arg, length, status
  character(1024), allocatable :: args(:)
  integer, parameter :: ft_len = 21
  integer :: i, j, it
  real(4), allocatable :: fcst(:, :), obs(:, :)
  integer, parameter :: nx = 321
  integer, parameter :: ny = 321
  integer, parameter :: nz = 57
  integer, parameter :: nt = 1
  integer, parameter :: kz = 8 + 1 !z = 2 km
  real(4), parameter :: thresholds(3) = (/ 1.0, 10.0, 30.0 /)
  character(3) :: c_ft
  character(5) :: c_th
  real(4), parameter :: missing = -327.68
  integer, parameter :: num_m = 6
  integer, parameter :: m(num_m) = (/ 0, 1, 2, 10, 20, 100 /)
  real(4) :: fss(num_m, 3, ft_len)
  character*5 :: c_num_m

  n_arg = command_argument_count()
  allocate(args(n_arg))
  do i = 1, n_arg
     call get_command_argument(i, args(i), length, status)
  end do !i

  allocate(fcst(nx, ny), obs(nx, ny))
  do it = 1, ft_len
     write(c_ft, '(I3.3)') it
     call read_fcst_rain_nc("raindata_001_" // c_ft // ".nc", nx, ny, kz, 1, fcst)
     call read_obs_rain_nc(trim(args(it)), nx, ny, kz, obs)

     do j = 1, 3
        call get_score(nx, ny, obs(:, :), fcst(:, :), rr2dbz(thresholds(j)), num_m, m(:), fss(:, j, it))
     end do !j
  end do !it

  do j = 1, 3
     write(c_th, '(I5)') int(thresholds(j))
     open(1, file = "fractions_skill_score_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     write(1, *) num_m
     write(c_num_m, '(I5)') num_m
     write(1, '(' // trim(c_num_m) // 'I15)') m
     do i = 1, ft_len
        write(1, '(' // trim(c_num_m) // 'E15.8)') fss(:, j, i)
     end do !i
     close(1)
  end do !j

  deallocate(fcst, obs, args)
  stop

contains

  function dbz2rr(dbz)
    real(4), intent(in) :: dbz
    real(4) :: dbz2rr

    dbz2rr = (10.0 ** (dbz * 0.1) / 200.0) ** (1.0 / 1.6)
  end function dbz2rr

  function rr2dbz(rr)
    real(4), intent(in) :: rr
    real(4) :: rr2dbz

    rr2dbz = log10(200.0 * (rr ** 1.6)) * 10.0
  end function rr2dbz

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine read_obs_rain_nc(fname, nx, ny, kz, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, kz
    real(4), intent(out) :: rain(nx, ny)
    integer ncid, varid

    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain, (/ 1, 1, kz /), (/ nx, ny, 1 /)))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_obs_rain_nc

  subroutine read_fcst_rain_nc(fname, nx, ny, kz, it, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, kz, it
    real(4), intent(out) :: rain(nx, ny)
    integer ncid, varid

    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain, (/ 1, 1, kz, it /), (/ nx, ny, 1, 1 /)))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_fcst_rain_nc

  subroutine get_score(nx, ny, obs, fcst, threshold, num_n, n, fss)
    integer, intent(in) :: nx, ny, num_n, n(num_n)
    real(4), intent(in) :: obs(nx, ny), fcst(nx, ny), threshold
    real(4), intent(out) :: fss(num_n)
    integer(1) :: oidx(nx, ny), fidx(nx, ny)
    integer :: ofrac, ffrac
    integer(8) :: mse, mse_ref

    integer :: i, j, k, ii, jj, ii0, ii1, jj0, jj1

!$omp parallel do private(i, j)
    do j = 1, ny
       do i = 1, nx
          if(obs(i, j) .ge. threshold) then
             oidx(i, j) = 1
          else
             oidx(i, j) = 0
          end if
          if(fcst(i, j) .ge. threshold) then
             fidx(i, j) = 1
          else
             fidx(i, j) = 0
          end if
       end do !i
    end do !j
!$omp end parallel do

    do k = 1, num_n
       mse = 0
       mse_ref = 0
!$omp parallel do private(i, j, ii, jj, ii0, ii1, jj0, jj1, ofrac, ffrac) reduction(+: mse, mse_ref)
       do j = 1, ny
          do i = 1, nx
             ofrac = 0
             ffrac = 0

             jj0 = max(j - n(k), 1)
             jj1 = min(j + n(k), ny)
             do jj = jj0, jj1
                ii0 = max(i - n(k), 1)
                ii1 = min(i + n(k), nx)
                do ii = ii0, ii1
                   ofrac = ofrac + oidx(ii, jj)
                   ffrac = ffrac + fidx(ii, jj)
                end do !ii
             end do !jj
             mse = mse + (ofrac - ffrac) ** 2
             mse_ref = mse_ref + ofrac * ofrac + ffrac * ffrac
          end do !i
       end do !j
!$omp end parallel do
       if(mse_ref == 0) mse_ref = 1

       write(*, *) k, n(k), mse, mse_ref

       fss(k) = 1.0d0 - dble(mse) / dble(mse_ref)
    end do !k

  end subroutine get_score
end program main
