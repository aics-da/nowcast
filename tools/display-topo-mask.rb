require "numru/ggraph"
require File.join(File.dirname(__FILE__), "parse_namelist")
include NumRu


if File.exist?("setting.namelist")
  nml = parse_namelist("setting.namelist")
  p mx = nml["domain_config"]["ddx"].to_i
  p my = nml["domain_config"]["ddy"].to_i
  p mz = nml["domain_config"]["ddz"].to_i
elsif File.exist?("setting.dat")
  setting = File.open("setting.dat"){|f| f.readlines}
  p nt = setting[1].to_i
  p st = setting[2].to_i
  p iter = setting[3].to_i
  p mx = setting[6].to_i
  p my = setting[7].to_i
  p mz = setting[8].to_i
  p nx = setting[9].to_i
  p ny = setting[10].to_i
  p nz = setting[11].to_i
  p posix = setting[15].to_i
  p posiy = setting[16].to_i
  p posiz = setting[17].to_i
  p setting[47]
  raise unless /\"(.*)\"/ =~ setting[47]
  path = File.dirname($1)
  puts "path = #{path}"
else
  raise "no setting.namelist nor setting.dat"
end

max_topo = mz #level
zrange = 0..(max_topo - 1)

p topo = NArray.to_na(File.read("topo_mask.dat"), NArray::INT).reshape!(mx, my)
p topo.max
p imiss = -999

DCL::gropn(4)
DCL::grfrm
DCL::grswnd(0, topo.shape[0] - 1, 0, topo.shape[1] - 1)
DCL::grsvpt(0.2, 0.8, 0.2, 0.8)
DCL::grstrf

DCL::glrset("rmiss", imiss.to_f)
DCL::gllset("lmiss", true)

DCL::uegtla(1, max_topo, -[max_topo, 99].min)
DCL::uwsgxa(NArray.sfloat(topo.shape[0]).indgen!)
DCL::uwsgya(NArray.sfloat(topo.shape[1]).indgen!)
DCL::uetonc(topo.to_f)
DCL::usdaxs
#DCL::udcntz(topo.to_f)
DCLExt.color_bar("tickintv" => 0)

DCL::grcls
