program main
  use netcdf
  implicit none

  integer :: n_arg, length, status
  character(1024), allocatable :: args(:)
  integer, parameter :: ft_len = 13
  integer :: i, j, k, l
  real(4), allocatable :: fcst(:, :, :, :), obs(:, :, :), obs0(:, :, :), tmpfcst(:, :)
  integer, parameter :: nx = 3600
  integer, parameter :: ny = 1200
  integer, parameter :: nz = 1
  integer, parameter :: nt = 13
  integer, parameter :: kz = 1
  integer, parameter :: n_region = 3 + 1 !NH, TR, SH, GL
  real(4) :: se(n_region - 1, 2), rmse(n_region, nt, 2)
  integer(8) :: ndat(n_region - 1, 2)
  character(3) :: c_ft
  character(5) :: c_th
  real(4), parameter :: missing = -1.0
  integer, parameter :: y_start(n_region - 1) = (/ 1, 401, 801 /)
  integer, parameter :: y_end(n_region - 1)   = (/ 400, 800, 1200 /)
  character(2), parameter :: region_name(n_region) = (/ "NH", "TR", "SH", "GL" /)
  character(11), parameter ::datset(2) = (/ "           ", "persistent_" /)
  integer access

  n_arg = command_argument_count()
  allocate(args(n_arg))
  do i = 1, n_arg
     call get_command_argument(i, args(i), length, status)
  end do !i

  allocate(fcst(nx, ny, nz, nt), obs(nx, ny, nz), obs0(nx, ny, nz))
  if(access('raindata_001.nc', ' ') == 0) then
     !write(*, *) "reading raindata_001.nc ..."
     call read_fcst_rain_nc("raindata_001.nc", nx, ny, nz, nt, fcst)
  else
     !write(*, *) "reading raindata_001.bin ..."
     call read_fcst_rain_bin("raindata_001.bin", nx, ny, nz, nt, fcst)
  end if
  obs0 = fcst(:, :, :, 1)
  do i = 1, ft_len
     !write(*, *) "reading " // trim(args(i)) // " ..."
     call read_obs_rain_bin(trim(args(i)), nx, ny, nz, obs)
     !if(i == 1) obs0 = obs
     !write(*, *) "computing scores ..."
     se = 0
     ndat = 0
     do k = 1, n_region - 1 !region
        allocate(tmpfcst(nx, y_start(k):y_end(k)))
        tmpfcst(:, y_start(k):y_end(k)) = fcst(:, y_start(k):y_end(k), kz, i)
        call get_score(nx, y_end(k) - y_start(k) + 1, &
             &         obs(:, y_start(k):y_end(k), kz), &
             &         tmpfcst, &
             &         missing, &
             &         se(k, 1), ndat(k, 1), rmse(k, i, 1))

        tmpfcst(:, y_start(k):y_end(k)) = obs0(:, y_start(k):y_end(k), kz)
        call get_score(nx, y_end(k) - y_start(k) + 1, &
             &         obs(:, y_start(k):y_end(k), kz), &
             &         tmpfcst, &
             &         missing, &
             &         se(k, 2), ndat(k, 2), rmse(k, i, 2))
        deallocate(tmpfcst)
     end do !k
     do l = 1, 2
        rmse(n_region, i, l) = sqrt(sum(se(:, l)) / real(sum(ndat(:, l))))
     end do
  end do !i

  do l = 1, 2 !dataset
     do k = 1, n_region !region
        open(1, file = trim(datset(l)) // "rmse_" // region_name(k) // ".txt", status = "replace")
        do i = 1, ft_len
           write(1, *) rmse(k, i, l)
        end do !i
        close(1)
     end do !k
  end do !l

  deallocate(fcst, obs, obs0, args)
  stop
contains

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine read_obs_rain_bin(fname, nx, ny, nz, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz
    real(4), intent(out) :: rain(nx, ny, nz)

    open(10, file = trim(fname), access = "stream", form = "unformatted", convert = "little_endian")
    read(10) rain
    close(10)
  end subroutine read_obs_rain_bin

  subroutine read_fcst_rain_bin(fname, nx, ny, nz, nt, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz, nt
    real(4), intent(out) :: rain(nx, ny, nz, nt)

    open(10, file = trim(fname), access = "stream", form = "unformatted", convert = "little_endian")
    read(10) rain
    close(10)
  end subroutine read_fcst_rain_bin

  subroutine read_fcst_rain_nc(fname, nx, ny, nz, nt, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz, nt
    real(4), intent(out) :: rain(nx, ny, nz, nt)
    integer ncid, varid

    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_fcst_rain_nc

  subroutine get_score(nx, ny, obs, fcst, missing, se, ndat, rmse)
    integer, intent(in) :: nx, ny
    real(4), intent(in) :: obs(nx, ny), fcst(nx, ny), missing
    real(4), intent(out) :: se, rmse
    integer(8), intent(out) :: ndat

    integer :: i, j

    se = 0
    ndat = 0

!$omp parallel do private(i, j) reduction(+: se, ndat)
    do j = 1, ny
       do i = 1, nx
          if(obs(i, j) .gt. missing .and. fcst(i, j) .gt. missing) then
             se = se + (fcst(i, j) - obs(i, j)) ** 2
             ndat = ndat + 1
          end if
       end do !i
    end do !j
!$omp end parallel do

    rmse = sqrt(se / real(ndat))
  end subroutine get_score
end program main
