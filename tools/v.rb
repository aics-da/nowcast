require "numru/ggraph"
include NumRu

puts "usage: ruby v.rb [id [xrange [yrange [kz [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

xrange = ARGV[1] ? eval(ARGV[1]) : true
yrange = ARGV[1] ? eval(ARGV[2]) : true

kz = 19
#kz = 59
#kz = 99
kz = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]

split = false
if File.exist?("raindata_#{id}_001.nc")
  split = true
  g = Dir.glob("raindata_#{id}_*.nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
  u = Dir.glob("burgersV_#{id}_*.nc").sort.map!{|m|
    begin
      GPhys::IO.open(m, "v")
    rescue
      nil
    end
  }
elsif File.exist?("raindata_#{id}.nc")
  g = GPhys::IO.open("raindata_#{id}.nc", "rain")
  u = GPhys::IO.open("burgersV_#{id}.nc", "v")
else
  g = GPhys::IO.open("raindata_#{id}.ctl", "var")
  u = GPhys::IO.open("burgersV_#{id}.ctl", "var")
end
kz = 0 if (split ? u[0] : u).shape[2] == 1

#p g.axis(3).pos.units
#p g.axis(3).pos.val

#DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
#DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
DCL::sgscmn(14)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.1, 0.7, 0.2, 0.8])
(split ? u.length : u.shape[3]).times{|i|
  #val = u[xrange, yrange, kz, i].val
  #puts "u[#{xrange}, #{yrange}, #{kz}, #{i}] min: #{val.min}, max: #{val.max}"
  #if val.is_a?(NArrayMiss)
  #  val.set_mask(val.get_mask * val.to_na.lt(1e4))
  #else
  #  val = NArrayMiss.to_nam_no_dup(val, val.lt(1e4))
  #end
  #p [val.min, val.max]
  GGraph.tone(split ? u[i][xrange, yrange, kz, 0] : u[xrange, yrange, kz, i], true, "min" => -20, "max" => 20, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "color_bar" => true, "tonc" => true, "auto" => false)
  GGraph.contour(split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i], false, "levels" => levels)
}

DCL::grcls

