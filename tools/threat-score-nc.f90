program main
  use netcdf
  implicit none

  integer :: n_arg, length, status
  character(1024), allocatable :: args(:)
  integer, parameter :: ft_len = 21
  integer :: i, j
  real(4), allocatable :: fcst(:, :, :, :), obs(:, :, :)
  integer, parameter :: nx = 321
  integer, parameter :: ny = 321
  integer, parameter :: nz = 57
  integer, parameter :: nt = 1
  integer, parameter :: kz = 8 + 1 !z = 2 km
  real(4), parameter :: thresholds(3) = (/ 1.0, 10.0, 30.0 /)
  real(4) :: ts(3, ft_len)
  integer(4) :: tp(3, ft_len), fp(3, ft_len), fn(3, ft_len), tn(3, ft_len), ms(3, ft_len)
  character(3) :: c_ft
  character(5) :: c_th
  real(4), parameter :: missing = -327.68

  n_arg = command_argument_count()
  allocate(args(n_arg))
  do i = 1, n_arg
     call get_command_argument(i, args(i), length, status)
  end do !i

  allocate(fcst(nx, ny, nz, nt), obs(nx, ny, nz))
  do i = 1, ft_len
     write(c_ft, '(I3.3)') i
     !write(*, *) "reading " // "raindata_001_" // c_ft // ".nc"
     call read_fcst_rain_nc("raindata_001_" // c_ft // ".nc", nx, ny, nz, nt, fcst)
     !write(*, *) "reading " // trim(args(i))
     call read_obs_rain_nc(trim(args(i)), nx, ny, nz, obs)
     do j = 1, 3
        call get_score(nx, ny, obs(:, :, kz), fcst(:, :, kz, 1), thresholds(j), missing, &
             & tp(j, i), fp(j, i), fn(j, i), tn(j, i), ms(j, i), ts(j, i))
     end do !j
  end do !i

  do j = 1, 3
     write(c_th, '(I5)') int(thresholds(j))
     open(1, file = "threat_score_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) ts(j, i)
     end do !i
     close(1)
     open(1, file = "true_positive_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) tp(j, i)
     end do !i
     close(1)
     open(1, file = "false_positive_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) fp(j, i)
     end do !i
     close(1)
     open(1, file = "false_negative_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) fn(j, i)
     end do !i
     close(1)
     open(1, file = "true_negative_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) tn(j, i)
     end do !i
     close(1)
     open(1, file = "missing_" // trim(adjustl(c_th)) // "mm.txt", status = "replace")
     do i = 1, ft_len
        write(1, *) ms(j, i)
     end do !i
     close(1)
  end do !j

  deallocate(fcst, obs, args)
  stop
contains

  function dbz2rr(dbz)
    real(4), intent(in) :: dbz
    real(4) :: dbz2rr

    dbz2rr = (10.0 ** (dbz * 0.1) / 200.0) ** (1.0 / 1.6)
  end function dbz2rr

  subroutine check_nc_err(stat)
    integer, intent(in) :: stat
    if(stat .ne. nf90_noerr) then
       write(*, *) trim(nf90_strerror(stat))
       stop 999
    end if
  end subroutine check_nc_err

  subroutine read_obs_rain_nc(fname, nx, ny, nz, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz
    real(4), intent(out) :: rain(nx, ny, nz)
    integer ncid, varid

    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_obs_rain_nc

  subroutine read_fcst_rain_nc(fname, nx, ny, nz, nt, rain)
    character(*), intent(in) :: fname
    integer, intent(in) :: nx, ny, nz, nt
    real(4), intent(out) :: rain(nx, ny, nz, nt)
    integer ncid, varid

    call check_nc_err(nf90_open(trim(fname), NF90_NOWRITE, ncid))
    call check_nc_err(nf90_inq_varid(ncid, "rain", varid))
    call check_nc_err(nf90_get_var(ncid, varid, rain))
    call check_nc_err(nf90_close(ncid))
  end subroutine read_fcst_rain_nc

  subroutine get_score(nx, ny, obs, fcst, threshold, missing, tp, fp, fn, tn, ms, threat_score)
    integer, intent(in) :: nx, ny
    real(4), intent(in) :: obs(nx, ny), fcst(nx, ny), threshold, missing
    real(4), intent(out) :: threat_score
    integer, intent(out) :: tp, fp, fn, tn, ms
    real(4) :: orr, frr, miss_rr

    integer :: i, j

    tp = 0
    fp = 0
    fn = 0
    tn = 0
    ms = 0

    miss_rr = dbz2rr(missing)

!$omp parallel do private(i, j, orr, frr) reduction(+: tp, fp, fn, tn, ms)
    do j = 1, ny
       do i = 1, nx
          orr = dbz2rr(obs(i, j))
          frr = dbz2rr(fcst(i, j))
          if(orr .le. miss_rr .or. frr .le. miss_rr) then
             ms = ms + 1
          else
             if(orr .ge. threshold) then
                if(frr .ge. threshold) then
                   tp = tp + 1
                else
                   fn = fn + 1
                end if
             else if(frr .ge. threshold) then
                fp = fp + 1
             else
                tn = tn + 1
             end if
          end if
       end do !i
    end do !j
!$omp end parallel do

    threat_score = real(tp) / real(tp + fp + fn)
    !write(*, *) threshold, ": ", threat_score
  end subroutine get_score
end program main
