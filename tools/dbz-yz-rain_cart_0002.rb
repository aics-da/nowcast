require "numru/ggraph"
include NumRu

def gen_color
  # default colors
  rrr = [65535,     0, 65535,     0,     0, 65535, 56797,     0, 65535,     0]
  ggg = [65535,     0,     0, 65535,     0, 65535, 41120,     0, 49344, 65535]
  bbb = [65535,     0,     0,     0, 65535,     0, 56797, 32896, 52171, 65535]

  colors = [
            [[1.0,  0.9,  0.95], 1], # white red purple
            [[1.0,  0.0,  0.5 ], 5], # red purple
            [[0.5,  0.0,  0.25], 5], # dark red purple
            [[0.5,  0.0,  0.5 ], 5], # dark magenta
            [[1.0,  0.0,  1.0 ], 5], # magenta
            [[1.0,  0.5,  1.0 ], 5], # light magenta
            [[1.0,  0.5,  0.5 ], 5], # light red
            [[1.0,  0.0,  0.0 ], 5], # red
            [[1.0,  0.75, 0.5 ], 5], # light orange
            [[1.0,  1.0,  1.0 ], 4], # white
            [[1.0,  1.0,  1.0 ], 1], # white
            [[0.5,  1.0,  0.75], 4], # light green
            [[0.0,  0.5,  0.5 ], 5], # cyan
            [[0.5,  1.0,  1.0 ], 5], # light cyan
            [[0.5,  0.5,  1.0 ], 5], # light blue
            [[0.0,  0.0,  1.0 ], 5], # blue
            [[0.0,  0.0,  0.5 ], 5], # dark blue
            [[0.25, 0.0,  0.5 ], 5], # dark blue purple
            [[0.5,  0.0,  1.0 ], 5], # blue purple
            [[0.95, 0.9,  1.0 ], 5]  # white blue purple
           ]

  tmp = NArray[0.0, 0.0, 0.0]
  colors.each{|cl|
    diff = NArray.to_na(cl[0]) - tmp
    cl[1].times{|i|
      newcolor = ((tmp + diff * (i + 1).to_f / cl[1]) * 0xffff).round
      rrr.push newcolor[0]
      ggg.push newcolor[1]
      bbb.push newcolor[2]
    }
    tmp = NArray.to_na(cl[0])
  }

  File.open("colormap.x11", "w"){|f|
    ncol = 100
    f.puts "#{ncol}: NO._OF_COLORS"
    ncol.times{|i|
      f.puts format("%5d %5d %5d : C%02d", rrr[i], ggg[i], bbb[i], i)
    }
  }
end


puts "usage: ruby u.rb [id [yrange [zrange [ix [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

yrange = ARGV[1] ? eval(ARGV[1]) : true
zrange = ARGV[2] ? eval(ARGV[2]) : true

ix = 80
#kz = 8
#kz = 59
#kz = 99
kz = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]

split = false
if File.exist?("rain_cart_0002.nc")
  split = true
  g = Dir.glob("rain_cart_0002.nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
else
  raise
end
#if File.exist?("burgersU_#{id}.nc")
#  p u = GPhys::IO.open("burgersU_#{id}.nc", "u")
#else
#  p u = GPhys::IO.open("burgersU_#{id}.ctl", "var")
#end
kz = 0 if (split ? g[0] : g).shape[2] == 1

if split
else
  p g.axis(3).pos.units
  p g.axis(3).pos.val
end

#p g[0][0,0,true,0].val.to_a

#gen_color
DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
#DCL::sgscmn(14)
DCL::sgscmn(56)
DCL::sglset("lcntl", false)
DCL::sgpset("lclip", true)
DCL::uzpset("inner", -1)
GGraph.set_fig("viewport" => [0.15, 0.75, 0.2, 0.8])
(split ? g.length : g.shape[3]).times{|i|
  #val = u[xrange, yrange, kz, i].val
  #puts "u[#{xrange}, #{yrange}, #{kz}, #{i}] min: #{val.min}, max: #{val.max}"
  #if val.is_a?(NArrayMiss)
  #  val.set_mask(val.get_mask * val.to_na.lt(1e4))
  #else
  #  val = NArrayMiss.to_nam_no_dup(val, val.lt(1e4))
  #end
  #p [val.min, val.max]
  GGraph.next_axes("xtitle" => "y grid num", "ytitle" => "z grid num", "xunits" => "", "yunits" => "")
  tmpdat = split ? g[i][ix, yrange, zrange, 0] : g[ix, yrange, zrange, i]
  GGraph.tone(tmpdat, true, "min" => -20, "max" => 70, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "tonc" => true, "auto" => false, "title" => "dBZ(kz = #{kz}) FT = #{i * 30}s", "annotate" => false)
  DCLExt.color_bar("tickintv" => 0)
}

DCL::grcls

