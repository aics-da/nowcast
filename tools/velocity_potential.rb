require "numru/ggraph"
include NumRu

puts "usage: ruby u.rb [id [xrange [yrange [kz [contour level 1 [contour level 2 [...]]]]]]]"

id = format("%03d", (ARGV[0] || 1).to_i)

xrange = ARGV[1] ? eval(ARGV[1]) : true
yrange = ARGV[1] ? eval(ARGV[2]) : true

kz = 19
#kz = 59
#kz = 99
kz = ARGV[3].to_i if ARGV[3]

levels = ARGV[4] ? ARGV[5..-1].map{|m| m.to_f} : [10, 20, 30]
rainlevels = [0.1]

split = false
if File.exist?("raindata_#{id}_001.nc")
  split = true
  g = Dir.glob("raindata_#{id}_*.nc").sort.map!{|m| GPhys::IO.open(m, "rain")}
  u = Dir.glob("burgersU_#{id}_*.nc").sort.map!{|m|
    begin
      GPhys::IO.open(m, "u")
    rescue
      nil
    end
  }
  v = Dir.glob("burgersV_#{id}_*.nc").sort.map!{|m|
    begin
      GPhys::IO.open(m, "v")
    rescue
      nil
    end
  }
elsif File.exist?("raindata_#{id}.nc")
  g = GPhys::IO.open("raindata_#{id}.nc", "rain")
  u = GPhys::IO.open("burgersU_#{id}.nc", "u")
  v = GPhys::IO.open("burgersV_#{id}.nc", "v")
else
  g = GPhys::IO.open("raindata_#{id}.ctl", "var")
  u = GPhys::IO.open("burgersU_#{id}.ctl", "var")
  v = GPhys::IO.open("burgersV_#{id}.ctl", "var")
end
kz = 0 if (split ? u[0] : u).shape[2] == 1

#p g.axis(3).pos.units
#p g.axis(3).pos.val

DCL::swpset("iwidth", DCL::swpget("iwidth") * 1.5)
DCL::swpset("iheight", DCL::swpget("iheight") * 1.5)
DCL::gropn(1)
DCL::glrset("rmiss", -9e20)
#DCL::sgscmn(14)
DCL::sgscmn(41)
DCL::sglset("lcntl", false)
GGraph.set_fig("viewport" => [0.1, 0.7, 0.2, 0.8])
(split ? u.length : u.shape[3]).times{|i|
  #val = u[xrange, yrange, kz, i].val
  #puts "u[#{xrange}, #{yrange}, #{kz}, #{i}] min: #{val.min}, max: #{val.max}"
  #if val.is_a?(NArrayMiss)
  #  val.set_mask(val.get_mask * val.to_na.lt(1e4))
  #else
  #  val = NArrayMiss.to_nam_no_dup(val, val.lt(1e4))
  #end
  #p [val.min, val.max]

  tmpu = split ? u[i][false, kz, 0] : u[false, kz, i]
  tmpv = split ? v[i][false, kz, 0] : v[false, kz, i]
  tmpv = -tmpv #FOR GSMaP

  div = tmpu.copy
  div[false] = 0
  div[1..-2, 1..-2] = ((tmpu[2..-1, 1..-2] - tmpu[0..-3, 1..-2]) + (tmpv[1..-2, 0..-3] - tmpv[1..-2, 2..-1])) / 2
  div[0, 1..-2] = ((tmpu[1, 1..-2] - tmpu[-1, 1..-2]) + (tmpv[0, 0..-3] - tmpv[0, 2..-1])) / 2
  div[-1, 1..-2] = ((tmpu[0, 1..-2] - tmpu[-2, 1..-2]) + (tmpv[0, 0..-3] - tmpv[0, 2..-1])) / 2


  div_wn = div.detrend(1).cos_taper(1).fft(false)
  k = NArray.float(div_wn.shape[0]).indgen!
  if k.length % 2 == 0
    k[(k.length / 2 + 1)..-1] = -NArray.float(k.length / 2 - 1).indgen!(1, 1)[-1..0]
  else
    k[(k.length / 2)..-1] = -NArray.float(k.length / 2).indgen!(1, 1)[-1..0]
  end
  l = NArray.float(div_wn.shape[1]).indgen!
  if l.length % 2 == 0
    l[(l.length / 2 + 1)..-1] = -NArray.float(l.length / 2 - 1).indgen!(1, 1)[-1..0]
  else
    l[(l.length / 2)..-1] = -NArray.float(l.length / 2).indgen!(1, 1)[-1..0]
  end
  k.newdim!(-1)
  l.newdim!(0)
  p factor = -1.0 / (k * k + l * l)
  div_wn[1..-1, 0] *= factor[1..-1, 0]
  for j in 1...(l.length)
    div_wn[true, j] *= factor[true, j]
  end
  g_vp = div_wn.fft(true).real

  p g_vp.val.abs.max

  #g_vp = tmpu.copy
  #g_vp.replace_val(sf)

  vpmax = 0.01

  GGraph.tone(g_vp[xrange, yrange], true, "min" => -vpmax, "max" => vpmax, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "tonc" => true, "auto" => false, "title" => "velocity_pot")
  #GGraph.contour(split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i], false, "levels" => rainlevels)
  DCLExt.color_bar("tickintv" => 0)

  GGraph.tone(g_vp[xrange, yrange], true, "min" => -vpmax, "max" => vpmax, "nlev" => 90, "clr_min" => 10, "clr_max" => 99, "tonc" => true, "auto" => false, "title" => "velocity_pot")
  GGraph.contour(split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i], false, "levels" => rainlevels)
  #GGraph.tone(split ? g[i][xrange, yrange, kz, 0] : g[xrange, yrange, kz, i], true, "levels" => [-99, 0.1, 1.0, 5.0, 10.0, 99], "patterns" => [0, 20999, 40999, 60999, 80999], "auto" => false, "tonf" => true, "title" => "rain / velocity_pot")
  #GGraph.contour(g_vp[xrange, yrange], false, "nlev" => nil, "title" => "streamfunc")
  DCLExt.color_bar("tickintv" => 0, "constwidth" => true)
}

DCL::grcls

