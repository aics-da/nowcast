require File.expand_path(File.dirname(__FILE__)) + "/parse_namelist"

puts "usage: ruby #{File.basename(__FILE__)} x0 dx y0 dy z0 dz"

x0 = ARGV[0] ? ARGV[0].to_f : 1
dx = ARGV[1] ? ARGV[1].to_f : 1
y0 = ARGV[2] ? ARGV[2].to_f : 1
dy = ARGV[3] ? ARGV[3].to_f : 1
z0 = ARGV[4] ? ARGV[4].to_f : 1
dz = ARGV[5] ? ARGV[5].to_f : 1

dat = parse_namelist("setting.namelist")

output_intv_s = dat["input_config"]["output_interval"].to_f # [sec]
p dt = dat["time_config"]["dt"].to_f
output_intv_it = (output_intv_s / dt).round ## MIGHT BE PROBLEMATIC
p st = dat["time_config"]["start"].to_i
p nx = dat["domain_config"]["dx"].to_i
p ny = dat["domain_config"]["dy"].to_i
p nz = dat["domain_config"]["dz"].to_i
p nt = (((dat["time_config"]["calnt"].to_i - 1) * dt).floor / output_intv_s + 1).round
p iter = dat["time_config"]["iteration_all"].to_i
p split = dat["input_config"]["split_history"].to_i

if split
  output_intv_m = 1 # fake
elsif (output_intv_s / 60).round * 60 - output_intv_s < 0.1 # interval does not have info of seconds
  output_intv_m = (output_intv_s / 60).round
else
  output_intv_m = output_intv_s.round # fake
end


fileinfo = [{:fname => "raindata", :title => "rain mixing ratio", :units => "kg/kg"},
            {:fname => "burgersU", :title => "u wind", :units => "m/s"},
            {:fname => "burgersV", :title => "v wind", :units => "m/s"},
            {:fname => "burgersW", :title => "w wind", :units => "m/s"},
            {:fname => "growth", :title => "rain growth rate", :units => ""}]

fileinfo.each{|info|
  iter.times{|i|
    File.open("#{info[:fname]}_#{format('%03d', st + i)}.ctl", "w"){|f|
      f.puts <<"EOF"
DSET ^#{info[:fname]}_#{format('%03d', st + i)}#{split == 1 ? '_%h3' : ''}.bin
TITLE #{info[:title]}
options LITTLE_ENDIAN #{split == 1 ? 'TEMPLATE' : ''}
undef -9.99e+08
XDEF #{nx} LINEAR #{x0} #{dx}
YDEF #{ny} LINEAR #{y0} #{dy}
ZDEF #{nz} LINEAR #{z0} #{dz}
TDEF #{nt} LINEAR 01z1jan1900 #{output_intv_m}hr
VARS 1
var #{nz} 99 #{info[:title]} #{info[:units]}
ENDVARS
EOF
    }
  }
}

fileinfo = [{:fname => "vector_u", :title => "u wind", :units => "m/s"},
            {:fname => "vector_v", :title => "v wind", :units => "m/s"},
            {:fname => "vector_w", :title => "w wind", :units => "m/s"},
            {:fname => "vector_u_filter", :title => "u wind", :units => "m/s"},
            {:fname => "vector_v_filter", :title => "v wind", :units => "m/s"},
            {:fname => "vector_w_filter", :title => "w wind", :units => "m/s"},
            {:fname => "vector_u_cotrec", :title => "u wind", :units => "m/s"},
            {:fname => "vector_v_cotrec", :title => "v wind", :units => "m/s"},
            {:fname => "vector_w_cotrec", :title => "w wind", :units => "m/s"},
            {:fname => "vector_u_anl", :title => "u wind", :units => "m/s"},
            {:fname => "vector_v_anl", :title => "v wind", :units => "m/s"},
            {:fname => "vector_w_anl", :title => "w wind", :units => "m/s"},
            {:fname => "vector_u_var", :title => "u wind analysis variance", :units => "m2/s2"},
            {:fname => "vector_v_var", :title => "v wind analysis variance", :units => "m2/s2"},
            {:fname => "vector_w_var", :title => "w wind analysis variance", :units => "m2/s2"},
            {:fname => "quality", :title => "corr", :units => "1"}]

fileinfo.each{|info|
  iter.times{|i|
    File.open("#{info[:fname]}_#{format('%03d', st + i)}.ctl", "w"){|f|
      f.puts <<"EOF"
DSET ^#{info[:fname]}_#{format('%03d', st + i)}.bin
TITLE #{info[:title]}
options little_endian
undef #{info[:fname] == 'quality' ? -9.99e+08 : 123456.0}
XDEF #{nx} LINEAR #{x0} #{dx}
YDEF #{ny} LINEAR #{y0} #{dy}
ZDEF #{nz} LINEAR #{z0} #{dz}
TDEF 1 LINEAR 00z1jan1900 #{output_intv_m}mn
VARS 1
var #{nz} 99 #{info[:title]} #{info[:units]}
ENDVARS
EOF
    }
  }
}
