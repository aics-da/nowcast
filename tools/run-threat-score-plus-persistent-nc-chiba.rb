require "numru/gphys"

/(\d{4})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})\/(\d{2})/ =~ Dir.pwd || raise

year = $1
month = $2
day = $3
hour = $4
min = $5
sec = $6

p time = Time.new(year, month, day, hour, min, sec)
#dt = NumRu::GPhys::IO.open("rain_cart_0001.nc", "rain").get_att("date")
#p time2 = Time.new(*dt)

##exit if time > time2
#time = time2 if time2 > time


#basedir = "/data13/nowcast_pawr/suita/obs"
basedir = "/lfs01/otsuka/JRC_PAWR_data/vr"
path = ""
basedir.split("/").each{|dd|
  next unless dd.length > 0
  path << "/" << dd
  path = File.readlink(path) if File.symlink?(path)
}
basedir = path

ary = []
21.times{|i|
  fname = "#{basedir}/%Y%m%d-?/%H/ZV_%Y_%m_%d_%H_%M_%S.nc"
  p ofile = Dir.glob(time.strftime(fname))
  p ofile = Dir.glob((time + 1).strftime(fname)) unless ofile.length > 0
  p ofile = Dir.glob((time - 1).strftime(fname)) unless ofile.length > 0
  
  raise "#{time} not found" unless ofile.length > 0
  ofile = ofile[0]
  ary.push ofile
  time += 30
}

system("HDF5_USE_FILE_LOCKING=FALSE /home/otsuka/bitbucket/nowcast/tools/threat-score-plus-persistent-nc-chiba.exe #{ary.join(' ')}") || raise
