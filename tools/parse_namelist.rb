def parse_namelist(fname)
  namelist = File.open(fname){|f| f.readlines}

  dat = {}
  tmpdat = nil
  section = nil
  namelist.each{|l|
    l = l[0..(l.index("!") - 1)] if l.index("!")
    if /\&/ =~ l
      section = l[(l.index("&") + 1)..-1].strip
      tmpdat = {}
    elsif /\=/ =~ l
      /(.+)=(.+)/ =~ l
      key = $1
      val = $2
      key = key.strip
      val = val.strip
      tmpdat[key] = val
    elsif /\// =~ l
      dat[section] = tmpdat
    end
  }
  dat
end

