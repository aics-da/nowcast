require "numru/gphys"

/(\d{4})(\d{2})\/(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd || /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd || raise

year = $1
month = $2
day = $3
hour = $4
min = $5
sec = $6

p time = Time.new(year, month, day, hour, min, sec)

ary = []
13.times{|i|
  ofile = time.strftime("/data14-2/otsuka/gsmap/realtime_ver/v8/%Y%m/gsmap_nrt.%Y%m%d.%H%M.dat")

  raise "#{ofile} not found" unless File.exist?(ofile)
  ary.push ofile
  time += 3600
}

system("/home/otsuka/bitbucket/nowcast/tools/gsmap-threat-score-plus-persistent-nc.exe #{ary.join(' ')}") || raise

datdir = "score_nrtv8"
system("mkdir -p #{datdir} && mv *NH*.txt *TR*.txt *SH*.txt *GL*.txt #{datdir}") || raise
