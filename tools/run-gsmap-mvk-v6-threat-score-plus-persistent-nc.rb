require "numru/gphys"

/(\d{4})(\d{2})\/(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd || /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/ =~ Dir.pwd || raise

year = $1
month = $2
day = $3
hour = $4
min = $5
sec = $6

p time = Time.new(year, month, day, hour, min, sec)

ary = []
13.times{|i|
  #ofile = time.strftime("/data14/otsuka/gsmap_rnc/bookers-production-2/out/%Y%m/%d%H%M%S/fcst/input_0002.bin")
  #ofile = time.strftime("/data3/kotsuki/DATA/GSMaP/realtime/%Y%m/gsmap_nrt.%Y%m%d.%H%M.dat")
  ofile = time.strftime("/data3/kotsuki/DATA/GSMaP/standard/v6/%Y%m/gsmap_mvk.%Y%m%d.%H%M.v6.4133.0.dat")
  ofile = time.strftime("/data3/kotsuki/DATA/GSMaP/standard/v6/%Y%m/gsmap_mvk.%Y%m%d.%H%M.v6.4133.1.dat") unless File.exist?(ofile)
  
  raise unless File.exist?(ofile)
  ary.push ofile
  time += 3600
}

system("/home/otsuka/bitbucket/nowcast/tools/gsmap-threat-score-plus-persistent-nc.exe #{ary.join(' ')}") || raise

system("mkdir -p score_mvk_v6 && mv *NH*.txt *TR*.txt *SH*.txt *GL*.txt score_mvk_v6") || raise
