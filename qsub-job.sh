#!/bin/sh
#PBS -l select=1:ncpus=40:mpiprocs=1
. /usr/share/modules/init/sh  ### this is required to use "module" command
module unload pgi-12.10
module load intel-2013.1.117

cd /home/otsuka/3d_rain_synthetic_dbz_advexp

ulimit -s unlimited

time ./riken

